from enum import Enum


class Interpolation(Enum):
    NEAREST_NEIGHBOR = 0
    BILINEAR = 1
    BICUBIC = 2
