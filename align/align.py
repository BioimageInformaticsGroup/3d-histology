import os
import sys
import glob
import shutil
import logging
import argparse

from ij import IJ, ImagePlus, Prefs
from ini.trakem2 import ControlWindow, Project
from ini.trakem2.display import Patch
from java.awt import Color
from java.util import HashSet
from mpicbg.trakem2.align import ElasticLayerAlignment

# setup logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def initialize_settings(args):
    logger.debug("Set ImageJ number of threads: {} -> {}".format(Prefs.getThreads(), args["maxnumthreads"]))
    Prefs.setThreads(args["maxnumthreads"])
    logger.debug("Saving preferences")
    Prefs.savePreferences()
    logger.debug("Current setting: {}".format(Prefs.getThreads()))
    # Redirect error messages to ImageJ log to avoid pausing in case of error.
    IJ.redirectErrorMessages()
    # Disable GUI.
    ControlWindow.setGUIEnabled(0)


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def parse_cli_arguments():
    # parse command line arguments
    parser = argparse.ArgumentParser(description="Elastic stack alignment algorithm")
    parser.add_argument("source_dir", help="", type=str)
    parser.add_argument("target_dir", nargs="?", help="", type=str)
    parser.add_argument("transforms_dir", nargs="?", help="", type=str)
    parser.add_argument("--imagepixelsize", default="0.46", help="Size of the pixel in both x and y directions in micrometers", type=float)
    parser.add_argument("--rod", default="0.874208375277442", help="", type=float)
    parser.add_argument("--rejectidentity", type=str2bool, nargs="?", default=False, help="")
    parser.add_argument("--identitytolerance", default="0", help="", type=int)
    parser.add_argument("--maxepsilon", default="561.4478", help="", type=float)
    parser.add_argument("--siftfdbins", default="8", help="", type=int)
    parser.add_argument("--siftfdsize", default="4", help="", type=int)
    parser.add_argument("--siftinitialsigma", default="1.6", help="", type=float)
    parser.add_argument("--siftmaxoctavesize", default="16384", help="Largest image scale to use for the SIFT descriptor. Decreasing this value leads to exclusion of smaller objects. The selected value ensures that the maximum resolution of the image is always used.", type=int)
    parser.add_argument("--siftminoctavesize", default="32", help="Smallest image scale to use for the SIFT descriptor. Increasing this value leads to exclusion of larger shapes. ", type=int)
    parser.add_argument("--siftsteps", default="3", help="", type=int)
    parser.add_argument("--maxnumfailures", default="1", help="", type=int)
    parser.add_argument("--maxnumneighbors", default="10", help="", type=int)
    parser.add_argument("--maxiterationsoptimize", default="1508", help="", type=int)
    parser.add_argument("--maxplateauwidthoptimize", default="302", help="", type=int)
    parser.add_argument("--layerscale", default="1.0", help="", type=float)
    parser.add_argument("--resolutionspringmesh", default="31", help="", type=int)
    parser.add_argument("--minr", default="0.723048369673122", help="", type=float)
    parser.add_argument("--maxcurvaturer", default="7.00582628641793", help="", type=float)
    parser.add_argument("--rodr", default="0.863975346789895", help="", type=float)
    parser.add_argument("--localmodelindex", default="1", help="", type=int)
    parser.add_argument("--maxlocaltrust", default="2.5798143212755", help="", type=float)
    parser.add_argument("--maxstretchspringmesh", default="17728.04", help="", type=float)
    parser.add_argument("--dampspringmesh", default="0.9", help="", type=float)
    parser.add_argument("--stiffnessspringmesh", default="0.584399040198663", help="", type=float)
    parser.add_argument("--maxplateauwidthspringmesh", default="302", help="", type=int)
    parser.add_argument("--maxiterationsspringmesh", default="1508", help="", type=int)
    parser.add_argument("--uselegacyoptimizer", type=str2bool, nargs="?", default=True, help="")
    parser.add_argument("--searchradius", default="272", help="", type=int)
    parser.add_argument("--blockradius", default="496", help="", type=int)
    parser.add_argument("--localregionsigma", default="907.3362", help="", type=float)
    parser.add_argument("--maxlocalepsilon", default="290.4741", help="", type=float)
    parser.add_argument("--sectionthickness", default="10", help="", type=float)
    parser.add_argument("--clearcache", type=str2bool, nargs="?", default=True, help="")
    parser.add_argument("--widestsetonly", type=str2bool, nargs="?", default=False, help="")
    parser.add_argument("--multiplehypotheses", type=str2bool, nargs="?", default=True, help="")
    parser.add_argument("--visualize", type=str2bool, nargs="?", default=False, help="")
    parser.add_argument("--isaligned", type=str2bool, nargs="?", default=False, help="Images are roughly prealigned")
    parser.add_argument("--uselocalsmoothnessfilter", type=str2bool, nargs="?", default=True, help="")
    parser.add_argument("--maxnumthreadssift", default="1", help="", type=int)
    parser.add_argument("--desiredmodelindex", default="1", help="", type=int)
    parser.add_argument("--expectedmodelindex", default="1", help="", type=int)
    parser.add_argument("--maxnumthreads", default="1", help="", type=int)
    parser.add_argument("--mininlierratio", default="0", help="", type=int)
    parser.add_argument("--minnuminliers", default="3", help="", type=int)
    args = parser.parse_args()
    args = vars(args)
    return args


def check_cli_arguments(args):
    if not args["transforms_dir"]:
        logger.error("Transforms dir parameter is empty, quitting")
        sys.exit(1)
    if not os.path.exists(args["transforms_dir"]):
        logger.debug("Transforms dir {} does not exist, creating it".format(args["transforms_dir"]))
        os.makedirs(args["transforms_dir"])


def set_esa_params(args):
    # Collect parameters here.
    p = ElasticLayerAlignment.Param()
    p.ppm.sift.fdBins = args["siftfdbins"]
    p.ppm.sift.fdSize = args["siftfdsize"]
    p.ppm.sift.initialSigma = args["siftinitialsigma"]
    p.ppm.sift.maxOctaveSize = args["siftmaxoctavesize"]
    p.ppm.sift.minOctaveSize = args["siftminoctavesize"]
    p.ppm.sift.steps = args["siftsteps"]
    p.ppm.clearCache = args["clearcache"]
    p.ppm.maxNumThreadsSift = args["maxnumthreadssift"]
    p.ppm.rod = args["rod"]
    p.desiredModelIndex = args["desiredmodelindex"]
    p.expectedModelIndex = args["expectedmodelindex"]
    p.identityTolerance = args["identitytolerance"]
    p.isAligned = args["isaligned"]
    p.maxEpsilon = args["maxepsilon"]
    p.maxIterationsOptimize = args["maxiterationsoptimize"]
    p.maxNumFailures = args["maxnumfailures"]
    p.maxNumNeighbors = args["maxnumneighbors"]
    p.maxNumThreads = args["maxnumthreads"]
    p.maxPlateauwidthOptimize = args["maxplateauwidthoptimize"]
    p.minInlierRatio = args["mininlierratio"]
    p.minNumInliers = args["minnuminliers"]
    p.multipleHypotheses = args["multiplehypotheses"]
    p.rejectIdentity = args["rejectidentity"]
    p.visualize = args["visualize"]
    p.blockRadius = args["blockradius"]
    p.dampSpringMesh = args["dampspringmesh"]
    p.layerScale = args["layerscale"]
    p.localModelIndex = args["localmodelindex"]
    p.localRegionSigma = args["localregionsigma"]
    p.maxCurvatureR = args["maxcurvaturer"]
    p.maxIterationsSpringMesh = args["maxiterationsspringmesh"]
    p.maxLocalEpsilon = args["maxlocalepsilon"]
    p.maxLocalTrust = args["maxlocaltrust"]
    p.maxPlateauwidthSpringMesh = args["maxplateauwidthspringmesh"]
    p.useLegacyOptimizer = args["uselegacyoptimizer"]
    p.maxStretchSpringMesh = args["maxstretchspringmesh"]
    p.minR = args["minr"]
    p.resolutionSpringMesh = args["resolutionspringmesh"]
    p.rodR = args["rodr"]
    p.searchRadius = args["searchradius"]
    p.stiffnessSpringMesh = args["stiffnessspringmesh"]
    p.useLocalSmoothnessFilter = args["uselocalsmoothnessfilter"]
    # p.widestSetOnly = args["widestsetonly"]
    return p


def print_arguments_debug(esa_params, args):
    logger.debug("SOURCE_DIR: {}".format(args["source_dir"]))
    logger.debug("TARGET_DIR: {}".format(args["target_dir"]))
    logger.debug("TRANSFORMS_DIR: {}".format(args["transforms_dir"]))

    # Create dict for checking input arguments
    param_dict = {
        "siftfdbins": esa_params.ppm.sift.fdBins,
        "siftfdsize": esa_params.ppm.sift.fdSize,
        "siftinitialsigma": esa_params.ppm.sift.initialSigma,
        "siftmaxoctavesize": esa_params.ppm.sift.maxOctaveSize,
        "siftminoctavesize": esa_params.ppm.sift.minOctaveSize,
        "siftsteps": esa_params.ppm.sift.steps,
        "clearcache": esa_params.ppm.clearCache,
        "maxnumthreadssift": esa_params.ppm.maxNumThreadsSift,
        "rod": esa_params.ppm.rod,
        "desiredmodelindex": esa_params.desiredModelIndex,
        "expectedmodelindex": esa_params.expectedModelIndex,
        "identitytolerance": esa_params.identityTolerance,
        "isaligned": esa_params.isAligned,
        "maxepsilon": esa_params.maxEpsilon,
        "maxiterationsoptimize": esa_params.maxIterationsOptimize,
        "maxnumfailures": esa_params.maxNumFailures,
        "maxnumneighbors": esa_params.maxNumNeighbors,
        "maxnumthreads": esa_params.maxNumThreads,
        "maxplateauwidthoptimize": esa_params.maxPlateauwidthOptimize,
        "mininlierratio": esa_params.minInlierRatio,
        "minnuminliers": esa_params.minNumInliers,
        "multiplehypotheses": esa_params.multipleHypotheses,
        "rejectidentity": esa_params.rejectIdentity,
        "visualize": esa_params.visualize,
        "blockradius": esa_params.blockRadius,
        "dampspringmesh": esa_params.dampSpringMesh,
        "layerscale": esa_params.layerScale,
        "localmodelindex": esa_params.localModelIndex,
        "localregionsigma": esa_params.localRegionSigma,
        "maxcurvaturer": esa_params.maxCurvatureR,
        "maxiterationsspringmesh": esa_params.maxIterationsSpringMesh,
        "maxlocalepsilon": esa_params.maxLocalEpsilon,
        "maxlocaltrust": esa_params.maxLocalTrust,
        "maxplateauwidthspringmesh": esa_params.maxPlateauwidthSpringMesh,
        "uselegacyoptimizer": esa_params.useLegacyOptimizer,
        "maxstretchspringmesh": esa_params.maxStretchSpringMesh,
        "minr": esa_params.minR,
        "resolutionspringmesh": esa_params.resolutionSpringMesh,
        "rodr": esa_params.rodR,
        "searchradius": esa_params.searchRadius,
        "stiffnessspringmesh": esa_params.stiffnessSpringMesh,
        "uselocalsmoothnessfilter": esa_params.useLocalSmoothnessFilter
    }

    # print out input arguments and set parameters side by side to check that they are correct
    logger.debug("Print set ESA parameters with format: <Parameter name>: <input_argument> -> <set_parameter>")
    for key in args.keys():
        try:
            _ = args[key]
            _ = param_dict[key]
        except:
            pass
        else:
            logger.debug("    {}: {} -> {}".format(key, args[key], param_dict[key]))


def unix_filename_sort(filenames):
    """Use unix sort -V command to easily sort filenames according to numbers that exist somewhere in in the filename"""
    print_str = ""
    for f in filenames:
        print_str += f + " "
    result = os.popen("printf '%s\n' {} | sort -V".format(print_str)).read()
    result = list(filter(None, result.split("\n")))
    return result


def setup_registration(filenames):
    # If any projects are open, close them.
    allprojects = Project.getProjects()
    for project in allprojects:
        project.remove()

    logger.info("Create TrakEM2 project")
    project = Project.newFSProject("blank", None, args["transforms_dir"])
    if not project:
        logger.debug("Project object is: {}, check that transforms_dir is correct: {}".format(project,
                                                                                              args["transforms_dir"]))
    # Create the correct number of layers (one per image).
    logger.debug("Create layers within project..")
    layerset = project.getRootLayerSet()
    for i in range(len(filenames)):
        layerset.getLayer(i, 1, True)
    num_layers = len(layerset.getLayers())

    # Add one image as a patch to each layer.
    for i, layer in enumerate(layerset.getLayers()):
        logger.info("Setting images as layers {}/{}".format(i + 1, num_layers))
        # Add this image as a patch to the layer.
        filepath = os.path.join(args["source_dir"], filenames[i])
        logger.debug("createPatch arg1 project: {}".format(project))
        logger.debug("createPatch arg2 filepath: {}".format(filepath))
        patch = Patch.createPatch(project, filepath)
        layer.add(patch)
        # Update internal quadtree of the layer
        layer.recreateBuckets()

    # Set the correct section thickness for each layer.
    logger.info("Set layer thicknesses..")
    z = 0
    thickness = args["sectionthickness"] / args["imagepixelsize"]
    for i, layer in enumerate(layerset.getLayers()):
        logger.debug("Set thickness (z={}) for layer {}/{}".format(z, i + 1, num_layers))
        layer.setZ(z)
        layer.setThickness(thickness)
        z += thickness

    # Update the LayerTree:
    project.getLayerTree().updateList(layerset)

    # Update world coordinates.
    layerset.setMinimumDimensions()

    return project, layerset


def register(project, layerset):
    logger.info("Begin registration..")
    layerRange = layerset.getLayers()
    fixedLayers = HashSet()
    emptyLayers = HashSet()
    layerBounds = layerset.get2DBounds()
    propagateTransformBefore = False
    propagateTransformAfter = False
    try:
        ElasticLayerAlignment().exec(
            params,
            project,
            layerRange,
            fixedLayers,
            emptyLayers,
            layerBounds,
            propagateTransformBefore,
            propagateTransformAfter,
            None)
    except:
        logger.error("ElasticLayerAlignment failed")
        runsuccess = False
    else:
        runsuccess = True

    # Update world coordinates.
    layerset.setMinimumDimensions()

    logger.info("Registration done")
    return runsuccess


def save_images():
    logger.info("Saving images")

    layerBounds = layerset.get2DBounds()
    scale = 1.0
    backgroundcolor = Color.white
    for i, layer in enumerate(layerset.getLayers()):
        logger.debug("i: {}, layer: {}".format(i, layer))
        patches = layer.getDisplayables(Patch, layerBounds)
        logger.debug("i: {}, patches: {}".format(i, patches))
        ip = Patch.makeFlatImage(ImagePlus.COLOR_RGB, layer, layerBounds, scale, patches, backgroundcolor, True)
        logger.debug("i: {}, ip: {}".format(i, ip))
        imp = ImagePlus(filenames[i], ip)
        logger.debug("i: {}, imp: {}".format(i, imp))
        filepath = os.path.join(args["target_dir"], filenames[i])
        logger.debug("i: {}, filepath: {}".format(i, filepath))
        # IJ.saveAsTiff(imp, filepath)
        IJ.run(imp, "Bio-Formats Exporter", "save=[{}] compression={}".format(filepath, 'LZW'))


if __name__ == '__main__':

    args = parse_cli_arguments()
    check_cli_arguments(args)

    initialize_settings(args)

    params = set_esa_params(args)
    print_arguments_debug(params, args)

    #  Get the filenames of input images.
    filenames = os.listdir(args["source_dir"])

    # sort filenames according to the 'sequence number' in the filename (use unix tool 'sort -V')
    filenames = unix_filename_sort(filenames)
    logger.debug("Image order after sorting:")
    for f in filenames:
        logger.debug("    {}".format(f))

    project, layerset = setup_registration(filenames)

    status = register(project, layerset)

    # If the registration was successful, output images and save project.
    if status:
        save_images()

        # Disable mipmap regeneration to allow later reapplying transformations faster by loading a modified project XML.
        # loader = project.getLoader()
        # loader.setMipMapsRegeneration(False)

        project.getLoader().setMipMapsRegeneration(False)

        logger.info("Saving project file")
        project.saveAs(os.path.join(args["transforms_dir"], "tempproject.xml"), True)

        logger.info("Closing project")
        project.remove()

        # remove mipmaps
        rmdirs = glob.glob(os.path.join(os.path.abspath(args["transforms_dir"]), "trakem2*"))
        logger.info("Removing directories: {}".format(rmdirs))
        for f in rmdirs:
            shutil.rmtree(f)

        logger.info("Quitting")
        sys.exit(0)

    else:
        sys.exit(1)
