import argparse
import glob
import logging
import os
import shutil
import sys

from ij import IJ, ImagePlus, Prefs
from ini.trakem2 import ControlWindow, Project
from ini.trakem2.display import Patch
from java.awt import Color
from java.util import HashSet
from mpicbg.trakem2.align import ElasticLayerAlignment

# setup logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def initialize_settings(args):
    logger.debug("Set ImageJ number of threads: {} -> {}".format(Prefs.getThreads(), args["maxnumthreads"]))
    Prefs.setThreads(args["maxnumthreads"])
    logger.debug("Saving preferences")
    Prefs.savePreferences()
    logger.debug("Current setting: {}".format(Prefs.getThreads()))
    # Redirect error messages to ImageJ log to avoid pausing in case of error.
    IJ.redirectErrorMessages()
    # Disable GUI.
    ControlWindow.setGUIEnabled(0)


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def parse_cli_arguments():
    # parse command line arguments
    parser = argparse.ArgumentParser(description="Elastic stack alignment algorithm")
    parser.add_argument("inpath_fixed", help="", type=str)
    parser.add_argument("inpath_moving", help="", type=str)
    parser.add_argument("output_dir", help="", type=str)
    parser.add_argument("--work_dir", type=str,
                        help="Set align work dir, \
                                important: use the same work dir for reapply_transform as well! \
                                this is due to trackem limitations")
    parser.add_argument("--subsample", default=100,
                       help="Select subsample amount in percents, default 100 (no subsampling)", type=int)
    parser.add_argument("--imagepixelsize", default="0.46",
                        help="Size of the pixel in both x and y directions in micrometers", type=float)
    parser.add_argument("--rod", default="0.874208375277442", help="", type=float)
    parser.add_argument("--rejectidentity", type=str2bool, nargs="?", default=False, help="")
    parser.add_argument("--identitytolerance", default="0", help="", type=int)
    parser.add_argument("--maxepsilon", default="561.4478", help="", type=float)
    parser.add_argument("--siftfdbins", default="8", help="", type=int)
    parser.add_argument("--siftfdsize", default="4", help="", type=int)
    parser.add_argument("--siftinitialsigma", default="1.6", help="", type=float)
    parser.add_argument("--siftmaxoctavesize", default="16384",
                        help="Largest image scale to use for the SIFT descriptor. Decreasing this value leads to exclusion of smaller objects. The selected value ensures that the maximum resolution of the image is always used.",
                        type=int)
    parser.add_argument("--siftminoctavesize", default="32",
                        help="Smallest image scale to use for the SIFT descriptor. Increasing this value leads to exclusion of larger shapes. ",
                        type=int)
    parser.add_argument("--siftsteps", default="3", help="", type=int)
    parser.add_argument("--maxnumfailures", default="1", help="", type=int)
    parser.add_argument("--maxnumneighbors", default="10", help="", type=int)
    parser.add_argument("--maxiterationsoptimize", default="1508", help="", type=int)
    parser.add_argument("--maxplateauwidthoptimize", default="302", help="", type=int)
    parser.add_argument("--layerscale", default="1.0", help="", type=float)
    parser.add_argument("--resolutionspringmesh", default="31", help="", type=int)
    parser.add_argument("--minr", default="0.723048369673122", help="", type=float)
    parser.add_argument("--maxcurvaturer", default="7.00582628641793", help="", type=float)
    parser.add_argument("--rodr", default="0.863975346789895", help="", type=float)
    parser.add_argument("--localmodelindex", default="1", help="", type=int)
    parser.add_argument("--maxlocaltrust", default="2.5798143212755", help="", type=float)
    parser.add_argument("--maxstretchspringmesh", default="17728.04", help="", type=float)
    parser.add_argument("--dampspringmesh", default="0.9", help="", type=float)
    parser.add_argument("--stiffnessspringmesh", default="0.584399040198663", help="", type=float)
    parser.add_argument("--maxplateauwidthspringmesh", default="302", help="", type=int)
    parser.add_argument("--maxiterationsspringmesh", default="1508", help="", type=int)
    parser.add_argument("--uselegacyoptimizer", type=str2bool, nargs="?", default=True, help="")
    parser.add_argument("--searchradius", default="272", help="", type=int)
    parser.add_argument("--blockradius", default="496", help="", type=int)
    parser.add_argument("--localregionsigma", default="907.3362", help="", type=float)
    parser.add_argument("--maxlocalepsilon", default="290.4741", help="", type=float)
    parser.add_argument("--sectionthickness", default="10", help="", type=float)
    parser.add_argument("--clearcache", type=str2bool, nargs="?", default=True, help="")
    parser.add_argument("--widestsetonly", type=str2bool, nargs="?", default=False, help="")
    parser.add_argument("--multiplehypotheses", type=str2bool, nargs="?", default=True, help="")
    parser.add_argument("--visualize", type=str2bool, nargs="?", default=False, help="")
    parser.add_argument("--isaligned", type=str2bool, nargs="?", default=False, help="Images are roughly prealigned")
    parser.add_argument("--uselocalsmoothnessfilter", type=str2bool, nargs="?", default=True, help="")
    parser.add_argument("--maxnumthreadssift", default="1", help="", type=int)
    parser.add_argument("--desiredmodelindex", default="1", help="", type=int)
    parser.add_argument("--expectedmodelindex", default="1", help="", type=int)
    parser.add_argument("--maxnumthreads", default="1", help="", type=int)
    parser.add_argument("--mininlierratio", default="0", help="", type=int)
    parser.add_argument("--minnuminliers", default="3", help="", type=int)
    args = parser.parse_args()
    args = vars(args)
    return args


def check_cli_arguments(args):
    if args["work_dir"]:
        args["work_dir"] = os.path.abspath(args["work_dir"])
        if not os.path.exists(args["work_dir"]):
            logger.debug("Create work_dir: {}".format(args["work_dir"]))
            os.makedirs(args["work_dir"])
    else:
        args["work_dir"] = os.path.abspath(os.getcwd())
        logger.debug("Use current dir as work_dir: {}".format(args["work_dir"]))

    # create transforms_dir under output_dir
    args["transforms_dir"] = os.path.join(args["work_dir"], "transforms")
    if not os.path.exists(args["transforms_dir"]):
        os.makedirs(args["transforms_dir"])

    # check that other arguments exists and their path is absolute
    for key in ["inpath_fixed", "inpath_moving", "output_dir"]:
        if not args[key]:
            logger.error("{} parameter is empty, quitting".format(key))
            sys.exit(1)
        else:
            args[key] = os.path.abspath(args[key])

    if not os.path.exists(args["output_dir"]):
        os.makedirs(args["output_dir"])

    # FOR SOME REASON OS.CHDIR() BREAKS OS.ABSPATH().. BE CAREFUL
    # it seems using os.chdir is not a good idea under fiji / python 2.xx
    # probably better to cd into directory first and then launch
    logger.debug("Set work dir: {}".format(args["work_dir"]))
    os.chdir(args["work_dir"])


def set_esa_params(args):
    # Collect parameters here.
    p = ElasticLayerAlignment.Param()
    p.ppm.sift.fdBins = args["siftfdbins"]
    p.ppm.sift.fdSize = args["siftfdsize"]
    p.ppm.sift.initialSigma = args["siftinitialsigma"]
    p.ppm.sift.maxOctaveSize = args["siftmaxoctavesize"]
    p.ppm.sift.minOctaveSize = args["siftminoctavesize"]
    p.ppm.sift.steps = args["siftsteps"]
    p.ppm.clearCache = args["clearcache"]
    p.ppm.maxNumThreadsSift = args["maxnumthreadssift"]
    p.ppm.rod = args["rod"]
    p.desiredModelIndex = args["desiredmodelindex"]
    p.expectedModelIndex = args["expectedmodelindex"]
    p.identityTolerance = args["identitytolerance"]
    p.isAligned = args["isaligned"]
    p.maxEpsilon = args["maxepsilon"]
    p.maxIterationsOptimize = args["maxiterationsoptimize"]
    p.maxNumFailures = args["maxnumfailures"]
    p.maxNumNeighbors = args["maxnumneighbors"]
    p.maxNumThreads = args["maxnumthreads"]
    p.maxPlateauwidthOptimize = args["maxplateauwidthoptimize"]
    p.minInlierRatio = args["mininlierratio"]
    p.minNumInliers = args["minnuminliers"]
    p.multipleHypotheses = args["multiplehypotheses"]
    p.rejectIdentity = args["rejectidentity"]
    p.visualize = args["visualize"]
    p.blockRadius = args["blockradius"]
    p.dampSpringMesh = args["dampspringmesh"]
    p.layerScale = args["layerscale"]
    p.localModelIndex = args["localmodelindex"]
    p.localRegionSigma = args["localregionsigma"]
    p.maxCurvatureR = args["maxcurvaturer"]
    p.maxIterationsSpringMesh = args["maxiterationsspringmesh"]
    p.maxLocalEpsilon = args["maxlocalepsilon"]
    p.maxLocalTrust = args["maxlocaltrust"]
    p.maxPlateauwidthSpringMesh = args["maxplateauwidthspringmesh"]
    p.useLegacyOptimizer = args["uselegacyoptimizer"]
    p.maxStretchSpringMesh = args["maxstretchspringmesh"]
    p.minR = args["minr"]
    p.resolutionSpringMesh = args["resolutionspringmesh"]
    p.rodR = args["rodr"]
    p.searchRadius = args["searchradius"]
    p.stiffnessSpringMesh = args["stiffnessspringmesh"]
    p.useLocalSmoothnessFilter = args["uselocalsmoothnessfilter"]
    # p.widestSetOnly = args["widestsetonly"]
    return p


def print_arguments_debug(esa_params, args):
    logger.debug("INPATH_FIXED: {}".format(args["inpath_fixed"]))
    logger.debug("INPATH_MOVING: {}".format(args["inpath_moving"]))
    logger.debug("OUTPUT_DIR: {}".format(args["output_dir"]))
    logger.debug("TRANSFORMS_DIR: {}".format(args["transforms_dir"]))

    # Create dict for checking input arguments
    param_dict = {
        "siftfdbins": esa_params.ppm.sift.fdBins,
        "siftfdsize": esa_params.ppm.sift.fdSize,
        "siftinitialsigma": esa_params.ppm.sift.initialSigma,
        "siftmaxoctavesize": esa_params.ppm.sift.maxOctaveSize,
        "siftminoctavesize": esa_params.ppm.sift.minOctaveSize,
        "siftsteps": esa_params.ppm.sift.steps,
        "clearcache": esa_params.ppm.clearCache,
        "maxnumthreadssift": esa_params.ppm.maxNumThreadsSift,
        "rod": esa_params.ppm.rod,
        "desiredmodelindex": esa_params.desiredModelIndex,
        "expectedmodelindex": esa_params.expectedModelIndex,
        "identitytolerance": esa_params.identityTolerance,
        "isaligned": esa_params.isAligned,
        "maxepsilon": esa_params.maxEpsilon,
        "maxiterationsoptimize": esa_params.maxIterationsOptimize,
        "maxnumfailures": esa_params.maxNumFailures,
        "maxnumneighbors": esa_params.maxNumNeighbors,
        "maxnumthreads": esa_params.maxNumThreads,
        "maxplateauwidthoptimize": esa_params.maxPlateauwidthOptimize,
        "mininlierratio": esa_params.minInlierRatio,
        "minnuminliers": esa_params.minNumInliers,
        "multiplehypotheses": esa_params.multipleHypotheses,
        "rejectidentity": esa_params.rejectIdentity,
        "visualize": esa_params.visualize,
        "blockradius": esa_params.blockRadius,
        "dampspringmesh": esa_params.dampSpringMesh,
        "layerscale": esa_params.layerScale,
        "localmodelindex": esa_params.localModelIndex,
        "localregionsigma": esa_params.localRegionSigma,
        "maxcurvaturer": esa_params.maxCurvatureR,
        "maxiterationsspringmesh": esa_params.maxIterationsSpringMesh,
        "maxlocalepsilon": esa_params.maxLocalEpsilon,
        "maxlocaltrust": esa_params.maxLocalTrust,
        "maxplateauwidthspringmesh": esa_params.maxPlateauwidthSpringMesh,
        "uselegacyoptimizer": esa_params.useLegacyOptimizer,
        "maxstretchspringmesh": esa_params.maxStretchSpringMesh,
        "minr": esa_params.minR,
        "resolutionspringmesh": esa_params.resolutionSpringMesh,
        "rodr": esa_params.rodR,
        "searchradius": esa_params.searchRadius,
        "stiffnessspringmesh": esa_params.stiffnessSpringMesh,
        "uselocalsmoothnessfilter": esa_params.useLocalSmoothnessFilter
    }

    # print out input arguments and set parameters side by side to check that they are correct
    logger.debug("Print set ESA parameters with format: <Parameter name>: <input_argument> -> <set_parameter>")
    for key in args.keys():
        try:
            _ = args[key]
            _ = param_dict[key]
        except:
            pass
        else:
            logger.debug("    {}: {} -> {}".format(key, args[key], param_dict[key]))


def unix_filename_sort(filenames):
    """Use unix sort -V command to easily sort filenames according to numbers that exist somewhere in in the filename"""
    print_str = ""
    for f in filenames:
        print_str += f + " "
    result = os.popen("printf '%s\n' {} | sort -V".format(print_str)).read()
    result = list(filter(None, result.split("\n")))
    return result


def setup_registration(inpath_fixed, inpath_moving):
    # If any projects are open, close them.
    allprojects = Project.getProjects()
    for project in allprojects:
        project.remove()

    logger.info("Create TrakEM2 project")
    project = Project.newFSProject("blank", None, args["transforms_dir"])
    if not project:
        logger.debug("Project object is: {}, check that transforms_dir is correct: {}".format(project,
                                                                                              args["transforms_dir"]))
    # Create the correct number of layers (one per image).
    logger.debug("Create layers within project..")
    filepaths = [inpath_fixed, inpath_moving]
    layerset = project.getRootLayerSet()
    for i in range(len(filepaths)):
        layerset.getLayer(i, 1, True)
    num_layers = len(layerset.getLayers())

    # Add one image as a patch to each layer.
    for i, layer in enumerate(layerset.getLayers()):
        logger.info("Setting images as layers {}/{}".format(i + 1, num_layers))
        # Add this image as a patch to the layer.
        filepath = filepaths[i]
        logger.debug("createPatch arg1 project: {}".format(project))
        logger.debug("createPatch arg2 filepath: {}".format(filepath))
        patch = Patch.createPatch(project, filepath)
        layer.add(patch)
        # Update internal quadtree of the layer
        layer.recreateBuckets()

    # layerset.getLayer(0, 1, True)
    # layerset.getLayer(1, 1, True)
    # layerset.getLayer(2, 1, True)
    # num_layers = len(layerset.getLayers())

    # # add fixed image to its layer
    # logger.info("Setting fixed image as layer 0")
    # patch = Patch.createPatch(project, inpath_fixed)
    # layer_fixed = layerset.getLayers()[0]
    # layer_fixed.add(patch)
    # layer_fixed.recreateBuckets()  # Update internal quadtree of the layer
    #
    # # add moving image to its layer
    # logger.info("Setting moving image as layer 1")
    # patch = Patch.createPatch(project, inpath_fixed)
    # layer_moving = layerset.getLayers()[1]
    # layer_moving.add(patch)
    # layer_moving.recreateBuckets()  # Update internal quadtree of the layer
    #
    # # add fixed again
    # logger.info("Setting fixed image as layer 0")
    # patch = Patch.createPatch(project, inpath_fixed)
    # layer_fixed = layerset.getLayers()[2]
    # layer_fixed.add(patch)
    # layer_fixed.recreateBuckets()  # Update internal quadtree of the layer

    # Set the correct section thickness for each layer.
    logger.info("Set layer thicknesses..")
    z = 0
    thickness = args["sectionthickness"] / args["imagepixelsize"]
    for i, layer in enumerate(layerset.getLayers()):
        logger.debug("Set thickness (z={}) for layer {}/{}".format(z, i + 1, num_layers))
        layer.setZ(z)
        layer.setThickness(thickness)
        z += thickness

    # Update the LayerTree:
    project.getLayerTree().updateList(layerset)

    # Update world coordinates.
    layerset.setMinimumDimensions()

    return project, layerset


def register(project, layerset):
    logger.info("Begin registration..")
    layerRange = layerset.getLayers()
    fixedLayers = HashSet()
    # set first layer as fixed
    fixedLayers.add(layerset.getLayers()[0])
    emptyLayers = HashSet()
    layerBounds = layerset.get2DBounds()
    propagateTransformBefore = False
    propagateTransformAfter = False
    try:
        ElasticLayerAlignment().exec(
            params,
            project,
            layerRange,
            fixedLayers,
            emptyLayers,
            layerBounds,
            propagateTransformBefore,
            propagateTransformAfter,
            None)
    except Exception as e:
        logger.error("ElasticLayerAlignment failed with following exception:")
        logger.error(e)
        runsuccess = False
    else:
        runsuccess = True

    # Update world coordinates.
    layerset.setMinimumDimensions()

    logger.info("Registration done")
    return runsuccess


def save_layer(layer, path):
    """Save full res layer"""
    layerBounds = layerset.get2DBounds()
    scale = 1.0
    backgroundcolor = Color.white
    logger.debug("layer: {}".format(layer))
    patches = layer.getDisplayables(Patch, layerBounds)
    logger.debug("patches: {}".format(patches))
    ip = Patch.makeFlatImage(ImagePlus.COLOR_RGB, layer, layerBounds, scale, patches, backgroundcolor, True)
    logger.debug("ip: {}".format(ip))
    imp = ImagePlus(path, ip)
    logger.debug("imp: {}".format(imp))
    logger.debug("Saving moving image to path {}".format(path))

    if os.path.splitext(path)[1].lower() == ".jpg":
        IJ.run(imp, "Bio-Formats Exporter", "save=[{}]".format(path))
    elif os.path.splitext(path)[1].lower() == ".tiff" or os.path.splitext(path)[1].lower() == ".tif":
        IJ.run(imp, "Bio-Formats Exporter", "save=[{}] compression={}".format(path, 'LZW'))


def save_layer_subsampled(layer, path, subsample_amount):
    """

    Args:
        layer:
        path: path to saved file
        subsample_amount: 1-100 %

    Returns:

    """

    subsample_dir = os.path.dirname(path)
    if not os.path.exists(subsample_dir):
        os.makedirs(subsample_dir)

    layerBounds = layerset.get2DBounds()
    scale = 1.0
    backgroundcolor = Color.white
    logger.debug("layer: {}".format(layer))
    patches = layer.getDisplayables(Patch, layerBounds)
    logger.debug("patches: {}".format(patches))
    ip = Patch.makeFlatImage(ImagePlus.COLOR_RGB, layer, layerBounds, scale, patches, backgroundcolor, True)
    logger.debug("ip: {}".format(ip))
    imp = ImagePlus(path, ip)
    logger.debug("imp: {}".format(imp))
    logger.debug("Saving moving image to path {}".format(path))

    resized_imp = resize_image(imp, subsample_amount, use_averaging=True, interpolation=Interpolation.BILINEAR)

    if os.path.splitext(path)[1].lower() == ".jpg":
        IJ.run(resized_imp, "Bio-Formats Exporter", "save=[{}]".format(path))
    elif os.path.splitext(path)[1].lower() == ".tiff" or os.path.splitext(path)[1].lower() == ".tif":
        IJ.run(resized_imp, "Bio-Formats Exporter", "save=[{}] compression={}".format(path, 'LZW'))



def save_images(subsample=100):
    logger.info("Saving images")

    layer_moving = layerset.getLayers()[1]
    moving_save_path = os.path.join(args["output_dir"], os.path.basename(args["inpath_moving"]))
    save_layer(layer_moving, moving_save_path)

    # save also fixed image to the output directory for registration quality evaluation
    layer_fixed = layerset.getLayers()[0]
    fixed_save_path = os.path.join(args["output_dir"], os.path.basename(args["inpath_fixed"]))
    save_layer(layer_fixed, fixed_save_path)

    if subsample < 100:
        subsample_save_dir = os.path.join(os.path.dirname(os.path.dirname(moving_save_path)), "registered_subsampled")
        moving_save_path_sub = os.path.join(subsample_save_dir, os.path.basename(moving_save_path))
        save_layer_subsampled(layer_moving, moving_save_path_sub, subsample_amount=subsample)
        fixed_save_path_sub = os.path.join(subsample_save_dir, os.path.basename(fixed_save_path))
        save_layer_subsampled(layer_fixed, fixed_save_path_sub, subsample_amount=subsample)



# Enum for interpolation method
# (This is one way to use enums in python2)
class Interpolation:
    NEAREST_NEIGHBOR = 0
    BILINEAR = 1
    BICUBIC = 2


def resize_image(imp, resize_amount, use_averaging, interpolation):
    """Resize image"""

    if resize_amount == 0:
        print("Resize amount 0 is not sane, skipping resize")
        return imp
    elif resize_amount == 100:
        print("Resize parameter 100, skipping resize")
        return imp
    else:
        # select interpolation mathod according to input form command line
        ip = imp.getProcessor()
        if interpolation == Interpolation.BILINEAR:
            interp_str = "bilinear"
            ip.setInterpolationMethod(ip.BILINEAR)
        elif interpolation == Interpolation.BICUBIC:
            interp_str = "bicubic"
            ip.setInterpolationMethod(ip.BICUBIC)
        elif interpolation == Interpolation.NEAREST_NEIGHBOR:
            interp_str = "nearest-neighbor"
            ip.setInterpolationMethod(ip.NEAREST_NEIGHBOR)

        # calculate new image dimensions
        h = imp.getProcessor().getHeight()
        w = imp.getProcessor().getWidth()
        new_h = int(h * resize_amount / 100.0)
        new_w = int(w * resize_amount / 100.0)

        print("Resizing with factor {} using {} interpolation".format(resize_amount / 100.0, interp_str))
        print("\told dimensions: {}x{}".format(w, h))
        print("\tnew dimensions: {}x{}".format(new_w, new_h))
        bimg = ip.resize(new_w, new_h, use_averaging).getBufferedImage()
        new_imp = ImagePlus("Resized", bimg)

    return new_imp


if __name__ == '__main__':

    args = parse_cli_arguments()
    check_cli_arguments(args)
    initialize_settings(args)
    params = set_esa_params(args)
    print_arguments_debug(params, args)

    project, layerset = setup_registration(args['inpath_fixed'], args['inpath_moving'])

    status = register(project, layerset)

    if not status:
        sys.exit(1)

    # If the registration was successful, output images and save project.
    save_images(args["subsample"])

    # Disable mipmap regeneration to allow later reapplying transformations
    # faster by loading a modified project XML.
    project.getLoader().setMipMapsRegeneration(False)

    logger.info("Saving project file")
    project.saveAs(os.path.join(args["transforms_dir"], "tempproject.xml"), True)

    logger.info("Closing project")
    project.remove()

    # remove mipmaps
    rmdirs = glob.glob(os.path.join(os.path.abspath(args["transforms_dir"]), "trakem2*"))
    logger.info("Removing directories: {}".format(rmdirs))
    for f in rmdirs:
        shutil.rmtree(f)


    logger.info("Quitting")
    sys.exit(0)
