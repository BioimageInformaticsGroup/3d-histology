#!/usr/bin/env python3
import logging
import signal
import sys

import cli.align
import cli.register
import cli.convert
import cli.grid_search
import cli.masks
from biit_slurm import slurm
from cli.align import align
from cli.convert import convert
from cli.grid_search import grid_search
from cli.histo_argument_parser import get_argument_parser
from cli.masks import process_masks_tumor, process_masks_tissue
from cli.register import register
from cli.settings import CONSOLE_LOG_LEVEL

# setup logging
logger = logging.getLogger(__name__)
logger.setLevel(CONSOLE_LOG_LEVEL)
slurm.logger.setLevel(CONSOLE_LOG_LEVEL)


# TODO: Combine log files from align, convert etc modules?

def sigint_handler(signum, frame):
    logger.debug("SIGINT captured (ctrl+c)")

    prompt = "(c/b)?: "

    ans = input("There are jobs currently running in slurm queue, do you want to:\n"
                "  (C)ANCEL ALL YOUR JOBS\n"
                "  Finish in the (B)ackground\n" + prompt)

    good_answer = False
    while not good_answer:
        if ans.lower() == "c":
            logger.debug("User selected job cancellation")
            # TODO: implement this properly so that only jobs started with histo command get cancelled
            job_list = slurm.get_user_slurm_queue()
            job_ids = slurm.parse_job_ids(job_list)
            slurm.cancel_job_ids(job_ids)
            good_answer = True
        elif ans.lower() == "b":
            logger.debug("User selected background execution")
            good_answer = True
        else:
            ans = input("Try again\n" + prompt)

    logger.info("Quitting")
    sys.exit(0)


signal.signal(signal.SIGINT, sigint_handler)


def check_common_arguments(args, parser):
    # print help if no arguments are given
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(0)

    # Enable debug logging if '--debug' was passed from command line
    if args["debug"]:
        # TODO: There is most probably a better way to do this
        logger.setLevel(logging.DEBUG)
        slurm.logger.setLevel(logging.DEBUG)
        cli.align.logger.setLevel(logging.DEBUG)
        cli.register.logger.setLevel(logging.DEBUG)
        cli.masks.logger.setLevel(logging.DEBUG)
        cli.convert.logger.setLevel(logging.DEBUG)
        cli.grid_search.logger.setLevel(logging.DEBUG)

    logger.debug("Command line arguments after check: {}".format(args))

    return args


def main():
    parser = get_argument_parser()
    args = vars(parser.parse_args())
    args = check_common_arguments(args, parser)

    if args["command"] == "register":
        register(args)
    elif args["command"] == "convert":
        convert(args)
    elif args["command"] == "align":
        align_job_id = align(args)
        slurm.wait_for_ids([align_job_id])
    elif args["command"] == "gsearch":
        grid_search(args)
    elif args["command"] == "reapply-tumor":
        process_masks_tumor(args)
    elif args["command"] == "reapply-tissue":
        process_masks_tissue(args)
    else:
        logger.error("Unrecognized command {}".format(args["command"]))


if __name__ == '__main__':
    main()
