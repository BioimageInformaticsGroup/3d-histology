import glob
import os
import re
import sys
from argparse import ArgumentParser
from pprint import pprint
from typing import List

import psutil
import natsort
import numpy as np
from PIL import Image
from skimage.io import imread
from skimage.transform import resize

# sys.path.append("../")  # find a better way to manage imports
sys.path.insert(1, os.path.join(sys.path[0], '..'))  # find a better way to manage imports
from utils.utils import extract_sequence_numbers_from_filenames
from biit_image_tools.imagetools.io import load_images_generator_dir

Image.MAX_IMAGE_PIXELS = 5000000000


def print_mem_usage():
    process = psutil.Process(os.getpid())
    # output in megabytes
    print("Memory usage: {:.2f} MiB".format(process.memory_info().rss / (1024 * 1024)))
    sys.stdout.flush()

def fromarray_large(array, mode=None):
    """ This replaces PIL's Image.fromarray() method for images with more than 2^28 pixels.
    Image is split into N pieces horizontally and the pieces are then pasted ( Image.paste() )
    into PIL image to circumvent using fromarray() for large images.

    Args:
        array: input image as numpy ndarray
        mode: PIL image mode for the output image, if none mode is deduced automatically

    Returns:
        Pil image that has the same size and content as the input array
    """

    # Use fromarray if image is small enough
    if array.shape[0] * array.shape[1] < 2 ** 28:
        print("fromarray_large(): Using fromarray")
        return Image.fromarray(array, mode=mode)

    else:
        print("fromarray_large(): Using fromarray large")
        if mode is None:
            # get mode by creating one pixel image
            mode = Image.fromarray(array[0:1, 0:1]).mode
            print("fromarray_large(): Determined output image mode: {}".format(mode))

        # divide image into N equal sized pieces.
        # Here the N is selected to be as large as possible so that
        # size of every piece is below 2^28.
        N = int(np.ceil((array.shape[0] * array.shape[1]) / (2 ** 28)))
        print("fromarray_large(): Splitting image into N={} pieces".format(N))

        # divide array's x-axis into N slices
        piece_inds = np.linspace(0, array.shape[1], N + 1).astype(int)

        # create empty image having the same size as input array
        # PIL uses coordinates in order x, y whereas numpy uses y, x
        pil_im = Image.new(mode, array.shape[:2][::-1])  # 8-bits / channel (3x8)

        # iterate over all pieces and paste them into the output image
        y_min = 0
        y_max = array.shape[0]
        for i in range(N):
            x_min = piece_inds[i]
            x_max = piece_inds[i + 1]
            # left, upper, right, lower coordinates of the pasted image in the output coordinates
            box = (x_min, y_min, x_max, y_max)
            print(
                "fromarray_large(): Pasting piece {} with box coordinates (l,t,r,b) {} "
                "into image with size {}".format(i, box, pil_im.size)
            )
            piece = Image.fromarray(array[:, x_min:x_max])
            pil_im.paste(piece, box)

        return pil_im


# def load_images_generator(image_folder_path: str,
#                           regex: str = None,
#                           num_images_to_load: int = -1,
#                           recursive: bool = False,
#                           grayscale: bool = False):
#     """
#     Image loading generator
#
#     Args:
#         image_folder_path: Folder to load images from
#         regex: Regular expression to select only som of the files
#         num_images_to_load: number of images to load from the start of the list
#         recursive: Search images recursively from the 'image_folder_path' and its subfolders
#         grayscale: Convert images to grayscale
#
#     Yields:
#         Each iteration yields a tuple with the following elements
#
#         image: np.ndarray
#         filepath: str
#         total_number_of_images: int
#
#         (im, filepath, num_images)
#     """
#
#     fns = list(glob.iglob(os.path.join(image_folder_path, "**"), recursive=recursive))
#     # filter out folders
#     fns = list(filter(os.path.isfile, fns))
#
#     # select specific images according to regex
#     if regex:
#         fns = filter_filelist_with_regex(regex, fns)
#
#     if num_images_to_load >= 0:
#         fns = fns[0:num_images_to_load]
#
#     for i, fn in enumerate(fns):
#         im = imread(os.path.join(image_folder_path, fn), as_gray=grayscale)
#         yield im, fn, len(fns)


def filter_filelist_with_regex(pattern: str, fnames: List[str]):
    matches = [re.findall(".*{}.*".format(pattern), f) for f in fnames]
    out = [f[0] for f in filter(None, matches)]
    return out


def merge_masks_from_same_slice(unmerged_images: List[np.ndarray], unmerged_sequence: List[int]):
    """ Merge masks from the same slice.
    Images list contains uint8 masks, sequence contains the corresponding slice indices.
    If there are two or more images with the same slice index, the images are added together
    """
    # create a new list 'images_merged' containing one merged image in place of multiple masks from the same slice
    images_merged = []
    # sequence_merged contains similarly the sequence numbers
    # e.g. [1, 2, 3, 3, 3, 4, 5] -> [1, 2, 3_merged, 4, 5]
    sequence_merged = []
    # iterate over image list and join together masks from same slice
    for i, s in enumerate(unmerged_sequence):
        inds = [ind for ind, ss in enumerate(unmerged_sequence) if ss == s]
        print("Mask i: {}, merge indices: {} - unique labels {}".format(i, inds, np.unique(unmerged_images[i])))
        if len(inds) > 1:
            # get images from list
            idx_images = [unmerged_images[ind] for ind in inds]
            # check that the dimensions match (they should)
            assert (np.array([(idx_images[0].shape == im.shape) for im in idx_images[1:]]).all())
            # join them together
            array = np.array(idx_images)
            merged = np.add.reduce(array, axis=0, dtype=np.uint8)
            # TODO: This code does not check if the annotations overlap, everything is merged using add.reduce
            # append only if merged image is not there yet
            # (this could be more optimized but is arguably more readable like this)
            if sequence_merged.count(s) <= 0:
                images_merged.append(merged)
                sequence_merged.append(s)
        else:
            images_merged.append(unmerged_images[inds[0]])
            sequence_merged.append(s)

    return images_merged, sequence_merged


def postprocess_masks(images: List[np.ndarray], filenames: List[str], sequence: List[int], upscale):
    """Perform preprocessing steps for the mask images
    These include:
         Mask inversion
         Coding tumor label (A, B, C ...) into mask's pixel value
         Merge images from the same slice together
         Upscale masks with nearest neighbor interpolation by 10x
    """

    # when using grayscale=True with load_images_generator (uses skimage.io.imread + as_gray argument)
    # it appears that the returned image ranges within [0, 1]. Without as_gray argument, the range is [0, 255]

    # code labels into images
    images = [im * 255 > 127 for im in images]
    images = [im.astype(np.uint8) * int(ord(fn[fn.find(".") - 1]) - 64) for im, fn in zip(images, filenames)]

    # print which characters were converted to which labels
    for i, (im, fn) in enumerate(zip(images, filenames)):
        label = np.max(im.ravel())
        charr = fn[fn.find(".") - 1]
        print("Mask i: {} - Letter {} -> {}, mask fn: {}".format(i, charr, label, fn))

    # merge images
    images_merged, sequence_merged = merge_masks_from_same_slice(images, sequence)

    if upscale:
        # Upscale images with NN, 10x
        preprocessed = [resize(im, tuple(np.array(im.shape) * 10), order=0) for im in images_merged]
    else:
        preprocessed = images_merged

    return preprocessed, sequence_merged


def save_masks(masks: List[np.ndarray], full_save_paths: List[str], output_path: str):
    # ensure output directory exists
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    for i, (im, fn) in enumerate(zip(masks, full_save_paths), start=1):
        print("Converting mask {}/{} to uint8 - dtype {} - unique values {}"
              "".format(i, len(full_save_paths), im.dtype, list(set(im.ravel()))))
        im = im.astype(np.uint8)
        print("Saving empty mask {}/{} with size {}"
              "".format(i, len(full_save_paths), im.shape))
        # use scipy to save the actual pixel values without scaling
        # toimage(im, cmin=0, cmax=255).save(fn)
        fromarray_large(im).save(fn)


def parse_arguments():
    parser = ArgumentParser(description="Postprocess one directory of masks")
    parser.add_argument("masks_path", metavar="masks-path",
                        help="Path to masks", type=str)
    parser.add_argument("images_path", metavar="images-path",
                        help="Path to slice images", type=str)
    parser.add_argument("output_path", metavar="output-path",
                        help="Path in which postprocessed masks are saved",
                        type=str)
    parser.add_argument("--image-regex", default=None,
                        help="Regular expression to use when reading images from 'images-path'",
                        type=str)
    parser.add_argument("--mask-regex", default=None,
                        help="Regular expression to use when reading masks from 'masks-path'",
                        type=str)
    parser.add_argument("--save-template", default="ROIMask_{}.png",
                        help="file name of the saved mask where {} is replaced by corresponding slice file name",
                        type=str)
    parser.add_argument("--upscale", default=False,
                        help="Upscale masks, this is useful if masks are (post)processed before reapply transform",
                        type=bool)
    parser.add_argument("--create-only-empties", default=False,
                        help="Create empty masks for images missing a mask", action='store_true')
    args = parser.parse_args()

    return args


def check_arguments(args):
    # check input arguments
    if not os.path.exists(args.masks_path):
        print("Path to masks does not exist", file=sys.stderr)
        sys.exit(1)
    if not os.path.exists(args.images_path):
        print("Path to images does not exist", file=sys.stderr)
        sys.exit(2)
    if args.save_template.find("{") == -1 or args.save_template.find("}") == -1:
        print("Save template has invalid form", file=sys.stderr)
        sys.exit(3)


def load_image_information(images_path: str, images_load_regex: str = None, every_image_same_shape: bool = True):
    """
    Load information about images in images_path.
    Reads image shapes and filenames.
    Loads images into memory but only one at a time.
    """
    print("Reading slice image shapes..")
    # if we can assume same shape for every image, execution time reduces drastically because majority of the
    # time is spent loading huge images from disk
    if every_image_same_shape:
        image_filenames = os.listdir(images_path)
        if images_load_regex:
            image_filenames = filter_filelist_with_regex(images_load_regex, image_filenames)
        if image_filenames:
            im = imread(os.path.join(images_path, image_filenames[0]), as_gray=True)
            im = np.squeeze(im)
            image_shapes = [im.shape] * len(image_filenames)
        else:
            print("File list empty, check image folder path and images regex", file=sys.stderr)
            sys.exit(1)
    else:
        image_shapes = []
        image_filenames = []
        for i, (im, fn, N) in enumerate(load_images_generator_dir(images_path,
                                                              regex=images_load_regex,
                                                              grayscale=True),
                                        start=1):
            im = np.squeeze(im)
            print("{}/{} image shape {} - {}".format(i, N, im.shape, fn))
            print_mem_usage()
            image_filenames.append(fn)
            image_shapes.append(im.shape)
            del im

    print("Sorting slice image file names according to sequence number (slice index)")
    image_sequence = extract_sequence_numbers_from_filenames(image_filenames)
    image_sort_idx = np.argsort(image_sequence)
    image_shapes = [image_shapes[i] for i in image_sort_idx]
    image_sequence = [image_sequence[i] for i in image_sort_idx]
    image_filenames = [image_filenames[i] for i in image_sort_idx]

    # get rid of full path to images, only filenames are needed
    image_filenames = [fn.split("/")[-1] for fn in image_filenames]
    # remove also file extension
    image_filenames = [fn[:fn.rfind(".")] for fn in image_filenames]

    # construct dicts where sequence number is the key and image filename or shape is the value
    fn_dict = dict(zip(image_sequence, image_filenames))
    shape_dict = dict(zip(image_sequence, image_shapes))

    return image_sequence, fn_dict, shape_dict


def load_masks(masks_path: str, mask_load_regex: str):
    # load masks in alphabetical order
    unsorted_fns = []
    unsorted_images = []
    print("MEM usage before images:")
    print_mem_usage()
    for i, (im, fn, N) in enumerate(load_images_generator_dir(masks_path, regex=mask_load_regex, grayscale=True)):
        print("Loading mask {}/{} - {}".format(i, N, fn))
        print_mem_usage()
        unsorted_images.append(im)
        unsorted_fns.append(fn)

    unsorted_sequence = extract_sequence_numbers_from_filenames(unsorted_fns)

    # sort sequence, fns, and images according to sequence number (slice index found in mask's filename)
    sort_idx = np.argsort(unsorted_sequence)
    images = [unsorted_images[i] for i in sort_idx]
    filenames = [unsorted_fns[i] for i in sort_idx]
    sequence = [unsorted_sequence[i] for i in sort_idx]

    return images, filenames, sequence


def create_only_empties(args):
    # read masks
    mask_fns = os.listdir(args.masks_path)
    if args.mask_regex:
        mask_fns = filter_filelist_with_regex(args.mask_regex, mask_fns)
    mask_fns = natsort.natsorted(mask_fns)

    print("Mask filenames:")
    pprint(mask_fns)
    sequence = extract_sequence_numbers_from_filenames(mask_fns)
    print("Mask sequence: {}".format(sequence))
    sequence = list(set(sequence))  # get uniques
    print("Mask uniques sequence : {}".format(sequence))
    print("Loading image information..")
    image_sequence, fn_dict, shape_dict = load_image_information(args.images_path, args.image_regex)
    print("Images sequence : {}".format(sequence))
    images_without_mask = [s for s in image_sequence if sequence.count(s) < 1]
    print("Images without mask : {}".format(sequence))
    print("Creating empty masks..")
    empty_masks = [np.zeros(shape_dict[s]) for s in images_without_mask]
    print("Empty mask shapes: ")
    empty_mask_fns = [os.path.join(args.output_path, args.save_template.format(fn_dict[s])) for s in
                      images_without_mask]
    print("Saving empty masks..")
    save_masks(empty_masks, empty_mask_fns, args.output_path)


def main(args):
    print("Start loading images..")
    images, filenames, sequence = load_masks(args.masks_path, args.mask_regex)
    print("Loaded mask filenames: {}".format(filenames))
    print("Mask sequence: {}".format(sequence))

    # merge masks if multiple exist per slice
    print("Sequence before merging: {}".format(sequence))
    # NOTE!!: the number of images is less than before from this point forward due to mask merging
    masks_prep, sequence = postprocess_masks(images, filenames, sequence, args.upscale)
    print("Sequence after merging: {}".format(sequence))

    # remove mask filenames to reduce confusion,
    # they are not that meaningful now that images have been merged
    del filenames

    image_sequence, fn_dict, shape_dict = load_image_information(args.images_path, args.image_regex)

    # save masks
    # create mask file names by matching mask with image filename using sequence numbers
    print("Saving masks..")
    mask_filenames = [os.path.join(args.output_path, args.save_template.format(fn_dict[s])) for s in sequence]
    save_masks(masks_prep, mask_filenames, args.output_path)

    images_without_mask = [s for s in image_sequence if sequence.count(s) < 1]
    if images_without_mask:
        print("Saving empty masks..")
        empty_masks = [np.zeros(shape_dict[s]) for s in images_without_mask]
        empty_mask_fns = [os.path.join(args.output_path, args.save_template.format(fn_dict[s])) for s in
                          images_without_mask]
        save_masks(empty_masks, empty_mask_fns, args.output_path)
    print("Done")


if __name__ == '__main__':
    cli_args = parse_arguments()
    check_arguments(cli_args)

    print("Command line arguments:")
    pprint(cli_args)

    # flush print stream for debugging purposes
    sys.stdout.flush()

    if cli_args.create_only_empties:
        create_only_empties(cli_args)
    else:
        main(cli_args)

    sys.exit(0)
