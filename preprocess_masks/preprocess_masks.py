import os
import sys
import logging
from typing import List
from argparse import ArgumentParser

import numpy as np
from PIL import Image
# use this
from skimage.transform import resize

Image.MAX_IMAGE_PIXELS = 5000000000

sys.path.insert(1, os.path.join(sys.path[0], '..'))  # find a better way to manage imports

from biit_image_tools.imagetools.io import load_images_generator_dir

logging.basicConfig(level=logging.DEBUG)


def preprocess_masks(images: List[np.ndarray], invert: bool = False):
    """Perform preprocessing steps for the mask images
    These include:
         Mask inversion
         Upscale masks with nearest neighbor interpolation by 10x
    """

    if invert:
        preprocessed = [np.invert(im.astype(bool)) * 255 for im in images]
    else:
        preprocessed = [im.astype(bool) * 255 for im in images]
    
    # Older fixed function definitions, preserved here for documentation

    # Tumor mask preprocesing
    # This was used for tumor annotations (new python preprocessing. Input images are the same size as as tumor masks)
    # preprocessed = [np.invert(im.astype(bool)) * 255 for im in images]

    # # Tumor mask preprocesing
    # # This was used for tumor annotations (old matlab preprocessing. Input images for this function are 10% subsampled )
    # images = [np.invert(im.astype(bool)) * 255 for im in images]
    # preprocessed = [imresize(im.astype(np.uint8), tuple(np.array(im.shape) * 10), interp='nearest') for im in images]

    # # Tissuemask preprocessing
    # # This was used for tissue annotations (new python preprocessing. Input images are twice as large (200% upscaled))
    # images = [im.astype(bool) * 255 for im in images]
    # preprocessed = [imresize(im.astype(np.uint8), tuple((np.array(im.shape) * 8).astype(int)), interp='nearest') for im in images]

    return preprocessed


def fromarray_large(array, mode=None):
    """ This replaces PIL's Image.fromarray() method for images with more than 2^28 pixels.
    Image is split into N pieces horizontally and the pieces are then pasted ( Image.paste() )
    into PIL image to circumvent using fromarray() for large images.

    Args:
        array: input image as numpy ndarray
        mode: PIL image mode for the output image, if none mode is deduced automatically

    Returns:
        Pil image that has the same size and content as the input array
    """

    # Use fromarray if image is small enough
    if array.shape[0] * array.shape[1] < 2 ** 28:
        print("fromarray_large(): Using fromarray")
        return Image.fromarray(array, mode=mode)

    else:
        print("fromarray_large(): Using fromarray large")
        if mode is None:
            # get mode by creating one pixel image
            mode = Image.fromarray(array[0:1, 0:1]).mode
            print("fromarray_large(): Determined output image mode: {}".format(mode))

        # divide image into N equal sized pieces.
        # Here the N is selected to be as large as possible so that
        # size of every piece is below 2^28.
        N = int(np.ceil((array.shape[0] * array.shape[1]) / (2 ** 28)))
        print("fromarray_large(): Splitting image into N={} pieces".format(N))

        # divide array's x-axis into N slices
        piece_inds = np.linspace(0, array.shape[1], N + 1).astype(int)

        # create empty image having the same size as input array
        # PIL uses coordinates in order x, y whereas numpy uses y, x
        pil_im = Image.new(mode, array.shape[:2][::-1])  # 8-bits / channel (3x8)

        # iterate over all pieces and paste them into the output image
        y_min = 0
        y_max = array.shape[0]
        for i in range(N):
            x_min = piece_inds[i]
            x_max = piece_inds[i + 1]
            # left, upper, right, lower coordinates of the pasted image in the output coordinates
            box = (x_min, y_min, x_max, y_max)
            print(
                "fromarray_large(): Pasting piece {} with box coordinates (l,t,r,b) {} "
                "into image with size {}".format(i, box, pil_im.size)
            )
            piece = Image.fromarray(array[:, x_min:x_max])
            pil_im.paste(piece, box)

        return pil_im


def save_masks(masks: List[np.ndarray], full_save_paths: List[str]):
    for i, (im, fn) in enumerate(zip(masks, full_save_paths), start=1):
        logging.info("Converting mask {}/{} to uint8 - dtype {} - unique values {}"
              "".format(i, len(full_save_paths), im.dtype, list(set(im.ravel()))))
        im = im.astype(np.uint8)
        logging.info("Saving mask {}/{} - dtype {} - unique values {}"
              "".format(i, len(full_save_paths), im.dtype, list(set(im.ravel()))))
        # use scipy to save the actual pixel values without scaling
        # toimage(im, cmin=0, cmax=255).save(fn)
        fromarray_large(im).save(fn)


if __name__ == '__main__':

    parser = ArgumentParser(description="Preprocess masks before applying transforms")
    parser.add_argument("input_directory", metavar="input-directory", help="Path to to the masks", type=str)
    parser.add_argument("output_directory", metavar="output-directory", help="Path to save preprocessed masks",
                        type=str)
    parser.add_argument("--save-template",
                        default="Preprocessed_{}.png",
                        help="file name of the saved mask where {} is replaced by corresponding slice file name",
                        type=str)
    parser.add_argument("--invert",
                        default=False,
                        action='store_true',
                        help="Invert masks")
    args = parser.parse_args()

    logging.debug("Command line arguments: {}".format(args))

    # check input arguments
    logging.info("Checking input arguments..")
    if not os.path.exists(args.input_directory):
        logging.error("Path to input directory does not exist", file=sys.stderr)
        sys.exit(1)
    if args.save_template.find("{") == -1 or args.save_template.find("}") == -1:
        logging.error("Save template has invalid form", file=sys.stderr)
        sys.exit(2)
    # create path to output dir if it does not exist
    if not os.path.exists(args.output_directory):
        logging.warn("Path to output directory does not exist", file=sys.stderr)
        os.makedirs(args.output_directory)
    logging.info("Checking done")

    # load images in alphabetical order
    masks = []
    mask_filenames = []
    for i, (im, fn, N) in enumerate(load_images_generator_dir(args.input_directory, grayscale=True), start=1):
        logging.debug("Loading image {}/{} - fn: {}".format(i, N, fn))
        masks.append(im)
        mask_filenames.append(fn)

    logging.info("Start processing loaded images..")
    preprocessed = preprocess_masks(masks, args.invert)
    logging.info("Processing done")

    # get rid of full path to images, only filenames are needed
    mask_filenames = [fn.split("/")[-1] for fn in mask_filenames]
    # remove also file extension
    mask_filenames = [fn[:fn.rfind(".")] for fn in mask_filenames]

    # save masks
    logging.info("Saving preprocessed masks..")
    # create mask file names by matching mask with image filename using sequence numbers
    mask_save_paths = [os.path.join(args.output_directory, args.save_template.format(mfn)) for mfn in mask_filenames]
    save_masks(preprocessed, mask_save_paths)
    logging.info("Saving done")

    logging.info("Preprocessing complete")
