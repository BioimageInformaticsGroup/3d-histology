import logging
import os
import sys
from argparse import ArgumentParser

import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import psutil
import natsort
import numpy as np
from PIL import Image
from scipy.signal import convolve2d
from skimage.color import rgb2gray
from skimage import filters

sys.path.insert(1, os.path.join(sys.path[0], '..'))
from biit_image_tools.imagetools.io import load_images_generator

Image.MAX_IMAGE_PIXELS = 5000000000

NAME_MASK_SAVE_DIR = "saved_masks"


def print_mem_usage():
    process = psutil.Process(os.getpid())
    # output in megabytes
    print("Memory usage: {:.2f} MiB".format(
        process.memory_info().rss / (1024 * 1024)))
    sys.stdout.flush()


def parse_arguments():
    parser = ArgumentParser(description="")
    parser.add_argument("dir_path", metavar="dir-path", help="", type=str)
    parser.add_argument("--save_masks", help="", action="store_true")
    parser.add_argument("--method", help="", default="laplacianMSE", type=str)
    args = vars(parser.parse_args())
    return args


def check_arguments(args):
    # check input arguments
    if not os.path.exists(args["dir_path"]):
        print("Path does not exist", file=sys.stderr)
        sys.exit(1)


def laplacian_mse(im, prev_im):
    # laplacian weighted MSE
    win_size = 15
    L = filters.laplace(im, win_size)
    mask = np.abs(L)
    # No need to scale mask between 0-1, normalization in the end ensures it has no effect
    # mask = filters.gaussian(mask, sigma=2)
    # mask /= mask.max()
    logging.debug("Mask min {} max {}".format(mask.min(), mask.max()))

    # compute MSE
    I = (im - prev_im) ** 2  # numpy.pow is slower than basic python **
    # weight scores with mask
    I = I * mask
    summ = np.sum(I[:])
    logging.debug("weighted diff sum: {}".format(summ))
    # normalize by mask's intensity & size
    score = summ / np.sum(mask[:])
    return score, mask


def mse(im, prev_im):
    I = (im - prev_im) ** 2  # numpy.pow is slower than basic python **
    score = np.mean(I[:])
    return score


def variance_weighted_mse(im, prev_im):
    # variance weighted MSE
    kern = np.ones((15, 15))
    kern = kern / np.sum(kern[:])
    diff = im - prev_im
    mean = convolve2d(im, kern, mode='same', boundary='symm')
    mean_sq = convolve2d(im ** 2, kern, mode='same', boundary='symm')
    # variance mask
    mask = mean_sq - mean ** 2

    # logging.debug("Mask min {} max {}".format(mask.min(), mask.max()))
    # compute MSE
    I = (im - prev_im) ** 2  # numpy.pow is slower than basic python **
    logging.debug("diff sum: {}".format(np.sum(I)))
    # weight scores with mask
    I = I * mask
    logging.debug("weighted diff sum: {}".format(np.sum(I)))
    score = np.sum(I[:]) / np.sum(mask)
    return score, mask


def save_mask(mask, fname):
    logging.info(os.getcwd())
    if not os.path.exists(NAME_MASK_SAVE_DIR):
        os.makedirs(NAME_MASK_SAVE_DIR)
    save_path = os.path.join(NAME_MASK_SAVE_DIR, fname)
    logging.info("Saving mask: {}".format(save_path))
    plt.set_cmap("gray")
    plt.imsave(save_path, mask)


def evaluate_stack(fns, save_masks, method):
    logging.debug("Evaluating stack: {}".format(fns))
    if not fns:
        logging.debug("Stack is empty, stack score {}".format(np.inf))
        return np.inf

    scores = []
    prev_im = None
    for i, (im, fn, num_files) in enumerate(load_images_generator(fns), start=1):

        if len(im.shape) > 3:
            im = np.squeeze(im)
        im = rgb2gray(im)

        # skip first iteration
        if prev_im is None:
            prev_im = im
            continue

        assert np.all(prev_im.shape == im.shape)

        if method == "laplacianMSE":
            score, mask = laplacian_mse(im, prev_im)
            if save_masks:
                save_mask(mask, os.path.basename(fn) + "_mask.png")
        elif method == "varianceMSE":
            score, mask = variance_weighted_mse(im, prev_im)
            if save_masks:
                save_mask(mask, os.path.basename(fn) + "_mask.png")
        elif method == "MSE":
            score = mse(im, prev_im)
            if save_masks:
                logging.info("No masks to save with MSE")
        else:
            logging.error("Unrecognized method, quitting")
            sys.exit(1)

        del prev_im
        logging.debug("score: {}".format(score))
        scores.append(score)

        prev_im = im

    total_score = np.array(scores).mean()
    if np.isnan(total_score):
        total_score = np.inf
    logging.debug("stack score: {}".format(total_score))
    return total_score


def main():
    logging.basicConfig(level=logging.INFO)

    args = parse_arguments()
    check_arguments(args)

    fns = os.listdir(args["dir_path"])
    fns = [os.path.join(args["dir_path"], f) for f in fns]
    fns = natsort.natsorted(fns)

    score = evaluate_stack(fns, args["save_masks"], args["method"])

    logging.info("{} : {}".format(args["dir_path"], score))


if __name__ == '__main__':
    main()
