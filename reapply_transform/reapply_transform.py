from __future__ import print_function

import argparse
import glob
import os
import re
import shutil
import sys
import xml.etree.ElementTree as ET
from collections import OrderedDict
from copy import deepcopy
from pprint import pprint
from shutil import copyfile

from ij import IJ, ImagePlus
from ini.trakem2 import ControlWindow, Project
from ini.trakem2.display import Patch
from java.awt import Color

sys.path.insert(1, os.path.join(sys.path[0], '..'))  # find a better way to manage imports


def setup():
    # Disable GUI.
    ControlWindow.setGUIEnabled(0)

    # If any projects are open, close them.
    allprojects = Project.getProjects()
    for project in allprojects:
        project.remove()


def extract_sequence_numbers_from_filenames(filenames):
    """
    Extract sequence number / slice index from list of filenames.
    Sequence number is the running number (usually between 1-99) in a slice filename
    """
    seq = [int(re.findall("-([0-9]*)_", x, flags=0)[-1]) for x in filenames]
    if not seq:
        raise ValueError("Filenames do not match regex '-([0-9]*)_'")
    return seq


def insert_trackem2_header(original_pf, modified_pf):
    # get header
    with open(original_pf, 'r') as f:
        original_content = f.read()
    head = original_content[:original_content.find("<trakem2>")]
    # insert int to the beginning of modified_pf
    with open(modified_pf, 'r+') as f:
        modified_content = f.read()
        all = head + modified_content
        f.seek(0)  # move pointer to the beginning of file after read()
        f.write(all)


def parse_aruments():
    # parse command line arguments
    parser = argparse.ArgumentParser(description="Apply transformations from TrackEM2 project to images")
    parser.add_argument("source_dir", metavar="source-dir", help="", type=str)
    parser.add_argument("target_dir", metavar="target-dir", help="", type=str)
    parser.add_argument("project_file_path", metavar="project-file-path", help="", type=str)
    parser.add_argument("--grayscale", default=True, help="", dest="isimagecolor", action="store_false")
    parser.add_argument("--modify-project-file",
                        default=False,
                        help="Modify project file to have it match the number of images in source_dir",
                        action="store_true")
    parser.add_argument("--save-template",
                        default="Transformed_{}.png",
                        help="file name of the saved mask where {} is replaced by corresponding slice file name",
                        type=str)
    parser.add_argument("--work_dir",
                        type=str,
                        help="Set reapply_transform work dir, e.g. modified project file is saved into this directory "
                             "at runtime due to trackem limitations")
    args = parser.parse_args()
    return args


def check_input_arguments(args):
    # set current work dir if given
    dargs = vars(args)
    if "work_dir" in dargs.keys() and dargs["work_dir"] is not None:
        print("Set work dir: {}".format(dargs["work_dir"]))
        os.chdir(dargs["work_dir"])
    else:
        print("Leave work dir as is: {}".format(os.getcwd()))

    if not os.path.exists(args.source_dir):
        print("Path to source directory does not exist", file=sys.stderr)
        sys.exit(1)

    if not os.path.exists(args.project_file_path):
        print("Path to project file does not exist", file=sys.stderr)
        sys.exit(2)

    if args.save_template.find("{") == -1 or args.save_template.find("}") == -1:
        print("Save template has invalid form", file=sys.stderr)
        sys.exit(3)

    # create path to output dir if it does not exist
    if not os.path.exists(args.target_dir):
        print("Path to output directory {} does not exist, creating it".format(args.target_dir))
        os.makedirs(args.target_dir)

    # empty output directory because Bioformats exporter somehow "appends" to existing images
    file_list = os.listdir(args.target_dir)
    if len(file_list) > 0:
        print("WARNING: Output directory not empty, clearing its content")
        for f in file_list:
            os.remove(os.path.join(args.target_dir, f))

    # convert all paths to relative. Trakem2 seems to have problem with absolute paths
    args.source_dir = os.path.relpath(args.source_dir)
    args.target_dir = os.path.relpath(args.target_dir)
    args.project_file_path = os.path.relpath(args.project_file_path)


def print_tree_hierarchy(root_node):
    print("Tree hierarchy:")
    for i, layer in enumerate(root_node.iter("t2_layer")):
        print("   i: {} - layer oid={} - file_path={}".format(i, layer.attrib["oid"], layer[0].attrib["file_path"]))


def disassemble_pf_tree(root_node):
    layer_dict = OrderedDict()
    for layer in root_node.iter("t2_layer"):
        file_path = layer[0].attrib["file_path"]
        seq = extract_sequence_numbers_from_filenames([file_path])[0]
        print("Layer sequence num {}".format(seq))
        layer_dict[seq] = layer

    # remove layer from the tree after storing it
    for k, v in layer_dict.items():
        root_node[1].remove(v)

    return layer_dict


def reassemble_pf_tree(root_node, image_sequence, layer_dict):
    for i, s in enumerate(image_sequence):
        # insert into layerset
        c = deepcopy(layer_dict[s])
        root_node[1].insert(i, c)


def modify_project_file(original_pf_path, modified_pf_path, image_fns):
    """
    Create a modified version of the project file that contains correct
    transformations for each image file in the input directory
    save modified project file to the same directory as the original
    """
    # extract sequence numbers
    sequence = extract_sequence_numbers_from_filenames(image_fns)

    # create dictionary of layers and patches where sequence number is the key
    tree = ET.parse(original_pf_path)
    root = tree.getroot()

    print_tree_hierarchy(root)
    print("Disassemble xml tree")
    dlayer = disassemble_pf_tree(root)
    print_tree_hierarchy(root)
    # compare dict keys and sequence and add missing elements to the project file
    print("Reassemble xml tree")
    reassemble_pf_tree(root, sequence, dlayer)
    print_tree_hierarchy(root)
    print("Modify xml content")
    modify_pf_content(root, image_fns)
    # delete sequence because the order is not correct anymore because modify_pf_content relies on
    # ordering given by xml parse's dictionary
    print_tree_hierarchy(root)

    tree.write(modified_pf_path)

    # insert also some additional metadata from the original project file's doctype tag
    # is seems the project file won't work without it
    insert_trackem2_header(original_pf_path, modified_pf_path)

    return image_fns


class FileNameMatcher:
    """
    Match filenames with other filenames using sequence numbers or substrings.
    Returns the same matched filename only once
    """
    fns = []
    seq_fns = []

    def __init__(self, fnames):
        self.fns = deepcopy(fnames)
        self.seq_fns = extract_sequence_numbers_from_filenames(fnames)

    def get_matching_filename_sequence(self, fn):
        seq = extract_sequence_numbers_from_filenames([fn])[0]
        # find first filename that matches fn's sequence number
        ind = self.seq_fns.index(seq)
        out = self.fns[ind]
        # remove "used" filenames and sequences from the lists
        self.seq_fns.pop(ind)
        self.fns.pop(ind)
        return out

    def get_matching_filename_substring(self, fn):
        try:
            ind = [i for i, fname in enumerate(self.fns) if fname.find(fn) != -1][0]
        except IndexError:
            print("Could not find matching file for {} among {}".format(fn, self.fns))
            sys.exit(8)
        out = self.fns[ind]
        # remove "used" filenames and sequences from the lists
        self.seq_fns.pop(ind)
        self.fns.pop(ind)
        return out


def modify_pf_content(root_node, new_fns):
    """
    Opens project file and replaces the following content in each t2_layer tag:
        title
        file_path
        z
    """

    # check that number of layers equals number of images
    layers = [l for l in root_node.iter("t2_layer")]
    if len(layers) != len(new_fns):
        print("Number of images_does not match number of layers", file=sys.stderr)
        sys.exit(4)

    matcher = FileNameMatcher(new_fns)

    # get first layer oid and increment it for every layer
    layer_oid = int(root_node[1][0].attrib["oid"])
    # get first patch oid and increment it for every patch
    patch_oid = int(root_node[1][0][0].attrib["oid"])
    for layer in root_node.iter("t2_layer"):
        layer.attrib["oid"] = str(layer_oid)
        patch = layer[0]
        # match new file path with the old one using sequence number
        # this matching is needed because ptyhon's xml parser uses dictionary internally
        # and does not preserve element ordering. Therefore we need to match filenames using sequence numbers
        # instead of relying on order of filenames
        fn = matcher.get_matching_filename_sequence(patch.attrib["file_path"])
        print("Matching filename {} with {}".format(patch.attrib["file_path"], fn))
        # now replace the project xml's filepath with the matching filename from the input image directory
        patch.attrib["file_path"] = fn
        patch.attrib["oid"] = str(patch_oid)
        patch.attrib["title"] = os.path.split(fn)[-1]
        patch_oid += 1
        layer_oid += 2


def sort_filenames(filenames, regex="-([0-9]*)_"):
    """
    sort filenames according to the 'sequence number' in the filename
    use regular expression to grab the last number between '-' and '_'
    and convert that to int to sort the list numerically
    """
    try:
        filenames.sort(key=lambda x: int(re.findall(regex, x, flags=0)[-1]))
    except:
        print("File names in source directory do not match regex: '{}'\n"
              "   example filename: {}".format(regex, filenames[0]), file=sys.stderr)
        return None
    return filenames


def transform_and_save(path_to_pf, output_fn_paths, isimagecolor):
    # Output images.
    print("Transform and save - using project file from {}".format(path_to_pf))
    print("Current working directory: {}".format(os.getcwd()))
    project = Project.openFSProject(path_to_pf, False)
    scale = 1.0
    backgroundcolor = Color.black
    layerset = project.getRootLayerSet()
    layerBounds = layerset.get2DBounds()
    layers = layerset.getLayers()
    assert (len(layers) == len(output_fn_paths))
    matcher = FileNameMatcher(output_fn_paths)
    for i, layer in enumerate(layers):
        print("Transforming patch {}/{} - {}".format(i + 1, len(output_fn_paths), layer.displayables))
        patches = layer.getDisplayables(Patch, layerBounds)
        if isimagecolor:
            ip = Patch.makeFlatImage(ImagePlus.COLOR_RGB, layer, layerBounds, scale, patches, backgroundcolor, True)
        else:
            ip = Patch.makeFlatImage(ImagePlus.GRAY8, layer, layerBounds, scale, patches, backgroundcolor, True)
        # get layer's title (filename) and strip type extension
        fn = str(layer.displayables[0]).split(" ")[0]
        split_fn = fn.split(".")
        fn_no_ext = os.sep.join(split_fn[:-1])
        ext = split_fn[-1]
        # match filenames without extension, to allow files to be saved in different format than what was inputted
        match = matcher.get_matching_filename_substring(fn_no_ext)
        match = os.path.abspath(match)
        print("Matching filename {} with {}".format(fn_no_ext, match))
        imp = ImagePlus(match, ip)
        print("Imp to save: {} - size (y, x) {}".format(imp, (imp.height, imp.width)))
        print("Saving patch to: {}".format(match))
        print("Current dir: {}".format(os.getcwd()))
        if ext.lower() == "tif" or ext.lower() == "tiff":
            IJ.run(imp, "Bio-Formats Exporter", "save=[{}] compression={}".format(match, 'LZW'))
        elif ext.lower() == "png":
            IJ.run(imp, "Bio-Formats Exporter", "save=[{}]".format(match))
        elif ext.lower() == "jpg":
            IJ.run(imp, "Bio-Formats Exporter", "save=[{}]".format(match))
        else:
            raise ValueError("Unsupported filename extension, use jpg, png or tiff")

    # Close the project.
    project.remove()


def get_sorted_filenames(args):
    #  Get the input_fn_names of input images.
    input_fn_names = os.listdir(args.source_dir)
    if not input_fn_names:
        print("No files found in the input directory, quitting", sys.stderr)
        sys.exit(5)

    # sort input_fn_names in ascending sequence number order
    if len(input_fn_names) > 1:
        input_fn_names = sort_filenames(input_fn_names)
        if not input_fn_names:
            print("Sorting input_fn_names failed, quitting", sys.stderr)
            sys.exit(6)

    # prepend input directory path to input_fn_names
    input_fn_paths = [os.path.join(args.source_dir, f) for f in input_fn_names]

    # create save_filenames according to save template, remove also existing extension to prevent duplicating it
    output_fn_paths = [os.path.join(args.target_dir, args.save_template.format(fn[:fn.rfind(".")])) for fn in
                       input_fn_names]

    return input_fn_names, input_fn_paths, output_fn_paths


def main():
    args = parse_aruments()

    print("Input arguments:")
    pprint(vars(args))

    check_input_arguments(args)

    setup()

    # input_fn_names: input files, filename only
    # input_fn_paths: input files, full paths
    # output_fn_paths: output files, full paths
    input_fn_names, input_fn_paths, output_fn_paths = get_sorted_filenames(args)

    print("Sorted input files:")
    pprint(input_fn_paths)

    # trakem2 assumes project root directory to be the one where project file is
    # This is why a modified version of it saved to the current directory path 
    modified_pf_path = "Modified_{}".format(os.path.basename(args.project_file_path))

    if args.modify_project_file:
        # duplicate transforms in the project file based on sequence numbers
        modify_project_file(args.project_file_path, modified_pf_path, input_fn_paths)
    else:
        # if modify_project_file is disabled, just copy the original to prevent overwriting it
        copyfile(args.project_file_path, modified_pf_path)

    # Do transform and save images
    transform_and_save(modified_pf_path, output_fn_paths, args.isimagecolor)

    # Remove mipmaps
    print("Removing mipmaps..")
    trans_dir = os.path.dirname(args.project_file_path)
    print("Removing mipmaps from {}/{}".format(trans_dir, "trakem2*"))
    rmdirs = glob.glob(os.path.join(trans_dir, "trakem2*"))
    for f in rmdirs:
        shutil.rmtree(f)
    print("Mipmaps removed")

    print("Reapply complete")
    # exit is needed because python scripts in Fiji do not exit by default for some reason
    sys.exit(0)


if __name__ == '__main__':
    main()
