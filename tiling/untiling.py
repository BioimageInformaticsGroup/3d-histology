# -*- coding: utf-8 -*-
"""

@authors: Peter Ström, Masi Valkonen

"""

import re
import os
import time
import logging
from argparse import ArgumentParser, ArgumentTypeError

import natsort
import numpy as np
from PIL import Image
from skimage import io

Image.MAX_IMAGE_PIXELS = 5000000000

logging.basicConfig(level=logging.INFO)


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise ArgumentTypeError("Boolean value expected.")


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("path_input_dir", help="Path to input tiles", type=str)
    parser.add_argument("path_output_image", help="Path to save output image to", type=str)
    parser.add_argument(
        "--output_res_x", help="Output image x resolution", default=None, type=int
    )
    parser.add_argument(
        "--output_res_y", help="Output image y resolution", default=None, type=int
    )
    parser.add_argument(
        "--background_color",
        help="Image background color. Supports ~140 HTML color names, e.g. white, balck, green, red (case insensitive). Default white.",
        default="white",
        type=str,
    )
    params = vars(parser.parse_args())
    return params


def listdir_fullpath_sorted(d):
    fns = [os.path.join(d, f) for f in os.listdir(d)]
    fns = natsort.natsorted(fns)
    return fns


def read_tile_size(fn):
    im = Image.open(fn)
    width, height = im.size
    return height, width


def main():
    t = time.time()

    args = parse_arguments()

    logging.info("Retiling {}".format(args["path_input_dir"]))

    # get sorted full paths
    im_fns = listdir_fullpath_sorted(args["path_input_dir"])
    
    restr = "([0-9]+)_([0-9]+)_([0-9]+)_([0-9]+)(_|\.|[a-zA-Z])(?![0-9]+)"

    matches = [re.findall(restr, fn) for fn in im_fns]
    matches = np.squeeze(np.array(matches))
    matches = matches[:, 0:4].astype(np.int)
    
    logging.info(f"Found coordinates {matches[0, :]} from filename {im_fns[0]}")
    
    h, w = read_tile_size(im_fns[0])

    # create output image
    if args["output_res_y"] is not None and args["output_res_x"] is not None:
        imh = args["output_res_y"]
        imw = args["output_res_x"]
    else:
        # get image size from tile coordinates
        imh = matches[:, 1].max()
        imw = matches[:, 3].max()

    # create output image of size imw x imh
    out_img = Image.new('RGB', (imw, imh), color=args["background_color"]) 

    # now paste all pieces to the image
    for i, fn in enumerate(im_fns):
        logging.debug(f"Reading and pasting tile {i}/{len(im_fns)}")
        tile = Image.open(fn)
        # (left, upper, right, lower)
        box = (matches[i, 2], matches[i, 0], matches[i, 3] + 1, matches[i, 1] + 1)
        out_img.paste(tile, box)

    out_img.save(args["path_output_image"])

    logging.info(f"Saved assembled image file {args['path_output_image']} to disk")


if __name__ == "__main__":
    main()
