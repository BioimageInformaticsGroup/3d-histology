# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 10:42:56 2017

@authors: Peter Ström, Kimmo Kartasalo, Masi Valkonen
"""

import gc
import os
import time
import tarfile
import logging
import warnings
import io as stdio
from argparse import ArgumentParser, ArgumentTypeError

import natsort
import numpy as np
from joblib import Parallel, delayed
from PIL import Image
from PIL import ImageOps
from skimage import io

USE_OPENSLIDE = False
if USE_OPENSLIDE:
    import openslide

Image.MAX_IMAGE_PIXELS = 5000000000
warnings.simplefilter("ignore", Image.DecompressionBombWarning)

logging.basicConfig(level=logging.DEBUG)


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise ArgumentTypeError("Boolean value expected.")


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("path_input_dir", help="Path to input images", type=str)
    parser.add_argument("path_input_masks_dir", help="Path to input mask images", type=str)
    parser.add_argument("path_output_dir", help="Path to save output tiles to", type=str)
    parser.add_argument("--path_label_masks_dir", help="Path to label mask images", type=str)
    parser.add_argument(
        "--num_cores",
        help="Number of parallel jobs, one image per core. Default 4",
        default=4,
        type=int,
    )
    parser.add_argument(
        "--tissue_cutoff",
        help="Amount of tissue saved tile should contain. Default 0.5",
        default=0.5,
        type=float,
    )
    parser.add_argument(
        "--output_format",
        help="Tile output format e.g. '.jpg' or '.png'. Default '.png'",
        default=".png",
        type=str,
    )
    parser.add_argument(
        "--tile_size", help="Tile side length, square shaped. Default 224", default=224, type=int
    )
    parser.add_argument(
        "--stride",
        help="Number of pixels between adjacent tiles. Default 224",
        default=224,
        type=int,
    )
    parser.add_argument(
        "--mask_scaling",
        help="Integer downsampling factor for masks. \
                Used only during computations, \
                does not affect output image size. Default 16",
        default=16,
        type=int,
    )
    parser.add_argument(
        "--reduction_level",
        help="Integer downsampling factor for images. \
                Reductionlevel 4 equals 1/(2^4) = 1/16 downscaling. \
                Default 0",
        default=0,
        type=int,
    )
    parser.add_argument(
        "--label_threshold",
        help="Within a window in label_image, if the ratio "
        "(non-background pixels)/(all pixels) > threshold then, "
        "return label that is most frequent among non-background pixels. "
        "Otherwise return background label.",
        default=0.5,
        type=float,
    )
    parser.add_argument(
        "--background_label", help="Background label in label image", default=0, type=int
    )
    parser.add_argument(
        "--path_output_tile_info",
        help="If path provided, save list of dictionaries containing information "
        "about tile coordinates and labels. "
        "A .npy extension will be appended if not provided",
        type=str,
    )
    # to disable, call --write_tiles False
    parser.add_argument(
        "--write_tiles",
        type=str2bool,
        nargs="?",
        const=True,
        default=True,
        help="Write tiles to disk",
    )
    params = parser.parse_args()
    return params


def check_arguments(params):
    # Create output folder for tiles if necessary.
    if params.write_tiles:
        os.makedirs(params.path_output_dir, exist_ok=True)
    return params


def read_tissue_mask(path_tis, maskscaling):
    ext = os.path.splitext(path_tis)[1].lower()

    # Read the TissueMask at reduced resolution and convert to Boolean.
    if ext == ".tiff" or ext == ".tif":
        tis = io.imread(path_tis, plugin="tifffile", as_gray=True)
    else:
        tis = io.imread(path_tis, plugin="pil", as_gray=True)

    full_res_h, full_res_w = tis.shape[0:2]

    tis = tis[0::maskscaling, 0::maskscaling]

    return tis, full_res_h, full_res_w


def read_label_mask(path_lab, maskscaling):
    ext = os.path.splitext(path_lab)[1].lower()

    # Read the TissueMask at reduced resolution and convert to Boolean.
    if ext == ".tiff" or ext == ".tif":
        lab = io.imread(path_lab, plugin="tifffile", as_gray=True)
    else:
        lab = io.imread(path_lab, plugin="pil", as_gray=True)

    full_res_h, full_res_w = lab.shape[0:2]

    lab = lab[0::maskscaling, 0::maskscaling]

    return lab, full_res_h, full_res_w


def read_tile_openslide(
    img, h_start, w_start, resolutionleveltouse, tile_size_touse, path_img, tile_size
):
    # Read the tissue tile at the resolution level to use.
    # Note that starting indices are given to OpenSlide in full-res
    # coordinates, but tile size needs to be given in the current
    # level's coordinates, since it's the number of pixels to read.
    try:
        tile = img.read_region(
            (w_start, h_start), resolutionleveltouse, (tile_size_touse, tile_size_touse)
        )
    # If anything goes wrong (e.g. corrupt tiles), attempt reading
    # at full-res.
    except openslide.OpenSlideError:
        img.close()
        img = openslide.OpenSlide(path_img)
        tile = img.read_region((w_start, h_start), 0, (tile_size, tile_size))

    return tile


def read_tile(img, h_start, w_start, tile_size):
    # use this function to read tiles if openslide is not used
    tile = img[h_start : h_start + tile_size, w_start : w_start + tile_size]
    return Image.fromarray(tile)


def get_win_label(win, threshold=0.5, background_label=0):
    """ Determine label for mask window

        If the ratio (non-background pixels)/(all pixels) > threshold
        then, return label that is most frequent among non-background pixels.
        Otherwise return background label.

        Return:
            label: label for the input window
            r: ratio for amount of nonbackground in the window
    """
    # compute unique labels and number of occurrences wintin window
    unique_labels, unique_label_counts = np.unique(win[:], return_counts=True)
    bg_bool = unique_labels == background_label
    # get rid of background label (if present)..
    nobg_uvals = unique_labels[~bg_bool]
    # ..and counts
    nobg_counts = unique_label_counts[~bg_bool]
    # compute ratio of nonbackground pixels to all pixels
    r = np.sum(nobg_counts) / np.sum(unique_label_counts)
    if r > threshold:
        # return label that is most frequent among nonbackground
        label = nobg_uvals[np.argmax(nobg_counts)]
    else:
        # If ratio threshold is not exceeded, return background label
        label = background_label
    return label, r, unique_labels, unique_label_counts


def tile(
    path_img,
    path_tis,
    outputpath,
    slide,
    path_lab=None,
    tile_size=598,
    stride=299,
    resolutionlevel=0,
    pixelscaling=1,
    maskscaling=16,
    tissuecutoff=0.5,
    writetiles=True,
    tartiles=False,
    readfullres=True,
    ending=".png",
    label_threshold=0.5,
    background_label=0,
):
    """Tile a WSI image in serial with a single CPU core.

    Args:
        path_img: full path to input image
        path_tis: full path to input mask image
        outputpath: path to output directory
        slide: slide/image ID. This is used in output filenames
        path_tis: full path to label mask image
        tile_size: integer width/height of tiles in full-resolution pixels.
        stride: integer number of full-resolution pixels to stride.
        reductionlevel: integer downsampling level to use (e.g. 1 = 2^1 = 2).
        pixelscaling: rescaling factor for pixel size. If < 1, tiles are read
                      at reduced dimensions and upscaled during writing to the
                      required dimensions (-> pixel size reduced). If > 1,
                      tiles are read at increased dimensions and downscaled
                      during writing to the required dimensions (-> pixel size
                      increased). E.g. 0.5 -> 50 % pixel size -> higher res.
        pixelscaling: rescaling for pixel size (e.g. 0.5 doubles resolution)
        maskscaling: integer downsampling factor for masks (e.g. 16).
        tissuecutoff: percentage of tissue on tile required (e.g. 0.5).
        writetiles: boolean, write output tiles to disk?
        tartiles: boolean, store tiles in a single tar archive?
        readfullres: boolean, always read images at full-res and downsample?
        ending: tile saving format.

    Return: List, (write tiles to subfolder).

    Kimmo Kartasalo & Peter Ström, 2017. Masi Valkonen, 2019.
    """

    # This setting is lost somewhere between in joblib
    # setting it here works
    Image.MAX_IMAGE_PIXELS = 5000000000

    # Create dict of input arguments for logging purposes
    input_args = {
        "path_img": path_img,
        "path_tis": path_tis,
        "outputpath": outputpath,
        "slide": slide,
        "tile_size": tile_size,
        "stride": stride,
        "resolutionlevel": resolutionlevel,
        "pixelscaling": pixelscaling,
        "maskscaling": maskscaling,
        "tissuecutoff": tissuecutoff,
        "writetiles": writetiles,
        "tartiles": tartiles,
        "readfullres": readfullres,
        "ending": ending,
    }
    logging.info("Calling tile() with arguments:")
    for k in input_args.keys():
        logging.info("   {}: {}".format(k, input_args[k]))

    data = []

    # Create output folder for tiles if necessary.
    if writetiles and not os.path.exists(outputpath):
        os.makedirs(outputpath)
    # If output to tar files enabled, create tar file.
    if writetiles and tartiles:
        tar = tarfile.open(os.path.join(outputpath, slide + ".tar"), "w")

    # Check if TissueMask exist for the image.
    assert os.path.isfile(path_tis), "Missing file: " + path_tis + "!"

    scalingfactor = 2 ** resolutionlevel

    if USE_OPENSLIDE:
        # Initialize the OpenSlide image pointer and get full-res dimensions.
        img = openslide.OpenSlide(path_img)
        img_w, img_h = img.dimensions

        # Figure out the desired scaling factor for images and the resolution
        # level to use. If required to read at full-res, use level 0. Otherwise,
        # find the closest level with resolution better or equal to desired.
        if readfullres:
            scalingfactortouse = 1
            resolutionleveltouse = 0
        else:
            downsamplefactors = img.level_downsamples
            scalingfactortouse = max(x for x in downsamplefactors if x <= scalingfactor)
            resolutionleveltouse = downsamplefactors.index(scalingfactortouse)

    else:
        # NOTE: skimage.io
        # plugin=pil cannot read properly weird? specific tiff images (hyperstack?) produced by ESA
        # imageio can
        img = io.imread(path_img, plugin="imageio", as_gray=False)
        img_h, img_w = img.shape[0:2]
        scalingfactortouse = scalingfactor
        resolutionleveltouse = resolutionlevel

    # Figure out the final tile size at the desired resolution level.
    tile_size_final = np.floor(tile_size / scalingfactor).astype("int")
    # Figure out the tile size at the resolution level to use.
    tile_size_touse = np.floor(tile_size / scalingfactortouse).astype("int")

    # Read tissue mask at reduced resolution
    tis, tis_full_res_h, tis_full_res_w = read_tissue_mask(path_tis, maskscaling)
    assert (tis_full_res_h, tis_full_res_w) == (
        img_h,
        img_w,
    ), "Tissue mask dimensions {} and {} do not match! {} != {}".format(
        path_tis, path_img, (tis_full_res_h, tis_full_res_w), (img_h, img_w)
    )

    # load label mask to memory if path given
    if path_lab is not None:
        # Read label mask at reduced resolution if provided
        lab, lab_full_res_h, lab_full_res_w = read_label_mask(path_lab, maskscaling)
        assert (lab_full_res_h, lab_full_res_w) == (
            img_h,
            img_w,
        ), "Label mask dimensions {} and {} do not match! {} != {}".format(
            path_lab, path_img, (lab_full_res_h, lab_full_res_w), (img_h, img_w)
        )
    else:
        # otherwise use tissue mask as label mask
        lab, lab_full_res_h, lab_full_res_w = tis, tis_full_res_h, tis_full_res_w

    # Scale tile sizes and stride if pixel size rescaling is used.
    # For example, if pixel size needs to be scaled down (higher res), tiling
    # has to be done with smaller tiles (in pixels) to compensate.
    if pixelscaling != 1:
        tile_size = np.floor(pixelscaling * tile_size).astype("int")
        tile_size_touse = np.floor(pixelscaling * tile_size_touse).astype("int")
        stride = np.floor(pixelscaling * stride).astype("int")

    # Figure out the number of tile rows and columns.
    numtilerows = np.floor((img_h - tile_size + stride) / stride).astype("int")
    numtilecols = np.floor((img_w - tile_size + stride) / stride).astype("int")

    # Loop over the image.
    for rowind in range(0, numtilerows):
        for colind in range(0, numtilecols):

            # Top-left coordinates of tile at full-res.
            h_start = rowind * stride
            w_start = colind * stride
            # Bottom-right coordinates of tile at full-res.
            h_end = h_start + tile_size - 1
            w_end = w_start + tile_size - 1
            # Top-left and bottom-right of this block in low-res coordinates.
            r = np.array([h_start, w_start, h_end, w_end])
            r = np.floor(r / maskscaling).astype("int")

            # Check if region contains enough tissue. Otherwise skip to next tile
            # perform boolean converion just in time here to be able to use tissue mask
            # as label mask if label mask is not provided
            tis_tile = tis[r[0] : r[2] + 1, r[1] : r[3] + 1] > 0
            if tis_tile.sum() / tis_tile.size < tissuecutoff:
                continue

            # Determine tile label from label image
            if path_lab is not None:
                lab_tile = lab[r[0] : r[2] + 1, r[1] : r[3] + 1]
            else:
                lab_tile = tis_tile
            tile_label, ratio, win_unique_labels, win_label_counts = get_win_label(
                lab_tile, threshold=label_threshold, background_label=background_label
            )

            # Store tile information into a list of dictionaries.
            tile_name = (
                slide
                + "_"
                + str(h_start)
                + "_"
                + str(h_end)
                + "_"
                + str(w_start)
                + "_"
                + str(w_end)
                + ending
            )
            keys = [
                "slide",
                "tile_name",
                "top",
                "bottom",
                "left",
                "right",
                "stride",
                "maskscaling",
                "label",
                "tile_unique_labels",
                "tile_label_counts",
            ]
            values = [
                slide,
                tile_name,
                np.uint32(h_start),
                np.uint32(h_end),
                np.uint32(w_start),
                np.uint32(w_end),
                np.uint32(stride),
                maskscaling,
                np.uint32(tile_label),
                win_unique_labels,
                win_label_counts,
            ]
            data.append(dict(zip(keys, values)))

            # Write tiles to disk.
            if writetiles:

                if USE_OPENSLIDE:
                    tile = read_tile_openslide(
                        img,
                        h_start,
                        w_start,
                        resolutionleveltouse,
                        tile_size_touse,
                        path_img,
                        tile_size,
                    )
                else:
                    tile = read_tile(img, h_start, w_start, tile_size_touse)

                if USE_OPENSLIDE:
                    # Set pixels with missing data, indicated in the alpha channel,
                    # to white (255,255,255).
                    alphamask = tile.getchannel(3)
                    alphamask = ImageOps.invert(alphamask)
                    tile.paste((255, 255, 255, 255), mask=alphamask)

                tile = tile.convert("RGB")

                # Scale tile to final size.
                if tile.width != tile_size_final or tile.height != tile_size_final:
                    tile = tile.resize((tile_size_final, tile_size_final), Image.LANCZOS)

                # Write tile directly to tar archive.
                if tartiles:
                    # Create byte stream.
                    f = stdio.BytesIO()
                    # Write tile to byte stream.
                    if ending == ".png":
                        tile.save(f, format="png", compress_level=1)
                    elif ending == ".jpg":
                        tile.save(f, format="jpeg", quality=80)
                    else:
                        tile.save(f, format=ending[1:])
                    tile.close()
                    # Create TarInfo with tile name.
                    tarinfo = tarfile.TarInfo(name=tile_name)
                    # Set number of bytes to write (-1 for some reason).
                    tarinfo.size = f.getbuffer().nbytes - 1
                    # Write bytestream from the beginning to tar file.
                    f.seek(0)
                    tar.addfile(tarinfo, fileobj=f)
                    f.close()
                # Or write tile to folder.
                else:
                    if ending == ".png":
                        tile.save(os.path.join(outputpath, tile_name), compress_level=1)
                    elif ending == ".jpg":
                        tile.save(os.path.join(outputpath, tile_name), quality=80)
                    else:
                        tile.save(os.path.join(outputpath, tile_name))
                    tile.close()

    # Close tar file.
    if writetiles and tartiles:
        tar.close()

    # Make sure the OpenSlide image is closed.
    if USE_OPENSLIDE:
        img.close()

    return data


def tiling_wrap(
    image_fns,
    mask_fns,
    image_ids,
    output_paths,
    tile_size,
    stride,
    label_mask_fns=None,
    output_format=".png",
    reduction_level=0,
    mask_scaling=16,
    tissue_cutoff=0.5,
    write_tiles=True,
    num_cores=1,
):
    """Tile a WSI images by calling tile() function in parallel

    Args:
        image_fns (List[str]): full paths to images
        mask_fns (List[str]): full paths to mask images
        image_ids (List[str]): identifiers for each tile. These are used in tile filenames
        outputpaths (List[str]): List of output paths for each image
        tile_size (int): width/height of tiles in full-resolution pixels.
        stride (int): number of full-resolution pixels to stride.
        label_mask_fns (List[str]): full paths to label mask images
        reduction_level: integer downsampling level to use (e.g. 1 = 2^1 = 2).
        pixelscaling: rescaling factor for pixel size. If < 1, tiles are read
                      at reduced dimensions and upscaled during writing to the
                      required dimensions (-> pixel size reduced). If > 1,
                      tiles are read at increased dimensions and downscaled
                      during writing to the required dimensions (-> pixel size
                      increased). E.g. 0.5 -> 50 % pixel size -> higher res.
        pixelscaling: rescaling for pixel size (e.g. 0.5 doubles resolution)
        mask_scaling: integer downsampling factor for masks (e.g. 16).
        tissuecutoff: percentage of tissue on tile required (e.g. 0.5).
        writetiles: boolean, write output tiles to disk?
        tartiles: boolean, store tiles in a single tar archive?
        readfullres: boolean, always read images at full-res and downsample?
        ending: tile saving format.

    Return: List, (write tiles to subfolder).

    Kimmo Kartasalo & Peter Ström, 2017. Masi Valkonen, 2019.
    """

    assert len(image_fns) == len(mask_fns) == len(image_ids) == len(output_paths)

    # Process in parallel over WSIs, each WSI in serial.
    data = Parallel(n_jobs=num_cores)(
        delayed(tile)(
            path_img=image_fns[i],
            path_tis=mask_fns[i],
            outputpath=output_paths[i],
            slide=image_ids[i],
            tile_size=tile_size,
            stride=stride,
            resolutionlevel=reduction_level,
            pixelscaling=1,
            maskscaling=mask_scaling,
            tissuecutoff=tissue_cutoff,
            writetiles=write_tiles,
            tartiles=False,
            readfullres=False,
            ending=output_format,
        )
        for i in range(len(image_fns))
    )
    gc.collect()
    return data


def listdir_fullpath_sorted(d):
    fns = [os.path.join(d, f) for f in os.listdir(d)]
    fns = natsort.natsorted(fns)
    return fns


def main():
    t = time.time()

    params = parse_arguments()
    params = check_arguments(params)

    logging.info("Tiling {}".format(params.path_input_dir))

    # get sorted full paths
    im_fns = listdir_fullpath_sorted(params.path_input_dir)
    mask_fns = listdir_fullpath_sorted(params.path_input_masks_dir)
    if params.path_label_masks_dir is not None:
        label_mask_fns = listdir_fullpath_sorted(params.path_label_masks_dir)
    else:
        label_mask_fns = None

    # create image ids that are used e.g. in saved tile filename & output folder
    im_ids = [os.path.splitext(os.path.basename(p))[0] for p in im_fns]

    # create output paths for tiles. Store tiles from each image to separate folders
    outputs = [os.path.join(params.path_output_dir, im_id) for im_id in im_ids]

    # perfrom tiling
    data = tiling_wrap(
        im_fns,
        mask_fns,
        im_ids,
        outputs,
        tile_size=params.tile_size,
        stride=params.stride,
        label_mask_fns=label_mask_fns,
        output_format=params.output_format,
        reduction_level=params.reduction_level,
        mask_scaling=params.mask_scaling,
        tissue_cutoff=params.tissue_cutoff,
        write_tiles=params.write_tiles,
        num_cores=params.num_cores,
    )

    if params.path_output_tile_info is not None:
        savep = os.path.splitext(params.path_output_tile_info)[0] + ".npy"
        np.save(savep, data)

    elapsed = (time.time() - t) // 60
    logging.info("Tiling took " + str(elapsed) + " min")


if __name__ == "__main__":
    main()
