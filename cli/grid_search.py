import logging
import os
import sys
import time
from typing import List

from biit_slurm import slurm
from cli.align import align
from cli.settings import CONSOLE_LOG_LEVEL
from cli.histo_argument_parser import get_argument_parser
from cli import settings

logger = logging.getLogger(__name__)
logger.setLevel(CONSOLE_LOG_LEVEL)

def parse_gsearch_file(path: str) -> List[dict]:
    """read inputfile line by line and convert them to argument dicts"""

    list_of_args = []
    parser = get_argument_parser()
    with open(path, "r") as f:
        lines = f.readlines()
        for i, line in enumerate(lines, start=1):

            # remove newlines and preceding and trailing whitespace
            line = line.replace("\n", "").lstrip().rstrip()
            line_argument_list = line.split(" ")

            # log lines in the grid search file
            logger.debug("Gsearch file line {}:".format(i))
            for l in line_argument_list:
                logger.debug("    '{}'".format(l))

            list_of_args.append(parser.parse_args(line_argument_list))
    # convert all namespaces to dicts
    list_of_args = [vars(a) for a in list_of_args]
    return list_of_args


def check_gsearch_arguments(args: dict):
    if not os.path.exists(args["gsearch_file"]):
        raise ValueError("gsearch_file does not exist")

    if not os.path.exists(args["log_dir"]):
        os.makedirs(args["log_dir"])


def grid_search(args: dict):
    logger.info("Gsearch subcommand")

    check_gsearch_arguments(args)

    # parse input file into list of dicts, where each dict contains arguments for one align job
    list_of_args = parse_gsearch_file(args["gsearch_file"])

    # iterate over each set of arguments (dict) and send them for execution
    line_job_ids = []
    username = slurm.get_username()
    for i, line_args in enumerate(list_of_args, start=1):
        if line_args["command"] == "align":

            # if '--partition' argument was passed to gsearch command
            # use that partition for every line in grid search file
            if args["partition"]:
                line_args["partition"] = args["partition"]

            if args["time"]:
                line_args["time"] = args["time"]

            # put all grid search log files into logfile directory
            line_args["log_file"] = os.path.join(args["log_dir"],
                                                 "{}_{}".format(i - 1, os.path.basename(line_args["log_file"])))

            # send job to queue and store its job id
            slurm.wait_until_N_or_less_jobs_in_queue(settings.GSEARCH_CONCURRENT_JOBS_LIMIT)
            time.sleep(1)
            line_job_ids.append(align(line_args))

        else:
            logger.error("Only 'align' command is supported at the moment, "
                         "you gave '{}'".format(line_args.get("command")))
            sys.exit(3)

    logger.info("Sent {} align jobs to queue".format(len(line_job_ids)))
    slurm.wait_for_ids(line_job_ids)
