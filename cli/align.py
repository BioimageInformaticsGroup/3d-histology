import copy
import logging
import os

from biit_slurm import slurm
from cli.settings import CONSOLE_LOG_LEVEL, REMOVE_TRANSFORMS_CACHE, FIJI_PATH, ALIGN_PATH, FIJICONVERT_PATH, \
    JAVA_HEAP_SCALING
from utils.esa_params import EsaParams

logger = logging.getLogger(__name__)
logger.setLevel(CONSOLE_LOG_LEVEL)


def check_align_arguments(args):
    if args["source_dir"] is None:
        raise ValueError("source_dir is none")

    if args["target_dir"] is None:
        raise ValueError("target_dir is none")

    if args["transforms_dir"] is None:
        raise ValueError("transforms_dir is none")

    # make paths absolute
    for key in ["source_dir", "target_dir", "transforms_dir", "log_file", "params_file"]:
        if not os.path.isabs(args[key]):
            logger.debug("Convert {} to absolute path: {} -> {}".format(key, args[key], os.path.abspath(args[key])))
            args[key] = os.path.abspath(args[key])

    # ensure file path arguments' existence and type (not dir)
    for k in ["log_file", "params_file"]:
        if os.path.exists(args[k]):
            if os.path.isdir(args[k]):
                raise ValueError("{} path is a directory, select different path".format(k))
            else:
                logger.warning("overwriting existing {} path: {}".format(k, args[k]))
        else:
            d = os.sep.join(os.path.split(args[k])[:-1])
            if not os.path.exists(d):
                logger.debug("Creating {} dir: {}".format(k, d))
                os.makedirs(d)
            else:
                logger.debug("directory to {} exists: {}".format(k, d))

    # ensure directories exist
    for key in ["source_dir", "target_dir", "transforms_dir"]:
        if not os.path.exists(args[key]):
            logger.debug("Directory {} does not exist, creating it".format(args[key]))
            os.makedirs(args[key])
        else:
            logger.debug("Directory {} exists".format(args[key]))
            # TODO: check if empty and warn

    # handle subsampling arguments separately
    if args["subsample"] != 100:
        if args["subsample_dir"] is None:
            raise ValueError("subsample_dir is none")
        else:
            if not os.path.isabs(args["subsample_dir"]):
                args["subsample_dir"] = os.path.abspath(args["subsample_dir"])

            if not os.path.exists(args["subsample_dir"]):
                os.makedirs(args["subsample_dir"])


def align(args):
    logger.debug("Align subcommand")
    check_align_arguments(args)


    # Use explicity given ESA params, otherwise use the default
    # ESA params have default of None that is used to detect explicitly given args
    esa_params = EsaParams()
    # write used parameter in an args obejct to write the parameter file below
    to_params_args = copy.deepcopy(args)
    for k in esa_params.params.keys():
        if args[k] is not None:
            logger.debug("User gave: {} = {}".format(k, args[k]))
            esa_params.params[k] = args[k]
        else:
            logger.debug("Using default for: {} = {}".format(k, args[k]))

        to_params_args[k] = esa_params.params[k]

    id = align_sbatch(args["source_dir"],
                      args["target_dir"],
                      args["transforms_dir"],
                      log_file=args["log_file"],
                      params=esa_params,
                      partition=args["partition"],
                      max_time=args["time"],
                      subsample_amount=args["subsample"],
                      subsample_dir=args["subsample_dir"],
                      remove_full_res=args["remove_full_res"])

    write_parameter_file(args["params_file"], to_params_args)

    return id


# TODO: Use Slurm options class..
def align_sbatch(source_dir: str,
                 target_dir: str,
                 transforms_dir: str,
                 params: EsaParams,
                 job_name: str = "H_align",
                 log_file: str = "align.log",
                 ntasks: int = 1,
                 cpus_per_task: int = 6,
                 max_time: str = "6-23:59:59",
                 mem: int = 80000,
                 partition: str = "normal",
                 subsample_amount: int = 100,
                 subsample_dir: str = None,
                 remove_full_res: bool = False):
    """
    Run Elastic Stack Alignment as Slurm job with the given parameters

    subsampling is also sent to be performed in the same job to be able to leave jobs
    running on the cluster without the need to keep the terminal session open during the execution

    """

    script = slurm.get_sbatch_script_header(slurm_job_name=job_name,
                                            slurm_log_file=log_file,
                                            slurm_ntasks=ntasks,
                                            slurm_cpus_per_task=cpus_per_task,
                                            slurm_time=max_time,
                                            slurm_mem=mem,
                                            slurm_partition=partition)

    # -Xms10g: use an initial heap size of 10 Gb(i.e.start fiji with 10 Gb of RAM preallocated to it)
    # -Xmx10g: use a maximum heap size of 10 Gb. Note it's the same amount -> heap cannot be resized.
    script.append("{} -Xms{}g -Xmx{}g --headless -- {} {} {} {} {}".format(FIJI_PATH,
                                                                           round(JAVA_HEAP_SCALING*mem/1000),
                                                                           round(JAVA_HEAP_SCALING*mem/1000),
                                                                           ALIGN_PATH,
                                                                           source_dir,
                                                                           target_dir,
                                                                           transforms_dir,
                                                                           params.get_cli_arguments()))

    # TODO: currently all the files in the target directory are being resized and removed, this could be dangerous
    # TODO: Only tiff supported for now, give warning is other formats are used

    if subsample_amount != 100:
        script.append("echo 'Start file subsampling'")
        # iterate over each file after performing ESA
        script.append('find {} -maxdepth 1 -regextype sed -regex ".*\.tiff\?" -printf "%f\n" | while read line; do'.format(
            target_dir))
        script.append(
            "    {} --headless -- {} {}/$line {}/$line --resize {} --use-averaging".format(FIJI_PATH, FIJICONVERT_PATH,
                                                                                           target_dir, subsample_dir,
                                                                                           subsample_amount))
        script.append("done")

    if remove_full_res:
        script.append("echo 'Start removing full res images'")
        script.append('find {} -maxdepth 1 -regextype sed -regex ".*\.tiff\?" -printf "%p\n" | xargs rm'.format(target_dir))

    if REMOVE_TRANSFORMS_CACHE:
        script.append("echo 'removing transforms cache with: rm -rf {}/trakem2*'".format(transforms_dir))
        script.append("rm -rf {}/trakem2*".format(transforms_dir))

    job_id = slurm.send_sbatch_script(script)
    return job_id


def write_parameter_file(path: str, args: dict):
    logger.debug("Writing parameter file to ")

    a = copy.deepcopy(args)

    with open(path, "w") as f:

        for path_name in ["source_dir", "target_dir", "transforms_dir"]:
            f.write("{}\n".format(a[path_name]))
            del a[path_name]

        # write subsampling arguments after other paths
        for k in ["log_file", "params_file", "subsample_dir", "subsample"]:
            f.write("--{} {}\n".format(k.replace("_", "-"), a[k]))
            del a[k]

        # iterate over ESA parameter in order and write this in file
        p = EsaParams()
        for k in p.params.keys():
            f.write("--{} {}\n".format(k, a[k]))
            del a[k]

        # iterate over the rest of the parameter in order
        keys = sorted(list(a.keys()))
        for k in keys:
            f.write("--{} {}\n".format(k.replace("_", "-"), a[k]))
            del a[k]
