import datetime
import logging
import os

import align
import conversion
import preprocess_masks
import reapply_transform
import postprocess_masks

CONSOLE_LOG_LEVEL = logging.INFO
FILE_LOG_LEVEL = logging.DEBUG

TIME_FORMAT = "%Y-%m-%d__%H_%M_%S"
TIME_STR = datetime.datetime.now().strftime(TIME_FORMAT)

# Should transforms cache directory be removed after registering is done?
REMOVE_TRANSFORMS_CACHE = True

# PATHS
FIJI_PATH = "/bmt-data/bii/tools/Fiji.app/ImageJ-linux64"

# get paths automatically of imported modules by inspecting its object
FIJICONVERT_PATH = os.path.join(conversion.__path__._path[0], "fijiconvert.py")
ALIGN_PATH = os.path.join(str(align.__path__._path[0]), "align.py")
REAPPLY_PATH = os.path.join(str(reapply_transform.__path__._path[0]), "reapply_transform.py")
PREPROCESS_PATH = os.path.join(str(preprocess_masks.__path__._path[0]), "preprocess_masks.py")
POSTPROCESS_PATH = os.path.join(str(postprocess_masks.__path__._path[0]), "postprocess_masks.py")

ACTIVATE_PATH = "/home/opt/anaconda3/bin/activate"
ENV_PATH = "/bmt-data/bii/conda_envs/3dee"

# The proportion of total available memory to allocate to java heap
# JAVA_HEAP_SCALING = 0.625
JAVA_HEAP_SCALING = 0.95

GSEARCH_CONCURRENT_JOBS_LIMIT = 50
