import logging
import os

from cli.settings import CONSOLE_LOG_LEVEL, TIME_STR

logger = logging.getLogger(__name__)
logger.setLevel(CONSOLE_LOG_LEVEL)


def check_register_arguments(args):
    # check if target_dir is given. If not, use current dir
    if args["target_dir"] is None:
        args["target_dir"] = os.path.join(os.getcwd(),
                                          "{}_{}_{}".format("registered", args["source_dir"], TIME_STR))
        logger.debug("No target_dir given, output images in {}"
                     "".format(args["target_dir"]))

    # check if transforms_dir is given. If not, use output dir
    if args["transforms_dir"] is None:
        args["transforms_dir"] = os.path.join(args["target_dir"], "transforms")
        logger.debug("No transforms_dir given, output transforms in {}"
                     "".format(args["transforms_dir"]))


def register(args):
    logger.debug("Register subcommand")

    check_register_arguments(args)

    full_res_dir = os.path.join(args["target_dir"], "full_res")
    transforms_dir = os.path.join(args["target_dir"], "transforms")
    if not os.path.exists(full_res_dir):
        os.makedirs(full_res_dir)
    if not os.path.exists(transforms_dir):
        os.makedirs(transforms_dir)

    # check if files are in tiff format

    # if not, convert all to tiff

    # register images

    # if masks were given, reapply transformation for them

    # TODO: Remove transforms_dir flag
    # TODO: Remove full_res_dir flag
    # TODO: Subsample
