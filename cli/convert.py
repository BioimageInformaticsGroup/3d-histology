import logging
import os
import sys

from biit_slurm import slurm
from cli.settings import CONSOLE_LOG_LEVEL, FIJI_PATH, FIJICONVERT_PATH
from utils.interpolation import Interpolation

logger = logging.getLogger(__name__)
logger.setLevel(CONSOLE_LOG_LEVEL)


def check_convert_arguments(args):
    if not os.path.exists(args["output"]):
        logger.debug("output path does not exist, creating {}".format(args["output"]))
        os.makedirs(args["output"])

    if not os.path.isdir(args["output"]):
        logger.error("Output path is not a directory, quitting")
        sys.exit(2)

    # TODO: if output is a directory, an argument specifying the new filetype is needed
    if os.path.isdir(args["output"]):
        pass


def convert(args):
    logger.debug("Convert subcommand")

    check_convert_arguments(args)

    if os.path.isdir(args["output"]):
        logger.debug("output folder {} is a directory".format(args["output"]))
        filenames = os.listdir(args["input"])

        # prepend path to filenames
        input_paths = [os.path.join(args["input"], p) for p in filenames]
        # construct output paths
        new_ext = ".tif"  # TODO: command line argument for this
        output_paths = [os.path.join(args["output"], ".".join(p.split(".")[:-1]) + new_ext) for p in filenames]

        ids = []
        for ip, op in zip(input_paths, output_paths):
            ids.append(convert_sbatch(ip,
                                      op,
                                      resize_percent=args["resize"],
                                      use_averaging=args["averaging"],
                                      interpolation=args["interpolation"]))
        # wait for jobs to finish
        slurm.wait_for_ids(ids)
    else:
        id = convert_sbatch(args["input"],
                            args["output"],
                            resize_percent=args["resize"],
                            use_averaging=args["averaging"],
                            interpolation=args["interpolation"])
        slurm.wait_for_ids([id])


def convert_sbatch(input: str,
                   output: str,
                   resize_percent: int = None,
                   use_averaging: bool = False,
                   interpolation: Interpolation = Interpolation.BICUBIC,
                   job_name: str = "H_convert",
                   log_file: str = "convert.log",
                   ntasks: int = 1,
                   max_time: str = "01:01:00",
                   mem_per_cpu: int = 20000,
                   partition: str = "test"):
    script = slurm.get_sbatch_script_header(slurm_job_name=job_name,
                                            slurm_log_file=log_file,
                                            slurm_ntasks=ntasks,
                                            slurm_time=max_time,
                                            slurm_mem_per_cpu=mem_per_cpu,
                                            slurm_partition=partition)

    arguments = ""
    if interpolation == Interpolation.BICUBIC:
        arguments += " --bicubic"
    elif interpolation == Interpolation.BILINEAR:
        arguments += " --bilinear"
    elif interpolation == Interpolation.NEAREST_NEIGHBOR:
        arguments += " --nearest-neighbor"
    if resize_percent:
        arguments += " --resize {}".format(resize_percent)
    if use_averaging:
        arguments += " --use-averaging"

    script.append("{} --headless {} {} {} {}".format(FIJI_PATH, FIJICONVERT_PATH, input, output, arguments))
    job_id = slurm.send_sbatch_script(script)
    return job_id
