from argparse import ArgumentParser

from utils.interpolation import Interpolation
from utils.utils import str2bool


def get_argument_parser():
    # create the top-level parser, options defined here can be given to any subcommand
    parser = ArgumentParser(description="Histo")
    parser.add_argument("--debug", default=False, help="Enable debug logging", action="store_true")
    # parser.add_argument("--run_id", metavar="run-id", help="Provide unique id or description for execution", type=str)
    # dest="command" stores the used command into the args dict with 'command' key
    subparsers = parser.add_subparsers(help="Commands", dest="command")

    # REGISTER ARGUMENTS
    register = subparsers.add_parser("register",
                                     help="Register images with the whole pipeline",
                                     prog="histo.py register")
    register.add_argument('inputs', help="Input image")
    register.add_argument('--outputs', help="Output image")

    # CONVERT ARGUMENTS
    convert = subparsers.add_parser("convert", help="Convert images to various formats", prog="histo.py convert")
    convert.add_argument('input', help="Input image")
    convert.add_argument('output', help="Output image")
    convert.add_argument('--resize', help="Resize image", default=0, type=float)
    convert.add_argument('--use-averaging', dest="averaging",
                         help="Resize image with averaging", default=False, action="store_true")

    # define group 'interpolation' under convert command that allows only one of defined arguments to be used at a time
    interpolation = convert.add_mutually_exclusive_group(required=False)
    default_interpolation = Interpolation.BICUBIC
    interpolation.add_argument(
        '--bilinear',
        dest="interpolation",
        help="Select bilinear interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.BILINEAR)
    interpolation.add_argument(
        '--bicubic',
        dest="interpolation",
        help="Select bicubic interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.BICUBIC)
    interpolation.add_argument(
        '--nearest-neighbor',
        dest="interpolation",
        help="Select nearest-neighbor interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.NEAREST_NEIGHBOR)

    # ALIGN ARGUMENTS
    # TODO: Populate help arguments with Kimmo's parameter descriptions
    align = subparsers.add_parser("align", help="Elastic stack alignment algorithm", prog="histo.py align")

    align.add_argument("--partition", default="normal", help="Set slurm partition")
    align.add_argument("--time", default="6-23:59:59", help="Set maximum execution time, default is the largest possible 6-23:59:59")
    align.add_argument("--log-file", default="align.log", help="Path to log file, default is the current directory")
    align.add_argument("--params-file", default="params.txt",
                       help="Path to params file, default is the current directory")
    align.add_argument("--subsample", default=100,
                       help="Select subsample amount in percents, default 100 (no subsampling)", type=int)
    align.add_argument("--subsample-dir", default=None,
                       help="Select directory for subsampled files, default next to target dir", type=str)
    align.add_argument("--remove-full-res", metavar="bool", type=str2bool, nargs="?", default=False, help="")

    # align.add_argument("source_dir", help="", type=str)
    # align.add_argument("target_dir", help="", type=str)
    # align.add_argument("transforms_dir", help="", type=str)
    # align.add_argument("--imagepixelsize", default="0.46", help="", type=float)
    # align.add_argument("--rod", default="0.874208375277442", help="", type=float)
    # align.add_argument("--rejectidentity", type=str2bool, nargs="?", default=False, help="")
    # align.add_argument("--identitytolerance", default="0", help="", type=int)
    # align.add_argument("--maxepsilon", default="561.4478", help="", type=float)
    # align.add_argument("--siftfdbins", default="8", help="", type=int)
    # align.add_argument("--siftfdsize", default="4", help="", type=int)
    # align.add_argument("--siftinitialsigma", default="1.6", help="", type=float)
    # align.add_argument("--siftmaxoctavesize", default="16384", help="", type=int)
    # align.add_argument("--siftminoctavesize", default="32", help="", type=int)
    # align.add_argument("--siftsteps", default="3", help="", type=int)
    # align.add_argument("--maxnumfailures", default="1", help="", type=int)
    # align.add_argument("--maxnumneighbors", default="10", help="", type=int)
    # align.add_argument("--maxiterationsoptimize", default="1508", help="", type=int)
    # align.add_argument("--maxplateauwidthoptimize", default="302", help="", type=int)
    # align.add_argument("--layerscale", default="1.0", help="", type=float)
    # align.add_argument("--resolutionspringmesh", default="31", help="", type=int)
    # align.add_argument("--minr", default="0.723048369673122", help="", type=float)
    # align.add_argument("--maxcurvaturer", default="7.00582628641793", help="", type=float)
    # align.add_argument("--rodr", default="0.863975346789895", help="", type=float)
    # align.add_argument("--localmodelindex", default="1", help="", type=int)
    # align.add_argument("--maxlocaltrust", default="2.5798143212755", help="", type=float)
    # align.add_argument("--maxstretchspringmesh", default="17728.04", help="", type=float)
    # align.add_argument("--dampspringmesh", default="0.9", help="", type=float)
    # align.add_argument("--stiffnessspringmesh", default="0.584399040198663", help="", type=float)
    # align.add_argument("--maxplateauwidthspringmesh", default="302", help="", type=int)
    # align.add_argument("--maxiterationsspringmesh", default="1508", help="", type=int)
    # align.add_argument("--uselegacyoptimizer", type=str2bool, nargs="?", default=True, help="")
    # align.add_argument("--searchradius", default="272", help="", type=int)
    # align.add_argument("--blockradius", default="496", help="", type=int)
    # align.add_argument("--localregionsigma", default="907.3362", help="", type=float)
    # align.add_argument("--maxlocalepsilon", default="290.4741", help="", type=float)
    # align.add_argument("--sectionthickness", default="10", help="", type=float)
    # align.add_argument("--clearcache", type=str2bool, nargs="?", default=True, help="")
    # align.add_argument("--widestsetonly", type=str2bool, nargs="?", default=False, help="")
    # align.add_argument("--multiplehypotheses", type=str2bool, nargs="?", default=True, help="")
    # align.add_argument("--visualize", type=str2bool, nargs="?", default=False, help="")
    # align.add_argument("--isaligned", type=str2bool, nargs="?", default=False, help="")
    # align.add_argument("--uselocalsmoothnessfilter", type=str2bool, nargs="?", default=True, help="")
    # align.add_argument("--maxnumthreadssift", default="1", help="", type=int)
    # align.add_argument("--desiredmodelindex", default="1", help="", type=int)
    # align.add_argument("--expectedmodelindex", default="1", help="", type=int)
    # align.add_argument("--maxnumthreads", default="1", help="", type=int)
    # align.add_argument("--mininlierratio", default="0", help="", type=int)
    # align.add_argument("--minnuminliers", default="3", help="", type=int)


    align.add_argument("source_dir", help="", type=str)
    align.add_argument("target_dir", help="", type=str)
    align.add_argument("transforms_dir", help="", type=str)
    align.add_argument("--imagepixelsize", default="0.46", help="", type=float)
    align.add_argument("--rod", default=None, type=float)
    align.add_argument("--rejectidentity", type=str2bool, nargs="?", default=None, help="")
    align.add_argument("--identitytolerance", default=None, type=int)
    align.add_argument("--maxepsilon", default=None, type=float)
    align.add_argument("--siftfdbins", default=None, type=int)
    align.add_argument("--siftfdsize", default=None, type=int)
    align.add_argument("--siftinitialsigma", default=None, type=float)
    align.add_argument("--siftmaxoctavesize", default=None, type=int)
    align.add_argument("--siftminoctavesize", default=None, type=int)
    align.add_argument("--siftsteps", default=None, type=int)
    align.add_argument("--maxnumfailures", default=None, type=int)
    align.add_argument("--maxnumneighbors", default=None, type=int)
    align.add_argument("--maxiterationsoptimize", default=None, type=int)
    align.add_argument("--maxplateauwidthoptimize", default=None, type=int)
    align.add_argument("--layerscale", default=None, type=float)
    align.add_argument("--resolutionspringmesh", default=None, type=int)
    align.add_argument("--minr", default=None, type=float)
    align.add_argument("--maxcurvaturer", default=None, type=float)
    align.add_argument("--rodr", default=None, type=float)
    align.add_argument("--localmodelindex", default=None, type=int)
    align.add_argument("--maxlocaltrust", default=None, type=float)
    align.add_argument("--maxstretchspringmesh", default=None, type=float)
    align.add_argument("--dampspringmesh", default=None, type=float)
    align.add_argument("--stiffnessspringmesh", default=None, type=float)
    align.add_argument("--maxplateauwidthspringmesh", default=None, type=int)
    align.add_argument("--maxiterationsspringmesh", default=None, type=int)
    align.add_argument("--uselegacyoptimizer", type=str2bool, nargs="?", default=None, help="")
    align.add_argument("--searchradius", default=None, type=int)
    align.add_argument("--blockradius", default=None, type=int)
    align.add_argument("--localregionsigma", default=None, type=float)
    align.add_argument("--maxlocalepsilon", default=None, type=float)
    align.add_argument("--sectionthickness", default=None, type=float)
    align.add_argument("--clearcache", type=str2bool, nargs="?", default=None, help="")
    align.add_argument("--widestsetonly", type=str2bool, nargs="?", default=None, help="")
    align.add_argument("--multiplehypotheses", type=str2bool, nargs="?", default=None, help="")
    align.add_argument("--visualize", type=str2bool, nargs="?", default=None, help="")
    align.add_argument("--isaligned", type=str2bool, nargs="?", default=None, help="")
    align.add_argument("--uselocalsmoothnessfilter", type=str2bool, nargs="?", default=None, help="")
    align.add_argument("--maxnumthreadssift", default=None, type=int)
    align.add_argument("--desiredmodelindex", default=None, type=int)
    align.add_argument("--expectedmodelindex", default=None, type=int)
    align.add_argument("--maxnumthreads", default=None, type=int)
    align.add_argument("--mininlierratio", default=None, type=int)
    align.add_argument("--minnuminliers", default=None, type=int)


    # GSEARCH ARGUMENTS
    gsearch = subparsers.add_parser("gsearch", help="Grid search ESA parameters", prog="histo.py gsearch")
    gsearch.add_argument("gsearch_file", help="Grid search file", type=str)
    gsearch.add_argument("--partition", default=None, help="Select Slurm partition for every grid search job", type=str)
    gsearch.add_argument("--time", default=None, help="Set maximum execution time for each grid search job", type=str)
    gsearch.add_argument("--log-dir", default="gsearch_logs", help="Path to a directory for log files", type=str)
    # maybe this? gsearch.add_argument("--max-concurrent", metavar="max-concurrent", default=None, help="Maximum number of concurrent jobs in queue", type=int)


    # REAPPLY_TISSUE ARGUMENTS
    reapply_tissue = subparsers.add_parser("reapply-tissue", 
                                   help="Reapply the same transformations to mask images as for the registerted images",
                                   prog="histo.py reapply-tissue")
    reapply_tissue.add_argument("masks_dir", help="Path to maks images", type=str)
    reapply_tissue.add_argument("images_dir", help="Path to registered images", type=str)
    reapply_tissue.add_argument("projectfile_path", help="Path to the project file of the previously registered images", type=str)
    reapply_tissue.add_argument("output_dir", help="Path to output directory", type=str)
    reapply_tissue.add_argument("--intermediate-dir", help="Path to directory where intermediate results are stored",
                         default="intermediate_masks", type=str)
    reapply_tissue.add_argument("--modify-project-file", type=str2bool, nargs="?", default=True,
                       help="Modify project file to have it match the number of images in source_dir")
    reapply_tissue.add_argument("--grayscale", type=str2bool, nargs="?", default=False, help="")
    reapply_tissue.add_argument("--work_dir", type=str, help="Set working directory, e.g. modified project file" 
                                                     " and log files are saved into this directory")
    reapply_tissue.add_argument("--partition", default="normal", help="Set slurm partition")
    reapply_tissue.add_argument("--time", default="1-23", help="Set maximum execution time")

    # REAPPLY_TUMOR ARGUMENTS
    reapply_tumor = subparsers.add_parser("reapply-tumor", 
                                   help="Reapply the same transformations to mask images as for the registerted images",
                                   prog="histo.py reapply-tumor")
    reapply_tumor.add_argument("masks_dir", help="Path to maks images", type=str)
    reapply_tumor.add_argument("images_dir", help="Path to registered images", type=str)
    reapply_tumor.add_argument("projectfile_path", help="Path to the project file of the previously registered images", type=str)
    reapply_tumor.add_argument("output_dir", help="Path to output directory", type=str)
    reapply_tumor.add_argument("--intermediate-dir", help="Path to directory where intermediate results are stored",
                         default="intermediate_masks", type=str)
    reapply_tumor.add_argument("--modify-project-file", type=str2bool, nargs="?", default=True,
                       help="Modify project file to have it match the number of images in source_dir")
    reapply_tumor.add_argument("--grayscale", type=str2bool, nargs="?", default=False, help="")
    reapply_tumor.add_argument("--invert", type=str2bool, nargs="?", default=False, const=True, help="Invert masks in the preprocessing step")
    reapply_tumor.add_argument("--work_dir", type=str, help="Set working directory, e.g. modified project file" 
                                                     " and log files are saved into this directory")
    reapply_tumor.add_argument("--partition", default="normal", help="Set slurm partition")
    reapply_tumor.add_argument("--time", default="1-23", help="Set maximum execution time")

    return parser
