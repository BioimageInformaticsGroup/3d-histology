# @String source_dir
# @String target_dir
# @String transforms_dir
# @double imagepixelsize
# @double sectionthickness
# @int siftfdbins
# @int siftfdsize
# @double siftinitialsigma
# @int siftmaxoctavesize
# @int siftminoctavesize
# @int siftsteps
# @boolean clearcache
# @int maxnumthreadssift
# @double rod
# @int desiredmodelindex
# @int expectedmodelindex
# @double identitytolerance
# @boolean isaligned
# @double maxepsilon
# @int maxiterationsoptimize
# @int maxnumfailures
# @int maxnumneighbors
# @int maxnumthreads
# @int maxplateauwidthoptimize
# @double mininlierratio
# @int minnuminliers
# @boolean multiplehypotheses
# @boolean widestsetonly
# @boolean rejectidentity
# @boolean visualize
# @int blockradius
# @double dampspringmesh
# @double layerscale
# @int localmodelindex
# @double localregionsigma
# @double maxcurvaturer
# @int maxiterationsspringmesh
# @double maxlocalepsilon
# @double maxlocaltrust
# @int maxplateauwidthspringmesh
# @boolean uselegacyoptimizer
# @double maxstretchspringmesh
# @double minr
# @int resolutionspringmesh
# @double rodr
# @int searchradius
# @double stiffnessspringmesh
# @boolean uselocalsmoothnessfilter
#
import os
from ij import IJ, ImagePlus
from java.util import HashSet
from java.awt import Color
from mpicbg.trakem2.align import ElasticLayerAlignment
from ini.trakem2 import ControlWindow, Project
from ini.trakem2.display import Layer, Patch

# Redirect error messages to ImageJ log to avoid pausing in case of error.
IJ.redirectErrorMessages()

# Set success flag to false.
runsuccess = False

# Collect parameters here.
p = ElasticLayerAlignment.Param()
p.ppm.sift.fdBins = siftfdbins
p.ppm.sift.fdSize = siftfdsize
p.ppm.sift.initialSigma = siftinitialsigma
p.ppm.sift.maxOctaveSize = siftmaxoctavesize
p.ppm.sift.minOctaveSize = siftminoctavesize
p.ppm.sift.steps = siftsteps
p.ppm.clearCache = clearcache
p.ppm.maxNumThreadsSift = maxnumthreadssift
p.ppm.rod = rod
p.desiredModelIndex = desiredmodelindex
p.expectedModelIndex = expectedmodelindex
p.identityTolerance = identitytolerance
p.isAligned = isaligned
p.maxEpsilon = maxepsilon
p.maxIterationsOptimize = maxiterationsoptimize
p.maxNumFailures = maxnumfailures
p.maxNumNeighbors = maxnumneighbors
p.maxNumThreads = maxnumthreads
p.maxPlateauwidthOptimize = maxplateauwidthoptimize
p.minInlierRatio = mininlierratio
p.minNumInliers = minnuminliers
p.multipleHypotheses = multiplehypotheses
p.rejectIdentity = rejectidentity
p.visualize = visualize
p.blockRadius = blockradius
p.dampSpringMesh = dampspringmesh
p.layerScale = layerscale
p.localModelIndex = localmodelindex
p.localRegionSigma = localregionsigma
p.maxCurvatureR = maxcurvaturer
p.maxIterationsSpringMesh = maxiterationsspringmesh
p.maxLocalEpsilon = maxlocalepsilon
p.maxLocalTrust = maxlocaltrust
p.maxPlateauwidthSpringMesh = maxplateauwidthspringmesh
p.useLegacyOptimizer = uselegacyoptimizer
p.maxStretchSpringMesh = maxstretchspringmesh
p.minR = minr
p.resolutionSpringMesh = resolutionspringmesh
p.rodR = rodr
p.searchRadius = searchradius
p.stiffnessSpringMesh = stiffnessspringmesh
p.useLocalSmoothnessFilter = uselocalsmoothnessfilter
#p.widestSetOnly = widestsetonly

# DEBUG: revert to default settings
# p = ElasticLayerAlignment.Param()
# DEBUG: revert to default settings

# Disable GUI.
ControlWindow.setGUIEnabled(0)

# If any projects are open, close them.
allprojects = Project.getProjects()
for project in allprojects:
  project.remove()

# Create a TrakEM2 project.
project = Project.newFSProject("blank", None, transforms_dir)

#  Get the filenames of input images.
filenames = os.listdir(source_dir)
filenames.sort()
numfiles = len(filenames)

# Create the correct number of layers (one per image).
layerset = project.getRootLayerSet()
for i in range(numfiles):
  layerset.getLayer(i, 1, True)

# Add one image as a patch to each layer.
for i,layer in enumerate(layerset.getLayers()):
  # Add this image as a patch to the layer.
  filepath = os.path.join(source_dir, filenames[i])
  patch = Patch.createPatch(project, filepath)
  layer.add(patch)
  # Update internal quadtree of the layer
  layer.recreateBuckets()

# Set the correct section thickness for each layer.
z = 0
thickness = sectionthickness/imagepixelsize
for layer in layerset.getLayers():
  layer.setZ(z)
  layer.setThickness(thickness)
  z += thickness

# Update the LayerTree:
project.getLayerTree().updateList(layerset)

# Update world coordinates.
layerset.setMinimumDimensions()

# Run registration.
layerRange = layerset.getLayers()
fixedLayers = HashSet()
emptyLayers = HashSet()
layerBounds = layerset.get2DBounds()
propagateTransformBefore = False
propagateTransformAfter = False
try:
  ElasticLayerAlignment().exec(p, project, layerRange, fixedLayers, emptyLayers, layerBounds, propagateTransformBefore, propagateTransformAfter, None)
  runsuccess = True
except:
  pass

# If the registration was successful, output images and save project.
if runsuccess:
  # Update world coordinates.
  layerset.setMinimumDimensions()

  # Output images.
  layerBounds = layerset.get2DBounds()
  scale = 1.0
  backgroundcolor = Color.white
  for i,layer in enumerate(layerset.getLayers()):
    patches = layer.getDisplayables(Patch,layerBounds)
    ip = Patch.makeFlatImage(ImagePlus.COLOR_RGB,layer,layerBounds,scale,patches,backgroundcolor,True)
    imp = ImagePlus(filenames[i],ip)
    filepath = os.path.join(target_dir,filenames[i])
    IJ.saveAsTiff(imp,filepath)

  # Disable mipmap regeneration to allow later reapplying transformations faster by loading a modified project XML.
  loader = project.getLoader()
  loader.setMipMapsRegeneration(False)

  # Save the project.
  project.saveAs(os.path.join(transforms_dir, "tempproject.xml"), True)

  # Close the project.
  project.remove()

# Output success flag as a one pixel image for Matlab to read.
if runsuccess:
  impout = IJ.createImage("runsuccess","8-bit white",1,1,1)
else:
  impout = IJ.createImage("runsuccess","8-bit black",1,1,1)
impout.show()










