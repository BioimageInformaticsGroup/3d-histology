% % 3D features, full res input
% inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE';
% outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE_visualizations';
% images_dir_name='full_res_jp2';
% no_bubbles_dir_name='full_res_subsampled10_no_bubbles';
% tumormasks_dir_name='tumormasks';
% features_dir_name='matlab_features';
% ext='.jp2';
% extmask='.png';
% fullrespixelsize=0.92; % inputs have been subsampled to 50% and then again to 10% of original dims
% zscaling=0.5; % !!!
% slicethickness=50;
% smoothingfactor=5;
% reductionlevel=5;

% 3D features, subsampled10 input
inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE';
outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE_visualizations';
images_dir_name='full_res_subsampled10';
no_bubbles_dir_name='full_res_subsampled10_no_bubbles';
tumormasks_dir_name='tumormasks_subsampled10';
features_dir_name='USE_FULL_RES_DATA';
ext='.tiff';
extmask='.png';
fullrespixelsize=0.92/0.10; % inputs have been subsampled to 50% and then again to 10% of original dims
zscaling=0.5; % !!!
slicethickness=50;
smoothingfactor=5;
reductionlevel=0;

% % Pharmatest 1
% inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/EMAP_pharmatest/';
% outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/EMAP_pharmatest_visualizations';
% images_dir_name='full_res_subsampled6';
% tumormasks_dir_name='';
% features_dir_name='';
% ext='.tiff';
% extmask='.png';
% fullrespixelsize=0.92/0.10; % inputs have been subsampled to 50% and then again to 10% of original dims
% zscaling=0.5; % !!!
% slicethickness=50;
% smoothingfactor=5;
% reductionlevel=0;

% % Pharmatest poster
% inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/EMAP_pharmatest';
% outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/EMAP_pharmatest_visualizations';
% images_dir_name='full_res_subsampled6';
% tumormasks_dir_name='masks_smo2';
% features_dir_name='';
% no_bubbles_dir_name='';
% ext='.tiff';
% extmask='.png';
% fullrespixelsize=0.221089108910891 * 2^2 * (1 / 0.06); % rl2 & subsampled6
% zscaling=1.0; % !!!
% slicethickness=100;
% smoothingfactor=5;
% reductionlevel=0;


% % GP17
% % inputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/GP17';
% % outputdatapath='/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/GP17_visualizations';
% images_dir_name='preprocessed';
% tumormasks_dir_name='tumormasks';
% features_dir_name='features';
% ext='.tiff';
% extmask='.tiff';
% fullrespixelsize=0.92/0.10; % inputs have been subsampled to 50% and then again to 10% of original dims
% zscaling=0.5; % !!!
% slicethickness=50;
% smoothingfactor=5;
% reductionlevel=0;




% % coordinates [x, y], in half size (0.92 microns/pixel)
% POIs = {                                     
%     {'anatomical center', 'Lat-186_PTENX036-20-37_18114_46028_7452.tiff.jp2', 7500.01171875, 7071.949218750002, 'PTENX036-20'}                                             
%     {'anatomical center', 'Lat-186_PTENX036-15-43_19599_20556_9932.tiff.jp2', 8178.8662109375, 10918.8955078125, 'PTENX036-15'}                                             
%     {'anatomical center', 'Lat-186_Ptenx036-14-37_3855_4284_4156.tiff.jp2', 7430.30859375, 5981.800781249998, 'PTENX036-14'}                                             
%     {'anatomical center', 'Lat-186_PtenX036-011-28_17678_21900_24556.tiff.jp2', 5602.433593749999, 7132.015625, 'PTENX036-011'}                                            
%     {'anatomical center', 'LAT-186_PTENX036-033-37_6716_3964.tiff.jp2', 6883.2841796875, 10773.7373046875, 'PTENX036-033'}                                             
%     {'anatomical center', 'LAT-186_PTENX036-032-37_28079_4076_5340.tiff.jp2', 10925.0361328125, 5144.6630859375, 'PTENX036-032'}                                              
% };    

% POIs = {
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_Lat-186_PtenX036-011-28_17678_21900_24556.png', 4312.0, 4579.3, 'PTENX036-011' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_Lat-186_Ptenx036-14-37_3855_4284_4156.png', 4784.9, 5760.6, 'PTENX036-14' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_Lat-186_PTENX036-15-43_19599_20556_9932.png', 5957.7, 7154.0, 'PTENX036-15' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_Lat-186_PTENX036-20-37_18114_46028_7452.png', 8914.3, 5882.2, 'PTENX036-20' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_LAT-186_PTENX036-032-37_28079_4076_5340.png', 7007.9, 4933.0, 'PTENX036-032' }
%     {'anatomical center', 'Transformed_Preprocessed_landmarks_LAT-186_PTENX036-033-37_6716_3964.png', 5846.9, 4618.0, 'PTENX036-033' }
% };


% POIs = {
% };


% % 3D
% sampleID = {  'PTENX036-011', ...
%               'PTENX036-14', ...
%               'PTENX036-20', ...
%               'PTENX036-32', ...
%               'PTENX036-33', ...
%               'PTENX036-15'};


% % 3D
% sampleID = {'PTENX036-33', ...
%             'PTENX036-011'};


% sampleID = {'PTENX036-011',
%             'PTENX036-14',
%             'PTENX036-15',
%             'PTENX036-20',
%             'PTENX036-21',
%             'PTENX036-32',
%             'PTENX036-33',
%             'PTENX036-34',
%             'PTENX036-39',
%             'PTENX036-40',
%             'PTENX036-42',
%             'PTENX036-68',
%             'UTU186-xhiMyc-003',
%             'UTU186-xhiMyc-006',
%             'UTU186-xhiMyc-007',
%             'UTU186-xhiMyc-008',
%             'UTU186-xhiMyc-022',
%             'UTU186-xhiMyc-045',
%             'UTU186-xhiMyc-199',
%             'UTU186-xhiMyc-211',
%             'UTU186-xhiMyc-223',
%             'UTU186-xhiMyc-355',
%             'UTU186-xhiMyc-356',
%             'UTU186-xhiMyc-1387'};


sampleID = {'PTENX036-011'};
% sampleID = {'PTENX036-14'};
% sampleID = {'PTENX036-15'};
% sampleID = {'PTENX036-20'};
% sampleID = {'PTENX036-32'};
% sampleID = {'PTENX036-33'};


% % Pharmatest
% sampleID = {'pieces1_rl2', ...
%             'pieces2_rl3', ...
%             'pieces3_rl2'};

% sampleID = {'pieces1_rl2'};


% GP17
% sampleID = {'34623'};
% sampleID = {'34624'};
% sampleID = {'34625'};
% sampleID = {'34623', '34624', '34625'};

for i = 1:length(sampleID)
    
    inputpath = [inputdatapath,filesep,sampleID{i},filesep,images_dir_name];
    inputpath_no_bubbles = [inputdatapath,filesep,sampleID{i},filesep,no_bubbles_dir_name];
    inputpathmask = [inputdatapath,filesep,sampleID{i},filesep,tumormasks_dir_name];
    inputpathfeatures = [inputdatapath,filesep,sampleID{i},filesep,features_dir_name];
    
    
%     % Tissue *only
%     opacity = 1.0;
%     % inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity
%     visualize_tissue_3D(inputpath, ext, fullrespixelsize, slicethickness, zscaling, reductionlevel, opacity)
%     alphamap('rampup');
%     tempmap = alphamap;
%     tempmap(1:10) = 0;
%     alphamap(opacity*tempmap);
% %     alphamap(sqrt(tempmap))
% %     axis off
%     legend off
%     grid on
% %     saveas(gcf, [sampleID{i}, '_tissueonly.png'])
% %     close;

    
%     % Tissue *only (FULL RES INPUT)
%     opacity = 1.0;
%     % inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity
%     visualize_tissue_3D(inputpath, ext, fullrespixelsize, slicethickness, zscaling, reductionlevel, opacity)
%     alphamap('rampup');
%     tempmap = alphamap;
%     tempmap(1:10) = 0;
%     tempmap = tempmap * opacity;
% %     tempmap(end-1:end) = 0.1; disp('REMEMBER MEE');
%     alphamap(tempmap);
%     legend off
%     unify_volume_orientations2
%     saveas(gcf, [sampleID{i}, '_tissueonly_fullresdata.png'])
% %     grid off; saveas(gcf, [sampleID{i}, '_tissueonly_fullresdata_nogrid.png'])
% %     axis off; saveas(gcf, [sampleID{i}, '_tissueonly_fullresdata_noaxis.png'])
%     close;


%     % Tissue *only no bubbles
%     opacity = 1;
%     % inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity
%     visualize_tissue_3D(inputpath_no_bubbles, ext, fullrespixelsize, slicethickness, zscaling, reductionlevel, opacity)
%     alphamap('rampup');
%     tempmap = alphamap;
%     tempmap(1:5) = 0;
%     alphamap(opacity*tempmap);
% % %     alphamap(sqrt(tempmap))
% %     tempmap = alphamap;
% %     tempmap(end-1:end) = 0.018; disp('REMEMBER MEE');
% %     alphamap(tempmap);
%     % same viewing angle for all
%     unify_volume_orientations2
%     saveas(gcf, [sampleID{i}, '_tissueonly.png'])
% %     grid off; saveas(gcf, [sampleID{i}, '_tissueonly_nogrid.png'])       
% %     axis off; saveas(gcf, [sampleID{i}, '_tissueonly_noaxes.png'])
% %     axis off; visualize_video_3D([outputdatapath,filesep,sampleID{i},'_tissueonly_nobubbles.mp4']);
%     grid on; visualize_video_3D([outputdatapath,filesep,sampleID{i},'_tissueonly_nobubbles_grid.mp4']);
%     close all;


% %     ROI objects + tissue
%     opacity = 0.05; 
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'alltissue');
% %     visualize_roimasks_3D(inputpath_no_bubbles,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'alltissue');
%     alphamap('rampup');
%     tempmap = alphamap*opacity;
%     tempmap(1:10) = 0;
% %     tempmap(end-1:end) = 0.018; disp('REMEMBER MEE');
%     alphamap(tempmap);
%     set(gcf, 'Position', get(0, 'Screensize'));
%     legend off
%     unify_volume_orientations
% %     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_alltissue.fig'],'compact');
% %     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_alltissue.mp4']);
% %     colorbar    
%     saveas(gcf, [outputdatapath,filesep,sampleID{i}, '_roi_and_tissue_color_bar.png']);
% %     close all;


% %     ROI objects + tissue edges + center of mass cross
% %     opacity = 0.5;
%     opacity = 0.3;
% %     visualize_roimasks_3D(inputpath_no_bubbles,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'tissueedges', POIs);
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'tissueedges', POIs);
%     alphamap('rampup');
%     tempmap = alphamap;
%     tempmap(1:5) = 0;
%     alphamap(opacity*tempmap);
%     set(gcf, 'Position', get(0, 'Screensize'));
%     legend off
%     
%     % Load same angle for all PTEN mice
%     load([sampleID{i} '_camerapos_tissueonly.mat'])
%     set(gca, 'CameraPosition', pos)
%     camorbit(-70, 0)
% %     saveas(gcf, [sampleID{i}, '_roiobjects_tissueedges.png']);
% %     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_alltissue.fig'],'compact');
%     grid on; visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roiobjects_tissueedges_com_grid.mp4']);
% %     axis off; visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roiobjects_tissueedges_com.mp4']);
%     close all;
    

%     % Shoot pics
%    
%     % other side view
%     load PTENX036-011_camerapos_pos3.mat
%     set(gca, 'CameraPosition', pos3)
%     saveas(gcf, [sampleID{i}, '_pos3.png'])
%     
%     % side view
%     legend off;
%     load PTENX036-011_camerapos_pos1.mat
%     set(gca, 'CameraPosition', pos1)
%     % flip mouse 32
%     if strcmp(sampleID, 'PTENX036-32')
%         camorbit(180, 0)
%     end
%     saveas(gcf, [sampleID{i}, '_pos1.png'])
%     
%     % top view
%     legend off;
%     load PTENX036-011_camerapos_pos2.mat
%     load([sampleID{i}, '_CamUpVec'])
%     set(gca, 'CameraPosition', pos2)
%     set(gca, 'CameraUpVector', camupvec)
%     % flip mouse 32
%     if strcmp(sampleID, 'PTENX036-32')
%         camorbit(180, 0)
%     end
%     saveas(gcf, [sampleID{i}, '_pos2.png'])
%     set(gca, 'CameraUpVectorMode', 'auto')
%     
%     close;
    
    
%     ROI objects.
    opacity = 0.5;
    visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'roiobjects');

%     load('pharmatest_poster.mat')
%     set(gca, 'CameraPosition', pharmatest_poster_campos)
%     set(gca, 'CameraUpVector', pharmatest_poster_camupvec)
%     set(gca,'xdir','reverse')
%     set(gca,'ydir','reverse')
%     xlim(xli)
%     ylim(yli)
%     zlim(zli)
%     axis off
%     legend off
%     saveas(gcf, [sampleID{i}, '_roiobjects.png'])
%     close;
    
%     title(sampleID{i});

%     % Automatic angles
%     set(gcf, 'Position', get(0, 'Screensize'));
%     load PTENX036-011_camerapos_pcaaxis_top.mat
%     set(gca, 'CameraPosition', pos)
%     saveas(gcf, [sampleID{i}, '_roiobject_pca_axes_top.png'])
%     load PTENX036-011_camerapos_pcaaxis_side.mat
%     set(gca, 'CameraPosition', pos)
%     saveas(gcf, [sampleID{i}, '_roiobject_pca_axes_side.png'])
%     load PTENX036-011_camerapos_pcaaxis_side2.mat
%     set(gca, 'CameraPosition', pos)
%     saveas(gcf, [sampleID{i}, '_roiobject_pca_axes_side2.png'])
%     close;

%     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_roiobjects.fig'],'compact');
%     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roiobjects.mp4']);
%     close all;




% %     ROI object pictures.
%     opacity = 0.5;
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'roiobject_pictures');
%     title(sampleID{i});
%     set(gcf, 'Position', get(0, 'Screensize'));
% %     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_roiobjects.fig'],'compact');
% %     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roiobjects.mp4']);
% %     close all;


% %     ROI tissue.
%     opacity = 0.5;
%     visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'roitissue');
%     set(gcf, 'Position', get(0, 'Screensize'));
%     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_roitissue.fig'],'compact');
% %     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roitissue.mp4']);
% %     close all;


%     % Features, ROI only.
%     opacity = 0.5;
% %     featurename = 'nucNumber';
%     featurename = 'IntensityMean_E';
%     roionly = true;
%     visualize_features_3D(inputpath,ext,inputpathmask,extmask,inputpathfeatures,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,featurename,roionly);
%     set(gcf, 'Position', get(0, 'Screensize'));
%     unify_volume_orientations
% %     savefig(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'_roi.fig'],'compact');
% %     saveas(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'_featurevolume.png']);
%     visualize_video_3D([outputdatapath,filesep,sampleID{i},'_',featurename,'_roi.mp4']);
%     close all;


% % Literally the fetures from the article
% featureslist = {'nucNumber'
%                 'DensityLBP6'
%                 'mROI-BlockLBP-9H'
%                 'mROI-BlockLBP-6E'
%                 'NhoodMaxDist'
%                 'mROI-BlockLBP-6H'
%                 'NhoodNucAngleSkewABS'
%                 'ROI-BlockSIFT-ScaleMeanH'
%                 'NhoodNucAngleKurtABS'
%                 'NhoodStdDist'
%                 'SIFT-ScaleStdH'
%                 'mROI-BlockLBP-4H'
%                 'mROI-BlockLBP-9E'
%                 'mROI-BlockSIFT-ScaleMeanH'
%                 'NhoodNucAngleVar'
%                 'SIFT-ScaleMeanH'
%                 'NhoodSkewnessH'
%                 'meanNucSizeH'
%                 'meanNucDistInNucNB'
%                 'DensityLBP7'
%                 'NhoodMeanDist'};


% % Matching names from the article
% featureslist = {'nucNumber'                        
%                'DensityLBP6_Nuc_mean'             
%                'DensityLBP6_Nuc_std'              
%                'NhoodNucAngleVar_Nuc_mean'        
%                'NhoodNucAngleVar_Nuc_std'         
%                'meanNucDistInNucNB_Nuc_mean'      
%                'meanNucDistInNucNB_Nuc_std'       
%                'DensityLBP7_Nuc_mean'             
%                'DensityLBP7_Nuc_std'};


% all blockfeatures
% featurelist={'GLCMContrast-1_H'
%              'GLCMContrast-2_H'
%              'GLCMContrast-3_H'
%              'GLCMContrast-4_H'
%              'GLCMCorrelation-1_H'
%              'GLCMCorrelation-2_H'
%              'GLCMCorrelation-3_H'
%              'GLCMCorrelation-4_H'
%              'GLCMEnergy-1_H'
%              'GLCMEnergy-2_H'
%              'GLCMEnergy-3_H'
%              'GLCMEnergy-4_H'
%              'GLCMHomogeneity-1_H'
%              'GLCMHomogeneity-2_H'
%              'GLCMHomogeneity-3_H'
%              'GLCMHomogeneity-4_H'
%              'HOG1_H'
%              'HOG2_H'
%              'HOG3_H'
%              'HOG4_H'
%              'HOG5_H'
%              'HOG6_H'
%              'HOG7_H'
%              'HOG8_H'
%              'HOG9_H'
%              'HOG10_H'
%              'HOG11_H'
%              'HOG12_H'
%              'HOG13_H'
%              'HOG14_H'
%              'HOG15_H'
%              'HOG16_H'
%              'HOG17_H'
%              'HOG18_H'
%              'HOG19_H'
%              'HOG20_H'
%              'HOG21_H'
%              'HOG22_H'
%              'HOG23_H'
%              'HOG24_H'
%              'HOG25_H'
%              'HOG26_H'
%              'HOG27_H'
%              'HOG28_H'
%              'HOG29_H'
%              'HOG30_H'
%              'HOG31_H'
%              'HOG32_H'
%              'HOG33_H'
%              'HOG34_H'
%              'HOG35_H'
%              'HOG36_H'
%              'HOG37_H'
%              'HOG38_H'
%              'HOG39_H'
%              'HOG40_H'
%              'HOG41_H'
%              'HOG42_H'
%              'HOG43_H'
%              'HOG44_H'
%              'HOG45_H'
%              'HOG46_H'
%              'HOG47_H'
%              'HOG48_H'
%              'HOG49_H'
%              'HOG50_H'
%              'HOG51_H'
%              'HOG52_H'
%              'HOG53_H'
%              'HOG54_H'
%              'HOG55_H'
%              'HOG56_H'
%              'HOG57_H'
%              'HOG58_H'
%              'HOG59_H'
%              'HOG60_H'
%              'HOG61_H'
%              'HOG62_H'
%              'HOG63_H'
%              'HOG64_H'
%              'HOG65_H'
%              'HOG66_H'
%              'HOG67_H'
%              'HOG68_H'
%              'HOG69_H'
%              'HOG70_H'
%              'HOG71_H'
%              'HOG72_H'
%              'HOG73_H'
%              'HOG74_H'
%              'HOG75_H'
%              'HOG76_H'
%              'HOG77_H'
%              'HOG78_H'
%              'HOG79_H'
%              'HOG80_H'
%              'HOG81_H'
%              'IntensitySkew_H'
%              'IntensityKurt_H'
%              'IntensityVar_H'
%              'IntensityMean_H'
%              'IntensityMax_H'
%              'IntensityMin_H'
%              'SIFT-ScaleMean_H'
%              'SIFT-ScaleStd_H'
%              'SIFT-NroOfFrames_H'
%              'LBP-1_H'
%              'LBP-2_H'
%              'LBP-3_H'
%              'LBP-4_H'
%              'LBP-5_H'
%              'LBP-6_H'
%              'LBP-7_H'
%              'LBP-8_H'
%              'LBP-9_H'
%              'LBP-10_H'
%              'MSER-1_H'
%              'MSER-2_H'
%              'MSER-3_H'
%              'MSER-4_H'
%              'GLCMContrast-1_E'
%              'GLCMContrast-2_E'
%              'GLCMContrast-3_E'
%              'GLCMContrast-4_E'
%              'GLCMCorrelation-1_E'
%              'GLCMCorrelation-2_E'
%              'GLCMCorrelation-3_E'
%              'GLCMCorrelation-4_E'
%              'GLCMEnergy-1_E'
%              'GLCMEnergy-2_E'
%              'GLCMEnergy-3_E'
%              'GLCMEnergy-4_E'
%              'GLCMHomogeneity-1_E'
%              'GLCMHomogeneity-2_E'
%              'GLCMHomogeneity-3_E'
%              'GLCMHomogeneity-4_E'
%              'HOG1_E'
%              'HOG2_E'
%              'HOG3_E'
%              'HOG4_E'
%              'HOG5_E'
%              'HOG6_E'
%              'HOG7_E'
%              'HOG8_E'
%              'HOG9_E'
%              'HOG10_E'
%              'HOG11_E'
%              'HOG12_E'
%              'HOG13_E'
%              'HOG14_E'
%              'HOG15_E'
%              'HOG16_E'
%              'HOG17_E'
%              'HOG18_E'
%              'HOG19_E'
%              'HOG20_E'
%              'HOG21_E'
%              'HOG22_E'
%              'HOG23_E'
%              'HOG24_E'
%              'HOG25_E'
%              'HOG26_E'
%              'HOG27_E'
%              'HOG28_E'
%              'HOG29_E'
%              'HOG30_E'
%              'HOG31_E'
%              'HOG32_E'
%              'HOG33_E'
%              'HOG34_E'
%              'HOG35_E'
%              'HOG36_E'
%              'HOG37_E'
%              'HOG38_E'
%              'HOG39_E'
%              'HOG40_E'
%              'HOG41_E'
%              'HOG42_E'
%              'HOG43_E'
%              'HOG44_E'
%              'HOG45_E'
%              'HOG46_E'
%              'HOG47_E'
%              'HOG48_E'
%              'HOG49_E'
%              'HOG50_E'
%              'HOG51_E'
%              'HOG52_E'
%              'HOG53_E'
%              'HOG54_E'
%              'HOG55_E'
%              'HOG56_E'
%              'HOG57_E'
%              'HOG58_E'
%              'HOG59_E'
%              'HOG60_E'
%              'HOG61_E'
%              'HOG62_E'
%              'HOG63_E'
%              'HOG64_E'
%              'HOG65_E'
%              'HOG66_E'
%              'HOG67_E'
%              'HOG68_E'
%              'HOG69_E'
%              'HOG70_E'
%              'HOG71_E'
%              'HOG72_E'
%              'HOG73_E'
%              'HOG74_E'
%              'HOG75_E'
%              'HOG76_E'
%              'HOG77_E'
%              'HOG78_E'
%              'HOG79_E'
%              'HOG80_E'
%              'HOG81_E'
%              'IntensitySkew_E'
%              'IntensityKurt_E'
%              'IntensityVar_E'
%              'IntensityMean_E'
%              'IntensityMax_E'
%              'IntensityMin_E'
%              'SIFT-ScaleMean_E'
%              'SIFT-ScaleStd_E'
%              'SIFT-NroOfFrames_E'
%              'LBP-1_E'
%              'LBP-2_E'
%              'LBP-3_E'
%              'LBP-4_E'
%              'LBP-5_E'
%              'LBP-6_E'
%              'LBP-7_E'
%              'LBP-8_E'
%              'LBP-9_E'
%              'LBP-10_E'
%              'MSER-1_E'
%              'MSER-2_E'
%              'MSER-3_E'
%              'MSER-4_E'
%              'nucNumber'
%              'meanNucDist'
%              'stdNucDist'
%              'minNucDist'
%              'maxNucDist'
%              'NucDensityPerc10'
%              'NucDensityPerc25'
%              'NucDensityPerc50'
%              'NucDensityPerc75'
%              'NucDensityPerc90'}


% All nuclei features
% featureslist={'GLCMContrast-1_Nuc_mean_H'
%             'GLCMContrast-2_Nuc_mean_H'
%             'GLCMContrast-3_Nuc_mean_H'
%             'GLCMContrast-4_Nuc_mean_H'
%             'GLCMCorrelation-1_Nuc_mean_H'
%             'GLCMCorrelation-2_Nuc_mean_H'
%             'GLCMCorrelation-3_Nuc_mean_H'
%             'GLCMCorrelation-4_Nuc_mean_H'
%             'GLCMEnergy-1_Nuc_mean_H'
%             'GLCMEnergy-2_Nuc_mean_H'
%             'GLCMEnergy-3_Nuc_mean_H'
%             'GLCMEnergy-4_Nuc_mean_H'
%             'GLCMHomogeneity-1_Nuc_mean_H'
%             'GLCMHomogeneity-2_Nuc_mean_H'
%             'GLCMHomogeneity-3_Nuc_mean_H'
%             'GLCMHomogeneity-4_Nuc_mean_H'
%             'IntensityMean_Nuc_mean_H'
%             'IntensityVar_Nuc_mean_H'
%             'IntensitySkew_Nuc_mean_H'
%             'IntensityKurt_Nuc_mean_H'
%             'NhoodGLCMContrast-1_Nuc_mean_H'
%             'NhoodGLCMContrast-2_Nuc_mean_H'
%             'NhoodGLCMContrast-3_Nuc_mean_H'
%             'NhoodGLCMContrast-4_Nuc_mean_H'
%             'NhoodGLCMCorrelation-1_Nuc_mean_H'
%             'NhoodGLCMCorrelation-2_Nuc_mean_H'
%             'NhoodGLCMCorrelation-3_Nuc_mean_H'
%             'NhoodGLCMCorrelation-4_Nuc_mean_H'
%             'NhoodGLCMEnergy-1_Nuc_mean_H'
%             'NhoodGLCMEnergy-2_Nuc_mean_H'
%             'NhoodGLCMEnergy-3_Nuc_mean_H'
%             'NhoodGLCMEnergy-4_Nuc_mean_H'
%             'NhoodGLCMHomogeneity-1_Nuc_mean_H'
%             'NhoodGLCMHomogeneity-2_Nuc_mean_H'
%             'NhoodGLCMHomogeneity-3_Nuc_mean_H'
%             'NhoodGLCMHomogeneity-4_Nuc_mean_H'
%             'NhoodIntensityMean_Nuc_mean_H'
%             'NhoodIntensityVar_Nuc_mean_H'
%             'NhoodIntensitySkew_Nuc_mean_H'
%             'NhoodIntensityKurt_Nuc_mean_H'
%             'NhoodSIFT-ScaleMean_Nuc_mean_H'
%             'NhoodSIFT-ScaleStd_Nuc_mean_H'
%             'NhoodSIFT-NroOfFrames_Nuc_mean_H'
%             'NhoodLBP1_Nuc_mean_H'
%             'NhoodLBP2_Nuc_mean_H'
%             'NhoodLBP3_Nuc_mean_H'
%             'NhoodLBP4_Nuc_mean_H'
%             'NhoodLBP5_Nuc_mean_H'
%             'NhoodLBP6_Nuc_mean_H'
%             'NhoodLBP7_Nuc_mean_H'
%             'NhoodLBP8_Nuc_mean_H'
%             'NhoodLBP9_Nuc_mean_H'
%             'NhoodLBP10_Nuc_mean_H'
%             'NhoodMSER1_Nuc_mean_H'
%             'NhoodMSER2_Nuc_mean_H'
%             'NhoodMSER3_Nuc_mean_H'
%             'NhoodMSER4_Nuc_mean_H'
%             'HOG1_Nuc_mean_H'
%             'HOG2_Nuc_mean_H'
%             'HOG3_Nuc_mean_H'
%             'HOG4_Nuc_mean_H'
%             'HOG5_Nuc_mean_H'
%             'HOG6_Nuc_mean_H'
%             'HOG7_Nuc_mean_H'
%             'HOG8_Nuc_mean_H'
%             'HOG9_Nuc_mean_H'
%             'HOG10_Nuc_mean_H'
%             'HOG11_Nuc_mean_H'
%             'HOG12_Nuc_mean_H'
%             'HOG13_Nuc_mean_H'
%             'HOG14_Nuc_mean_H'
%             'HOG15_Nuc_mean_H'
%             'HOG16_Nuc_mean_H'
%             'HOG17_Nuc_mean_H'
%             'HOG18_Nuc_mean_H'
%             'HOG19_Nuc_mean_H'
%             'HOG20_Nuc_mean_H'
%             'HOG21_Nuc_mean_H'
%             'HOG22_Nuc_mean_H'
%             'HOG23_Nuc_mean_H'
%             'HOG24_Nuc_mean_H'
%             'HOG25_Nuc_mean_H'
%             'HOG26_Nuc_mean_H'
%             'HOG27_Nuc_mean_H'
%             'HOG28_Nuc_mean_H'
%             'HOG29_Nuc_mean_H'
%             'HOG30_Nuc_mean_H'
%             'HOG31_Nuc_mean_H'
%             'HOG32_Nuc_mean_H'
%             'HOG33_Nuc_mean_H'
%             'HOG34_Nuc_mean_H'
%             'HOG35_Nuc_mean_H'
%             'HOG36_Nuc_mean_H'
%             'HOG37_Nuc_mean_H'
%             'HOG38_Nuc_mean_H'
%             'HOG39_Nuc_mean_H'
%             'HOG40_Nuc_mean_H'
%             'HOG41_Nuc_mean_H'
%             'HOG42_Nuc_mean_H'
%             'HOG43_Nuc_mean_H'
%             'HOG44_Nuc_mean_H'
%             'HOG45_Nuc_mean_H'
%             'HOG46_Nuc_mean_H'
%             'HOG47_Nuc_mean_H'
%             'HOG48_Nuc_mean_H'
%             'HOG49_Nuc_mean_H'
%             'HOG50_Nuc_mean_H'
%             'HOG51_Nuc_mean_H'
%             'HOG52_Nuc_mean_H'
%             'HOG53_Nuc_mean_H'
%             'HOG54_Nuc_mean_H'
%             'HOG55_Nuc_mean_H'
%             'HOG56_Nuc_mean_H'
%             'HOG57_Nuc_mean_H'
%             'HOG58_Nuc_mean_H'
%             'HOG59_Nuc_mean_H'
%             'HOG60_Nuc_mean_H'
%             'HOG61_Nuc_mean_H'
%             'HOG62_Nuc_mean_H'
%             'HOG63_Nuc_mean_H'
%             'HOG64_Nuc_mean_H'
%             'HOG65_Nuc_mean_H'
%             'HOG66_Nuc_mean_H'
%             'HOG67_Nuc_mean_H'
%             'HOG68_Nuc_mean_H'
%             'HOG69_Nuc_mean_H'
%             'HOG70_Nuc_mean_H'
%             'HOG71_Nuc_mean_H'
%             'HOG72_Nuc_mean_H'
%             'HOG73_Nuc_mean_H'
%             'HOG74_Nuc_mean_H'
%             'HOG75_Nuc_mean_H'
%             'HOG76_Nuc_mean_H'
%             'HOG77_Nuc_mean_H'
%             'HOG78_Nuc_mean_H'
%             'HOG79_Nuc_mean_H'
%             'HOG80_Nuc_mean_H'
%             'HOG81_Nuc_mean_H'
%             'GLCMContrast-1_Nuc_std_H'
%             'GLCMContrast-2_Nuc_std_H'
%             'GLCMContrast-3_Nuc_std_H'
%             'GLCMContrast-4_Nuc_std_H'
%             'GLCMCorrelation-1_Nuc_std_H'
%             'GLCMCorrelation-2_Nuc_std_H'
%             'GLCMCorrelation-3_Nuc_std_H'
%             'GLCMCorrelation-4_Nuc_std_H'
%             'GLCMEnergy-1_Nuc_std_H'
%             'GLCMEnergy-2_Nuc_std_H'
%             'GLCMEnergy-3_Nuc_std_H'
%             'GLCMEnergy-4_Nuc_std_H'
%             'GLCMHomogeneity-1_Nuc_std_H'
%             'GLCMHomogeneity-2_Nuc_std_H'
%             'GLCMHomogeneity-3_Nuc_std_H'
%             'GLCMHomogeneity-4_Nuc_std_H'
%             'IntensityMean_Nuc_std_H'
%             'IntensityVar_Nuc_std_H'
%             'IntensitySkew_Nuc_std_H'
%             'IntensityKurt_Nuc_std_H'
%             'NhoodGLCMContrast-1_Nuc_std_H'
%             'NhoodGLCMContrast-2_Nuc_std_H'
%             'NhoodGLCMContrast-3_Nuc_std_H'
%             'NhoodGLCMContrast-4_Nuc_std_H'
%             'NhoodGLCMCorrelation-1_Nuc_std_H'
%             'NhoodGLCMCorrelation-2_Nuc_std_H'
%             'NhoodGLCMCorrelation-3_Nuc_std_H'
%             'NhoodGLCMCorrelation-4_Nuc_std_H'
%             'NhoodGLCMEnergy-1_Nuc_std_H'
%             'NhoodGLCMEnergy-2_Nuc_std_H'
%             'NhoodGLCMEnergy-3_Nuc_std_H'
%             'NhoodGLCMEnergy-4_Nuc_std_H'
%             'NhoodGLCMHomogeneity-1_Nuc_std_H'
%             'NhoodGLCMHomogeneity-2_Nuc_std_H'
%             'NhoodGLCMHomogeneity-3_Nuc_std_H'
%             'NhoodGLCMHomogeneity-4_Nuc_std_H'
%             'NhoodIntensityMean_Nuc_std_H'
%             'NhoodIntensityVar_Nuc_std_H'
%             'NhoodIntensitySkew_Nuc_std_H'
%             'NhoodIntensityKurt_Nuc_std_H'
%             'NhoodSIFT-ScaleMean_Nuc_std_H'
%             'NhoodSIFT-ScaleStd_Nuc_std_H'
%             'NhoodSIFT-NroOfFrames_Nuc_std_H'
%             'NhoodLBP1_Nuc_std_H'
%             'NhoodLBP2_Nuc_std_H'
%             'NhoodLBP3_Nuc_std_H'
%             'NhoodLBP4_Nuc_std_H'
%             'NhoodLBP5_Nuc_std_H'
%             'NhoodLBP6_Nuc_std_H'
%             'NhoodLBP7_Nuc_std_H'
%             'NhoodLBP8_Nuc_std_H'
%             'NhoodLBP9_Nuc_std_H'
%             'NhoodLBP10_Nuc_std_H'
%             'NhoodMSER1_Nuc_std_H'
%             'NhoodMSER2_Nuc_std_H'
%             'NhoodMSER3_Nuc_std_H'
%             'NhoodMSER4_Nuc_std_H'
%             'HOG1_Nuc_std_H'
%             'HOG2_Nuc_std_H'
%             'HOG3_Nuc_std_H'
%             'HOG4_Nuc_std_H'
%             'HOG5_Nuc_std_H'
%             'HOG6_Nuc_std_H'
%             'HOG7_Nuc_std_H'
%             'HOG8_Nuc_std_H'
%             'HOG9_Nuc_std_H'
%             'HOG10_Nuc_std_H'
%             'HOG11_Nuc_std_H'
%             'HOG12_Nuc_std_H'
%             'HOG13_Nuc_std_H'
%             'HOG14_Nuc_std_H'
%             'HOG15_Nuc_std_H'
%             'HOG16_Nuc_std_H'
%             'HOG17_Nuc_std_H'
%             'HOG18_Nuc_std_H'
%             'HOG19_Nuc_std_H'
%             'HOG20_Nuc_std_H'
%             'HOG21_Nuc_std_H'
%             'HOG22_Nuc_std_H'
%             'HOG23_Nuc_std_H'
%             'HOG24_Nuc_std_H'
%             'HOG25_Nuc_std_H'
%             'HOG26_Nuc_std_H'
%             'HOG27_Nuc_std_H'
%             'HOG28_Nuc_std_H'
%             'HOG29_Nuc_std_H'
%             'HOG30_Nuc_std_H'
%             'HOG31_Nuc_std_H'
%             'HOG32_Nuc_std_H'
%             'HOG33_Nuc_std_H'
%             'HOG34_Nuc_std_H'
%             'HOG35_Nuc_std_H'
%             'HOG36_Nuc_std_H'
%             'HOG37_Nuc_std_H'
%             'HOG38_Nuc_std_H'
%             'HOG39_Nuc_std_H'
%             'HOG40_Nuc_std_H'
%             'HOG41_Nuc_std_H'
%             'HOG42_Nuc_std_H'
%             'HOG43_Nuc_std_H'
%             'HOG44_Nuc_std_H'
%             'HOG45_Nuc_std_H'
%             'HOG46_Nuc_std_H'
%             'HOG47_Nuc_std_H'
%             'HOG48_Nuc_std_H'
%             'HOG49_Nuc_std_H'
%             'HOG50_Nuc_std_H'
%             'HOG51_Nuc_std_H'
%             'HOG52_Nuc_std_H'
%             'HOG53_Nuc_std_H'
%             'HOG54_Nuc_std_H'
%             'HOG55_Nuc_std_H'
%             'HOG56_Nuc_std_H'
%             'HOG57_Nuc_std_H'
%             'HOG58_Nuc_std_H'
%             'HOG59_Nuc_std_H'
%             'HOG60_Nuc_std_H'
%             'HOG61_Nuc_std_H'
%             'HOG62_Nuc_std_H'
%             'HOG63_Nuc_std_H'
%             'HOG64_Nuc_std_H'
%             'HOG65_Nuc_std_H'
%             'HOG66_Nuc_std_H'
%             'HOG67_Nuc_std_H'
%             'HOG68_Nuc_std_H'
%             'HOG69_Nuc_std_H'
%             'HOG70_Nuc_std_H'
%             'HOG71_Nuc_std_H'
%             'HOG72_Nuc_std_H'
%             'HOG73_Nuc_std_H'
%             'HOG74_Nuc_std_H'
%             'HOG75_Nuc_std_H'
%             'HOG76_Nuc_std_H'
%             'HOG77_Nuc_std_H'
%             'HOG78_Nuc_std_H'
%             'HOG79_Nuc_std_H'
%             'HOG80_Nuc_std_H'
%             'HOG81_Nuc_std_H'
%             'GLCMContrast-1_Nuc_mean_E'
%             'GLCMContrast-2_Nuc_mean_E'
%             'GLCMContrast-3_Nuc_mean_E'
%             'GLCMContrast-4_Nuc_mean_E'
%             'GLCMCorrelation-1_Nuc_mean_E'
%             'GLCMCorrelation-2_Nuc_mean_E'
%             'GLCMCorrelation-3_Nuc_mean_E'
%             'GLCMCorrelation-4_Nuc_mean_E'
%             'GLCMEnergy-1_Nuc_mean_E'
%             'GLCMEnergy-2_Nuc_mean_E'
%             'GLCMEnergy-3_Nuc_mean_E'
%             'GLCMEnergy-4_Nuc_mean_E'
%             'GLCMHomogeneity-1_Nuc_mean_E'
%             'GLCMHomogeneity-2_Nuc_mean_E'
%             'GLCMHomogeneity-3_Nuc_mean_E'
%             'GLCMHomogeneity-4_Nuc_mean_E'
%             'IntensityMean_Nuc_mean_E'
%             'IntensityVar_Nuc_mean_E'
%             'IntensitySkew_Nuc_mean_E'
%             'IntensityKurt_Nuc_mean_E'
%             'NhoodGLCMContrast-1_Nuc_mean_E'
%             'NhoodGLCMContrast-2_Nuc_mean_E'
%             'NhoodGLCMContrast-3_Nuc_mean_E'
%             'NhoodGLCMContrast-4_Nuc_mean_E'
%             'NhoodGLCMCorrelation-1_Nuc_mean_E'
%             'NhoodGLCMCorrelation-2_Nuc_mean_E'
%             'NhoodGLCMCorrelation-3_Nuc_mean_E'
%             'NhoodGLCMCorrelation-4_Nuc_mean_E'
%             'NhoodGLCMEnergy-1_Nuc_mean_E'
%             'NhoodGLCMEnergy-2_Nuc_mean_E'
%             'NhoodGLCMEnergy-3_Nuc_mean_E'
%             'NhoodGLCMEnergy-4_Nuc_mean_E'
%             'NhoodGLCMHomogeneity-1_Nuc_mean_E'
%             'NhoodGLCMHomogeneity-2_Nuc_mean_E'
%             'NhoodGLCMHomogeneity-3_Nuc_mean_E'
%             'NhoodGLCMHomogeneity-4_Nuc_mean_E'
%             'NhoodIntensityMean_Nuc_mean_E'
%             'NhoodIntensityVar_Nuc_mean_E'
%             'NhoodIntensitySkew_Nuc_mean_E'
%             'NhoodIntensityKurt_Nuc_mean_E'
%             'NhoodSIFT-ScaleMean_Nuc_mean_E'
%             'NhoodSIFT-ScaleStd_Nuc_mean_E'
%             'NhoodSIFT-NroOfFrames_Nuc_mean_E'
%             'NhoodLBP1_Nuc_mean_E'
%             'NhoodLBP2_Nuc_mean_E'
%             'NhoodLBP3_Nuc_mean_E'
%             'NhoodLBP4_Nuc_mean_E'
%             'NhoodLBP5_Nuc_mean_E'
%             'NhoodLBP6_Nuc_mean_E'
%             'NhoodLBP7_Nuc_mean_E'
%             'NhoodLBP8_Nuc_mean_E'
%             'NhoodLBP9_Nuc_mean_E'
%             'NhoodLBP10_Nuc_mean_E'
%             'NhoodMSER1_Nuc_mean_E'
%             'NhoodMSER2_Nuc_mean_E'
%             'NhoodMSER3_Nuc_mean_E'
%             'NhoodMSER4_Nuc_mean_E'
%             'HOG1_Nuc_mean_E'
%             'HOG2_Nuc_mean_E'
%             'HOG3_Nuc_mean_E'
%             'HOG4_Nuc_mean_E'
%             'HOG5_Nuc_mean_E'
%             'HOG6_Nuc_mean_E'
%             'HOG7_Nuc_mean_E'
%             'HOG8_Nuc_mean_E'
%             'HOG9_Nuc_mean_E'
%             'HOG10_Nuc_mean_E'
%             'HOG11_Nuc_mean_E'
%             'HOG12_Nuc_mean_E'
%             'HOG13_Nuc_mean_E'
%             'HOG14_Nuc_mean_E'
%             'HOG15_Nuc_mean_E'
%             'HOG16_Nuc_mean_E'
%             'HOG17_Nuc_mean_E'
%             'HOG18_Nuc_mean_E'
%             'HOG19_Nuc_mean_E'
%             'HOG20_Nuc_mean_E'
%             'HOG21_Nuc_mean_E'
%             'HOG22_Nuc_mean_E'
%             'HOG23_Nuc_mean_E'
%             'HOG24_Nuc_mean_E'
%             'HOG25_Nuc_mean_E'
%             'HOG26_Nuc_mean_E'
%             'HOG27_Nuc_mean_E'
%             'HOG28_Nuc_mean_E'
%             'HOG29_Nuc_mean_E'
%             'HOG30_Nuc_mean_E'
%             'HOG31_Nuc_mean_E'
%             'HOG32_Nuc_mean_E'
%             'HOG33_Nuc_mean_E'
%             'HOG34_Nuc_mean_E'
%             'HOG35_Nuc_mean_E'
%             'HOG36_Nuc_mean_E'
%             'HOG37_Nuc_mean_E'
%             'HOG38_Nuc_mean_E'
%             'HOG39_Nuc_mean_E'
%             'HOG40_Nuc_mean_E'
%             'HOG41_Nuc_mean_E'
%             'HOG42_Nuc_mean_E'
%             'HOG43_Nuc_mean_E'
%             'HOG44_Nuc_mean_E'
%             'HOG45_Nuc_mean_E'
%             'HOG46_Nuc_mean_E'
%             'HOG47_Nuc_mean_E'
%             'HOG48_Nuc_mean_E'
%             'HOG49_Nuc_mean_E'
%             'HOG50_Nuc_mean_E'
%             'HOG51_Nuc_mean_E'
%             'HOG52_Nuc_mean_E'
%             'HOG53_Nuc_mean_E'
%             'HOG54_Nuc_mean_E'
%             'HOG55_Nuc_mean_E'
%             'HOG56_Nuc_mean_E'
%             'HOG57_Nuc_mean_E'
%             'HOG58_Nuc_mean_E'
%             'HOG59_Nuc_mean_E'
%             'HOG60_Nuc_mean_E'
%             'HOG61_Nuc_mean_E'
%             'HOG62_Nuc_mean_E'
%             'HOG63_Nuc_mean_E'
%             'HOG64_Nuc_mean_E'
%             'HOG65_Nuc_mean_E'
%             'HOG66_Nuc_mean_E'
%             'HOG67_Nuc_mean_E'
%             'HOG68_Nuc_mean_E'
%             'HOG69_Nuc_mean_E'
%             'HOG70_Nuc_mean_E'
%             'HOG71_Nuc_mean_E'
%             'HOG72_Nuc_mean_E'
%             'HOG73_Nuc_mean_E'
%             'HOG74_Nuc_mean_E'
%             'HOG75_Nuc_mean_E'
%             'HOG76_Nuc_mean_E'
%             'HOG77_Nuc_mean_E'
%             'HOG78_Nuc_mean_E'
%             'HOG79_Nuc_mean_E'
%             'HOG80_Nuc_mean_E'
%             'HOG81_Nuc_mean_E'
%             'GLCMContrast-1_Nuc_std_E'
%             'GLCMContrast-2_Nuc_std_E'
%             'GLCMContrast-3_Nuc_std_E'
%             'GLCMContrast-4_Nuc_std_E'
%             'GLCMCorrelation-1_Nuc_std_E'
%             'GLCMCorrelation-2_Nuc_std_E'
%             'GLCMCorrelation-3_Nuc_std_E'
%             'GLCMCorrelation-4_Nuc_std_E'
%             'GLCMEnergy-1_Nuc_std_E'
%             'GLCMEnergy-2_Nuc_std_E'
%             'GLCMEnergy-3_Nuc_std_E'
%             'GLCMEnergy-4_Nuc_std_E'
%             'GLCMHomogeneity-1_Nuc_std_E'
%             'GLCMHomogeneity-2_Nuc_std_E'
%             'GLCMHomogeneity-3_Nuc_std_E'
%             'GLCMHomogeneity-4_Nuc_std_E'
%             'IntensityMean_Nuc_std_E'
%             'IntensityVar_Nuc_std_E'
%             'IntensitySkew_Nuc_std_E'
%             'IntensityKurt_Nuc_std_E'
%             'NhoodGLCMContrast-1_Nuc_std_E'
%             'NhoodGLCMContrast-2_Nuc_std_E'
%             'NhoodGLCMContrast-3_Nuc_std_E'
%             'NhoodGLCMContrast-4_Nuc_std_E'
%             'NhoodGLCMCorrelation-1_Nuc_std_E'
%             'NhoodGLCMCorrelation-2_Nuc_std_E'
%             'NhoodGLCMCorrelation-3_Nuc_std_E'
%             'NhoodGLCMCorrelation-4_Nuc_std_E'
%             'NhoodGLCMEnergy-1_Nuc_std_E'
%             'NhoodGLCMEnergy-2_Nuc_std_E'
%             'NhoodGLCMEnergy-3_Nuc_std_E'
%             'NhoodGLCMEnergy-4_Nuc_std_E'
%             'NhoodGLCMHomogeneity-1_Nuc_std_E'
%             'NhoodGLCMHomogeneity-2_Nuc_std_E'
%             'NhoodGLCMHomogeneity-3_Nuc_std_E'
%             'NhoodGLCMHomogeneity-4_Nuc_std_E'
%             'NhoodIntensityMean_Nuc_std_E'
%             'NhoodIntensityVar_Nuc_std_E'
%             'NhoodIntensitySkew_Nuc_std_E'
%             'NhoodIntensityKurt_Nuc_std_E'
%             'NhoodSIFT-ScaleMean_Nuc_std_E'
%             'NhoodSIFT-ScaleStd_Nuc_std_E'
%             'NhoodSIFT-NroOfFrames_Nuc_std_E'
%             'NhoodLBP1_Nuc_std_E'
%             'NhoodLBP2_Nuc_std_E'
%             'NhoodLBP3_Nuc_std_E'
%             'NhoodLBP4_Nuc_std_E'
%             'NhoodLBP5_Nuc_std_E'
%             'NhoodLBP6_Nuc_std_E'
%             'NhoodLBP7_Nuc_std_E'
%             'NhoodLBP8_Nuc_std_E'
%             'NhoodLBP9_Nuc_std_E'
%             'NhoodLBP10_Nuc_std_E'
%             'NhoodMSER1_Nuc_std_E'
%             'NhoodMSER2_Nuc_std_E'
%             'NhoodMSER3_Nuc_std_E'
%             'NhoodMSER4_Nuc_std_E'
%             'HOG1_Nuc_std_E'
%             'HOG2_Nuc_std_E'
%             'HOG3_Nuc_std_E'
%             'HOG4_Nuc_std_E'
%             'HOG5_Nuc_std_E'
%             'HOG6_Nuc_std_E'
%             'HOG7_Nuc_std_E'
%             'HOG8_Nuc_std_E'
%             'HOG9_Nuc_std_E'
%             'HOG10_Nuc_std_E'
%             'HOG11_Nuc_std_E'
%             'HOG12_Nuc_std_E'
%             'HOG13_Nuc_std_E'
%             'HOG14_Nuc_std_E'
%             'HOG15_Nuc_std_E'
%             'HOG16_Nuc_std_E'
%             'HOG17_Nuc_std_E'
%             'HOG18_Nuc_std_E'
%             'HOG19_Nuc_std_E'
%             'HOG20_Nuc_std_E'
%             'HOG21_Nuc_std_E'
%             'HOG22_Nuc_std_E'
%             'HOG23_Nuc_std_E'
%             'HOG24_Nuc_std_E'
%             'HOG25_Nuc_std_E'
%             'HOG26_Nuc_std_E'
%             'HOG27_Nuc_std_E'
%             'HOG28_Nuc_std_E'
%             'HOG29_Nuc_std_E'
%             'HOG30_Nuc_std_E'
%             'HOG31_Nuc_std_E'
%             'HOG32_Nuc_std_E'
%             'HOG33_Nuc_std_E'
%             'HOG34_Nuc_std_E'
%             'HOG35_Nuc_std_E'
%             'HOG36_Nuc_std_E'
%             'HOG37_Nuc_std_E'
%             'HOG38_Nuc_std_E'
%             'HOG39_Nuc_std_E'
%             'HOG40_Nuc_std_E'
%             'HOG41_Nuc_std_E'
%             'HOG42_Nuc_std_E'
%             'HOG43_Nuc_std_E'
%             'HOG44_Nuc_std_E'
%             'HOG45_Nuc_std_E'
%             'HOG46_Nuc_std_E'
%             'HOG47_Nuc_std_E'
%             'HOG48_Nuc_std_E'
%             'HOG49_Nuc_std_E'
%             'HOG50_Nuc_std_E'
%             'HOG51_Nuc_std_E'
%             'HOG52_Nuc_std_E'
%             'HOG53_Nuc_std_E'
%             'HOG54_Nuc_std_E'
%             'HOG55_Nuc_std_E'
%             'HOG56_Nuc_std_E'
%             'HOG57_Nuc_std_E'
%             'HOG58_Nuc_std_E'
%             'HOG59_Nuc_std_E'
%             'HOG60_Nuc_std_E'
%             'HOG61_Nuc_std_E'
%             'HOG62_Nuc_std_E'
%             'HOG63_Nuc_std_E'
%             'HOG64_Nuc_std_E'
%             'HOG65_Nuc_std_E'
%             'HOG66_Nuc_std_E'
%             'HOG67_Nuc_std_E'
%             'HOG68_Nuc_std_E'
%             'HOG69_Nuc_std_E'
%             'HOG70_Nuc_std_E'
%             'HOG71_Nuc_std_E'
%             'HOG72_Nuc_std_E'
%             'HOG73_Nuc_std_E'
%             'HOG74_Nuc_std_E'
%             'HOG75_Nuc_std_E'
%             'HOG76_Nuc_std_E'
%             'HOG77_Nuc_std_E'
%             'HOG78_Nuc_std_E'
%             'HOG79_Nuc_std_E'
%             'HOG80_Nuc_std_E'
%             'HOG81_Nuc_std_E'
%             'Area_Nuc_mean'
%             'numberOfNucInNucNB_Nuc_mean'
%             'NhoodNucAngleVar_Nuc_mean'
%             'NhoodNucAngleSkew_Nuc_mean'
%             'NhoodNucAngleSkew0_Nuc_mean'
%             'NhoodNucAngleKurt_Nuc_mean'
%             'NhoodNucAngleKurt0_Nuc_mean'
%             'maxNucDistInNucNB_Nuc_mean'
%             'minNucDistInNucNB_Nuc_mean'
%             'meanNucDistInNucNB_Nuc_mean'
%             'stdNucDistInNucNB_Nuc_mean'
%             'NucNhoodDensityMean_Nuc_mean'
%             'NucNhoodDensityVar_Nuc_mean'
%             'NucNhoodDensitySkew_Nuc_mean'
%             'NucNhoodDensityKurt_Nuc_mean'
%             'DensityLBP1_Nuc_mean'
%             'DensityLBP2_Nuc_mean'
%             'DensityLBP3_Nuc_mean'
%             'DensityLBP4_Nuc_mean'
%             'DensityLBP5_Nuc_mean'
%             'DensityLBP6_Nuc_mean'
%             'DensityLBP7_Nuc_mean'
%             'DensityLBP8_Nuc_mean'
%             'DensityLBP9_Nuc_mean'
%             'DensityLBP10_Nuc_mean'
%             'Area_Nuc_std'
%             'numberOfNucInNucNB_Nuc_std'
%             'NhoodNucAngleVar_Nuc_std'
%             'NhoodNucAngleSkew_Nuc_std'
%             'NhoodNucAngleSkew0_Nuc_std'
%             'NhoodNucAngleKurt_Nuc_std'
%             'NhoodNucAngleKurt0_Nuc_std'
%             'maxNucDistInNucNB_Nuc_std'
%             'minNucDistInNucNB_Nuc_std'
%             'meanNucDistInNucNB_Nuc_std'
%             'stdNucDistInNucNB_Nuc_std'
%             'NucNhoodDensityMean_Nuc_std'
%             'NucNhoodDensityVar_Nuc_std'
%             'NucNhoodDensitySkew_Nuc_std'
%             'NucNhoodDensityKurt_Nuc_std'
%             'DensityLBP1_Nuc_std'
%             'DensityLBP2_Nuc_std'
%             'DensityLBP3_Nuc_std'
%             'DensityLBP4_Nuc_std'
%             'DensityLBP5_Nuc_std'
%             'DensityLBP6_Nuc_std'
%             'DensityLBP7_Nuc_std'
%             'DensityLBP8_Nuc_std'
%             'DensityLBP9_Nuc_std'
%             'DensityLBP10_Nuc_std'};
           


%     featureslist = {'nucNumber'};
% %     featureslist = {'IntensityMean_E'};
% 
%     for f = 1:length(featureslist)
%         
%         % Features.
%         opacity = 0.5;
%         featurename = featureslist{f};
%         roionly = false;
%         visualize_features_3D(inputpath,ext,inputpathmask,extmask,inputpathfeatures,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,featurename,roionly);
% %         axis off;
%         grid on;
%         set(gcf, 'Position', get(0, 'Screensize'));
%         unify_volume_orientations
% %         savefig(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'_featurevolume.fig'],'compact');
% %         saveas(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'_featurevolume.png'])
%         visualize_video_3D([outputdatapath,filesep,sampleID{i},'_',featurename,'.mp4']);
%         close all;        
% 
%     end

   
end
    
