inputdatapath='C:\Users\kimmo\Dropbox (Compbio)\CB\3D-histology\data\output';
outputdatapath='C:\Users\kimmo\Dropbox (Compbio)\BioimageInformatics\results\Visualization\3D\ECDP2018';
ext='.tif';
fullrespixelsize=0.46;
slicethickness=50;
zscaling=1;
smoothingfactor=5;
reductionlevel=5;

sampleID = {'PTENX036-011', ...
              'PTENX036-14', ...
              'PTENX036-15', ...
              'PTENX036-20', ...
              'PTENX036-32', ...
              'PTENX036-33'};
          
for i = 1:length(sampleID)
    inputpath = [inputdatapath,filesep,sampleID{i},filesep,'full_res'];
    inputpathfeatures = [inputdatapath,filesep,sampleID{i},filesep,'intermediate_results',filesep,'5_features'];
    
    % Tissue + ROI objects.
    opacity = 0.5;
    visualize_roimasks_3D(inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'alltissue');
    set(gcf, 'Position', get(0, 'Screensize'));
    savefig(gcf,[outputdatapath,filesep,sampleID{i},'_alltissue.fig'],'compact');
    visualize_video_3D([outputdatapath,filesep,sampleID{i},'_alltissue.mp4']);
    close all;
    
    % ROI objects.
    opacity = 0.5;
    visualize_roimasks_3D(inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'roiobjects');
    set(gcf, 'Position', get(0, 'Screensize'));
    savefig(gcf,[outputdatapath,filesep,sampleID{i},'_roiobjects.fig'],'compact');
    visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roiobjects.mp4']);
    close all;
    
    % ROI tissue.
    opacity = 0.9;
    visualize_roimasks_3D(inputpath,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,'roitissue');
    set(gcf, 'Position', get(0, 'Screensize'));
    savefig(gcf,[outputdatapath,filesep,sampleID{i},'_roitissue.fig'],'compact');
    visualize_video_3D([outputdatapath,filesep,sampleID{i},'_roitissue.mp4']);
    close all;
    
    % Features.
    opacity = 0.5;
    featurename = 'nucNumber';
    roionly = false;
    visualize_features_3D(inputpath,inputpathfeatures,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,featurename,roionly);
    set(gcf, 'Position', get(0, 'Screensize'));
    savefig(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'.fig'],'compact');
    visualize_video_3D([outputdatapath,filesep,sampleID{i},'_',featurename,'.mp4']);
    close all;
    
    % Features, ROI only.
    opacity = 0.5;
    featurename = 'nucNumber';
    roionly = true;
    visualize_features_3D(inputpath,inputpathfeatures,ext,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,featurename,roionly);
    set(gcf, 'Position', get(0, 'Screensize'));
    savefig(gcf,[outputdatapath,filesep,sampleID{i},'_',featurename,'_roi.fig'],'compact');
    visualize_video_3D([outputdatapath,filesep,sampleID{i},'_',featurename,'_roi.mp4']);
    close all;
end
    