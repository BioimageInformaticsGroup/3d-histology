function visualize_features_3D(inputpath,ext,inputpathmask,extmask,inputpathfeatures,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,featurename,roionly)
% visualize_features_3D - Visualize tissue features in 3D.
%
%   visualize_features_3D(inputpath,inputpathfeatures,pixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,featurename,roionly)
%   Visualizes features computed for a reconstructed 3D stack.
%
%   'inputpath' is the path to the image files.
%
%   'inputpathfeatures' is the path to the feature files.
%
%   'ext' is the file extension (e.g. '.tif').
%
%   'fullrespixelsize' is the original size of pixels in XY-plane (e.g. 0.46).
%
%   'slicethickness' is the section spacing in Z direction (e.g. 5).
%
%   'zscaling' is the amount of scaling to apply in Z direction.
%
%   'reductionlevel' is the resampling factor to apply to the
%   images (e.g. 2 corresponds to 2^2 = 4-fold subsampling).
%
%   'opacity' is the amount of opacity (0-1) applied to the tissue.
%
%   'smoothingfactor' is the size of the 3D filter applied to the features.
%
%   'featurename' is the name of the feature to visualize.
%
%   'roionly' is either True or False.

addpath(genpath('../biit-wsi-featureextraction'));

% Scale pixel size.
resamplingfactorXY = 1/(2^reductionlevel);
pixelsize = fullrespixelsize/resamplingfactorXY;

% Get sorted filenames for images and read them.
filenames = dir([inputpath,filesep,'*',ext]);
filenames = {filenames.name};
numimages = length(filenames);
slidenumber = cell(numimages,1);
% Get the last number(if multiple exist) in filename that is between - and _
for imind = 1:numimages
    startinds = regexp(filenames{imind}, '-[0-9]+_', 'start');
    endinds = regexp(filenames{imind}, '-[0-9]+_', 'end');
    slidenumber{imind} = filenames{imind}(startinds(end)+1:endinds(end)-1);
end
slidenumber = cellfun(@str2num,slidenumber);
[~,ind] = sort(slidenumber);
filenames = filenames(ind);


if roionly
    % Get sorted filenames for masks
    filenamesmask = dir([inputpathmask,filesep,'*',extmask]);
    filenamesmask = {filenamesmask.name};

    assert(length(filenamesmask) == numimages)
    slidenumber = cell(numimages,1);
    % Get the last number (if multiple exist) in filename that is between - and _ 
    for imind = 1:numimages
        startinds = regexp(filenamesmask{imind}, '-[0-9]+_', 'start');
        endinds = regexp(filenamesmask{imind}, '-[0-9]+_', 'end');
        slidenumber{imind} = filenamesmask{imind}(startinds(end)+1:endinds(end)-1);
    end
    slidenumber = cellfun(@str2num,slidenumber);
    [~,ind] = sort(slidenumber);
    filenamesmask = filenamesmask(ind);
end

% Load metadata from last feature file.
tmp = load([inputpathfeatures,filesep,'Features_',strrep(filenames{imind},ext,'.mat')],'blockcorners','labels_block','labels_nuclei','fullsize');
blockcorners = tmp.blockcorners;
fullsize = tmp.fullsize;
labels_block = tmp.labels_block;
labels_nuclei = tmp.labels_nuclei;
clear tmp;
% Figure out the number of feature blocks, features and get feature names.
featurelabels = [labels_block labels_nuclei];
numblocks = [size(blockcorners) numimages];
numblockfeatures = length(labels_block);
numnucleifeatures =  length(labels_nuclei);

% Read feature files.
features = zeros(numblocks(1),numblocks(2),numblockfeatures+numnucleifeatures,numblocks(3),'single');
for imind = 1:numimages
    tmp = load([inputpathfeatures,filesep,'Features_',strrep(filenames{imind},ext,'.mat')]);
    tmp.features_block(cellfun(@isempty,tmp.features_block)) = {nan(1,numblockfeatures,'single')};
    tmp.features_nuclei(cellfun(@isempty,tmp.features_nuclei)) = {nan(1,numnucleifeatures,'single')};
    for row = 1:numblocks(1)
        for col = 1:numblocks(2)
            features(row,col,1:numblockfeatures,imind) = tmp.features_block{row,col};
            features(row,col,numblockfeatures+1:end,imind) = tmp.features_nuclei{row,col};
        end
    end
end

% Get feature of interest.
disp(['Finding feature: ',featurename])
thisfeature = squeeze(features(:,:,strcmp(featurelabels,featurename),:));

if all(isnan(thisfeature(:)))
    disp(['Feature: ',featurename,' is NaN'])
    return 
end

% !!!! noninterpolated stack, interpolation breaks this
% highlight slides 55 and 88 (011 prostate) on 

% marg = 75;
% thisfeature(marg:end-marg, marg:end-marg, 18) = 25;
% thisfeature(marg:end-marg, marg:end-marg, 29) = 25;

% idxx = 18;
% edgy = (thisfeature(:, :, idxx) > 0) - imerode(thisfeature(:, :, idxx) > 0, strel('disk', 3)) > 0;
% plane = thisfeature(:, :, idxx);
% plane(edgy) = 0;
% thisfeature(:, :, idxx) = plane;
% idxx = 29;
% edgy = (thisfeature(:, :, idxx) > 0) - imerode(thisfeature(:, :, idxx) > 0, strel('disk', 3)) > 0;
% plane = thisfeature(:, :, idxx);
% plane(edgy) = 0;
% thisfeature(:, :, idxx) = plane;
% disp('naaakka')

% % Fig 5 pics, images of eosin and nucNumber features on 2d sclice
% % thisfeature is the whole stack of features for this mouse
% % iterate_stack(thisfeature, filenames)
% idxx = 18;
% imshow(rot90(thisfeature(:, :, idxx),2), [min(thisfeature(:)) max(thisfeature(:))]), colormap(gcf, 'jet'), colorbar
% set(gca, 'FontSize', 22)
% disp('breakpoint here')
% idxx = 29;
% imshow(rot90(thisfeature(:, :, idxx),2), [min(thisfeature(:)) max(thisfeature(:))]), colormap(gcf, 'jet'), colorbar
% set(gca, 'FontSize', 22)
% disp('breakpoint here')

% Figure out image XY size at desired resolution.
imsize = round(resamplingfactorXY*fullsize);
% Scale feature stack to desired XY resolution and isotropic Z resolution.
thisfeature = imresize3(thisfeature,[imsize(1) imsize(2) round((slicethickness/pixelsize)*size(thisfeature,3))],'linear');
slicethickness = pixelsize;
% Smooth feature stack.
thisfeature = smooth3(thisfeature,'gaussian',smoothingfactor,0.2*smoothingfactor);

if roionly
    % Read ROI masks.
    roimasks = zeros(imsize(1),imsize(2),numimages,'uint8');
    for imind = 1:numimages
        tmp = biitwsi_imread([inputpathmask,filesep,filenamesmask{imind}],'ReductionLevel',reductionlevel);
        tmp = imresize(tmp,[imsize(1) imsize(2)],'nearest');
        roimasks(:,:,imind,:) = tmp;
    end
    
    % Interpolate original mask stack to isotropic resolution along Z axis.
    roimasks = imresize3(roimasks,[size(thisfeature,1) size(thisfeature,2) size(thisfeature,3)],'nearest');
    % Smooth objects using morphological operations.
    se = strel('sphere',smoothingfactor);
    roimasks_smooth = roimasks > 0;
    roimasks_smooth = imclose(roimasks_smooth,se);
    % Set feature values outside objects as NaN.
    thisfeature(~roimasks_smooth) = NaN;
end

% % !!!! interpolated stack
% val = max(thisfeature(:)) + max(thisfeature(:))*0.01;
% idxx = 31; % 18 * slicethickness/pixelsize:   30.5707
% edgy = (thisfeature(:, :, idxx) > 0) - imerode(thisfeature(:, :, idxx) > 0, strel('disk', 1)) > 0;
% plane = thisfeature(:, :, idxx);
% plane(edgy) = val;
% thisfeature(:, :, idxx) = plane;
% idxx = 49; % 29 * slicethickness/pixelsize:   49.2527
% edgy = (thisfeature(:, :, idxx) > 0) - imerode(thisfeature(:, :, idxx) > 0, strel('disk', 1)) > 0;
% plane = thisfeature(:, :, idxx);
% plane(edgy) = val;
% thisfeature(:, :, idxx) = plane;
% disp('naaakka')


% Render the volume and adjust its opacity.
h = vol3d('cdata',thisfeature,'texture','3D');
alphamap('rampup');
alphamap(opacity*alphamap);
alphamap('decrease');
colormap(gca,jet);
colorbar;

% disp('REMEMBER ME')
% mcmap = jet(256);
% mcmap(end, :) = zeros(1, 3);
% colormap(mcmap)
% colorbar;


% Adjust axes.
view(3);  
axis tight;
grid on; grid minor;
% flip Z axis
% set(gca,'Zdir','reverse')
% flip x axis to have data as it was originally
set(gca,'Xdir','reverse')

% Adjust Z-scaling.
daspect([1 1 zscaling*pixelsize/slicethickness]);

% Set tick marks as millimeters.
xticks((1/pixelsize)*[0 2000 4000 6000 8000 10000 12000]); xticklabels({'0','2','4','6','8','10','12'}); xlabel('mm');
yticks((1/pixelsize)*[0 2000 4000 6000 8000 10000 12000]); yticklabels({'0','2','4','6','8','10','12'}); ylabel('mm');
zticks((1/slicethickness)*[0 500 1000 1500]); zticklabels({'0','0.5','1','1.5'}); zlabel('mm');
set(gca,'FontSize',17) 


