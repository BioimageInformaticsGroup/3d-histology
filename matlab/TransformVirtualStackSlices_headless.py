# @String source_dir
# @String target_dir
# @String transforms_dir
# @boolean interpolate

from register_virtual_stack import Transform_Virtual_Stack_MT

# Run the transformations.
Transform_Virtual_Stack_MT.exec(source_dir, target_dir, transforms_dir, interpolate)