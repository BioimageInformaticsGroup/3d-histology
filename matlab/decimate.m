inputdir = '/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/PTENX036-14/tumormasks';
outputdir = '/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/PTENX036-14/tumormasks_subsampled10';

if exist(outputdir, 'dir') == 0
    mkdir(outputdir)
end

fns = dir([inputdir, filesep, '*png']);
for f = fns'
    inpath = [inputdir, filesep, f.name];
    outpath = [outputdir, filesep, f.name];
    disp(['Reading path ', inputdir])
    im = imread(inpath); 
    subim = im(1:10:end, 1:10:end); 
    imwrite(subim, outpath);
end
