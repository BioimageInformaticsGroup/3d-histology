% Add codes to path.
addpath(genpath('../biit-wsi-featureextraction'));

if numcores > 1
    myCluster = parcluster('local');
    myCluster.NumWorkers = numcores;
    % Then pool to local cluster
    parpool(myCluster.NumWorkers);
else
    ps = parallel.Settings;
    ps.Pool.AutoCreate = false;
end

% if numcores > 1
%    parpool(numcores);
%    %pc = parcluster('local');
%    %pool = parpool(pc,numcores);
%    %pctRunOnAll warning off;
% else
%    ps = parallel.Settings;
%    ps.Pool.AutoCreate = false;
%   %warning('off','all');
% end

% Extract features.
% Path to data.
%inputpath = '/bmt-data/bii/3D-histology/output/PTENX036-40';
ext = '.tif';
% Generate output folder if it doesnt exist.
if ~exist(outputpath,'dir')
    mkdir(outputpath);
end

% Feature extraction settings.
settings.fullrespixelsize = 0.46;
settings.reductionlevel = 4;
settings.reductionlevelcomp = 0;
settings.blocksize = 200; % In um.
settings.stride = 200; % In um.
settings.nucnhoodsize = 100; % In um.
settings.mintissueinblock = 0.25;
settings.nucleuslim1 = 5; % In um
settings.nucleuslim2 = 30; % In um.
settings.maskbackground = false;

% Process image
tic;
[inputpath,inputfilename,~] = fileparts(inputfilepath);
tissuemaskname = [tissuemaskpath,filesep,inputfilename,'.png'];
tissuemaskname = strrep(tissuemaskname,'Transformed_','Transformed_TissueMask_');
outputname = [outputpath,filesep,'Features_',inputfilename,'.mat'];
biitwsi_extractFeatures(inputfilepath,tissuemaskname,outputname, ...
'fullrespixelsize',settings.fullrespixelsize,'reductionlevel',settings.reductionlevel,'reductionlevelcomp',settings.reductionlevelcomp, ...
'blocksize',settings.blocksize,'stride',settings.stride,'nucnhoodsize',settings.nucnhoodsize,'mintissueinblock',settings.mintissueinblock, ...
'nucleuslim1',settings.nucleuslim1,'nucleuslim2',settings.nucleuslim2,'maskbackground',settings.maskbackground);
t = toc; disp(['Image ',inputfilepath,', ',num2str(t),' s']);

