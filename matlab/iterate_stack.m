function iterate_stack(images, filenames)

images(isnan(images)) = 0;

for imind = 1:size(images, 3)
    imshow(images(:, :, imind), [])
    title(filenames{imind}, 'Interpreter', 'none')
%     colormap jet
    pause
end

end