function [runsuccess] = reconstruct_RegisterVirtualStackSlices(inputpath_images,inputpath_masks,inputpath_fiducials,outputpath_images,outputpath_masks,outputpath_fiducials,transformpath,temppath,reference_name,registration_model_index,ransac,sift,bunwarpj)
% reconstruct_RegisterVirtualStackSlices - Form 3D reconstruction using RegisterVirtualStackSlices plugin for ImageJ.
%
%   [runsuccess] = reconstruct_RegisterVirtualStackSlices(inputpath_images,inputpath_masks,inputpath_fiducials,outputpath_images,outputpath_masks,outputpath_fiducials,transformpath,temppath,reference_name,registration_model_index,ransac,sift,bunwarpj)
%   Co-registers a stack of images using RegisterVirtualStackSlices plugin 
%   for ImageJ. The ImageJ-MATLAB interfacing package and Fiji installation
%   are required for running the code. Returns 1 if the run was successful,
%   otherwise returns zero.
%   
%   General settings:
%       'inputpath_images' is the path to the actual input image files.
%
%       'inputpath_masks' is the path to binary mask files specifying the
%       region of interest in each actual image.
%
%       'inputpath_fiducials' is the path to the fiducial image files
%       containing fiducial markers for the images.
%
%       'outputpath_images' is the path for saving the actual registered image files.
%
%       'outputpath_masks' is the path for saving registered binary mask
%       images.
%
%       'outputpath_fiducials' is the path for saving the registered fiducial
%       image files.
%
%       'transformpath' is the path for saving the estimated transformations as
%       .xml files.
%
%       'temppath' is the path for saving temporary files.
%   
%       'reference_name' is the name of the image file used as the reference image.
%
%       'registration_model_index' specifies which model (and method) to
%       use for transformations. 0=translation, 1=rigid, 2=similarity,
%       3=affine, 4=elastic (bUnwarpJ), 5=moving least squares.
%
%   ransac: a struct with the following fields:
%       'features_model_index' specifies which model to use for RANSAC.
%       0=translation, 1=rigid, 2=similarity, 3=affine.
%
%       'rod' is the maximum closest/next closest match distance ratio for
%       excluding ambiguous feature matches. Default 0.92.
%
%       'min_inlier_ratio' is the minimum ratio of inliers for an accepted
%       model. Models with less inliers are rejected. Default 0.05.
%
%       'max_epsilon' is the maximum error relative to the model allowed
%       for inliers (in pixels). Default 25 pix (Saaldeld: 10 % of image size).
%
%   sift: a struct with the following fields:
%       'fd_bins' is the number of orientation bins. Default 8.
%
%       'fd_size' is the size of the descriptor (pixels). Default 4.
%
%       'initial_sigma' is the initial Gaussian blur std (pixels). Default 1.6.
%
%       'max_octave_size' is the largest scale to use. Decrease this to
%       exclude smaller objects. Default 1024 pixels.
%
%       'min_octave_size' is the minimum scale to use. Increase this to
%       exclude larger shapes. Default 64 pixels.
%
%       'steps' is the number of steps per scale octave. Default 3.
%
%   bunwarpj: a struct with the following fields:
%       'mode' specifies which mode to use. 0=Fast, 1=Accurate, 2=Mono.
%       Fast and accurate modes encourage transformations which are
%       consistent in both directions.
%
%       'img_subsamp_fact' specifies how much downsampling to apply to the
%       images for calculations. The transformations are still applied to
%       the full-res images. From 0 to 7, representing 2^0=1 to 2^7 = 128.
%
%       'consistency_weight' specifies the relative weight assigned to the
%       bidirectional consistency of the result. Default 10.
%
%       'curl_weight' specifies the relative weight assigned to the low
%       curl of the result. Default 0.1.
%
%       'div_weight' specifies the relative weight assigned to the low
%       divergence of the result. Default 0.1.
%
%       'image_weight' specifies the relative weight assigned to pixel-wise
%       image similarity. Default 1.
%
%       'landmark_weight' specifies the relative weight assigned to
%       landmark correspondence. Default 0.1.
%
%       'min_scale_deformation' specifies the initial deformation field.
%       0=Very coarse, 1=Coarse, 2=Fine, 3=Very fine. Start with 2-3 if
%       the images are already in good alignment. Start with 0-1 if the 
%       initial alignment is poor.
%
%       'max_scale_deformation' specifies the final deformation field.
%       0=Very coarse, 1=Coarse, 2=Fine, 3=Very fine, 4=Super fine. Usually
%       3 works well, 4 might be too slow.
%
%       'stop_threshold' specifies the stopping threshold for iterations.
%       Default 0.01.
%

% Check if fiducial images should be transformed.
if isempty(inputpath_fiducials) || isempty(outputpath_fiducials)
    outputfiducials = false;
else
    outputfiducials = true;
end
% Check if mask images should be transformed.
if isempty(inputpath_masks) || isempty(outputpath_masks)
    outputmasks = false;
else
    outputmasks = true;
end

% Shrinking constraint regularization is always disabled. It does not even
% work for the elastic method.
shrinking_constraint = 'false';
% Interpolation for forming the output images is always enabled.
interpolate = 'true';

% Start ImageJ.
if ~exist('MIJ','var')
    addpath('C:\Users\kimmo\fiji-win64\Fiji.app\scripts');
    ImageJ(true,false);
end

% Get the filenames of input images.
[filenames_images, ~, filenames_fiducials] = util_getSortedFilenames(inputpath_images,inputpath_masks,inputpath_fiducials,'.tif');
numimages = length(filenames_images);

% Add file separator to paths, ImageJ wants it.
[inputpath_images,outputpath_images,transformpath,temppath] = util_addFilesep(inputpath_images,outputpath_images,transformpath,temppath);
if outputmasks == true
    [inputpath_masks,outputpath_masks] = util_addFilesep(inputpath_masks,outputpath_masks);
end
if outputfiducials == true
    [inputpath_fiducials,outputpath_fiducials] = util_addFilesep(inputpath_fiducials,outputpath_fiducials);
end

% Collect all parameters into a string.
macroParameters = ['source_dir=',inputpath_images,' target_dir=',outputpath_images,' transforms_dir=',transformpath,' reference_name=',reference_name,' interpolate=',interpolate, ...
                   ' use_shrinking_constraint=',shrinking_constraint,' registration_model_index=',num2str(registration_model_index),' features_model_index=',num2str(ransac.features_model_index), ...
                   ' rod=',num2str(ransac.rod),' min_inlier_ratio=',num2str(ransac.min_inlier_ratio),' max_epsilon=',num2str(ransac.max_epsilon), ...
                   ' fd_bins=',num2str(sift.fd_bins),' fd_size=',num2str(sift.fd_size),' initial_sigma=',num2str(sift.initial_sigma),' max_octave_size=',num2str(sift.max_octave_size),' min_octave_size=',num2str(sift.min_octave_size),' steps=',num2str(sift.steps), ...
                   ' mode=',num2str(bunwarpj.mode),' img_subsamp_fact=',num2str(bunwarpj.img_subsamp_fact),' consistency_weight=',num2str(bunwarpj.consistency_weight),' curl_weight=',num2str(bunwarpj.curl_weight), ...
                   ' div_weight=',num2str(bunwarpj.div_weight),' image_weight=',num2str(bunwarpj.image_weight),' landmark_weight=',num2str(bunwarpj.landmark_weight),' max_scale_deformation=',num2str(bunwarpj.max_scale_deformation), ...
                   ' min_scale_deformation=',num2str(bunwarpj.min_scale_deformation),' stop_threshold=',num2str(bunwarpj.stop_threshold)];

% Run registration plugin.
try
    MIJ.run('RegisterVirtualStackSlices headless', macroParameters);
catch
    MIJ.run('Close All');
    runsuccess = 0;
    return;
end
% This is a hack to get the Boolean success indicator as a one-pixel image
% from ImageJ. RVSS does not necessarily raise an exception even if it
% fails, so this is needed to make sure the script made it to the end.
runsuccess = MIJ.getImage('runsuccess');
runsuccess = runsuccess > 0;
MIJ.run('Close All');
if runsuccess == 0
    return;
end

% Verify that no excessive 'tissue drifting' has taken place. This can
% cause HUGE output images. Checking the first image should be enough.
inputinfo = imfinfo([inputpath_images,filenames_images{1}]);
inputsize = [inputinfo.Width inputinfo.Height];
outputinfo = imfinfo([outputpath_images,filenames_images{1}]);
outputsize = [outputinfo.Width outputinfo.Height];
if outputsize(1) > 5*inputsize(1) || outputsize(2) > 5*inputsize(2)
    % Clear the output image folder and stop.
    warning(['Output image size ',num2str(outputsize(1)),'x',num2str(outputsize(2)),' exceeds limit ',num2str(5*inputsize(1)),'x',num2str(5*inputsize(2)),', aborting!']);
    delete([outputpath_images,'*.tif']);
    MIJ.run('Close All');
    runsuccess = 0;
    return;
end

% Verify that all the output images have equal dimensions. This should
% always be the case but due to some nasty bug (?) in RVSS plugin the
% images sometimes do not get resized to a common canvas. In that case, run
% the TransformVirtualStackSlices plugin using the saved transforms.
imsize = zeros(numimages,2);
for imind = 1:numimages
    info = imfinfo([outputpath_images,filenames_images{imind}]);
    imsize(imind,:) = [info.Width info.Height];
end
if size(unique(imsize,'rows'),1) > 1
    % Clear the output image folder.
    delete([outputpath_images,'*.tif']);
    % Re-transform the images using the saved transformations.
    MIJ.run('TransformVirtualStackSlices headless',['source_dir=',inputpath_images,' target_dir=',outputpath_images,' transforms_dir=',transformpath,' interpolate=true']);
    MIJ.run('Close All');
end

% Transform masks.
if outputmasks == true
    MIJ.run('TransformVirtualStackSlices headless',['source_dir=',inputpath_masks,' target_dir=',outputpath_masks,' transforms_dir=',transformpath,' interpolate=false']);
    MIJ.run('Close All');
end

% Transform fiducial images.
if outputfiducials == true
    numfiducialimages = length(filenames_fiducials);
    % There is one fiducial image per image.
    if numfiducialimages == numimages
        % Transform the fiducial images.
        MIJ.run('TransformVirtualStackSlices headless',['source_dir=',inputpath_fiducials,' target_dir=',outputpath_fiducials,' transforms_dir=',transformpath,' interpolate=false']);
        MIJ.run('Close All');
    % There are two fiducial images per image.
    elseif numfiducialimages == 2*numimages-2
        % Copy the first half of the fiducial images into temp folder.
        for imind = [1 2:2:numfiducialimages]
            [success,message,~] = copyfile([inputpath_fiducials,filenames_fiducials{imind}],temppath);
            if success == 0
                error(['Copying fiducial image to temp folder failed: ',message]);
            end
        end

        % Transform the fiducial images.
        MIJ.run('TransformVirtualStackSlices headless',['source_dir=',temppath,' target_dir=',outputpath_fiducials,' transforms_dir=',transformpath,' interpolate=false']);
        MIJ.run('Close All');

        % Clear the temp folder.
        delete([temppath,'*.tif']);

        % Copy the second half of the fiducial images into temp folder.
        for imind = [1 3:2:numfiducialimages numfiducialimages]
            [success,message,~] = copyfile([inputpath_fiducials,filenames_fiducials{imind}],temppath);
            if success == 0
                error(['Copying fiducial image to temp folder failed: ',message]);
            end
        end

        % Transform the fiducial images.
        MIJ.run('TransformVirtualStackSlices headless',['source_dir=',temppath,' target_dir=',outputpath_fiducials,' transforms_dir=',transformpath,' interpolate=false']);
        MIJ.run('Close All');

        % Clear the temp folder.
        delete([temppath,'*.tif']);
    else
        error(['The number of images ',num2str(numimages),' and fiducial images ',num2str(numfiducialimages),' does not match!']);
    end
end