coeff = 0.2;
aaa = axis;
aaa(1:2) = [abs(diff(aaa(1:2)))*coeff/2 -abs(diff(aaa(1:2)))*coeff/2] + aaa(1:2);
aaa(3:4) = [abs(diff(aaa(3:4)))*coeff/2 -abs(diff(aaa(3:4)))*coeff/2] + aaa(3:4);
axis(int32(aaa));

