function centroid = imageCentroid(image)

image = double(image);

[X,Y,Z] = meshgrid(1:size(image, 2), 1:size(image, 1), 1:size(image, 3));

imsum = sum(image(:));

xw = image.*X;
xc = sum(xw(:)) / imsum;

yw = image.*Y;
yc = sum(yw(:)) / imsum;

zw = image.*Z;
zc = sum(zw(:)) / imsum;

centroid = [yc, xc, zc];

end
