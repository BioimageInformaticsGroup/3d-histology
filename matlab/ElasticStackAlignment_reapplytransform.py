# @String source_dir
# @String target_dir
# @String projectfile_path
# @boolean isimagecolor

import os
from ij import IJ, ImagePlus
from java.awt import Color
from ini.trakem2 import ControlWindow, Project
from ini.trakem2.display import Layer, Patch

# Disable GUI.
ControlWindow.setGUIEnabled(0)

# If any projects are open, close them.
allprojects = Project.getProjects()
for project in allprojects:
  project.remove()

# Open a TrakEM2 project.
project = Project.openFSProject(projectfile_path,False)

#  Get the filenames of input images.
filenames = os.listdir(source_dir)
filenames.sort()
numfiles = len(filenames)

# Output images.
layerset = project.getRootLayerSet()
layerBounds = layerset.get2DBounds()
scale = 1.0
backgroundcolor = Color.black
for i,layer in enumerate(layerset.getLayers()):
  patches = layer.getDisplayables(Patch,layerBounds)
  if isimagecolor:
    ip = Patch.makeFlatImage(ImagePlus.COLOR_RGB,layer,layerBounds,scale,patches,backgroundcolor,True)
  else:
    ip = Patch.makeFlatImage(ImagePlus.GRAY8,layer,layerBounds,scale,patches,backgroundcolor,True)
  imp = ImagePlus(filenames[i],ip)
  filepath = os.path.join(target_dir,filenames[i])
  IJ.saveAsTiff(imp,filepath)

# Close the project.
project.remove()












