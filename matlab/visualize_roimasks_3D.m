function visualize_roimasks_3D(inputpath,ext,inputpathmask,extmask,fullrespixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,plotmode,POIs)
% visualize_roimasks_3D - Visualize mask stack in 3D.
%
%   visualize_roimasks_3D(inputpath,pixelsize,slicethickness,zscaling,reductionlevel,opacity,smoothingfactor,plotmode)
%   Visualizes a reconstructed 3D mask stack.
%
%   'inputpath' is the path to the image files.
%
%   'ext' is the file extension (e.g. '.tif').
%
%   'fullrespixelsize' is the original size of pixels in XY-plane (e.g. 0.46).
%
%   'slicethickness' is the section spacing in Z direction (e.g. 5).
%
%   'zscaling' is the amount of scaling to apply in Z direction.
%
%   'reductionlevel' is the resampling factor to apply to the
%   images (e.g. 2 corresponds to 2^2 = 4-fold subsampling).
%
%   'opacity' is the amount of opacity (0-1) applied to the tissue.
%
%   'smoothingfactor' is the size of the 3D filter applied to the masks.
%
%   'plotmode' is either 'roiobjects', 'roitissue' or 'alltissue'.

addpath(genpath('../biit-wsi-featureextraction'));

% Scale pixel size.
resamplingfactorXY = 1/(2^reductionlevel);
pixelsize = fullrespixelsize/resamplingfactorXY;

% this is used in setting the aspect ratio of a plot at the end of this
% script and also in pca axes plotting
xyz_aspect = [1 1 zscaling*pixelsize/slicethickness];

% Get sorted filenames for images and read them.
filenames = dir([inputpath,filesep,'*',ext]);
filenames = {filenames.name};
numimages = length(filenames);
slidenumber = cell(numimages,1);
% Get the last number (if multiple exist) in filename that is between - and _ 
for imind = 1:numimages
    startinds = regexp(filenames{imind}, '-[0-9]+_', 'start');
    endinds = regexp(filenames{imind}, '-[0-9]+_', 'end');
    slidenumber{imind} = filenames{imind}(startinds(end)+1:endinds(end)-1);
end
slidenumber = cellfun(@str2num,slidenumber);
[~,ind] = sort(slidenumber);
filenames = filenames(ind);

% Get sorted filenames for masks
filenamesmask = dir([inputpathmask,filesep,'*',extmask]);
filenamesmask = {filenamesmask.name};
assert(length(filenamesmask) == numimages)
slidenumber = cell(numimages,1);
% Get the last number (if multiple exist) in filename that is between - and _ 
for imind = 1:numimages
    startinds = regexp(filenamesmask{imind}, '-[0-9]+_', 'start');
    endinds = regexp(filenamesmask{imind}, '-[0-9]+_', 'end');
    slidenumber{imind} = filenamesmask{imind}(startinds(end)+1:endinds(end)-1);
end
slidenumber = cellfun(@str2num,slidenumber);
[~,ind] = sort(slidenumber);
filenamesmask = filenamesmask(ind);


% Read images, resampling in XY if required.
info = imfinfo([inputpath,filesep,filenames{1}]);
imsize = [round(info(1).Height*resamplingfactorXY) round(info(1).Width*resamplingfactorXY)];
images = zeros(imsize(1),imsize(2),numimages,3,'uint8');
% parfor imind = 1:numimages
for imind = 1:numimages
    tmp = biitwsi_imread([inputpath,filesep,filenames{imind}],'ReductionLevel',reductionlevel);
    tmp = imresize(tmp,[imsize(1) imsize(2)],'nearest');
    if size(tmp, 3) == 1
        isblack = tmp(:,:,1) == 0;
    else
        isblack = tmp(:,:,1) == 0 & tmp(:,:,2) == 0 & tmp(:,:,3) == 0;
    end
%     isblack = imdilate(isblack,strel('square',round(200*resamplingfactorXY)));
    if size(tmp, 3) == 1
        tmp(isblack) = 255;
        tmp = repmat(tmp,1,1,3);
    else
        tmp(repmat(isblack,1,1,3)) = 255;
    end
    images(:,:,imind,:) = tmp;
end
clear tmp;


% remove bubbles from the background
images_size = size(images);
imsize = images_size([1,2,4]);
imsize2 = images_size;
imsize2(3) = 1;
for imind = 1:numimages
    disp(['Segmenting tissue & removing background ', num2str(imind), '/', num2str(numimages)])
    im = reshape(images(:, :, imind, :), imsize);

    % Convert image to grayscale.    
    V = rgb2gray(im);    

    % Apply Laplacian to detect regions with structures.    
    V = abs(imfilter(V,fspecial('laplacian')));    

    % Thresholding to get regions with variation.    
    BW = imbinarize(V,graythresh(V));    

    % Closing for some smoothing.
    BW = imclose(BW,strel('disk',5));
    BW = imopen(BW,strel('disk',5));

    % Fill holes.    
    BW = imfill(BW,'holes');    

    % Remove small objects.
%     BW = bwareaopen(BW,200);
    
    % preserve only largest object
    stats = regionprops(BW, 'Area', 'PixelIdxList');
    fmax = 0;
    fmaxind = 0;
    for i = 1:length(stats)
        cmax = max(stats(i).Area);
        if fmax < cmax
           fmax = cmax;
           fmaxind = i;
        end
    end
    BW = zeros(size(BW), 'logical');
    BW(stats(fmaxind).PixelIdxList) = 1;
    
%     figure()
%     subplot 121
%     imshow(im)
%     subplot 122
%     imshow(BW)
%     pause;
    
    % if we wish to plot only tissue edges
    % use erosion to keep only edges
    if strcmp(plotmode, 'tissueedges')
        BW = BW - imerode(BW, strel('disk', 3));
    end

%     imshow(final_mask)
%     pause
    final_mask = repmat(~BW, [1, 1, 3]);
    im(final_mask) = 255;
%     imshow(im)
%     pause

    images(:, :, imind, :) = reshape(im, imsize2);

%     imshow(im);
%     pause;
end


% Read ROI masks.
roimasks = zeros(imsize(1),imsize(2),numimages,'uint8');
for imind = 1:numimages
    tmp = biitwsi_imread([inputpathmask,filesep,filenamesmask{imind}],'ReductionLevel',reductionlevel);
    tmp = imresize(tmp,[imsize(1) imsize(2)],'nearest');
    roimasks(:,:,imind,:) = tmp;
end

% preserve only the largest connected component for each ROI
% Find out number of separate lesions and their labels.
roilabels = unique(roimasks);
roilabels = roilabels(2:end);
for ind = 1:length(roilabels)
   l = roilabels(ind);
   cc = bwconncomp(roimasks == l, 6);
   numcomp = length(cc.PixelIdxList);
   % find largest connected component within lesion
   lengths = zeros(1, numcomp);
   for si = 1:numcomp % skip background by starting from 2
       lengths(si) = length(cc.PixelIdxList{si});
   end
   [~, preserved_label] = max(lengths);
   % remove original tumor from roimasks
   roimasks(roimasks == l) = 0;
   % store only the largest component back with the same label
   roimasks(cc.PixelIdxList{preserved_label}) = l;
end

numrois = length(roilabels);
% Get colors, one per lesion.
colors = distinguishable_colors(numrois,'w');
%colors = lines(numrois);


% % highlight slides 55 and 88 (011 prostate) on 
% marg = 100;
% images(marg:end-marg, marg:end-marg, 18, :) = 0;
% images(marg:end-marg, marg:end-marg, 29, :) = 0;


% Interpolate original mask stack to isotropic resolution along Z axis.
roimasks = imresize3(roimasks,[size(roimasks,1) size(roimasks,2) round((slicethickness/pixelsize)*size(roimasks,3))],'nearest');
% Initialize another array for smoothed masks.
roimasks_smooth = zeros(size(roimasks),'uint8');
% Smooth each object in the masks.
for roiind = 1:numrois
    % Get the current object as binary mask.
    V = roimasks == roilabels(roiind);
    
    % Smooth object using morphological operations.
    %se = strel('cuboid',[smoothingfactor smoothingfactor smoothingfactor]);
    
    se = strel('sphere',smoothingfactor);
    V = imclose(V,se);

%     V = logical(smooth3(V, 'gaussian', smoothingfactor));

    % Insert smoothed object into array with the right label.
    roimasks_smooth(V) = roilabels(roiind);
end
% Scale the array down to original size.
roimasks_smooth = imresize3(roimasks_smooth,[imsize(1) imsize(2) numimages],'nearest');

figure; hold on;
switch plotmode
    case 'roiobjects'
%         pcaplots = {};
        % Visualize as isosurface.
        for roiind = 1:numrois
            % Get the current object as binary mask.
            V = roimasks_smooth == roilabels(roiind);
            isosurfs = isosurface(V,0);
            p(roiind) = patch(isosurfs,'FaceColor',colors(roiind,:),'EdgeColor','none');
            isonormals(V,p(roiind));
%             % print roi label next to roi object
%             [yy, xx, zz] = find(V);
%             yy = max(yy(:));
%             xx = max(xx(:));
%             zz = max(zz(:));
% %             text(xx, yy, zz, roiind)
% %             disp(yy)
% %             disp(xx)
% %             disp(zz)

            % visualize pca axes
            % find volume center of mass
            com = imageCentroid(V);
            [I, J, K] = ind2sub(size(V),find(V));
            Xcoords = [I / xyz_aspect(2), J / xyz_aspect(1), K / xyz_aspect(3)];
            coeff = pca(Xcoords);
            extents = [100, 100, 100];
            plotcolors = ['r', 'g', 'b'];
            for pcind = 1:3
                X = [com(2) - extents(pcind)*coeff(2, pcind)*xyz_aspect(2); com(2) + extents(pcind)*coeff(2, pcind)*xyz_aspect(2)];
                Y = [com(1) - extents(pcind)*coeff(1, pcind)*xyz_aspect(1); com(1) + extents(pcind)*coeff(1, pcind)*xyz_aspect(1)];
                Z = [com(3) - extents(pcind)*coeff(3, pcind)*xyz_aspect(3); com(3) + extents(pcind)*coeff(3, pcind)*xyz_aspect(3)];
%                 pcaplots{roiind, pcind} = [X, Y, Z];
%                 plot3(X, Y, Z, plotcolors(pcind), 'LineWidth', 2)
                plot3(X, Y, Z, plotcolors(pcind), 'LineWidth', 1)
                set(gca, 'FontSize', 17)
            end
            
        end
        
        % Adjust lighting and axes.
        lightangle(0,70);
        lightangle(90,70);
        lightangle(180,70);
        lightangle(270,70);
        lighting gouraud;
        
        % Add legend.
        legend(p, num2str(roilabels));
        
        
    case 'roiobject_pictures'
        % Visualize as isosurface.
        for roiind = 1:numrois
            close;
            figure; hold on;
            % Get the current object as binary mask.
            V = roimasks_smooth == roilabels(roiind);
            isosurfs = isosurface(V,0);
            p = patch(isosurfs,'FaceColor',colors(roiind,:),'EdgeColor','none');
            isonormals(V,p);
%             % print roi label next to roi object
%             [yy, xx, zz] = find(V);
%             yy = max(yy(:));
%             xx = max(xx(:));
%             zz = max(zz(:));
% %             text(xx, yy, zz, roiind)
% %             disp(yy)
% %             disp(xx)
% %             disp(zz)
            
            view(3);  
            axis tight;
            grid on; grid minor;
            % flip Z axis
            set(gca,'Zdir','reverse')
            % flip x axis to have data as it was originally
            set(gca,'Xdir','reverse')

            % Adjust Z-scaling.
            daspect([1 1 zscaling*pixelsize/slicethickness]);
            set(gcf, 'Position', get(0, 'Screensize'));
            drawnow;
            pause

        end
        
        % Adjust lighting and axes.
        lightangle(0,70);
        lightangle(90,70);
        lightangle(180,70);
        lightangle(270,70);
        lighting gouraud;
        
        % Add legend.
        legend(num2str(roilabels));
        
    
    case 'roitissue'
        % Remove tissue outside the lesions.
        roimaskszero = repmat(roimasks_smooth == 0,1,1,1,3);
        images(roimaskszero) = 255;

        % Render the volume and adjust its opacity.
        h = vol3d('cdata',images,'texture','3D');
        alphamap('rampup');
        alphamap(opacity*alphamap);
        alphamap('decrease');
        
    case 'alltissue'
        % Visualize as isosurface.
        for roiind = 1:numrois
            % Get the current object as binary mask.
            V = roimasks_smooth == roilabels(roiind);
            isosurfs = isosurface(V,0);
            p = patch(isosurfs,'FaceColor',colors(roiind,:),'EdgeColor','none');
            isonormals(V,p);
        end
        

        
        % Render the volume and adjust its opacity.
        h = vol3d('cdata',images,'texture','3D');
        alphamap('rampup');
        alphamap(opacity*alphamap);
        alphamap('decrease');
        
        % Add legend.
        legend(num2str(roilabels));
    
    case 'tissueedges'
        
        % Visualize as isosurface.
        for roiind = 1:numrois
            % Get the current object as binary mask.
            V = roimasks_smooth == roilabels(roiind);
            isosurfs = isosurface(V,0);
            p = patch(isosurfs,'FaceColor',colors(roiind,:),'EdgeColor','none');
            isonormals(V,p);
        end
        
%         % Adjust lighting and axes.
%         lightangle(0,70);
%         lightangle(90,70);
%         lightangle(180,70);
%         lightangle(270,70);
%         lighting gouraud;
        
        % remove any tissue inside tumors 
        images(cat(4,V,V,V)) = 255;
        
        % Render the volume and adjust its opacity.
        h = vol3d('cdata',images,'texture','3D');
        alphamap('rampup');
        alphamap(opacity*alphamap);
        alphamap('decrease');
        
        
        % draw points of interests
        for p = 1:length(POIs)
            fn = POIs{p}{2};
            [startIndex, endIndex] = regexp(lower(fn), 'ptenx.*?_');
            matchstr = lower(fn(startIndex:endIndex));
            for imind = 1:length(filenames)
                fn2 = filenames(imind);
                matches = regexp(lower(fn2), matchstr, 'once');
                if ~isempty(matches{1})
                    disp('=== Matched: ')
                    disp(matchstr)
                    disp(fn2)
                    
                    X = POIs{p}{3};
                    Y = POIs{p}{4}; 
                    Z = imind;
                    
                    % scale points to correct size
                    X = X * (1/10);
                    Y = Y * (1/10);

                    scatter3(X, Y, Z, 400, 'rx', 'LineWidth', 5)
                    break;
                end
            end
        end
        
        % Legend
        leg = {};
        start_char = 96;
        for i = 1:length(roilabels)
            strlab = num2str(char(start_char + i));
            leg{i} = strlab;
        end
        leg{i+1} = 'anatomical center';
        h = legend(leg);
        set(h, 'FontSize', 20)
        
end

view(3);  
axis tight;
grid on; grid minor;
% flip Z axis. Do not flip to stay consistent with previous publications
% set(gca,'Zdir','reverse')
% flip x axis to have data as it was originally
set(gca,'Xdir','reverse')



% Adjust Z-scaling.
daspect(xyz_aspect);

% Set tick marks as millimeters.
xticks((1/pixelsize)*[0 2000 4000 6000 8000 10000 12000 14000]); xticklabels({'0','2','4','6','8','10','12', '14'}); xlabel('mm');
yticks((1/pixelsize)*[0 2000 4000 6000 8000 10000 12000 14000]); yticklabels({'0','2','4','6','8','10','12', '14'}); ylabel('mm');
zticks((1/slicethickness)*[0 500 1000 1500 2000 2500 3000 3500 4000 4500 5000]); zticklabels({'0','0.5','1','1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5'}); zlabel('mm');
set(gca,'FontSize',17) 

% % make figure scale consistent across multiple figures
% xlim manual
% ylim manual
% zlim manual
% 
% % comppute centroid
% centroid = imageCentroid(backgroundmasks);
% 
% extent = 200;
% zextent = 20;
% ylim([centroid(1) - extent, centroid(1) + extent])
% xlim([centroid(2) - extent, centroid(2) + extent])
% zlim([centroid(3) - zextent, centroid(3) + zextent])


