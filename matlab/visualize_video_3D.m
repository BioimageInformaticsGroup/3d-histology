function visualize_video_3D(outputpath)

clear F;
axis vis3d;
for k=1:720
    drawnow;
    tmp = getframe(gcf);
    tmp.cdata = imresize(tmp.cdata,[NaN 1920]);
    padding = mod(size(tmp.cdata),2);
    padding = padding(1:2);
    tmp.cdata = padarray(tmp.cdata,padding,0,'post');
    F(k) = tmp;
    camorbit(0.5,0);
end

videoObj = VideoWriter(outputpath);
videoObj.FrameRate = 30;
videoObj.Quality = 90;
open(videoObj);

% !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
% save F

prev_size = size(F(1).cdata);
for i = 2:length(F)
    if ~all(size(F(i).cdata) == prev_size)
%         size(F(i).cdata)
%         prev_size
        
        diff = size(F(i).cdata) - prev_size;
        
        to_crop = zeros(size(prev_size));
        to_pad = zeros(size(prev_size));
        to_crop(diff > 0) = diff(diff > 0);
        to_pad(diff < 0) = abs(diff(diff < 0));
        
        if any(to_pad > 0)
            F(i).cdata = padarray(F(i).cdata, to_pad, 'replicate', 'post');
        end
        if any(to_crop > 0)
            F(i).cdata = F(i).cdata(1:end-to_crop(1), 1:end-to_crop(2), 1:end-to_crop(3));
        end
            
    end
end

writeVideo(videoObj,F);
close(videoObj);