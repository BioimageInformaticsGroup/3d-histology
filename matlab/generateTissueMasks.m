% Add codes to path.
addpath(genpath('../biit-wsi-featureextraction'));

if numcores > 1
    myCluster = parcluster('local');
    myCluster.NumWorkers = numcores;
    % Then pool to local cluster
    parpool(myCluster.NumWorkers);
else
    ps = parallel.Settings;
    ps.Pool.AutoCreate = false;
end

% if numcores > 1
%    parpool(numcores);
%    %pc = parcluster('local');
%    %pool = parpool(pc,numcores);
%    %pctRunOnAll warning off;
% else
%    ps = parallel.Settings;
%    ps.Pool.AutoCreate = false;
%   %warning('off','all');
% end

% Generate tissue masks.
% Path to data.
%inputpath = '/bmt-data/bii/3D-histology/output/PTENX036-40';
ext = '.tif';
% Generate output folder if it doesnt exist.
if ~exist(outputpath,'dir')
    mkdir(outputpath);
end

% Segmentation settings.
settings.fullrespixelsize = 0.46;
settings.reductionlevel = 4;
settings.closingsize = 50; % In um.
settings.minobjectsize = 100000; % In um^2.
settings.minhue = 0.5;
settings.clearborders = false;
settings.visualize = false;

% Process all images.
allfiles = dir([inputpath,filesep,'*',ext]);
allfiles = {allfiles.name}';
numimages = length(allfiles);
for imind = 1:numimages
    tic;
    inputname = [inputpath,filesep,allfiles{imind}];
    outputname = [outputpath,filesep,'TissueMask_',strrep(allfiles{imind},ext,'.png')];
    try
        biitwsi_extractTissue(inputname,outputname, ...
        'fullrespixelsize',settings.fullrespixelsize,'reductionlevel',settings.reductionlevel, ...
        'closingsize',settings.closingsize, ...
        'minobjectsize',settings.minobjectsize,'minhue',settings.minhue, ...
        'clearborders',settings.clearborders,'visualize',settings.visualize,'uselevel0',false);
    catch
        disp(['Attempting to read corrupted tiles at full-res: ',allfiles{imind}]);
        biitwsi_extractTissue(inputname,outputname, ...
        'fullrespixelsize',settings.fullrespixelsize,'reductionlevel',settings.reductionlevel, ...
        'closingsize',settings.closingsize, ...
        'minobjectsize',settings.minobjectsize,'minhue',settings.minhue, ...
        'clearborders',settings.clearborders,'visualize',settings.visualize,'uselevel0',true);
    end
    if settings.visualize
        pause; close all;
    end
    clc; t = toc; disp(['Image ',allfiles{imind},', ',num2str(imind),'/',num2str(numimages),', ',num2str(t),' s']);
end

