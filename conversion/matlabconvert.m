addpath('/home/memasva/Projects/Registration/3D-histology/conversion/');
addpath(genpath('/home/memasva/Projects/Registration/3D-histology/biit-wsi-preprocessing'));
addpath('/bmt-data/bii/tools/openslide/usr/lib64');
addpath('/usr/lib/x86_64-linux-gnu');


disp(['Converting ', inputpath, ' to ', outputpath, ' with reduction level ', num2str(reductionlevel)])

if str2num(reductionlevel) > 0
    disp('Using biitwsi_imread')
    im = biitwsi_imread(inputpath, 'ReductionLevel', reductionlevel);
elseif str2num(reductionlevel) == 0
    disp('Using Matlabs inbuilt imread')
    im = imread(inputpath);
else
    display(['Invalid reduction level ', num2str(reductionlevel)])
    exit;
end

disp(['Input image info:'])
disp(imfinfo(inputpath))

disp(['Read image size: ', num2str(size(im))])

imwrite(im, outputpath);

disp(['Output image info:'])
disp(imfinfo(outputpath))
