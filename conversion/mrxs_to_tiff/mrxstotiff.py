import os
import sys
from argparse import ArgumentParser

sys.path.insert(0, "./openslide_python")

import openslide
import numpy as np
from PIL import Image
import skimage.io
from skimage.io import imsave

skimage.io.use_plugin("tifffile")

Image.MAX_IMAGE_PIXELS = 42559063690


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("input_dir", help="path to input .mrxs image", type=str)
    parser.add_argument("output_dir", help="path to output image", type=str)
    parser.add_argument("--level", help="Resolution level to convert", type=int, default=0)
    parser.add_argument(
        "--upperleft", help="Upper left coordinate of the converted area, default (0, 0)", type=tuple, default=(0, 0)
    )
    parser.add_argument(
        "--widthheight",
        help="Width and height of the converted area, defaults to size of whole image",
        type=tuple,
        default=None,
    )
    args = vars(parser.parse_args())
    return args


def check_arguments(args):
    if args["output_dir"][-1] != os.sep:
        args["output_dir"] += os.sep

    os.makedirs(args["output_dir"], exist_ok=True)

    return args


def wantedLevel(slide):
    levels = slide.level_count
    print(levels, " available levels to use.\n")
    print("Available dimensions and downsamples:")
    availableLevels = []
    for i in range(0, levels):
        availableLevels.append(i)
        print(
            "level ", i, " dimensions: ", slide.level_dimensions[i], " downsample: ", slide.level_downsamples[i], sep=""
        )

    levelPrint = ", ".join(str(level) for level in availableLevels)

    levelMessage = "\nChoose zoom level by pressing " + levelPrint + ": "
    level = int(input(levelMessage))

    while level not in availableLevels:
        level = int(input(levelMessage))

    return level


def startingPixel():
    coordinateMessage = (
        "Give wanted picture's upper left corner coordinates "
        "separated with ',', one integer to use for both "
        "coordinates or empty input for 0,0 coordinates "
        "(original picture's upper left corner): "
    )
    x_y = input(coordinateMessage)

    if x_y == "":
        x_y = (0, 0)

    elif len(x_y.split(",")) == 1:
        x_y = (int(x_y), int(x_y))
    else:
        x, y = x_y.split(",")
        x_y = (int(x), int(y))

    return x_y


def getLevelSize(slide, level):
    width, height = slide.level_dimensions[level]
    width_height = (int(width), int(height))
    return width_height


def main():
    args = parse_arguments()
    args = check_arguments(args)

    fns = os.listdir(args["input_dir"])
    fns = list(filter(lambda x: os.path.splitext(x)[-1] == ".mrxs", fns))
    print("Converting files:")
    for fn in fns:
        print("  {}".format(fn))

    for pic in fns:
        fileName = os.path.join(args["input_dir"], os.path.basename(pic))
        if os.path.basename(pic)[-5:] == ".mrxs":
            slide = openslide.open_slide(fileName)
            del fileName
        else:
            print("Invalid filetype {}".format(os.path.splitext(os.path.basename(pic)[-1])))
            sys.exit(0)

        level = args["level"]
        x_y = args["upperleft"]
        # default width_height is the size of the selected level
        width_height = getLevelSize(slide, args["level"]) if args["widthheight"] is None else args["widthheight"]

        openedImage = openslide.OpenSlide.read_region(slide, x_y, level, width_height)
        del slide

        newFileName = os.path.basename(pic)[:-5] + "_level_" + str(level) + ".tiff"
        outputPath = args["output_dir"] + "{:}".format(newFileName)

        imsave(outputPath, np.array(openedImage), plugin="tifffile", compress=6)

        print(
            "Saved picture",
            newFileName,
            "to",
            args["output_dir"],
            " - level",
            level,
            "dimensions",
            width_height,
            " - starting pixel",
            x_y,
        )


if __name__ == "__main__":
    main()
