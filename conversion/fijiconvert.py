import argparse
import os
import sys

from ij import IJ  # the IJ class has utility methods for many common tasks.
from ij import ImagePlus
from ij.process import ImageConverter
from loci.plugins import BF


# Enum for interpolation method
# (This is one way to use enums in python2)
class Interpolation:
    NEAREST_NEIGHBOR = 0
    BILINEAR = 1
    BICUBIC = 2


def open_image(fn):
    """Open image from fn and return image object"""

    # imp = IJ.openImage(fn)
    imps = BF.openImagePlus(fn)
    # imp is the common name for an ImagePlus object,
    # ImageJ's base image class
    imp = imps[0]

    print("\tOpened image: {}".format(fn))
    print("\tImage size: {}x{}".format(imp.getProcessor().getWidth(), imp.getProcessor().getHeight()))
    print("\tImage color model: {}".format(imp.getProcessor().getColorModel()))

    ImageConverter(imp).convertToRGB()

    print("\tOpened image: {}".format(fn))
    print("\tImage size: {}x{}".format(imp.getProcessor().getWidth(), imp.getProcessor().getHeight()))
    print("\tImage color model: {}".format(imp.getProcessor().getColorModel()))

    return imp


def resize_image(imp, resize_amount, use_averaging, interpolation):
    """Resize image"""

    if resize_amount == 0:
        print("Resize amount 0 is not sane, skipping resize")
        return imp
    elif resize_amount == 100:
        print("Resize parameter 100, skipping resize")
        return imp
    else:
        # select interpolation mathod according to input form command line
        ip = imp.getProcessor()
        if interpolation == Interpolation.BILINEAR:
            interp_str = "bilinear"
            ip.setInterpolationMethod(ip.BILINEAR)
        elif interpolation == Interpolation.BICUBIC:
            interp_str = "bicubic"
            ip.setInterpolationMethod(ip.BICUBIC)
        elif interpolation == Interpolation.NEAREST_NEIGHBOR:
            interp_str = "nearest-neighbor"
            ip.setInterpolationMethod(ip.NEAREST_NEIGHBOR)

        # calculate new image dimensions
        h = imp.getProcessor().getHeight()
        w = imp.getProcessor().getWidth()
        new_h = int(h * resize_amount / 100.0)
        new_w = int(w * resize_amount / 100.0)

        print("Resizing with factor {} using {} interpolation".format(resize_amount / 100.0, interp_str))
        print("\told dimensions: {}x{}".format(w, h))
        print("\tnew dimensions: {}x{}".format(new_w, new_h))
        bimg = ip.resize(new_w, new_h, use_averaging).getBufferedImage()
        new_imp = ImagePlus("Resized", bimg)

    return new_imp


def export_image(imp, output_path):
    """Export image to given file format"""

    # if outputfile exists already, bioformats exporter is
    # going to just append the existing file so better 
    # remove it first
    if os.path.exists(output_path):
        print("Output path exists, removing file {}".format(output_path))
        os.remove(output_path)

    extension = output_path.split(".")[-1]

    if extension == "tif" or extension == "tiff":
        ImageConverter(imp).convertToRGB()
        IJ.run(imp, "Bio-Formats Exporter", "save=[{}] compression={}".format(output_path, 'LZW'))
        return True
    elif extension == "jp2":
        ImageConverter(imp).convertRGBtoIndexedColor(256)
        IJ.run(imp, "Bio-Formats Exporter", "save=[{}] compression={}".format(output_path, 'JPEG-2000'))
        return True
    elif extension == "png":
        ImageConverter(imp).convertToRGB()
        IJ.run(imp, "Bio-Formats Exporter", "save=[{}] compression={}".format(output_path, 'Animated PNG'))
        return True
    else:
        print("Unsupported file format")
        return False


def parse_command_line_args():
    # parse arguments
    parser = argparse.ArgumentParser(description="Convert image formats")
    parser.add_argument('input', help="Input image")
    parser.add_argument('output', help="Output image")
    parser.add_argument('--resize', help="Resize image", default=0, type=float)
    parser.add_argument('--use-averaging', dest="averaging", help="Resize image with averaging", default=False,
                        action="store_true")

    # define group 'interpolation' that allows only one of defined arguments to be used at a time
    interpolation = parser.add_mutually_exclusive_group(required=False)
    default_interpolation = Interpolation.BICUBIC
    interpolation.add_argument(
        '--bilinear',
        dest="interpolation",
        help="Select bilinear interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.BILINEAR)
    interpolation.add_argument(
        '--bicubic',
        dest="interpolation",
        help="Select bicubic interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.BICUBIC)
    interpolation.add_argument(
        '--nearest-neighbor',
        dest="interpolation",
        help="Select nearest-neighbor interpolation",
        default=default_interpolation,
        action='store_const',
        const=Interpolation.NEAREST_NEIGHBOR)

    args = parser.parse_args()

    return args


def check_arguments(args):
    pass
    # output_dir = os.path.dirname(args.output)
    # if not os.path.exists(output_dir):
    #     os.makedirs(output_dir)


def main():
    # parse command line arguments
    args = parse_command_line_args()
    check_arguments(args)

    # print command line aguments
    print("Input arguments:")
    for k in vars(args).keys():
        print("\t{}: {}".format(k, vars(args)[k]))

    # open input image
    imp = open_image(args.input)

    # resize image if necessary
    if args.resize > 0:
        imp = resize_image(imp, args.resize, args.averaging, args.interpolation)

    # save image
    ret = export_image(imp, args.output)
    if ret:
        print("Export successful")
        sys.exit(0)
    else:
        print("Export failed")
        sys.exit(1)


if __name__ == '__main__':
    main()
