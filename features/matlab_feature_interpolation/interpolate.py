#!/usr/bin/env python

import os
import copy
import time
import logging
from pprint import pprint
# form argparser import ArgumentParser
from argparse import ArgumentParser

import natsort
import numpy as np
import scipy.io as sio
from scipy.interpolate import interp1d


def TicTocGenerator():
    # Generator that returns time differences
    ti = 0  # initial time
    tf = time.time()  # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf - ti  # returns the time difference


TicToc = TicTocGenerator()  # create an instance of the TicTocGen generator


# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    # Prints the time difference yielded by generator instance TicToc
    tempTimeInterval = next(TicToc)
    if tempBool:
        print("Elapsed time: %f seconds.\n" % tempTimeInterval)


def tic():
    # Records a time in TicToc, marks the beginning of a time interval
    toc(False)


def interpolate(slice1_features, slice2_features, num_interpolated,
                num_features):
    """ Interpolate feature data.

        Parameters:
            slice1_features: features from slice1 (block or nuclei)
            slice2_features: features from slice2 (block or nuclei)
            num_interpolated: number of layers of features to interpolate
                              (including original ones)

        Return:
            4 dimensional ndarray with shape:
            (num_bloxks_y, num_blocks_x, num_intep_slices, num_features)

    """

    assert np.all(slice1_features.shape == slice2_features.shape)

    out_shape = (slice1_features.shape[0], slice1_features.shape[1],
                 num_interpolated, num_features)
    all_vals = np.empty(out_shape)
    contains_data = np.zeros(
        (slice1_features.shape[0], slice2_features.shape[1]), dtype=bool)

    for y in np.arange(slice1_features.shape[0]):
        for x in np.arange(slice1_features.shape[1]):

            # compute linear index for block location
            lin_ind = y * slice1_features.shape[1] + x
            tot_num_blocks = np.prod(slice1_features.shape)

            if lin_ind % 1000 == 0:
                logging.info("   Interpolating block {}/{}".format(
                    lin_ind, tot_num_blocks))

            # if there are no features in either top or bottom layer, skip interpolation
            if ((slice1_features[y, x]).size == 0) and ((slice2_features[y, x].size == 0)):  # yapf: disable
                # logging.debug("{}, {} skipping".format(y, x))
                logging.debug("{}/{} skipping".format(lin_ind, tot_num_blocks))
                # all_vals[y, x, :, :] = None
                contains_data[y, x] = False
                continue
            else:
                contains_data[y, x] = True

            if (slice1_features[y, x]).size != 0:
                f1 = slice1_features[y, x]
                f1[np.isnan(f1)] = 0
            else:
                f1 = np.zeros((1, num_features))

            if (slice2_features[y, x]).size != 0:
                f2 = slice2_features[y, x]
                f2[np.isnan(f2)] = 0
            else:
                f2 = np.zeros((1, num_features))

            f1 = np.squeeze(f1)
            f2 = np.squeeze(f2)

            vals = []
            for v1, v2 in zip(f1, f2):

                # create interpolation function
                f = interp1d([0, 1], [v1, v2])
                # define grid at which inteprolation function is sampled
                # add 2 and remove endpoints to get rid of 0 and 1 from the values
                # (these correspond to the original values, no need tu duplicate them)
                grid = np.linspace(0, 1, num_interpolated + 2)[1:-1]
                vals.append(f(grid))

            vals = np.transpose(np.array(vals))
            all_vals[y, x, :, :] = np.array(vals)

    return all_vals, contains_data


def save(slice1_data, slice1_path, num_interpolated_layers, interp_block,
         contains_data_block, interp_nuclei, contains_data_nuclei):
    # use this as a template and store layer specific data wihtin loop below to this object
    slice_interp_data = copy.copy(slice1_data)

    y_max, x_max = slice1_data['features_block'].shape

    # Modify data structure to be able to save each layer separately to disk
    for n in range(num_interpolated_layers):
        l_block = np.array([None for i in range(x_max * y_max)])
        l_nuclei = np.array([None for i in range(x_max * y_max)])
        l_block = l_block.reshape((y_max, x_max))
        l_nuclei = l_nuclei.reshape((y_max, x_max))

        # reconstruct data structure similar to matlab files
        for y in range(y_max):
            for x in range(x_max):

                if contains_data_block[y, x] is True:
                    l_block[y, x] = interp_block[y, x, n, :].reshape(
                        (1, num_feats_block))
                else:
                    l_block[y, x] = np.array([]).reshape((1, 0))

                if contains_data_nuclei[y, x] is True:
                    l_nuclei[y, x] = interp_nuclei[y, x, n, :].reshape(
                        (1, num_feats_nuclei))
                else:
                    l_nuclei[y, x] = np.array([]).reshape((1, 0))

        slice_interp_data['features_block'] = l_block
        slice_interp_data['features_nuclei'] = l_nuclei

        base, _ = os.path.splitext(slice1_path)
        slice_interp_path = "{}_interpolated_{}.mat".format(base, n)
        sio.savemat(slice_interp_path, slice_interp_data, appendmat=False)
        logging.info("   Saved layer {} with name {}".format(
            n, slice_interp_path))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    parser = ArgumentParser()
    parser.add_argument("path", type=str)
    args = vars(parser.parse_args())

    for v in args.keys():
        logging.debug("CLI arg: {}:{}".format(v, args[v]))

    path = args["path"]
    # path='/bmt-data/bii/projects/3D-histology/output/PTENX036-14/features'

    fns = os.listdir(path)
    fns = natsort.natsorted(fns)
    pprint(fns)

    num_interpolated_layers = 54
    num_feats_block = 250
    num_feats_nuclei = 602

    for i in range(len(fns) - 1):
        logging.info("Interpolating slices: {}-{}/{}".format(
            i, i + 1,
            len(fns) - 1))
        tic()

        slice1_path = os.path.join(path, fns[i])
        slice2_path = os.path.join(path, fns[i + 1])

        slice1_data = sio.loadmat(slice1_path)
        slice2_data = sio.loadmat(slice2_path)

        logging.info("interpolating block features")
        interp_block, contains_data_block = interpolate(
            slice1_data['features_block'], slice2_data['features_block'],
            num_interpolated_layers, num_feats_block)
        logging.info("interpolating nuclei features")
        interp_nuclei, contains_data_nuclei = interpolate(
            slice1_data['features_nuclei'], slice2_data['features_nuclei'],
            num_interpolated_layers, num_feats_nuclei)

        # for debugging
        # np.save("interp_block.npy", interp_block)
        # np.save("contains_data_block.npy", contains_data_block)
        # np.save("interp_nuclei.npy", interp_nuclei)
        # np.save("contains_data_nuclei.npy", contains_data_nuclei)

        # interp_block = np.load("interp_block.npy")
        # interp_nuclei = np.load("interp_nuclei.npy")
        # contains_data_block = np.load("contains_data_block.npy")
        # contains_data_nuclei = np.load("contains_data_nuclei.npy")

        logging.info("Saving inteprolated values")
        save(slice1_data, slice1_path, num_interpolated_layers, interp_block,
             contains_data_block, interp_nuclei, contains_data_nuclei)

        logging.info("Slices: {}-{}/{} done".format(i, i + 1, len(fns) - 1))
        toc()
