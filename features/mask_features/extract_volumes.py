import os
import sys
import logging
from typing import List
from argparse import ArgumentParser

import numpy as np
from PIL import Image
# from scipy.misc import imread
from skimage.io import imread
from collections import Counter
import cc3d

sys.path.insert(0, "../../")
sys.path.insert(0, "../")

from volume import Volume
from utils.utils import (end_timing, extract_sequence_numbers_from_filenames,
                         filter_filelist_with_regex, get_mem_usage,
                         start_timing)
from compress import save_compressed_object

Image.MAX_IMAGE_PIXELS = 5000000000

logging.basicConfig(level=logging.DEBUG)

MICRONS_BETWEEN_SLICES = 50.0
VOXEL_SIZE_SYMMETRIC_UM = 0.46


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("source_dir", help="Path to masks directory", type=str)
    parser.add_argument("output_dir",
                        help="Directory where volume files are saved",
                        type=str)
    parser.add_argument(
        "prior_subsampling",
        help="Prior subsampling perfromed to images w.r.t full resolution "
        "WSI scale. In percents e.g. 50 = 50%. Do not take --decimate into account",
        type=float)
    parser.add_argument("--image_regex",
                        default=None,
                        help="Regular expression to use when "
                        "reading images/masks from source_dir",
                        type=str)
    parser.add_argument("--decimate",
                        default=0,
                        help="Amount decimation applied to images",
                        type=int)
    parser.add_argument("--tissue",
                        default=False,
                        help="Use this when extracting tissue volumes",
                        action="store_true")
    parser.add_argument("--preserve_largest_component",
                        default=False,
                        help="Remove all but the largest connected component",
                        action="store_true")
    # parser.add_argument("--images_dir", help="Path to histological image derectory", type=str)
    args = vars(parser.parse_args())

    return args


def load_images(path: str, image_regex: str = None, decimate: int = 0):
    # select images to load and sort them by sequence number
    fns = os.listdir(path)
    if image_regex:
        fns = filter_filelist_with_regex(image_regex, fns)
    sequence = extract_sequence_numbers_from_filenames(fns)
    sort_idx = np.argsort(sequence)
    fns = [fns[i] for i in sort_idx]

    # load images
    images = []
    for i, fn in enumerate(fns, start=1):
        impath = os.path.join(path, fn)
        iml = imread(impath, as_gray=True, plugin='imageio')

        if decimate > 0:
            im = iml[::decimate, ::decimate].copy()
        else:
            im = iml
        del iml

        logging.info("Loaded image {}/{} - mem: {} - shape {} - {}"
                     .format(i, len(fns), get_mem_usage(), im.shape, fn))

        images.append(im)

        # if i == 8:
        #     break

    return images, fns


def check_arguments(args):
    if not os.path.exists(args["source_dir"]):
        logging.error("Path to source directory does not exist. Quitting")
        sys.exit(1)


def extract_volumes(mask_stack: np.ndarray, mask_filenames: List[str],
                    volume_labels: List[int], image_scale: float):
    """
    Extract volumes by finding a 3D bounding box around each unique label
    Args:
        mask_stack: Image stack where each slice of a volume is marked with a unique label
        volume_labels: lables denoting each region of interest in the images (e.g. lesions or tissue)
        image_scale: scal WSI images.

    Returns:
        List of Volume objects
    """
    
    assert((0 in volume_labels) == False)

    lesions = []
    for u in volume_labels:
        single_lesion_stack = mask_stack == u

        # since order of dimensions is (z, y, x)
        # get layers containign nonzero elements
        nonzero_layers = np.max(np.max(single_lesion_stack, axis=2),
                                axis=1).nonzero()[0]
        # get matching filenames
        stack_filenames = np.array(mask_filenames)[nonzero_layers].tolist()

        zmin, zmax, ymin, ymax, xmin, xmax = bounding_box_nonzero(
            single_lesion_stack)

        logging.debug("raw bounding box coordinats {}".format(
            bounding_box_nonzero(single_lesion_stack)))

        data = mask_stack[zmin:zmax + 1, ymin:ymax + 1, xmin:xmax + 1]

        # convert layer index to pixels
        pixels_between_slices = ((image_scale / 100) * MICRONS_BETWEEN_SLICES
                                 / VOXEL_SIZE_SYMMETRIC_UM)  
        zmin = zmin * pixels_between_slices
        zmax = zmax * pixels_between_slices

        lesion = Volume(data, image_scale,
                        (zmin, zmax, ymin, ymax, xmin, xmax), stack_filenames)

        lesions.append(lesion)
    return lesions


def bounding_box_nonzero(image_stack: np.ndarray):
    r = np.any(image_stack, axis=(1, 2))
    c = np.any(image_stack, axis=(0, 2))
    z = np.any(image_stack, axis=(0, 1))
    ymin, ymax = np.where(r)[0][[0, -1]]
    xmin, xmax = np.where(c)[0][[0, -1]]
    zmin, zmax = np.where(z)[0][[0, -1]]
    return ymin,ymax,xmin,xmax,zmin,zmax


def save_volumes(volumes: List[Volume], volume_labels, args):
    """Save volumes into compressed numpy files as Volume objects"""
    if not os.path.exists(args["output_dir"]):
        os.makedirs(args["output_dir"])
    for i, (volume, label) in enumerate(zip(volumes, volume_labels), start=1):

        # construct output filename for volume object
        # first {}: Tissuevolume / Tumorvolume
        # second {}: decimation info
        outname = "{}{}".format(
                "Tissuevolume" if args["tissue"] is True else "Tumorvolume{}".format(int(label)), 
                "_decimated{}".format(args["decimate"]) if args["decimate"] > 1 else "",
                ) 

        save_path = os.path.join(args["output_dir"], outname + ".pkl.bz2")
        logging.info("Saving volume {}/{} - {}".format(i, len(volumes), save_path))

        # save object to disk compressed
        save_compressed_object(save_path, volume)

        

def main():
    start_timing()

    args = parse_arguments()
    check_arguments(args)
    logging.info("Using DECIMATE = {}".format(args["decimate"]))
    if args["preserve_largest_component"]:
        logging.info("PRESERVING ONLY LARGEST CONNECTED COMPONENT FOR EACH LESION")
    logging.info("Command line arguments: {}".format(args))

    # load images
    images, fns = load_images(args["source_dir"], args["image_regex"], args["decimate"])

    stack = np.array(images, ndmin=3)
    del images
    logging.info("Stack shape {}".format(stack.shape))

    if args["tissue"]:
        # print(f"stack dtype {stack.dtype} - min {stack.min()} - max {stack.max()} - uniques {np.unique(stack)}")
        stack = (stack > 0.5).astype(np.float32)
        # print(f"stack dtype {stack.dtype} - min {stack.min()} - max {stack.max()} - uniques {np.unique(stack)}")
        volume_labels = np.unique(stack)
    else:
        volume_labels = np.unique(stack)
    # remove background (label 0) from volume_labels
    volume_labels = volume_labels[1:]
    logging.info("Unique labels in the mask images with background label (0) removed: {}".format(volume_labels))

    if args["preserve_largest_component"]:
        for l in volume_labels:
            labels_out = cc3d.connected_components(stack == l, connectivity=6) # only 26, 18, and 6 are allowed
            unique, counts = np.unique(labels_out, return_counts=True)
            unique = unique[1:]
            counts = counts[1:]
            preserved_label = unique[np.argmax(counts)]
            logging.info(f"Found {np.max(unique)} connected components under label {l} - keeping only sublabel {preserved_label}")
            # first modify stack to remove label l
            stack[stack == l] = 0
            # then store only preserved label to replace l
            stack[labels_out == preserved_label] = l

    # extract volume objects from images
    # mask scale is in percentages (e.g. 50%) and decimate is integer e.g. 2 -> 50%*(1/2)
    # -> correct scale in percentages
    volumes = extract_volumes(stack, fns, volume_labels, (1 / args["decimate"]) * args["prior_subsampling"])
   
    logging.info("Extracted {} volumes from images".format(len(volumes)))
    del stack

    # save found volume objects to disk
    save_volumes(volumes, volume_labels, args)

    end_timing()


if __name__ == '__main__':
    main()
