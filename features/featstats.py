import numpy as np
from scipy.stats import skew, kurtosis

def feature_statistics(F: dict, array: np.ndarray, feature_name: str, print_stats: bool = False, augment=False):
    """Insert statistics of array into F dictionary with keys beginning with 'feature_name'"""
    # convert input to numpy array if it is something else
    if not isinstance(type(array), type([])):
        array = np.array(array)
    
    # if array.size == 0:
    #     print(f"Attempting to extract {feature_name} features from empty array. Skipping..")
    #     return F

    # define dictionary key names
    key_mean = f"{feature_name}_mean"
    key_var = f"{feature_name}_var"
    key_min = f"{feature_name}_min"
    key_max = f"{feature_name}_max"
    key_skew = f"{feature_name}_skew"
    key_kurtosis = f"{feature_name}_kurtosis"
    # key_min_max_ratio = f"{feature_name}_min_max_ratio"
    key_percentile_10 = f"{feature_name}_ptile10"
    key_percentile_25 = f"{feature_name}_ptile25"
    key_median = f"{feature_name}_median"
    key_percentile_75 = f"{feature_name}_ptile75"
    key_percentile_90 = f"{feature_name}_ptile90"
    key_mean_median_deviation = f"{feature_name}_mean_med_dev"

    keys = []
    keys.append(key_mean)
    keys.append(key_var)
    keys.append(key_min)
    keys.append(key_max)
    keys.append(key_skew)
    keys.append(key_kurtosis)
    # keys.append(key_min_max_ratio)
    keys.append(key_percentile_10)
    keys.append(key_percentile_25)
    keys.append(key_median)
    keys.append(key_percentile_75)
    keys.append(key_percentile_90)

    # add feature to dictionary
    F[key_mean] = array.mean()
    F[key_var] = array.var()
    F[key_min] = array.min()
    F[key_max] = array.max()
    F[key_skew] = skew(array)
    F[key_kurtosis] = kurtosis(array)
    # F[key_min_max_ratio] = F[key_min] / F[key_max]
    F[key_percentile_10] = np.percentile(array, 10)
    F[key_percentile_25] = np.percentile(array, 25)
    F[key_median] = np.median(array)
    F[key_percentile_75] = np.percentile(array, 75)
    F[key_percentile_90] = np.percentile(array, 90)
    F[key_mean_median_deviation] = F[key_mean] - F[key_median]
    keys.append(key_mean_median_deviation)

    # Do not expect F to contain only features to be augmented
    # hence using the keys variable
    if augment:
        F_aug = {}
        for power in [2, 3]:
            for k in keys:
                # square for k in keys_to_augment:
                F_aug[f"{k}_pow{power}"] = np.power(F[k], power)
                # square_root
                F_aug[f"{k}_root{power}"] = np.power(F[k], 1/power)
        for k, v in F_aug.items():
            F[k] = v
        del F_aug

    if print_stats:
        for k, v in F.items():
            print(f"Feature {k}: {v}")

    return F


def feature_statistics_small(F: dict, array: np.ndarray, feature_name: str, print_stats: bool = False, augment=False):
    """Insert statistics of array into F dictionary with keys beginning with 'feature_name'"""
    # convert input to numpy array if it is something else
    if not isinstance(type(array), type([])):
        array = np.array(array)

    # define dictionary key names
    key_mean = f"{feature_name}_mean"
    key_var = f"{feature_name}_var"
    key_min = f"{feature_name}_min"
    key_max = f"{feature_name}_max"
    key_median = f"{feature_name}_median"

    keys = []
    keys.append(key_mean)
    keys.append(key_var)
    keys.append(key_min)
    keys.append(key_max)
    # keys.append(key_min_max_ratio)
    keys.append(key_median)

    # add feature to dictionary
    F[key_mean] = array.mean()
    F[key_var] = array.var()
    F[key_min] = array.min()
    F[key_max] = array.max()
    F[key_median] = np.median(array)

    # Do not expect F to contain only features to be augmented
    # hence using the keys variable
    if augment:
        F_aug = {}
        for power in [2, 3]:
            for k in keys:
                # square for k in keys_to_augment:
                F_aug[f"{k}_pow{power}"] = np.power(F[k], power)
                # square_root
                F_aug[f"{k}_root{power}"] = np.power(F[k], 1/power)
        for k, v in F_aug.items():
            F[k] = v
        del F_aug

    if print_stats:
        for k, v in F.items():
            print(f"Feature {k}: {v}")

    return F



def feature_statistics_less(F: dict, array: np.ndarray, feature_name: str, print_stats: bool = False, augment=False):
    """Insert statistics of array into F dictionary with keys beginning with 'feature_name'"""
    # convert input to numpy array if it is something else
    if not isinstance(type(array), type([])):
        array = np.array(array)
    
    # define dictionary key names
    key_mean = f"{feature_name}_mean"
    key_var = f"{feature_name}_var"
    key_min = f"{feature_name}_min"
    key_max = f"{feature_name}_max"
    key_skew = f"{feature_name}_skew"
    key_kurtosis = f"{feature_name}_kurtosis"
    key_median = f"{feature_name}_median"
    key_mean_median_deviation = f"{feature_name}_mean_med_dev"

    keys = []
    keys.append(key_mean)
    keys.append(key_var)
    keys.append(key_min)
    keys.append(key_max)
    keys.append(key_skew)
    keys.append(key_kurtosis)
    keys.append(key_median)

    # add feature to dictionary
    F[key_mean] = array.mean()
    F[key_var] = array.var()
    F[key_min] = array.min()
    F[key_max] = array.max()
    F[key_skew] = skew(array)
    F[key_kurtosis] = kurtosis(array)
    F[key_median] = np.median(array)
    F[key_mean_median_deviation] = F[key_mean] - F[key_median]
    keys.append(key_mean_median_deviation)

    # Do not expect F to contain only features to be augmented
    # hence using the keys variable
    if augment:
        F_aug = {}
        for power in [2, 3]:
            for k in keys:
                # square for k in keys_to_augment:
                F_aug[f"{k}_pow{power}"] = np.power(F[k], power)
                # square_root
                F_aug[f"{k}_root{power}"] = np.power(F[k], 1/power)
        for k, v in F_aug.items():
            F[k] = v
        del F_aug

    if print_stats:
        for k, v in F.items():
            print(f"Feature {k}: {v}")

    return F


