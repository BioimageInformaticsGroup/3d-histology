import os
import sys
import time
import logging
from argparse import ArgumentParser

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import scipy.io as sio
from scipy.misc import imresize
import natsort
from sklearn.manifold import TSNE
import skimage

sys.path.insert(0, "../")

from extract_features import load_compressed_object

NUM_INTERPOLATED_LAYERS = 54
NUM_FEATS_BLOCK = 250
NUM_FEATS_NUCLEI = 602

# basic config did not work for me, for some reason
# logging.basicConfig(level=logging.DEBUG)
logging.getLogger().setLevel(logging.DEBUG)


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("--path_matlab_features",
                        help="path to directory containing .mat features",
                        default=None,
                        type=str)
    parser.add_argument("--path_images",
                        help="path to images directory",
                        default=None,
                        type=str)
    parser.add_argument("--path_tumor_volumes",
                        help="path to images directory",
                        default=None,
                        type=str)
    parser.add_argument("--path_tissue_volume",
                        help="path to images directory",
                        default=None,
                        type=str)
    args = vars(parser.parse_args())
    return args


def load_matlab_features(path_to_dir: str):
    """Load matlab features from a directory

    Return:
        features is a list of dictionaries of numpy arrays:
          - list of features per images
          - dictionary of keys: 'fullsize': Input image resolution (y, x)
                                'features_nuclei': nuclei location and shape? features, based on nuclei segmentation
                                'features_block': features from histology tiles
                                'blockcorners': coordinates of the upper left coordinate of each block
                                                (matlab coordinates! indexing starts from 1)
                                'labels_block': names of features
                                'labels_nuclei': names of features
          - under each key is a NxM numpy array of features
        fns are the filenames of th images in dir

    """
    fns = os.listdir(path_to_dir)
    fns = natsort.natsorted(fns)
    features = []
    for fn in fns:
        logging.debug("Loading file {}".format(fn))
        fpath = os.path.join(path_to_dir, fn)
        data = sio.loadmat(fpath)
        features.append(data)
    return features, fns


def TicTocGenerator():
    """ Generator that returns time differences """
    ti = 0  # initial time
    tf = time.time()  # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf - ti  # returns the time difference


TicToc = TicTocGenerator()  # create an instance of the TicTocGen generator


# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    """ Prints the time difference yielded by generator instance TicToc """
    tempTimeInterval = next(TicToc)
    if tempBool:
        print("Elapsed time: %f seconds.\n" % tempTimeInterval)


def tic():
    """ Records a time in TicToc, marks the beginning of a time interval """
    toc(False)


def plot_tissue_volume(args):
    fns = os.listdir(args["path_images"])
    fns = [os.path.join(args["path_images"], f) for f in fns]
    fns = natsort.natsorted(fns)

    logging.info("Images:")
    for fn in fns:
        logging.debug("    {}".format(fn))

    stack = []
    for fn in fns:
        im = np.squeeze(skimage.io.imread(fn, as_gray=True))
        stack.append(im)
    stack = np.array(stack)

    # load tissue mask volume
    mask_volume = load_compressed_object(args["path_tissue_volume"])
    bounding_cube = mask_volume.get_bounding_cube_coords()
    # scale bounding cube to image scale (images 5%, cube 25%)
    coeff = 0.05 / 0.25
    bounding_cube = (np.array(bounding_cube) * coeff).astype(int)
    # crop tissue volume accordingly

    # crop stack according to tissue masks
    logging.debug("Bounding cube: {}".format(bounding_cube))
    logging.debug("Stack shape before cropping: {}".format(stack.shape))
    # yapf: disable
    stack = stack[bounding_cube[0]:bounding_cube[1],
                  bounding_cube[2]:bounding_cube[3],
                  bounding_cube[4]:bounding_cube[5]]
    # yapf: enable
    logging.debug("Stack shape after cropping: {}".format(stack.shape))

    # plt.figure()
    # for p in stack:
    #     plt.imshow(p)
    #     plt.show()

    # fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # # ax.voxels(stack > stack.mean(), facecolors=colors, edgecolor='k')
    # ax.voxels(stack > stack.mean(), edgecolor='k')

    resized = []
    for p in stack:
        resized.append(imresize(p, 0.02))
    resized = np.array(resized)
    
    print(resized.shape)

    plot_cube(resized)


def explode(data):
    shape_arr = np.array(data.shape)
    size = shape_arr[:3] * 2 - 1
    exploded = np.zeros(np.concatenate([size, shape_arr[3:]]),
                        dtype=data.dtype)
    exploded[::2, ::2, ::2] = data
    return exploded


def expand_coordinates(indices):
    x, y, z = indices
    x[1::2, :, :] += 1
    y[:, 1::2, :] += 1
    z[:, :, 1::2] += 1
    return x, y, z


def normalize(arr):
    arr_min = np.min(arr)
    return (arr - arr_min) / (np.max(arr) - arr_min)


def plot_cube(cube, angle=320):
    cube = normalize(cube)

    facecolors = cm.viridis(cube)
    facecolors[:, :, :, -1] = cube
    facecolors = explode(facecolors)

    filled = facecolors[:, :, :, -1] != 0
    x, y, z = expand_coordinates(np.indices(np.array(filled.shape) + 1))

    fig = plt.figure(figsize=(30 / 2.54, 30 / 2.54))
    ax = fig.gca(projection='3d')
    ax.view_init(30, angle)
    # ax.set_xlim(right=IMG_DIM * 2)
    # ax.set_ylim(top=IMG_DIM * 2)
    # ax.set_zlim(top=IMG_DIM * 2)

    ax.voxels(x, y, z, filled, facecolors=facecolors)
    plt.show()


def main():
    args = parse_arguments()
    logging.debug("Command line arguments: {}".format(args))
    plot_tissue_volume(args)


if __name__ == "__main__":
    tic()
    main()
    toc()
