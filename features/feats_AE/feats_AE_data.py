import os
import glob
import pickle
import logging
from typing import List

import numpy as np
from skimage.io import imread


# normalization test
# DATA_PATH = "/home/masi/Projects/deep_registration/images/test_norm"
# train_data, validation_data = create_training_data(DATA_PATH, split=1)


PREPROCESS_DIR = "preprocess_data"


def get_wild_non_wild_fns():

    tempfile = os.path.join(PREPROCESS_DIR, "filelist.pkl")

    if not os.path.exists(tempfile):

        base_path = "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/"

        wild_type_mice = ["PTENX036-21", "PTENX036-34", "PTENX036-39", "PTENX036-40", "PTENX036-42", "PTENX036-68"]
        non_wild_mice = [
            "PTENX036-011",
            "PTENX036-14",
            "PTENX036-15",
            "PTENX036-20",
            "PTENX036-32",
            "PTENX036-33",
            "UTU186-xhiMyc-003",
            "UTU186-xhiMyc-006",
            "UTU186-xhiMyc-007",
            "UTU186-xhiMyc-008",
            "UTU186-xhiMyc-022",
            "UTU186-xhiMyc-045",
            "UTU186-xhiMyc-199",
            "UTU186-xhiMyc-211",
            "UTU186-xhiMyc-223",
            "UTU186-xhiMyc-355",
            "UTU186-xhiMyc-356",
            "UTU186-xhiMyc-1387",
        ]

        wild_type_fns = []
        logging.info("Listing wild type tiles")
        for m in wild_type_mice:
            dpath = os.path.join(base_path, m, "tiles")
            fns = _listdir_fullpath_recursive(dpath, files_only=True)
            wild_type_fns.extend(fns)

        non_wild_fns = []
        logging.info("Listing non-wild type tiles")
        for m in non_wild_mice:
            dpath = os.path.join(base_path, m, "tiles")
            fns = _listdir_fullpath_recursive(dpath, files_only=True)
            non_wild_fns.extend(fns)

        with open(tempfile, 'wb') as f:
            pickle.dump([wild_type_fns, non_wild_fns], f)

    else:
        with open(tempfile, 'rb') as f:
            wild_type_fns, non_wild_fns = pickle.load(f)

    return wild_type_fns, non_wild_fns


def _listdir_fullpath_recursive(path_dir: str, files_only: bool = True, N_first: int = None):
    logging.info(f"    Listing files under {path_dir}")
    path_dir_full = os.path.abspath(path_dir)
    if N_first is not None:
        fns = [next(glob.iglob(os.path.join(path_dir_full, "**"), recursive=True)) for i in range(N_first)]
    else:
        fns = list(glob.iglob(os.path.join(path_dir_full, "**"), recursive=True))
    if files_only:
        # filter out folders
        fns = list(filter(os.path.isfile, fns))
    return fns


def _split_data(fns: List[str], split_ratio: float = 0.8, randomize: bool = True, seed: int = 123):
    if randomize:
        # set seed only if randomize, no need to set it otherwise
        np.random.seed(seed)
        np.random.shuffle(fns)
    split_ind = int(len(fns) * split_ratio)
    part1 = fns[:split_ind]
    part2 = fns[split_ind:]
    return part1, part2


def _train_data_mean_max_min(image_paths: List[str], save: bool = False):
    """
    Assume same resolution for all images.
    Pixels are needed to have the same weight in the final mean
    """
    mean_fn = os.path.join(PREPROCESS_DIR, "train_mean.npy")
    max_fn = os.path.join(PREPROCESS_DIR, "train_max.npy")
    min_fn = os.path.join(PREPROCESS_DIR, "train_min.npy")

    # if all the files exist, we can load them
    if (os.path.exists(mean_fn) and os.path.exists(max_fn) and os.path.exists(min_fn)) and not save:

        mean = np.load(mean_fn)
        maximum = np.load(max_fn)
        minimum = np.load(min_fn)

        logging.info("Loaded training set mean {:.2f} from file {}".format(mean, mean_fn))
        logging.info("Loaded training set max {:.2f} from file {}".format(maximum, max_fn))
        logging.info("Loaded training set min {:.2f} from file {}".format(minimum, min_fn))

    else:
        logging.info("Starting to compute training set mean, max & min..")
        mean = 0
        maximum = -np.inf
        minimum = np.inf
        for fn in image_paths:
            im = imread(fn, plugin="pil")
            mean += im.mean() / len(image_paths)
            maximum = max(maximum, im.max())
            minimum = min(minimum, im.min())

        logging.info("Saving training set mean, max & min..")
        # make sure output path exists
        os.makedirs(PREPROCESS_DIR, exist_ok=True)

        np.save(mean_fn, mean)
        logging.info("    Saved training set mean {:.2f}".format(mean))
        logging.info("    Save path: {}".format(mean_fn))

        np.save(max_fn, maximum)
        logging.info("    Saved training set maximum {:.2f}".format(maximum))
        logging.info("    Save path: {}".format(max_fn))

        np.save(min_fn, minimum)
        logging.info("    Saved training set minimum {:.2f}".format(minimum))
        logging.info("    Save path: {}".format(min_fn))

        logging.info("..done")
    return mean, maximum, minimum


def _train_data_std(image_paths: List[str], mean: float, save: bool = False):
    """
    Assume same resolution for all images.
    Pixels are needed to have the same weight in the final mean
    """

    std_fn = os.path.join(PREPROCESS_DIR, "train_std.npy")

    if os.path.exists(std_fn) and not save:

        std = np.load(std_fn)

        logging.info("Loaded training set std {:.2f} from file {}".format(std, std_fn))

    else:
        logging.info("Starting to compute training set std..")
        var = 0
        for fn in image_paths:
            im = imread(fn, plugin="pil")
            v = np.power(im - mean, 2).mean()
            var += v / len(image_paths)
        std = np.sqrt(var)
        logging.info("Saving training set std..")
        # make sure output path exists
        os.makedirs(PREPROCESS_DIR, exist_ok=True)

        np.save(std_fn, std)
        logging.info("    Saved training set std {:.2f}".format(std))
        logging.info("    Save path: {}".format(std_fn))

        logging.info("..done")
    return std


def preprocess_zero_mean_unit_std(array: np.ndarray):
    """Array can be multidimensional"""
    p = array - TRAIN_MEAN
    p = p / TRAIN_STD
    return p


def preprocess_range_01(array: np.ndarray):
    """Array can be multidimensional"""
    p = array - TRAIN_MIN
    p = p / TRAIN_MAX
    return p


logging.basicConfig(level=logging.INFO)

logging.info("Loading data module..")

# # DEBUG:
# if not os.path.exists("test_extract_tempfns.pkl"):


if os.getenv("HOSTNAME") != "x1g6":
    wild_fns, non_wild_fns = get_wild_non_wild_fns()
else:
    fns11 = _listdir_fullpath_recursive(
        "/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/"
        + "projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/PTENX036-011/tiles/"
    )
    fns14 = _listdir_fullpath_recursive(
        "/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi/bmt-data/bii/"
        + "projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/PTENX036-14/tiles/"
    )
    wild_fns = fns11 + fns14
    non_wild_fns = wild_fns


# # DEBUG:
#     with open("test_extract_tempfns.pkl", 'wb') as f:
#         pickle.dump([wild_fns, non_wild_fns], f)
# else:
#     with open("test_extract_tempfns.pkl", 'rb') as f:
#         wild_fns, non_wild_fns = pickle.load(f)


# take every other wild type tile as training set
TRAIN_DATA = wild_fns[0::2]
# the rest is test and validation data
_rest = wild_fns[1::2]
VALIDATION_DATA = _rest[0::2]
TEST_DATA = _rest[1::2]
# Every tile from non wild type are also test data
TEST_DATA.extend(non_wild_fns)

seed = 123
np.random.seed(seed)
# suffles in-place
np.random.shuffle(TRAIN_DATA)
np.random.shuffle(VALIDATION_DATA)
np.random.shuffle(TEST_DATA)

# get input data shape
INPUT_SHAPE = imread(TRAIN_DATA[0], plugin="pil").shape
NUM_CHANNELS = INPUT_SHAPE[-1]
logging.info("AE_data.py: Tile shape: {}".format(INPUT_SHAPE))

# compute and save mean and std to disk
SAVE = False
# Save train data mean, max, min and standard deviation as module wide variables
TRAIN_MEAN, TRAIN_MAX, TRAIN_MIN = _train_data_mean_max_min(TRAIN_DATA, save=SAVE)
TRAIN_STD = _train_data_std(TRAIN_DATA, TRAIN_MEAN, save=SAVE)

logging.info("Data module loading done")
