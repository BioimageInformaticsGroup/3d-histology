import os
import sys
import logging
from pprint import pprint
from argparse import ArgumentParser
from typing import List

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
from skimage.io import imread

os.environ["KERAS_BACKEND"] = "tensorflow"
from keras import backend as K

K.set_image_data_format("channels_last")
from keras.layers import Input, Conv2D, UpSampling2D, MaxPooling2D, BatchNormalization, Flatten, Dense, Reshape
from keras.models import Model
from keras.regularizers import l1
from keras.utils import Sequence
from keras import optimizers
from keras.callbacks import LambdaCallback

sys.path.insert(0, "..")

import feats_AE_data as data

logging.basicConfig(level=logging.DEBUG)


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("path_saved_model", help="path to saved model", type=str)
    parser.add_argument("--epochs", help="Number of epochs to train", type=int, default=300)
    parser.add_argument("--batch_size", help="Set batch size", type=int, default=64)
    args = vars(parser.parse_args())
    return args


class TrainSequence(Sequence):
    def __init__(self, fns: List[str], batch_size: int = 128, tile_size: int = 224, num_channels: int = 3):
        self.fns = fns
        self.batch_size = batch_size
        self.tile_size = tile_size
        self.num_channels = num_channels

    # Required method for calculating total number of batches.
    def __len__(self):
        return int(np.ceil(len(self.fns) / float(self.batch_size)))

    # Required method for extracting data for batch number 'batch_idx'.
    # Keras takes care of picking up batches at random and distributing batches
    # among CPU cores. If we want to be sure the sampling is random, we can
    # shuffle fns and classes before supplying them to the generator.
    def __getitem__(self, batch_idx):
        batch_fns = self.fns[batch_idx * self.batch_size : (batch_idx + 1) * self.batch_size]

        x_train = np.zeros((len(batch_fns), self.tile_size, self.tile_size, self.num_channels), dtype=np.float64)
        y_train = np.zeros((len(batch_fns), self.tile_size, self.tile_size, self.num_channels), dtype=np.float64)

        for i, fn in enumerate(batch_fns):
            im = imread(fn, plugin="pil").astype(np.float64)
            # for autoencoder, x and y are the same
            x_train[i, ...] = im
            y_train[i, ...] = im

        # preprocess data
        # func = data.preprocess_zero_mean_unit_std
        func = data.preprocess_range_01
        x_train = func(x_train)
        y_train = func(y_train)

        return x_train, y_train

    def get_batch_fns(self, batch_idx):
        batch_fns = self.fns[batch_idx * self.batch_size : (batch_idx + 1) * self.batch_size]
        return batch_fns


def pop_layers(model, num_layers):
    for n in range(num_layers):
        model.layers.pop()
    model.outputs = [model.layers[-num_layers].output]
    model.layers[-num_layers].outbound_nodes = []


# # UNDERCOMPLETE / TIGHT
# def create_model(input_shape):

#     lr = 0.0001
#     amsgrad = True
#     model_name = f"undercomplete_lR{lr}_AMSGRAD{amsgrad}"

#     # ENCODER
#     encoder_input = Input(shape=input_shape)
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(encoder_input)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(3, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     encoded = MaxPooling2D((2, 2), padding="same")(x)

#     decoder_input_shape = K.int_shape(encoded)[1:]
#     logging.debug("Shape of encoded {}".format(decoder_input_shape))

#     # DECODER
#     decoder_input = Input(shape=decoder_input_shape)
#     x = Conv2D(3, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(decoder_input)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = UpSampling2D((2, 2))(x)
#     decoded = Conv2D(3, (5, 5), activation="sigmoid", padding="same", kernel_initializer="glorot_uniform")(x)
#     logging.debug("shape of decoded {}".format(K.int_shape(decoded)))

#     # define intermediate model
#     encoder = Model(inputs=[encoder_input], outputs=[encoded])
#     decoder = Model(inputs=[decoder_input], outputs=[decoded])
#     autoencoder = Model(encoder.input, decoder(encoder.output))
#     opti=optimizers.Adam(lr=lr, amsgrad=amsgrad)
#     logging.info(f"Using optimizer: {opti}")
#     autoencoder.compile(optimizer=opti, loss="mean_squared_error")
#     logging.info(f"Compiled autoencoder {autoencoder}")

#     return autoencoder, encoder, decoder, model_name


# # UNDERCOMPLETE / TIGHTER
# def create_model(input_shape):

#     lr = 0.0001
#     amsgrad = True
#     model_name = f"undercomplete32-16-16-8_lR{lr}_AMSGRAD{amsgrad}"

#     # ENCODER
#     encoder_input = Input(shape=input_shape)
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(encoder_input)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(8, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     encoded = MaxPooling2D((2, 2), padding="same")(x)

#     decoder_input_shape = K.int_shape(encoded)[1:]
#     logging.debug("Shape of encoded {}".format(decoder_input_shape))

#     # DECODER
#     decoder_input = Input(shape=decoder_input_shape)
#     x = UpSampling2D((2, 2))(decoder_input)
#     x = BatchNormalization()(x)
#     x = Conv2D(8, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = UpSampling2D((2, 2))(x)
#     x = BatchNormalization()(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = UpSampling2D((2, 2))(x)
#     x = BatchNormalization()(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = UpSampling2D((2, 2))(x)
#     x = BatchNormalization()(x)
#     decoded = Conv2D(3, (9, 9), activation="relu", kernel_initializer="glorot_uniform")(x)
#     # decoded = Conv2D(3, (5, 5), activation="sigmoid", padding="same", kernel_initializer="glorot_uniform")(x)
#     logging.debug("shape of decoded {}".format(K.int_shape(decoded)))

#     # define intermediate model
#     encoder = Model(inputs=[encoder_input], outputs=[encoded])
#     decoder = Model(inputs=[decoder_input], outputs=[decoded])
#     autoencoder = Model(encoder.input, decoder(encoder.output))
#     opti=optimizers.Adam(lr=lr, amsgrad=amsgrad)
#     logging.info(f"Using optimizer: {opti}")
#     autoencoder.compile(optimizer=opti, loss="mean_squared_error")
#     logging.info(f"Compiled autoencoder {autoencoder}")

#     return autoencoder, encoder, decoder, model_name

# # SPARSECONV
# def create_model(input_shape):
#     ar = 0.001
#     lr = 0.01
#     amsgrad = True
#     model_name = f"sparseconv_AR{ar}_LR{lr}_AMSGRAD{amsgrad}"

#     # ENCODER
#     encoder_input = Input(shape=input_shape)
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(encoder_input)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(3, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform", activity_regularizer=l1(ar))(x)
#     encoded = MaxPooling2D((2, 2), padding="same")(x)

#     decoder_input_shape = K.int_shape(encoded)[1:]
#     logging.debug("Shape of encoded {}".format(decoder_input_shape))

#     # DECODER
#     decoder_input = Input(shape=decoder_input_shape)
#     x = Conv2D(3, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(decoder_input)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = UpSampling2D((2, 2))(x)
#     decoded = Conv2D(3, (5, 5), activation="sigmoid", padding="same", kernel_initializer="glorot_uniform")(x)
#     logging.debug("shape of decoded {}".format(K.int_shape(decoded)))

#     # define intermediate model
#     encoder = Model(inputs=[encoder_input], outputs=[encoded])
#     decoder = Model(inputs=[decoder_input], outputs=[decoded])
#     autoencoder = Model(encoder.input, decoder(encoder.output))
#     opti=optimizers.Adam(lr=lr, amsgrad=amsgrad)
#     logging.info(f"Using optimizer: {opti}")
#     autoencoder.compile(optimizer=opti, loss="mean_squared_error")
#     logging.info(f"Compiled autoencoder {autoencoder}")

#     return autoencoder, encoder, decoder, model_name


# # SPARSECONV TIGHTER
# def create_model(input_shape):

#     ar = 0.001
#     lr = 0.01
#     amsgrad = True
#     model_name = f"sparseconv_tighter_AR{ar}_LR{lr}_AMSGRAD{amsgrad}"

#     # ENCODER
#     encoder_input = Input(shape=input_shape)
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(encoder_input)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(8, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(3, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform", activity_regularizer=l1(ar))(x)
#     encoded = MaxPooling2D((2, 2), padding="same")(x)

#     decoder_input_shape = K.int_shape(encoded)[1:]
#     logging.debug("Shape of encoded {}".format(decoder_input_shape))

#     # DECODER
#     decoder_input = Input(shape=decoder_input_shape)
#     x = Conv2D(3, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(decoder_input)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(8, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = UpSampling2D((2, 2))(x)
#     decoded = Conv2D(3, (5, 5), activation="sigmoid", padding="same", kernel_initializer="glorot_uniform")(x)
#     logging.debug("shape of decoded {}".format(K.int_shape(decoded)))

#     # define intermediate model
#     encoder = Model(inputs=[encoder_input], outputs=[encoded])
#     decoder = Model(inputs=[decoder_input], outputs=[decoded])
#     autoencoder = Model(encoder.input, decoder(encoder.output))
#     opti=optimizers.Adam(lr=lr, amsgrad=amsgrad)
#     logging.info(f"Using optimizer: {opti}")
#     autoencoder.compile(optimizer=opti, loss="mean_squared_error")
#     logging.info(f"Compiled autoencoder {autoencoder}")

#     return autoencoder, encoder, decoder, model_name


# # CONV + DENSE SPARSE, does not work
# def create_model(input_shape):

#     model_name = f"conv_dense_sparse"

#     # ENCODER
#     encoder_input = Input(shape=input_shape)
#     # conv
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(encoder_input)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = MaxPooling2D((2, 2), padding="same")(x)
#     x = Conv2D(3, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform", activity_regularizer=l1(AE))(x)
#     encoded_conv = MaxPooling2D((2, 2), padding="same")(x)
#     encoded_conv_shape = K.int_shape(encoded_conv)[1:]
#     # dense
#     N_dense = np.prod(encoded_conv_shape)
#     x = Flatten()(encoded_conv)
#     encoded = Dense(N_dense, activation = "relu", activity_regularizer=l1(AE))(x)

#     decoder_input_shape = K.int_shape(encoded)[1:]
#     logging.info(f"Shape of conv output {encoded_conv_shape}")
#     logging.info(f"Shape of encoder's output {decoder_input_shape}")

#     # DECODER
#     decoder_input = Input(shape=decoder_input_shape)
#     # dense
#     x = Dense(N_dense, activation = 'relu')(decoder_input)
#     x = Reshape(encoded_conv_shape)(x)
#     # conv
#     x = Conv2D(3, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(16, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = BatchNormalization()(x)
#     x = UpSampling2D((2, 2))(x)
#     x = Conv2D(32, (3, 3), activation="relu", padding="same", kernel_initializer="glorot_uniform")(x)
#     x = UpSampling2D((2, 2))(x)
#     decoded = Conv2D(3, (5, 5), activation="sigmoid", padding="same", kernel_initializer="glorot_uniform")(x)
#     logging.debug("shape of decoded {}".format(K.int_shape(decoded)))

#     # define intermediate model
#     encoder = Model(inputs=[encoder_input], outputs=[encoded])
#     decoder = Model(inputs=[decoder_input], outputs=[decoded])
#     autoencoder = Model(encoder.input, decoder(encoder.output))
#     opti=optimizers.Adam(lr=lr, amsgrad=amsgrad)
#     logging.info(f"Using optimizer: {opti}")
#     autoencoder.compile(optimizer=opti, loss="mean_squared_error")
#     logging.info(f"Compiled autoencoder {autoencoder}")

#     return autoencoder, encoder, decoder, model_name


def save_reconstructions(model: Model, input_images: list, path_save_dir: str = None):
    logging.info("Plot reconstructions")

    batch_idx = 0
    testgen = TrainSequence(
        input_images, batch_size=len(input_images), tile_size=data.INPUT_SHAPE[0], num_channels=data.NUM_CHANNELS
    )

    x_train, y_train = testgen.__getitem__(batch_idx)

    mses = []
    for i, n in enumerate(range(x_train.shape[0])):

        original = x_train[i, :, :, :]
        reconstructed = model.predict(original.reshape((1, original.shape[0], original.shape[1], original.shape[2])))
        reconstructed = np.squeeze(reconstructed)

        # compute mse between images
        mse = (((original - reconstructed).ravel()) ** 2).mean()
        mses.append(mse)

        plt.figure()
        plt.subplot(1, 2, 1)
        plt.imshow(original)
        plt.title("original image")
        plt.axis("off")
        plt.subplot(1, 2, 2)
        plt.imshow(reconstructed)
        plt.title("reconstructed, MSE {:.5f}".format(mse))
        plt.axis("off")
        plt.tight_layout()

        if path_save_dir is None:
            plt.show()
        else:
            os.makedirs(path_save_dir, exist_ok=True)
            # save figure
            plt.savefig(os.path.join(path_save_dir, "patch_fig_{}.png".format(n)))
            # save images separately also
            # plt.imsave(os.path.join(path_save_dir, "reconstructed_patch_{}.png".format(n)), reconstructed)
            # plt.imsave(os.path.join(path_save_dir, "original_patch_{}.png".format(n)), original)
            plt.close()


def main():
    args = parse_arguments()

    model, encoder, decoder, model_name = create_model(data.INPUT_SHAPE)
    encoder.summary()
    decoder.summary()

    # construct model name
    fname, ext = os.path.splitext(args["path_saved_model"])
    ext = ".h5"
    args["path_saved_model"] = f"{fname}_{model_name}_epochs{args['epochs']}{ext}"

    # callbacks
    callbacks = []
    save_rec_cb = LambdaCallback(on_epoch_begin=lambda epoch, logs: save_reconstructions(model, data.TEST_DATA[0:10], f"recs_{os.path.join(args['path_saved_model'][:-3], str(epoch))}"))
    callbacks.append(save_rec_cb)

    logging.info("Start AE features training, model {}".format(args["path_saved_model"]))
    logging.info("Batch size: {}".format(args["batch_size"]))
    logging.info("Epochs: {}".format(args["epochs"]))

    traingen = TrainSequence(
        data.TRAIN_DATA, batch_size=args["batch_size"], tile_size=data.INPUT_SHAPE[0], num_channels=data.NUM_CHANNELS
    )
    validgen = TrainSequence(
        data.VALIDATION_DATA,
        batch_size=args["batch_size"],
        tile_size=data.INPUT_SHAPE[0],
        num_channels=data.NUM_CHANNELS,
    )

    model.fit_generator(
        generator=traingen, validation_data=validgen, validation_steps=3, epochs=args["epochs"], shuffle=True, verbose=1, max_queue_size=8, workers=3, use_multiprocessing=True, callbacks=callbacks
    )
    logging.info("Training done")

    # plot training and validation error graphs and save them to disk
    plt.figure()
    plt.plot(model.history.history['loss'])
    plt.plot(model.history.history['val_loss'])
    plt.title('model error')
    plt.ylabel('MSE')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    figname = args["path_saved_model"][:-3] + ".png"
    plt.savefig(figname)
    plt.close()

    # to_json crashes with this model so we use save_weights()
    model.save_weights(args["path_saved_model"])
    logging.info("Model saved to file {}".format(args["path_saved_model"]))


if __name__ == "__main__":
    main()
