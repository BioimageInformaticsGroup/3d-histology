# architecture
import os
import gc
import re
import sys
import time
import logging
import pickle
from typing import List
from argparse import ArgumentParser

import scipy
import natsort
import numpy as np
from joblib import Parallel, delayed
from skimage.io import imread
from skimage.color import rgb2hsv, combine_stains, hdx_from_rgb, separate_stains

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

os.environ["KERAS_BACKEND"] = "tensorflow"
from keras import backend as K
K.set_image_data_format("channels_last")
from keras.layers import Input, Conv2D, UpSampling2D, MaxPooling2D, BatchNormalization, K
from keras.models import Model
from keras.utils import Sequence

sys.path.insert(0, "../..")

from biit_image_tools.imagetools import filter as biitfilter
import feats_AE_data as data
import feats_AE_train
import compress

logging.basicConfig(level=logging.INFO)


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("path_to_model", help="Path to .h5 model", type=str)
    parser.add_argument("output_path_base", help="Path to output base directory", type=str)
    parser.add_argument("--save_examples", help="Save example reconstruction images", default=None, type=int)
    parser.add_argument("--num_cores", help="Number of cores to use for feature extraction", default=1, type=int)
    parser.add_argument("--features_dir_name", help="Name of the features directory", default="autoencoder_features", type=str)
    parser.add_argument("--only_for_mouse", help="Extract features only for selected mouse", default=None, type=str)
    args = vars(parser.parse_args())
    return args


def check_arguments(args):
    os.makedirs(args["output_path_base"], exist_ok=True)


def plot_reconstructed_test_data(model: Model, path_save_dir: str = None, N_images: int = 10):
    logging.info("Plot reconstructions")

    batch_idx = 0
    testgen = feats_AE_train.TrainSequence(
        data.VALIDATION_DATA, batch_size=N_images, tile_size=data.INPUT_SHAPE[0], num_channels=data.NUM_CHANNELS
    )

    x_train, y_train = testgen.__getitem__(batch_idx)

    mses = []
    for i, n in enumerate(range(x_train.shape[0])):

        original = x_train[i, :, :, :]
        reconstructed = model.predict(original.reshape((1, original.shape[0], original.shape[1], original.shape[2])))
        reconstructed = np.squeeze(reconstructed)

        # compute mse between images
        mse = (((original - reconstructed).ravel()) ** 2).mean()
        mses.append(mse)

        plt.figure()
        plt.subplot(1, 2, 1)
        plt.imshow(original)
        plt.title("original image")
        plt.axis("off")
        plt.subplot(1, 2, 2)
        plt.imshow(reconstructed)
        plt.title("reconstructed, MSE {:.5f}".format(mse))
        plt.axis("off")
        plt.tight_layout()

        if path_save_dir is None:
            plt.show()
        else:
            os.makedirs(path_save_dir, exist_ok=True)
            # save figure
            plt.savefig(os.path.join(path_save_dir, "patch_fig_{}.png".format(n)))
            # save images separately also
            # plt.imsave(os.path.join(path_save_dir, "reconstructed_patch_{}.png".format(n)), reconstructed)
            # plt.imsave(os.path.join(path_save_dir, "original_patch_{}.png".format(n)), original)
            plt.close()

    # logging.info("======= RESULTS {} test images =========")
    # logging.info("MSE mean: {}".format(np.array(mses).mean()))
    # logging.info("MSE std: {}".format(np.array(mses).std()))
    # logging.info("========================================")


def image_bandpass_filter(image: np.ndarray, kernel_size: int, sigma1: float, sigma2: float):
    # make sure sigma 1 is smaller
    if sigma1 > sigma2:
        sigma2, sigma1 = sigma1, sigma2

    k1 = biitfilter.gaussian_kernel(
        kernel_size_x=kernel_size, kernel_size_y=kernel_size, sigma_x=sigma1, sigma_y=sigma1
    )
    k2 = biitfilter.gaussian_kernel(
        kernel_size_x=kernel_size, kernel_size_y=kernel_size, sigma_x=sigma2, sigma_y=sigma2
    )
    # create bandpass filter by subtracting kernels
    k = k1 - k2
    bp = scipy.signal.convolve2d(image, k, mode="same", boundary="symm")
    return bp


def compute_fband_stats(image: np.ndarray, fname: str):
    abssum = np.abs(image).sum()
    minimum = image.min()
    maximum = image.max()
    energy = (image ** 2).sum()
    mean_energy = energy / image.size

    F = []
    Fnames = []

    F.append(abssum)
    Fnames.append("{}_abssum".format(fname))

    F.append(image.var())
    Fnames.append("{}_var".format(fname))

    F.append(minimum)
    Fnames.append("{}_min".format(fname))

    F.append(maximum)
    Fnames.append("{}_max".format(fname))

    F.append(minimum / maximum)
    Fnames.append("{}_min_max_ratio".format(fname))

    F.append(energy)
    Fnames.append("{}_energy".format(fname))

    F.append(mean_energy)
    Fnames.append("{}_mean_energy".format(fname))

    return F, Fnames


def bandpass_stats(image: np.ndarray, fname: str):
    F = []
    Fnames = []

    kernel_size = 101
    filter_step = 120 / 4
    filter_pair_sigmas = np.arange(0, 120, filter_step)
    filter_pair_sigmas = list(zip(filter_pair_sigmas, filter_pair_sigmas + filter_step))

    # precompute filters
    unique_sigmas = np.unique(filter_pair_sigmas)
    filters = {}
    for s in unique_sigmas:
        k1 = biitfilter.gaussian_kernel_symmetric(kernel_size=kernel_size, sigma=s)
        filters[str(s)] = k1

    # Iterate over different frequency bands and compute features
    for s1, s2 in filter_pair_sigmas:
        # create bandpass filter by subtracting kernels
        k = filters[str(s1)] - filters[str(s2)]
        bp = scipy.signal.convolve2d(image, k, mode="same", boundary="symm")

        # plt.subplot(121)
        # plt.imshow(image)
        # plt.subplot(122)
        # plt.imshow(bp.astype(np.float), cmap="gray")
        # plt.title("{}, {}".format(s1, s2))
        # plt.show()

        Fbp, Fbp_names = compute_fband_stats(bp, "{}_band_s{}-s{}".format(fname, int(s1), int(s2)))

        F.extend(Fbp)
        Fnames.extend(Fbp_names)

    return F, Fnames


def im_stats(image: np.ndarray, fname: str):
    summ = image.sum()
    mean = image.mean()
    med = np.median(image)
    minimum = image.min()
    maximum = image.max()
    energy = (image ** 2).sum()
    mean_energy = energy / image.size
    F = []
    Fnames = []
    F.append(summ)
    Fnames.append("{}_sum".format(fname))
    F.append(mean)
    Fnames.append("{}_mean".format(fname))
    F.append(med)
    Fnames.append("{}_median".format(fname))
    F.append(image.var())
    Fnames.append("{}_var".format(fname))
    F.append(minimum)
    Fnames.append("{}_min".format(fname))
    F.append(maximum)
    Fnames.append("{}_max".format(fname))
    F.append(minimum / maximum)
    Fnames.append("{}_min_max_ratio".format(fname))
    F.append(mean - med)
    Fnames.append("{}_mean_med_dev".format(fname))
    F.append(energy)
    Fnames.append("{}_energy".format(fname))
    F.append(mean_energy)
    Fnames.append("{}_mean_energy".format(fname))
    return F, Fnames


def compute_image_features(image: np.ndarray, fname: str):
    F, Fnames = im_stats(image, "{}".format(fname))

    # stats for each hsv component separately
    hsv = rgb2hsv(image)
    Ff, Ffnames = im_stats(hsv[:, :, 0], "{}_Hcomp".format(fname))
    F.extend(Ff)
    Fnames.extend(Ffnames)
    Ff, Ffnames = im_stats(hsv[:, :, 1], "{}_Scomp".format(fname))
    F.extend(Ff)
    Fnames.extend(Ffnames)
    Ff, Ffnames = im_stats(hsv[:, :, 2], "{}_Vcomp".format(fname))
    F.extend(Ff)
    Fnames.extend(Ffnames)

    # # bandpass stats for each hsv component separately
    # Fbp, Fbp_names = bandpass_stats(hsv[:, :, 0], "{}_Hcomp".format(fname))
    # F.extend(Fbp)
    # Fnames.extend(Fbp_names)
    # Fbp, Fbp_names = bandpass_stats(hsv[:, :, 1], "{}_Scomp".format(fname))
    # F.extend(Fbp)
    # Fnames.extend(Fbp_names)
    # Fbp, Fbp_names = bandpass_stats(hsv[:, :, 2], "{}_Vcomp".format(fname))
    # F.extend(Fbp)
    # Fnames.extend(Fbp_names)

    return F, Fnames


def load_model(path_model: str):
    # load model
    model, encoder, decoder, model_name = feats_AE_train.create_model(data.INPUT_SHAPE)

    # there were some issues loading the model
    wait_times = [10, 20, 100, 200, 300]
    for wt in wait_times:
        try:
            model.load_weights(path_model)
        except KeyError:
            logging.error(f"Loading model failed, waiting {wt}s. Wait times: {wait_times}")
            time.sleep(wt)
            continue
        break

    return model, encoder, decoder


def TicTocGenerator():
    """ Generator that returns time differences """
    ti = 0  # initial time
    tf = time.time()  # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf - ti  # returns the time difference


TicToc = TicTocGenerator()  # create an instance of the TicTocGen generator


# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    """ Prints the time difference yielded by generator instance TicToc """
    tempTimeInterval = next(TicToc)
    if tempBool:
        print("Elapsed time: %f seconds.\n" % tempTimeInterval)


def tic():
    """ Records a time in TicToc, marks the beginning of a time interval """
    toc(False)


def get_ae_features(fns: List[str], encoder: Model, decoder: Model, model: Model):
    logging.info("Extracting features for {} images".format(len(fns)))

    # get output
    features = []
    for i, fn in enumerate(fns):
        logging.debug("Loading image: {}".format(fn))
        im = imread(fn, plugin="pil").astype(np.float64)
        im_resh = im.reshape((1, *im.shape))

        # compute bottleneck features
        botfeats = encoder.predict(im_resh)

        # compute reconstructed image features
        reconstructed = decoder.predict(botfeats).reshape(im.shape)
        recfeats, recnames = compute_image_features(reconstructed, "reconst")

        # compute reconstruction error features
        error_im = im - reconstructed
        errfeats, errnames = compute_image_features(error_im, "recerr")

        # concatenate features
        tile_features = np.concatenate([botfeats.ravel(), np.array(recfeats).ravel(), np.array(errfeats).ravel()])
        features.append(tile_features)

    # create feature names outside loop
    feature_names = []
    botnames = ["Autoencoded{}".format(i) for i in range(botfeats.size)]
    # concatenate them together
    feature_names.extend(botnames)
    feature_names.extend(recnames)
    feature_names.extend(errnames)

    features = np.array(features)
    return features, feature_names


def parse_tile_coordinates_from_fn(fn):
    fn_no_ext = ".".join(os.path.basename(fn).split(".")[:-1])
    # parse coordinates from filenames
    match = re.findall("[0-9]+?_[0-9]+?_[0-9]+?_[0-9]+?$", fn_no_ext)[0]
    sp = match.split("_")
    ymin = sp[0]
    ymax = sp[1]
    xmin = sp[2]
    xmax = sp[3]
    return ymin, ymax, xmin, xmax


def extract_and_save(fns: List[str], encoder: Model, decoder: Model, model: Model, save_path: str):
    """ Save list of filenames into single serialized object on disk"""

    feats, fnames = get_ae_features(fns, encoder, decoder, model)

    # create a dictionary with ymin and xmin as key
    save_dict = {}
    for fn, f in zip(fns, feats):
        ymin, ymax, xmin, xmax = parse_tile_coordinates_from_fn(fn)
        # insert data into dict
        # save_dict["{},{}".format(ymin, xmin)] = (fn, f, fnames)
        save_dict["{},{}".format(ymin, xmin)] = (fn, f)

    save_dict["feature_names"] = fnames

    logging.info(f"Saving features to a data structure {save_path}")
    # save dictionary
    compress.save_object(save_path, save_dict)


def extract_slice_ids(fns: List[str]):
    slice_id = []
    for p in fns:
        base = os.path.basename(p)
        ite = re.finditer("-[0-9]*?_", base)
        for s in ite:
            pass
        slic = base[: s.end() - 1]
        slice_id.append(slic)
    return slice_id


def extract_and_save_all(output_path: str, encoder: Model, decoder: Model, model: Model, num_cores: int, features_dir_name: str, only_for_mouse: str = None):

    # comment out dirs here to compute only for select dirs
    mice_dirs = [
        "PTENX036-011",
        "PTENX036-14",
        "PTENX036-15",
        "PTENX036-20",
        "PTENX036-21",
        "PTENX036-32",
        "PTENX036-33",
        "PTENX036-34",
        "PTENX036-39",
        "PTENX036-40",
        "PTENX036-42",
        "PTENX036-68",
        "UTU186-xhiMyc-003",
        "UTU186-xhiMyc-006",
        "UTU186-xhiMyc-007",
        "UTU186-xhiMyc-008",
        "UTU186-xhiMyc-022",
        "UTU186-xhiMyc-045",
        "UTU186-xhiMyc-199",
        "UTU186-xhiMyc-211",
        "UTU186-xhiMyc-223",
        "UTU186-xhiMyc-355",
        "UTU186-xhiMyc-356",
        "UTU186-xhiMyc-1387",
    ]

    # we wish to save all features to the corresponding mice directory (names listed above)
    # to be able to easily recompute features in case of changes
    # however mice filenames have much variability so one consistent part
    # is the number (here called mouse_dir_id) after ptenx036 or xhiMyc so let's use that

    # extract mouse dir number part e.g. 011 in PTENX036-011 
    mouse_dir_ids = [re.findall("-[0-9]*?$", m)[0][1:] for m in mice_dirs]
    slice_ids = extract_slice_ids(data.TEST_DATA)
    uniq_slices = np.unique(slice_ids)

    # prepare parallel call
    # Iterate over slices and save AE features one file per slice
    out_list = []
    slice_fns_list = []
    for i, s in enumerate(uniq_slices, start=1):
        logging.info(f"Preparing datastructures for feature exctraction {i}/{len(uniq_slices)}")
        # extract mouse id
        mouse_id = re.sub("-[0-9]*?$", "", s)
        # extract mouse dir id
        mouse_dir_id = re.findall("-[0-9]*?$", mouse_id)[0][1:]
        # find correct directory for current slice
        try:
            dir_idx = mouse_dir_ids.index(mouse_dir_id)
        except ValueError:
            if mouse_dir_id[0] == "0":
                # try omitting first zero (e.g. 032 -> 32) if index is not found
                dir_idx = mouse_dir_ids.index(mouse_dir_id[1:])
        mouse_dir = mice_dirs[dir_idx]

        # If mouse dir does not match mouse user wants to process, skip to next loop iteration
        # only slices from the selected mouse gets computed
        if (only_for_mouse is not None) and (only_for_mouse != mouse_dir): 
            continue

        # filter out filenames that do not belong to current slice
        # put undersocre after slice name to prevent something-1 matching something-10 etc.
        slice_fns = list(filter(lambda x: x.find(s + "_") != -1, data.TEST_DATA))
        # sort files to ensure systematic order
        slice_fns = natsort.natsorted(slice_fns)
        slice_fns_list.append(slice_fns)

        # output paths
        out_dir = os.path.join(output_path, mouse_dir, features_dir_name)
        os.makedirs(out_dir, exist_ok=True)
        out = os.path.join(out_dir, "AE_feats_{}".format(s))
        out_list.append(out)

    logging.info("Processing slices:")
    for s in slice_fns_list:
        logging.info("    {}".format(s))

    # # iterate over each slice to save features from one slice (all tiles from one slice) to one file
    # # in parallel
    # Parallel(n_jobs=num_cores)(
    #     delayed(extract_and_save)(slice_fns_list[i], encoder, decoder, model, out_list[i])
    #     for i in range(len(slice_fns_list))
    # )
    # gc.collect()

    # iterate over each slice to save features from one slice (all tiles from one slice) to one file
    # single core joblib alternative
    for sfns, sout in zip(slice_fns_list, out_list):
        extract_and_save(sfns, encoder, decoder, model, sout)



def print_feature_information(encoder: Model, decoder: Model, model: Model, example_fn):
    logging.info("Print feature information:")
    feats, fnames = get_ae_features([example_fn], encoder, decoder, model)
    logging.info("    AE features shape: {}".format(feats.shape))
    logging.info("    AE feature names length: {}".format(len(fnames)))


def main():
    args = parse_arguments()
    check_arguments(args)
    
    logging.info(f"Extracting features for mouse {args['only_for_mouse']}")
    logging.info(f"Using model {args['path_to_model']}")

    model, encoder, decoder = load_model(args["path_to_model"])

    # print info
    # print_feature_information(encoder, decoder, model, data.TEST_DATA[0])

    # save reconstrcted images if needed
    if (args["save_examples"] is not None) and (args["save_examples"] > 0):
        logging.info(f"Saving {args['save_examples']} example images")
        # if output_path_is_given, the function saves the images to disk
        plot_reconstructed_test_data(model, "reconstructed_tiles", N_images=args["save_examples"])

        # # show images instead of saving to disk
        # matplotlib.use('Qt5Agg')
        # plot_reconstructed_test_data(model, N_images=args["save_examples"])

    # extract and save features in parallel
    extract_and_save_all(args["output_path_base"], encoder, decoder, model, args["num_cores"], args["features_dir_name"], args["only_for_mouse"])


if __name__ == "__main__":
    main()
