import os
import re
import sys
import glob
import socket
import logging
from typing import List
from argparse import ArgumentParser
from argparse import ArgumentTypeError
from pprint import pprint

import matplotlib.transforms
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import seaborn as sns

sys.path.insert(0, "../")
sys.path.insert(0, "../../")

from compress import load_object
from biit_wsi_io import wsiio

NUM_FEATS_BLOCK = 250
NUM_FEATS_NUCLEI = 602

# path to image data for interactive plot
INTERACTIVE_PLOT_DATA_PATH = "./interactive_plot_data"


# basic config did not work for me, for some reason
# logging.basicConfig(level=logging.DEBUG)
logging.getLogger().setLevel(logging.INFO)


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise ArgumentTypeError("Boolean value expected.")


def interactive_scatter(
    scatter_fig,
    display_fig,
    axes_object_scatter,
    axes_object_image_view,
    scatter_object,
    plot_labels,
    display_image_fns,
    display_image_tile_coords,
    display_image_reduction_level,
):
    """Interactive scatter plot

    "Extension" to matplotlib scatter plot to display image on provided axes object

    Use biit_wsi_io to read image from file and use ranom access (if supported)
    to read only the tile specified in display_image_tile_coords.

    Only a tile from display image is showed at once, not the whole image

    # Get figure handles like this
    fig = plt.figure()
    ax = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    sc = ax.scatter(..)

    Args:
        scatter_fig (matplotlib figure): Handle to scatter figure
        display_fig (matplotlib figure): Handle to tile display figure
        axes_object_scatter (matplotlib axes): Handle to
        axes_object_image_view (matplotlib axes):
        scatter_object (matplotlib scatter object):
        plot_labels: Label to show on mouse click
        display_image_fns: Path to the image from which tile is shown
        display_image_tile_coords (List[List[int]]): Tile coordinates in the image. Inner lists should have coordinates (y_min, y_max, x_min, x_max)

    """
    annot = axes_object_scatter.annotate(
        "",
        xy=(0, 0),
        xytext=(20, 20),
        textcoords="offset points",
        bbox=dict(boxstyle="round", fc="w"),
        arrowprops=dict(arrowstyle="->"),
    )
    annot.set_visible(False)

    # image to dispaly when hovering not on a dot
    empty_image = np.zeros((1, 1))

    def update_annot(ind):
        # select first if multiple dots under cursor
        # idx = ind["ind"][0]
        idx = ind["ind"][-1]
        pos = scatter_object.get_offsets()[idx]
        annot.xy = pos
        text = "{}".format(plot_labels[idx])
        annot.set_text(text)
        # annot.get_bbox_patch().set_facecolor(cmap(norm(c[ind["ind"][0]])))
        annot.get_bbox_patch().set_alpha(0.4)

    def event_callback(event):
        vis = annot.get_visible()
        if event.inaxes == axes_object_scatter:
            cont, ind = scatter_object.contains(event)
            if cont:
                update_annot(ind)
                annot.set_visible(True)
                scatter_fig.canvas.draw_idle()
                # select last if multiple dots under cursor
                # seems like the last ones are those also drawn last and stay on top
                idx = ind["ind"][-1]
                impath = display_image_fns[idx]
                # select coodinates matching tile location
                tile_coords = display_image_tile_coords[idx].astype(int)

                # USE BIITWSI_IMREAD
                # read tile directly from correct jp2 image by using jp2 random access support
                # convert from ymin, ymax, xmin, xmax
                # to ymin, xmin, ymax, xmax
                tile_coords = (tile_coords[0], tile_coords[2], tile_coords[1], tile_coords[3])
                tile, _ = wsiio.biitwsi_imread(impath, display_image_reduction_level, tile_coords)

                # # USE SKIMAGE/IMAGEIO
                # # in case of tiffs, read whole image and crop
                # tile = skimage.io.imread(impath, plugin="imageio")
                # tile = tile[tile_coords[0] : tile_coords[1], tile_coords[2] : tile_coords[3]]
                # if display_image_reduction_level > 0:
                #     factor = 1 / (2 ** display_image_reduction_level)
                #     tile = skimage.io.resize(tile, (tile.shape[:2] * factor).astype(int))

                # display image
                axes_object_image_view.imshow(tile)
                # redraw display figure
                display_fig.canvas.draw_idle()

            else:
                if vis:
                    annot.set_visible(False)
                    scatter_fig.canvas.draw_idle()
                    axes_object_image_view.imshow(empty_image)

    # https://matplotlib.org/3.1.1/users/event_handling.html
    # scatter_fig.canvas.mpl_connect("motion_notify_event", event_callback)
    scatter_fig.canvas.mpl_connect("button_press_event", event_callback)

    plt.draw()


def legend(ax, x0=1, y0=1, direction="v", padpoints=3, **kwargs):
    otrans = ax.figure.transFigure
    t = ax.legend(bbox_to_anchor=(x0, y0), loc=1, bbox_transform=otrans, **kwargs)
    plt.tight_layout(pad=0)
    ax.figure.canvas.draw()
    plt.tight_layout(pad=0)
    ppar = [0, -padpoints / 72.0] if direction == "v" else [-padpoints / 72.0, 0]
    trans2 = (
        matplotlib.transforms.ScaledTranslation(ppar[0], ppar[1], ax.figure.dpi_scale_trans)
        + ax.figure.transFigure.inverted()
    )
    tbox = t.get_window_extent().transformed(trans2)
    bbox = ax.get_position()
    if direction == "v":
        ax.set_position([bbox.x0, bbox.y0, bbox.width, tbox.y0 - bbox.y0])
    else:
        ax.set_position([bbox.x0, bbox.y0, tbox.x0 - bbox.x0, bbox.height])


def this_scatter_plot_is_cancer(
    features: str,
    labels: List,
    unique_labels: List = None,  # This is needed to ensure correct order when matching colors with labels
    unique_label_colors: List = None,
    label_legends: List = None,
    unique_label_transparencies: List = None,
    save_path: str = None,
    save_dpi: int = 200,
    sample_annotations: List[str] = None,  # TODO
    title: str = None,
    display_image_fns: List[str] = None,
    display_image_tile_coords: List[List[int]] = None,
    marker_size: float = None,
):
    """Docstring example

    Additional example paragraph

    Args:
        display_image_fns (int):
        display_image_tile_coords (str): make sure tile coordinates match images

    Returns:
        bool: The return value. True for success, False otherwise.

    """

    # TODO: LABEL COLORS NEED TO BE RGB? Trasparencies are appaneded to that to get RGBA

    if unique_labels is None:
        unique_labels = np.unique(labels).tolist()

    # check that dimensions match
    logging.debug("Scatter plot: unique labels: {}".format(unique_labels))
    assert len(features) == len(labels)
    if unique_label_colors is not None:
        assert len(unique_labels) == len(unique_label_colors)
    if unique_label_transparencies is not None:
        assert len(unique_labels) == len(unique_label_transparencies)

    # # if colors no given, use some good default colors
    # if unique_label_colors is None:
    #     # old color cycle
    #     # color_cycle = ["b", "g", "r", "c", "m", "y", "k"]
    #     # seaborn default color cycle
    #     # use category10 colormap if need for matplotlib default color cycle
    #     color_cycle = [
    #         (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
    #         (1.0, 0.4980392156862745, 0.054901960784313725),
    #         (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
    #         (0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
    #         (0.5803921568627451, 0.403921568627451, 0.7411764705882353),
    #         (0.5490196078431373, 0.33725490196078434, 0.29411764705882354),
    #         (0.8901960784313725, 0.4666666666666667, 0.7607843137254902),
    #         (0.4980392156862745, 0.4980392156862745, 0.4980392156862745),
    #         (0.7372549019607844, 0.7411764705882353, 0.13333333333333333),
    #         (0.09019607843137255, 0.7450980392156863, 0.8117647058823529),
    #     ]
    #     # len(label_colors_for_each_sample) == len(features)
    #     label_colors_for_each_sample = [color_cycle[unique_labels.index(l) % len(color_cycle)] for l in labels]
    #     # len(unique_label_colors) == len(unique_labels)
    #     unique_label_colors = [color_cycle[unique_labels.index(l) % len(color_cycle)] for l in unique_labels]
    # else:
    #     label_colors_for_each_sample = [unique_label_colors[unique_labels.index(l)].tolist() for l in labels]

    # if unique_label_transparencies is not None:
    #     # create a list of transparenices for each sample
    #     # len(label_transparencies_for_each_sample) == len(features)
    #     label_transparencies_for_each_sample = [unique_label_transparencies[unique_labels.index(l)] for l in labels]
    #     # Unzip and rezip to make RGBA colors
    #     R, G, B = zip(*label_colors_for_each_sample)
    #     label_colors_for_each_sample = list(zip(R, G, B, label_transparencies_for_each_sample))

    fig = plt.figure()
    fig2 = plt.figure()
    sns.set_style("darkgrid")
    sns.color_palette()
    if save_path is None:
        # NOTE: This can be done with 3d projection as well
        ax = fig.add_subplot(111, aspect="equal")
        ax2 = fig2.add_subplot(111, aspect="equal")
        # hide grid and tick markers for display axes
        ax.axis("off")
        ax2.axis("off")
    else:
        ax = plt.gca()

    fig.set_size_inches((8, 8))
    fig2.set_size_inches((8, 8))

    # !!!!!!!!!!!!

    # Colors from matlab's distinguishable_colors function
    color_cycle = [
        (0, 0, 1.0000),
        (1.0000, 0, 0),
        (0, 1.0000, 0),
        (0, 0, 0.1724),
        (1.0000, 0.1034, 0.7241),
        (1.0000, 0.8276, 0),
        (0, 0.3448, 0),
        (0.5172, 0.5172, 1.0000),
        (0.6207, 0.3103, 0.2759),
        (0, 1.0000, 0.7586),
        (0, 0.5172, 0.5862),
        (0, 0, 0.4828),
        (0.5862, 0.8276, 0.3103),
        (0.9655, 0.6207, 0.8621),
        (0.8276, 0.0690, 1.0000),
        (0.4828, 0.1034, 0.4138),
        (0.9655, 0.0690, 0.3793),
        (1.0000, 0.7586, 0.5172),
        (0.1379, 0.1379, 0.0345),
        (0.5517, 0.6552, 0.4828),
        (0.9655, 0.5172, 0.0345),
        (0.5172, 0.4483, 0),
        (0.4483, 0.9655, 1.0000),
        (0.6207, 0.7586, 1.0000),
        (0.4483, 0.3793, 0.4828),
        (0.6207, 0, 0),
        (0, 0.3103, 1.0000),
        (0, 0.2759, 0.5862),
        (0.8276, 1.0000, 0),
        (0.7241, 0.3103, 0.8276),
        (0.2414, 0, 0.1034),
        (0.9310, 1.0000, 0.6897),
        (1.0000, 0.4828, 0.3793),
        (0.2759, 1.0000, 0.4828),
        (0.0690, 0.6552, 0.3793),
        (0.8276, 0.6552, 0.6552),
        (0.8276, 0.3103, 0.5172),
        (0.4138, 0, 0.7586),
        (0.1724, 0.3793, 0.2759),
        (0, 0.5862, 0.9655),
    ]

    # color_default = np.array([31, 119, 180])/255
    # color_default = np.array([50, 50, 50]) / 255
    # color_default = np.array((0, 1.0000, 0.7586))
    color_default = np.array((80, 80, 80)) / 255

    uni, uni_counts = np.unique(labels, return_counts=True)
    idx = np.where(uni == "Normal")[0][0]
    uni_nonormal = np.delete(uni, idx)
    uni_counts_nonormal = np.delete(uni_counts, idx)

    lesion_color_idx = list(range(len(uni_nonormal)))
    idx_dict = dict(zip(uni_nonormal, lesion_color_idx))

    label_colors_for_each_sample = []
    for lab in labels:
        if lab == "Normal":
            label_colors_for_each_sample.append(color_default)
        else:
            # idx = int(re.sub("Lesion ([0-9])", r"\1", lab)) - 1

            label_colors_for_each_sample.append(color_cycle[idx_dict[lab]])

    ff = np.array(labels)
    inds = []
    for u in uni:
        ii = np.where(ff == u)[0]
        inds.append(ii)

    fig = plt.figure()
    sns.set_style("darkgrid")
    sns.color_palette()
    ax = fig.add_subplot(111)

    label_colors_for_each_sample = np.array(label_colors_for_each_sample)

    # plot separately and in this order to have tumor colors on top
    sort_idx = np.argsort(uni_counts)
    sort_idx = sort_idx[::-1]  # make descending order
    inds = np.array(inds)[sort_idx].tolist()
    uni = np.array(uni)[sort_idx].tolist()
    uni_counts = np.array(uni_counts)[sort_idx].tolist()

    for ii, u in zip(inds, uni):
        # sc = ax.scatter(FD["feats"][ii, 0], FD["feats"][ii, 1], color=label_colors_for_each_sample[ii], s=marker_size, label=u)
        # sc = ax.scatter(features[:, 0], features[:, 1], color=label_colors_for_each_sample, s=marker_size, label=u)
        sc = ax.scatter(
            features[ii, 0], features[ii, 1], color=label_colors_for_each_sample[ii], s=marker_size, label=u
        )
    # plt.xlabel(FD["feats_names"][0])
    # plt.ylabel(FD["feats_names"][1])
    plt.legend(uni)

    if title is not None:
        ax.set_title(title)

    # if label_legends is not None:
    #     # create a custom legend
    #     legend_dict = dict(zip(unique_labels, unique_label_colors))
    #     patchList = []
    #     for key in legend_dict:
    #         data_key = mpatches.Patch(color=legend_dict[key], label=key)
    #         patchList.append(data_key)
    #     lgd = ax.legend(loc="upper left", handles=patchList)
    #     # make draggable legend
    #     lgd = matplotlib.legend.DraggableLegend(lgd)

    savep = save_path
    savedir = os.path.dirname(savep)
    if not os.path.exists(savedir):
        os.makedirs(savedir)
    pathnoext, ext = os.path.splitext(savep)
    savep = "{}{}".format(pathnoext, ext)
    print(f"Saving plot {savep}")
    if ext.lower() == ".jpg" or ext.lower() == ".jpeg":
        plt.savefig(savep, dpi=save_dpi, format="jpg", quality=90)
    else:
        plt.savefig(savep, dpi=save_dpi)
    plt.close()


def pca_loadings_plot(
    features: str,
    labels: List,
    unique_labels: List = None,  # This is needed to ensure correct order when matching colors with labels
    unique_label_colors: List = None,
    label_legends: List = None,
    unique_label_transparencies: List = None,
    save_path: str = None,
    save_dpi: int = 200,
    sample_annotations: List[str] = None,  # TODO
    title: str = None,
    display_image_fns: List[str] = None,
    display_image_tile_coords: List[List[int]] = None,
    marker_size: float = None,
    pca_object = None,
    arrow_labels = None,
    plot_n_loadings = None,
    show_only_loadings: bool = False,
    loadings_sort_method: str = "both_norm"
):
    """Docstring example

    Additional example paragraph

    Args:
        display_image_fns (int):
        display_image_tile_coords (str): make sure tile coordinates match images

    Returns:
        bool: The return value. True for success, False otherwise.

    """

    # TODO: LABEL COLORS NEED TO BE RGB? Trasparencies are appaneded to that to get RGBA

    if unique_labels is None:
        unique_labels = np.unique(labels).tolist()

    # check that dimensions match
    logging.debug("Scatter plot: unique labels: {}".format(unique_labels))
    assert len(features) == len(labels)
    if unique_label_colors is not None:
        assert len(unique_labels) == len(unique_label_colors)
    if unique_label_transparencies is not None:
        assert len(unique_labels) == len(unique_label_transparencies)

    # if colors no given, use some good default colors
    if unique_label_colors is None:
        # old color cycle
        # color_cycle = ["b", "g", "r", "c", "m", "y", "k"]
        # seaborn default color cycle
        # use category10 colormap if need for matplotlib default color cycle
        color_cycle = [
            (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
            (1.0, 0.4980392156862745, 0.054901960784313725),
            (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
            (0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
            (0.5803921568627451, 0.403921568627451, 0.7411764705882353),
            (0.5490196078431373, 0.33725490196078434, 0.29411764705882354),
            (0.8901960784313725, 0.4666666666666667, 0.7607843137254902),
            (0.4980392156862745, 0.4980392156862745, 0.4980392156862745),
            (0.7372549019607844, 0.7411764705882353, 0.13333333333333333),
            (0.09019607843137255, 0.7450980392156863, 0.8117647058823529),
        ]
        # len(label_colors_for_each_sample) == len(features)
        label_colors_for_each_sample = [color_cycle[unique_labels.index(l) % len(color_cycle)] for l in labels]
        # len(unique_label_colors) == len(unique_labels)
        unique_label_colors = [color_cycle[unique_labels.index(l) % len(color_cycle)] for l in unique_labels]
    else:
        label_colors_for_each_sample = [unique_label_colors[unique_labels.index(l)].tolist() for l in labels]

    if unique_label_transparencies is not None:
        # create a list of transparenices for each sample
        # len(label_transparencies_for_each_sample) == len(features)
        label_transparencies_for_each_sample = [unique_label_transparencies[unique_labels.index(l)] for l in labels]
        # Unzip and rezip to make RGBA colors
        R, G, B = zip(*label_colors_for_each_sample)
        label_colors_for_each_sample = list(zip(R, G, B, label_transparencies_for_each_sample))

    fig = plt.figure()
    fig2 = plt.figure()
    sns.set_style("darkgrid")
    sns.color_palette()
    if save_path is None:
        # NOTE: This can be done with 3d projection as well
        ax = fig.add_subplot(111, aspect="equal")
        ax2 = fig2.add_subplot(111, aspect="equal")
        # hide grid and tick markers for display axes
        ax.axis("off")
        ax2.axis("off")
    else:
        ax = plt.gca()

    fig.set_size_inches((8, 8))
    fig2.set_size_inches((8, 8))

    # Plot scores
    xs = features[:, 0]
    ys = features[:, 1]
    scalex = xs.max() - xs.min()
    scaley = ys.max() - ys.min()
    print(f"Scale x: {scalex}")
    print(f"Scale y: {scaley}")

    if show_only_loadings is False: # plot also score plot
        sc = ax.scatter(features[:, 0], features[:, 1], color=label_colors_for_each_sample, s=marker_size)

    # plot loadings
    # after transpose: original features on y axis, principal axes on x axis
    coeff = np.transpose(pca_object.components_[0:2, :])

    arrows = np.copy(coeff)
    # sort arrows according to most meaningful 
    # sum components from both PC axes
    if loadings_sort_method == "both_norm":
        coeff_sort_scores = np.sqrt(arrows[:, 0] ** 2 + arrows[:, 1] ** 2)
    elif loadings_sort_method == "both_sum":
        coeff_sort_scores = np.abs(arrows[:, 0]) + np.abs(arrows[:, 1])
    elif loadings_sort_method == "pc1":
        coeff_sort_scores = np.abs(arrows[:, 0])
    elif loadings_sort_method == "pc2":
        coeff_sort_scores = np.abs(arrows[:, 1])

    # pprint(f"Label legends before sort: {label_legends}") 

    sort_inds = np.argsort(coeff_sort_scores)
    # make sort descending 
    sort_inds = sort_inds[::-1]
    # sort arrows
    arrows = arrows[sort_inds, :]
    # sort feature_names
    label_legends = [label_legends[s] for s in sort_inds]
        
    # largest
    # plot_scaling_coeff = np.linalg.norm(arrows[0, :])
    # median
    plot_scaling_coeff = np.linalg.norm(arrows[int(coeff.shape[0]/2), :])

    text_positions = np.copy(arrows)

    # pprint(f"Label legends after sort: {label_legends}") 

    N = coeff.shape[0] if plot_n_loadings is None else plot_n_loadings
    # add numbers to labels and overwrite label_legends
    label_legends = [f"{i+1}: {l}" for i, l in enumerate(label_legends)]
    with open(save_path + ".txt", 'w') as f:
        for l in label_legends:
            f.write(l + "\n")

    arrow_handles = []
    for i in range(N):
        # print(f"i: {i}")
        # prevent drawing more text to the same location
        # add y offset if close to another already drawn coordinate 

        # for 3D
        min_allowed_dist = 0.012
        # for 3D + matlab
        # min_allowed_dist = 0.0012

        distance_to_closest_drawn_label = -np.inf
        counter = 0
        while distance_to_closest_drawn_label < min_allowed_dist:
            counter += 1
            # print(f"counter: {counter}")
            # compute distances to previously drawn labels
            diff = text_positions[:i, :] - text_positions[i, :]
            # pprint(f"Diff: {diff}")
            dists = np.sqrt(diff[:, 0] ** 2 + diff[:, 1] ** 2)
            # pprint(f"dists: {dists}")
            if dists.size == 0:
                break
            distance_to_closest_drawn_label = dists.min()
            # print(f"distance_to_closest_drawn_label: {distance_to_closest_drawn_label}")
            if distance_to_closest_drawn_label < min_allowed_dist:
                # print(f"incr")
                # increment y position only
                text_positions[i, 1] += 0.0001
                # text_positions[i, 1] += plot_scaling_coeff * 0.01
            else:
                break
            if counter > 1000:
                break

        
        # draw arrow
        # arrow_color = 'r' if i < 10 else 'b'
        arrow_color = 'r'

        ah = plt.arrow(0, 0, arrows[i, 0], arrows[i, 1], color = arrow_color, alpha = 0.5)
        arrow_handles.append(ah)

        # draw text
        # text_color =  'r' if i < 10 else 'k'
        text_color =  'k'

        # create fixed length arrow to the same direction as the vector itself
        text_offset = 0.15 * plot_scaling_coeff * text_positions[i, :] / np.sqrt(np.sum(text_positions[i, :] ** 2))
        if arrow_labels is None:
            plt.text(text_positions[i, 0] + text_offset[0], text_positions[i, 1] + text_offset[1], str(i + 1), color = text_color, ha = 'center', va = 'center')
        else:
            plt.text(text_positions[i, 0] + text_offset[0], text_positions[i, 1] + text_offset[1], arrow_labels[i], color = text_color, ha = 'center', va = 'center')


    if show_only_loadings:
        limit_multi = 1.4
        min0 = arrows[:, 0].min()
        max0 = arrows[:, 0].max()
        min1 = arrows[:, 1].min()
        max1 = arrows[:, 1].max()
        xl = np.array([min0, max0]) * limit_multi
        yl = np.array([min1, max1]) * limit_multi
        plt.xlim(xl)
        plt.ylim(yl)

        # # with text labels
        # # limit_multi = text_multi * 1.6
        # # with number labels
        # limit_multi = 1.4
        # min0 = arrows[:, 0].min()
        # max0 = arrows[:, 0].max()
        # min1 = arrows[:, 1].min()
        # max1 = arrows[:, 1].max()
        # xl = [limit_multi * (min0 + np.sign(min0) * text_offset[0]), limit_multi * (max0 + np.sign(max0) * text_offset[0])]
        # yl = [limit_multi * (min1 + np.sign(min1) * text_offset[1]), limit_multi * (max1 + np.sign(max1) * text_offset[1])]
        # plt.xlim(xl)
        # plt.ylim(yl)

    if title is not None:
        ax.set_title(title)

    if label_legends is not None:

        patchList = []
        for key in label_legends[:N]:
            data_key = mpatches.Patch(color=(236/255.0, 236/255.0, 242/255.0, 1), label=key)
            patchList.append(data_key)

        # Shrink current axis by 20%
        box = ax.get_position()

        fig2.set_size_inches((10, 8))

        scalee_x = 0.65
        scalee_y = 0.90
        offsett = 0.04
        ax.set_position([box.x0 - offsett, box.y0, box.width * scalee_x, box.height * scalee_y])

        lgd = ax.legend(loc="upper right", handles=patchList, bbox_to_anchor=(1.75, 1.01))
        # make draggable legend
        # lgd = matplotlib.legend.DraggableLegend(lgd)

        for h in lgd.legendHandles:
            h.set_width(0.01)
            h.set_height(0.01)


    
    plt.xlabel("PC1")
    plt.ylabel("PC2")
    
    if save_path is None:
        # if save_path not given plot create interactive plot
        # comment out interactive_scatter() and leave just plt.show() to get regular matplotlib scatter

        interactive_scatter(
            scatter_fig=fig,
            display_fig=fig2,
            axes_object_scatter=ax,
            axes_object_image_view=ax2,
            scatter_object=sc,
            plot_labels=labels,
            display_image_fns=display_image_fns,
            display_image_tile_coords=display_image_tile_coords,
            display_image_reduction_level=0,
        )
        plt.show()
    else:
        # save figure instead of showing it if save_path given
        savep = save_path
        savedir = os.path.dirname(savep)
        if not os.path.exists(savedir):
            os.makedirs(savedir)
        pathnoext, ext = os.path.splitext(savep)
        savep = "{}{}".format(pathnoext, ext)
        print(f"Saving plot {savep}")
        if ext.lower() == ".jpg" or ext.lower() == ".jpeg":
            plt.savefig(savep, dpi=save_dpi, format="jpg", quality=90)
        else:
            plt.savefig(savep, dpi=save_dpi)
        plt.close()


# def pca_biplot(
#     features: str,
#     labels: List,
#     unique_labels: List = None,  # This is needed to ensure correct order when matching colors with labels
#     unique_label_colors: List = None,
#     label_legends: List = None,
#     unique_label_transparencies: List = None,
#     save_path: str = None,
#     save_dpi: int = 200,
#     sample_annotations: List[str] = None,  # TODO
#     title: str = None,
#     display_image_fns: List[str] = None,
#     display_image_tile_coords: List[List[int]] = None,
#     marker_size: float = None,
#     pca_object = None,
#     feats_names = None,
#     plot_n_loadings = None,
#     show_only_loadings: bool = False
# ):
#     """Docstring example

#     Additional example paragraph

#     Args:
#         display_image_fns (int):
#         display_image_tile_coords (str): make sure tile coordinates match images

#     Returns:
#         bool: The return value. True for success, False otherwise.

#     """

#     # TODO: LABEL COLORS NEED TO BE RGB? Trasparencies are appaneded to that to get RGBA

#     if unique_labels is None:
#         unique_labels = np.unique(labels).tolist()

#     # check that dimensions match
#     logging.debug("Scatter plot: unique labels: {}".format(unique_labels))
#     assert len(features) == len(labels)
#     if unique_label_colors is not None:
#         assert len(unique_labels) == len(unique_label_colors)
#     if unique_label_transparencies is not None:
#         assert len(unique_labels) == len(unique_label_transparencies)

#     # if colors no given, use some good default colors
#     if unique_label_colors is None:
#         # old color cycle
#         # color_cycle = ["b", "g", "r", "c", "m", "y", "k"]
#         # seaborn default color cycle
#         # use category10 colormap if need for matplotlib default color cycle
#         color_cycle = [
#             (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
#             (1.0, 0.4980392156862745, 0.054901960784313725),
#             (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
#             (0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
#             (0.5803921568627451, 0.403921568627451, 0.7411764705882353),
#             (0.5490196078431373, 0.33725490196078434, 0.29411764705882354),
#             (0.8901960784313725, 0.4666666666666667, 0.7607843137254902),
#             (0.4980392156862745, 0.4980392156862745, 0.4980392156862745),
#             (0.7372549019607844, 0.7411764705882353, 0.13333333333333333),
#             (0.09019607843137255, 0.7450980392156863, 0.8117647058823529),
#         ]
#         # len(label_colors_for_each_sample) == len(features)
#         label_colors_for_each_sample = [color_cycle[unique_labels.index(l) % len(color_cycle)] for l in labels]
#         # len(unique_label_colors) == len(unique_labels)
#         unique_label_colors = [color_cycle[unique_labels.index(l) % len(color_cycle)] for l in unique_labels]
#     else:
#         label_colors_for_each_sample = [unique_label_colors[unique_labels.index(l)].tolist() for l in labels]

#     if unique_label_transparencies is not None:
#         # create a list of transparenices for each sample
#         # len(label_transparencies_for_each_sample) == len(features)
#         label_transparencies_for_each_sample = [unique_label_transparencies[unique_labels.index(l)] for l in labels]
#         # Unzip and rezip to make RGBA colors
#         R, G, B = zip(*label_colors_for_each_sample)
#         label_colors_for_each_sample = list(zip(R, G, B, label_transparencies_for_each_sample))

#     fig = plt.figure()
#     fig2 = plt.figure()
#     sns.set_style("darkgrid")
#     sns.color_palette()
#     if save_path is None:
#         # NOTE: This can be done with 3d projection as well
#         ax = fig.add_subplot(111, aspect="equal")
#         ax2 = fig2.add_subplot(111, aspect="equal")
#         # hide grid and tick markers for display axes
#         ax.axis("off")
#         ax2.axis("off")
#     else:
#         ax = plt.gca()

#     fig.set_size_inches((8, 8))
#     fig2.set_size_inches((8, 8))

#     # Plot scores
#     xs = features[:, 0]
#     ys = features[:, 1]
#     scalex = xs.max() - xs.min()
#     scaley = ys.max() - ys.min()
#     print(f"Scale x: {scalex}")
#     print(f"Scale y: {scaley}")

#     if show_only_loadings is False: # plot also score plot
#         sc = ax.scatter(features[:, 0], features[:, 1], color=label_colors_for_each_sample, s=marker_size)

#     # plot loadings
#     coeff = np.transpose(pca_object.components_[0:2, :])

#     # for 3D
#     arrows = np.copy(coeff)
#     arrows[:, 0] = 1.2 * scalex * arrows[:, 0]
#     arrows[:, 1] = 1.2 * scaley * arrows[:, 1]
#     text_positions = np.copy(arrows)

#     # # for 3D + matlab
#     # arrows = np.copy(coeff)
#     # arrows[:, 0] = 10.0 * scalex * arrows[:, 0]
#     # arrows[:, 1] = 10.0 * scaley * arrows[:, 1]
    
#     N = coeff.shape[0] if plot_n_loadings is None else plot_n_loadings
#     for i in range(N):

#         # prevent drawing more text to the same location
#         # add y offset if close to another already drawn coordinate 
#         allowed_dist = 0.3
#         min_dist = -np.inf
#         while min_dist < allowed_dist:

#             # compute distances to previously drawn arrows
#             diff = text_positions[:i, :] - text_positions[i, :]
#             # pprint(f"Diff: {diff}")

#             dists = np.sqrt(diff[:, 0] ** 2 + diff[:, 1] ** 2)
#             if not np.any(dists):
#                 break

#             # pprint(f"dists: {dists}")

#             min_dist = dists.min()
#             print(f"Min_dist: {min_dist}")

#             if min_dist < allowed_dist:
#                 # increment y position
#                 text_positions[i, 1] += 0.001

        
#         # draw arrow
#         arrow_color = 'r' if i < 10 else 'b'
#         plt.arrow(0, 0, arrows[i, 0], arrows[i, 1], color = arrow_color, alpha = 0.5)

#         # draw text
#         text_color =  'r' if i < 10 else 'k'
#         text_multi = 1.25
#         if feats_names is None:
#             plt.text(text_positions[i, 0] * text_multi, text_positions[i, 1] * text_multi, str(i + 1), color = text_color, ha = 'center', va = 'center')
#         else:
#             plt.text(text_positions[i, 0] * text_multi, text_positions[i, 1] * text_multi, feats_names[i], color = text_color, ha = 'center', va = 'center')


#     if show_only_loadings:
#         limit_multi = text_multi * 1.6
#         xl = [limit_multi * arrows[:, 0].min(), limit_multi * arrows[:, 0].max()]
#         yl = [limit_multi * arrows[:, 1].min(), limit_multi * arrows[:, 1].max()]
#         plt.xlim(xl)
#         plt.ylim(yl)

#     if title is not None:
#         ax.set_title(title)

#     if label_legends is not None:
#         # create a custom legend
#         legend_dict = dict(zip(unique_labels, unique_label_colors))
#         patchList = []
#         for key in legend_dict:
#             data_key = mpatches.Patch(color=legend_dict[key], label=key)
#             patchList.append(data_key)
#         lgd = ax.legend(loc="upper right", handles=patchList, bbox_to_anchor=(1.13, 1.00))
#         # lgd = ax.legend(loc="upper left", handles=patchList)
#         # make draggable legend
#         lgd = matplotlib.legend.DraggableLegend(lgd)

#     if save_path is None:
#         # if save_path not given plot create interactive plot
#         # comment out interactive_scatter() and leave just plt.show() to get regular matplotlib scatter

#         interactive_scatter(
#             scatter_fig=fig,
#             display_fig=fig2,
#             axes_object_scatter=ax,
#             axes_object_image_view=ax2,
#             scatter_object=sc,
#             plot_labels=labels,
#             display_image_fns=display_image_fns,
#             display_image_tile_coords=display_image_tile_coords,
#             display_image_reduction_level=0,
#         )
#         plt.show()
#     else:
#         # save figure instead of showing it if save_path given
#         savep = save_path
#         savedir = os.path.dirname(savep)
#         if not os.path.exists(savedir):
#             os.makedirs(savedir)
#         pathnoext, ext = os.path.splitext(savep)
#         savep = "{}{}".format(pathnoext, ext)
#         print(f"Saving plot {savep}")
#         if ext.lower() == ".jpg" or ext.lower() == ".jpeg":
#             plt.savefig(savep, dpi=save_dpi, format="jpg", quality=90)
#         else:
#             plt.savefig(savep, dpi=save_dpi)
#         plt.close()



def scatter_plot(
    features: str,
    labels: List,
    unique_labels: List = None,  # This is needed to ensure correct order when matching colors with labels
    unique_label_colors: List = None,
    label_legends: List = None,
    unique_label_transparencies: List = None,
    save_path: str = None,
    save_dpi: int = 200,
    sample_annotations: List[str] = None,  # TODO
    title: str = None,
    display_image_fns: List[str] = None,
    display_image_tile_coords: List[List[int]] = None,
    marker_size: float = None,
):
    """Docstring example

    Additional example paragraph

    Args:
        display_image_fns (int):
        display_image_tile_coords (str): make sure tile coordinates match images

    Returns:
        bool: The return value. True for success, False otherwise.

    """

    # TODO: LABEL COLORS NEED TO BE RGB? Trasparencies are appaneded to that to get RGBA

    if unique_labels is None:
        unique_labels = np.unique(labels).tolist()

    # check that dimensions match
    logging.debug("Scatter plot: unique labels: {}".format(unique_labels))
    assert len(features) == len(labels)
    if unique_label_colors is not None:
        assert len(unique_labels) == len(unique_label_colors)
    if unique_label_transparencies is not None:
        assert len(unique_labels) == len(unique_label_transparencies)

    # if colors no given, use some good default colors
    if unique_label_colors is None:
        # old color cycle
        # color_cycle = ["b", "g", "r", "c", "m", "y", "k"]
        # seaborn default color cycle
        # use category10 colormap if need for matplotlib default color cycle
        color_cycle = [
            (0.12156862745098039, 0.4666666666666667, 0.7058823529411765),
            (1.0, 0.4980392156862745, 0.054901960784313725),
            (0.17254901960784313, 0.6274509803921569, 0.17254901960784313),
            (0.8392156862745098, 0.15294117647058825, 0.1568627450980392),
            (0.5803921568627451, 0.403921568627451, 0.7411764705882353),
            (0.5490196078431373, 0.33725490196078434, 0.29411764705882354),
            (0.8901960784313725, 0.4666666666666667, 0.7607843137254902),
            (0.4980392156862745, 0.4980392156862745, 0.4980392156862745),
            (0.7372549019607844, 0.7411764705882353, 0.13333333333333333),
            (0.09019607843137255, 0.7450980392156863, 0.8117647058823529),
        ]
        # len(label_colors_for_each_sample) == len(features)
        label_colors_for_each_sample = [color_cycle[unique_labels.index(l) % len(color_cycle)] for l in labels]
        # len(unique_label_colors) == len(unique_labels)
        unique_label_colors = [color_cycle[unique_labels.index(l) % len(color_cycle)] for l in unique_labels]
    else:
        label_colors_for_each_sample = [unique_label_colors[unique_labels.index(l)].tolist() for l in labels]

    if unique_label_transparencies is not None:
        # create a list of transparenices for each sample
        # len(label_transparencies_for_each_sample) == len(features)
        label_transparencies_for_each_sample = [unique_label_transparencies[unique_labels.index(l)] for l in labels]
        # Unzip and rezip to make RGBA colors
        R, G, B = zip(*label_colors_for_each_sample)
        label_colors_for_each_sample = list(zip(R, G, B, label_transparencies_for_each_sample))

    fig = plt.figure()
    fig2 = plt.figure()
    sns.set_style("darkgrid")
    sns.color_palette()
    if save_path is None:
        # NOTE: This can be done with 3d projection as well
        ax = fig.add_subplot(111, aspect="equal")
        ax2 = fig2.add_subplot(111, aspect="equal")
        # hide grid and tick markers for display axes
        ax.axis("off")
        ax2.axis("off")
    else:
        ax = plt.gca()

    fig.set_size_inches((8, 8))
    fig2.set_size_inches((8, 8))

    sc = ax.scatter(features[:, 0], features[:, 1], color=label_colors_for_each_sample, s=marker_size)

    if title is not None:
        ax.set_title(title)

    if label_legends is not None:
        # create a custom legend
        legend_dict = dict(zip(unique_labels, unique_label_colors))
        patchList = []
        for key in legend_dict:
            data_key = mpatches.Patch(color=legend_dict[key], label=key)
            patchList.append(data_key)
        lgd = ax.legend(loc="upper right", handles=patchList, bbox_to_anchor=(1.13, 1.00))
        # lgd = ax.legend(loc="upper left", handles=patchList)
        # make draggable legend
        lgd = matplotlib.legend.DraggableLegend(lgd)

    if save_path is None:
        # if save_path not given plot create interactive plot
        # comment out interactive_scatter() and leave just plt.show() to get regular matplotlib scatter

        interactive_scatter(
            scatter_fig=fig,
            display_fig=fig2,
            axes_object_scatter=ax,
            axes_object_image_view=ax2,
            scatter_object=sc,
            plot_labels=labels,
            display_image_fns=display_image_fns,
            display_image_tile_coords=display_image_tile_coords,
            display_image_reduction_level=0,
        )
        plt.show()
    else:
        # save figure instead of showing it if save_path given
        savep = save_path
        savedir = os.path.dirname(savep)
        if not os.path.exists(savedir):
            os.makedirs(savedir)
        pathnoext, ext = os.path.splitext(savep)
        savep = "{}{}".format(pathnoext, ext)
        print(f"Saving plot {savep}")
        if ext.lower() == ".jpg" or ext.lower() == ".jpeg":
            plt.savefig(savep, dpi=save_dpi, format="jpg", quality=90)
        else:
            plt.savefig(savep, dpi=save_dpi)
        plt.close()


# def create_unique_label_colors(labels):
#     """ One base color for a mouse and all tumors have slightly different shade of that """
#     # len(labels) == len(label_mice)
#     unique_labels = np.unique(labels)
#     regex_mouse = "Tilelabel[0-9]+_(.*)-$"
#     label_mice = [re.findall(regex_mouse, lab) for lab in labels]
#     unique_mice = np.unique(label_mice).tolist()
#     logging.info("Unique mice: {}".format(unique_mice))
#     # use qualitative colormap to get distinct colors
#     cmap = plt.cm.get_cmap("tab10")
#     mice_base_colors = [cmap(color_ind)[0:3] for color_ind in np.arange(0, 1, 1 / len(unique_mice))]
#     tumor_color_offset = np.array([0.01, 0.01, 0.01]) * 7
#     # generate list with mice_colors and tumor_color_offset
#     regex_tilelabel = "Tilelabel([0-9]+)_.*-$"
#     unique_label_colors = []
#     for lab in unique_labels:
#         tilelabel = int(re.findall(regex_tilelabel, lab)[0])
#         mouse = re.findall(regex_mouse, lab)[0]
#         mouse_color = np.array(mice_base_colors[unique_mice.index(mouse)])
#         lcolor = mouse_color + tumor_color_offset * tilelabel
#         # make sure values are within 0-1
#         lcolor = np.array([min(1, max(0, v)) for v in lcolor])
#         logging.debug("mouse {}, label {} -> color {}".format(mouse, tilelabel, lcolor))
#         unique_label_colors.append(lcolor)
#     return unique_label_colors


def create_unique_label_colors(labels):
    """ One base color for a mouse and all tumors have slightly different shade of that """
    # len(labels) == len(label_mice)
    unique_labels = np.unique(labels)
    regex_mouse = "Tilelabel[0-9]+_(.*)-$"
    label_mice = [re.findall(regex_mouse, lab) for lab in labels]
    unique_mice = np.unique(label_mice).tolist()
    logging.info("Unique mice: {}".format(unique_mice))
    # use qualitative colormap to get distinct colors
    cmap = plt.cm.get_cmap("tab10")
    mice_base_colors = [cmap(color_ind)[0:3] for color_ind in np.arange(0, 1, 1 / len(unique_mice))]
    tumor_color_offset = np.array([0.01, 0.01, 0.01]) * 7
    # generate list with mice_colors and tumor_color_offset
    regex_tilelabel = "Tilelabel([0-9]+)_.*-$"
    unique_label_colors = []
    for lab in unique_labels:
        tilelabel = int(re.findall(regex_tilelabel, lab)[0])
        mouse = re.findall(regex_mouse, lab)[0]
        mouse_color = np.array(mice_base_colors[unique_mice.index(mouse)])
        lcolor = mouse_color + tumor_color_offset * tilelabel
        # make sure values are within 0-1
        lcolor = np.array([min(1, max(0, v)) for v in lcolor])
        logging.debug("mouse {}, label {} -> color {}".format(mouse, tilelabel, lcolor))
        unique_label_colors.append(lcolor)
    return unique_label_colors


def create_unique_label_colors_by_genotype(labels):
    """ One base color for a mouse and all tumors have slightly different shade of that """
    
    # len(labels) == len(label_mice)
    unique_labels = np.unique(labels)
    regex_mouse = "Tilelabel[0-9]+_(.*)-$"
    label_mice = [re.findall(regex_mouse, lab) for lab in labels]
    unique_mice = np.unique(label_mice).tolist()
    logging.info("Unique mice: {}".format(unique_mice))

    # dict of mice genotypes
    genotypes_list = [1, 2, 1, 1, 2, 2]
    gen_dict = dict(zip(unique_mice, genotypes_list))
    unique_genotypes = np.unique(genotypes_list).tolist()

    # use qualitative colormap to get distinct colors
    cmap = plt.cm.get_cmap("tab10")
    genotype_colors = [cmap(color_ind)[0:3] for color_ind in np.arange(0, 1, 1 / len(unique_genotypes))]
    regex_tilelabel = "Tilelabel([0-9]+)_.*-$"
    unique_label_colors = []
    for lab in unique_labels:
        tilelabel = int(re.findall(regex_tilelabel, lab)[0])
        mouse = re.findall(regex_mouse, lab)[0]
        genotype = gen_dict[mouse]
        lcolor = np.array(genotype_colors[unique_genotypes.index(genotype)])
        # make sure values are within 0-1
        lcolor = np.array([min(1, max(0, v)) for v in lcolor])
        logging.debug(f"mouse {mouse}, label {tilelabel} -> genotype {genotype}, color {lcolor}")
        unique_label_colors.append(lcolor)
    return unique_label_colors


def create_unique_label_colors_by_mouse(labels):
    """ One base color for a mouse and all tumors have slightly different shade of that """
    # len(labels) == len(label_mice)
    unique_labels = np.unique(labels)
    regex_mouse = "Tilelabel[0-9]+_(.*)-$"
    label_mice = [re.findall(regex_mouse, lab) for lab in labels]
    unique_mice = np.unique(label_mice).tolist()
    logging.info("Unique mice: {}".format(unique_mice))
    # use qualitative colormap to get distinct colors
    cmap = plt.cm.get_cmap("tab10")
    mice_base_colors = [cmap(color_ind)[0:3] for color_ind in np.arange(0, 1, 1 / len(unique_mice))]
    regex_tilelabel = "Tilelabel([0-9]+)_.*-$"
    unique_label_colors = []
    for lab in unique_labels:
        tilelabel = int(re.findall(regex_tilelabel, lab)[0])
        mouse = re.findall(regex_mouse, lab)[0]
        mouse_color = np.array(mice_base_colors[unique_mice.index(mouse)])
        lcolor = mouse_color
        # make sure values are within 0-1
        lcolor = np.array([min(1, max(0, v)) for v in lcolor])
        logging.debug("mouse {}, label {} -> color {}".format(mouse, tilelabel, lcolor))
        unique_label_colors.append(lcolor)
    return unique_label_colors


def create_color_by_feature_values(color_by_feature: str, tsne_dict: dict):
    """ One base color for a mouse and all tumors have slightly different shade of that """
    unique_labels = np.unique(tsne_dict["feats_labels"])
    cmap = plt.cm.get_cmap("bwr")

    # find featue and scale it between 0 and 1
    fea = tsne_dict["original_features"]
    names = tsne_dict["feats_names"]
    try:
        feat_ind = names.index(color_by_feature)
    except ValueError:
        logging.warning(f"Feature '{color_by_feature}' is not in feature names")
        return None
    fe = fea[:, feat_ind]
    fe_scaled = fe - fe.min()
    fe_scaled = fe / fe.max()

    unique_label_colors = [cmap(color_ind)[0:3] for color_ind in fe_scaled]

    assert len(unique_labels) == len(
        unique_label_colors
    ), "Number of distinct colors should be the same as unique labels"

    return np.array(unique_label_colors)


def plot_tsne_objects(
    fns: List[str],
    path_output_dir: str = None,
    highlights: bool = True,
    color_by_feature: str = None,
    color_by_mouse: bool = False,
    color_by_genotype: bool = False,
    marker_size: float = None,
):
    for fn in fns:
        if path_output_dir is not None:
            outpath = os.path.join(path_output_dir, os.path.splitext(fn)[0])
        else:
            outpath = None

        plot_tsne_object(
            fn,
            outpath,
            highlights=highlights,
            color_by_feature=color_by_feature,
            color_by_mouse=color_by_mouse,
            color_by_genotype=color_by_genotype,
            marker_size=marker_size,
        )


def rename_lesions(names_list):

    # # replace mice
    # tumor_labels = [re.sub("ptenx036-011-", "A", l, flags=re.IGNORECASE) for l in names_list]
    # tumor_labels = [re.sub("ptenx036-14-", "B", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-15-", "C", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-20-", "D", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-21-", "E", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-032-", "F", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-033-", "G", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-0?34-", "H", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-0?39-", "I", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-0?40-", "J", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-0?42-", "K", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("ptenx036-0?68-", "L", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?003-", "M", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?006-", "N", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?007-", "O", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?008-", "P", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?022-", "Q", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?045-", "R", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?1387-", "S", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?199-", "T", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?211-", "U", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?223-", "V", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?355-", "W", l, flags=re.IGNORECASE) for l in tumor_labels]
    # tumor_labels = [re.sub("utu186-?xhimyc-?356-", "X", l, flags=re.IGNORECASE) for l in tumor_labels]
    # # replace tumors
    # tumor_labels = [re.sub("Tilelabel([1-9]+)_", r"\1", l) for l in tumor_labels]
    # # tumor_labels = [re.sub("Tilelabel0_", "normal_", l) for l in tumor_labels]
    # # swap labels
    # tumor_labels = [re.sub("([0-9])([A-Z])", r"\2\1", l) for l in tumor_labels]

    # replace mice
    tumor_labels = [re.sub("ptenx036-011-", "1", l, flags=re.IGNORECASE) for l in names_list]
    tumor_labels = [re.sub("ptenx036-14-", "2", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-15-", "4", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-20-", "5", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-21-", "7", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-032-", "6", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-033-", "3", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-0?34-", "8", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-0?39-", "9", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-0?40-", "10", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-0?42-", "11", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("ptenx036-0?68-", "12", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?003-", "13", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?006-", "14", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?007-", "15", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?008-", "16", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?022-", "17", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?045-", "18", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?1387-", "19", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?199-", "20", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?211-", "21", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?223-", "22", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?355-", "23", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub("utu186-?xhimyc-?356-", "24", l, flags=re.IGNORECASE) for l in tumor_labels]

    # replace tumors
    new_tumor_labels = []
    for l in tumor_labels:
        match = re.search("Tilelabel([0-9]+)", l)
        if l[match.start() : match.end()] == "Tilelabel0":
            new_tumor_labels.append("Normal tissue")
        else:
            new_l = chr(int(l[match.start() : match.end()].replace("Tilelabel", "")) + 64)
            new_tumor_labels.append(new_l + l[match.end() :])

    # swap labels
    tumor_labels = [re.sub("([A-Z])_([0-9])", r"\2 \1", l) for l in new_tumor_labels]

    # use continuous labels 1a 1b 1c instead of 1a 1c 1d
    rename_dict = dict()
    for tl in range(1, 25):
        unique_labels_per_mouse = []
        for i, lab in enumerate(tumor_labels):
            match = re.findall(f"{tl} [A-Z]", lab)
            if match:
                unique_labels_per_mouse.append(match)
        old_labels = np.unique(unique_labels_per_mouse)
        new_labels = [f"{tl}{chr(num + 96 + 1)}" for num in range(len(unique_labels_per_mouse))]
        # append renaming rules to rename_dict
        # update extends existing dict
        rename_dict.update(dict(zip(old_labels, new_labels)))
    # use rename_dict to rename labels
    tumor_labels = [rename_dict[lab] for lab in tumor_labels]

    return tumor_labels


def rename_labels_by_genotype(names_list):

    # replace labels
    tumor_labels = [re.sub(".*ptenx036-011-.*", "G1", l, flags=re.IGNORECASE) for l in names_list]
    tumor_labels = [re.sub(".*ptenx036-14-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-15-.*", "G2", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-20-.*", "G2", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-21-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-032-.*", "G2", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-033-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-0?34-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    # the rest are wrong but not used
    tumor_labels = [re.sub(".*ptenx036-0?39-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-0?40-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-0?42-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*ptenx036-0?68-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?003-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?006-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?007-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?008-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?022-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?045-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?1387-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?199-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?211-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?223-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?355-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]
    tumor_labels = [re.sub(".*utu186-?xhimyc-?356-.*", "G1", l, flags=re.IGNORECASE) for l in tumor_labels]

    # # replace tumors
    # new_tumor_labels = []
    # for l in tumor_labels:
    #     match = re.search("Tilelabel([0-9]+)", l)
    #     if l[match.start() : match.end()] == "Tilelabel0":
    #         new_tumor_labels.append("Normal tissue")
    #     else:
    #         new_l = chr(int(l[match.start() : match.end()].replace("Tilelabel", "")) + 64)
    #         new_tumor_labels.append(new_l + l[match.end() :])
    # # swap labels
    # tumor_labels = [re.sub("([A-Z])_([0-9])", r"\2 \1", l) for l in new_tumor_labels]

    # # use continuous labels 1a 1b 1c instead of 1a 1c 1d
    # rename_dict = dict()
    # for tl in range(1, 25):
    #     unique_labels_per_mouse = []
    #     for i, lab in enumerate(tumor_labels):
    #         match = re.findall(f"{tl} [A-Z]", lab)
    #         if match:
    #             unique_labels_per_mouse.append(match)
    #     old_labels = np.unique(unique_labels_per_mouse)
    #     new_labels = [f"{tl}{chr(num + 96 + 1)}" for num in range(len(unique_labels_per_mouse))]
    #     # append renaming rules to rename_dict
    #     # update extends existing dict
    #     rename_dict.update(dict(zip(old_labels, new_labels)))
    # # use rename_dict to rename labels
    # tumor_labels = [rename_dict[lab] for lab in tumor_labels]

    return tumor_labels



def plot_tsne_object(
    fn: str,
    path_output_dir: str = None,
    highlights: bool = True,
    marker_size: float = None,
    color_by_feature: str = None,
    color_by_mouse: bool = False,
    color_by_genotype: bool = False,
):
    tsne_dict = load_object(fn)
    logging.debug("TSNE dictionary keys: {}".format(tsne_dict.keys()))
    features = tsne_dict["features"]
    labels = tsne_dict["feats_labels"]
    method = tsne_dict["method"]
    perplexity = tsne_dict["perplexity"]
    fname = os.path.basename(fn).split(".")[0]
    label_legends = tsne_dict["label_legends"].tolist()

    # CREATE PLOT COLORS
    if color_by_feature is not None:
        unique_labels = np.unique(labels).tolist()
        unique_label_colors = create_color_by_feature_values(color_by_feature, tsne_dict)
        if unique_label_colors is None:
            # return if trying to plot something silly like coloring
            # scatter with 'volume' feature when using matlab features
            logging.warning(f"Skipping tsne_object {fn}")
            return

        # rename labels from Tilelabel[0-9].. format to 1 A , 1 B ...
        labels = rename_lesions(labels)
        unique_labels = rename_lesions(unique_labels)
    # if labels are in the format "Tilelabel[0-9]_mouseid" then use specific colors for labels
    elif color_by_mouse:
        unique_labels = np.unique(labels).tolist()
        unique_label_colors = create_unique_label_colors_by_mouse(labels)
        # rename labels from Tilelabel[0-9].. format to 1 A , 1 B ...
        labels = rename_lesions(labels)
        unique_labels = rename_lesions(unique_labels)

    elif color_by_genotype:
        labels = rename_labels_by_genotype(labels)
        unique_labels = ["G1", "G2"]
        # unique_label_colors = create_unique_label_colors_by_genotype(labels)  # unnecessarily complex

        # # blue and brown
        # unique_label_colors = [np.array([0.12156863, 0.46666667, 0.70588235]), np.array([0.54901961, 0.3372549 , 0.29411765])]

        # yellow and green
        yellow = np.array([240, 222, 50]) / 255.0
        green = np.array([61, 207, 50]) / 255.0
        unique_label_colors = [yellow, green]

        # unique_label_colors
        # rename labels from Tilelabel[0-9].. format to 1 A , 1 B ...
        # labels = rename_lesions(labels)
        # unique_labels = rename_lesions(unique_labels)

    elif labels[0].find("Tilelabel") == 0:
        unique_labels = np.unique(labels).tolist()
        unique_label_colors = create_unique_label_colors(labels)
        # rename labels from Tilelabel[0-9].. format to 1 A , 1 B ...
        labels = rename_lesions(labels)
        unique_labels = rename_lesions(unique_labels)
        label_legends = rename_lesions(label_legends)
    else:
        unique_labels = None
        unique_label_colors = None

    # CREATE TITLE STRINGS
    # colorbyfeat
    colorbyfeat_title_str = " colors from feature " + color_by_feature if color_by_feature is not None else ""
    # perplexity
    if method == "tsne":
        perplexity_title_str = f" Perplexity {perplexity} "
    elif method == "umap":
        perplexity_title_str = f" n_neighbors {perplexity} "
    elif method == "pca":
        perplexity_title_str = ""
    else:
        raise ValueError(f"Unrecognized method {method}")

    if path_output_dir is None:
        # PLOT SCATTER INTERACTIVELY

        # create display filenames
        try:
            displ_fns = tsne_dict["feats_fns"]
            displ_fns = [os.path.basename(p) for p in displ_fns]
            # Get rid of Features_ pefix and change .mat to .tiff or .jp2
            displ_fns = [os.path.splitext(p)[0][9:] + ".jp2" for p in displ_fns]
            displ_fns = [os.path.join(INTERACTIVE_PLOT_DATA_PATH, p) for p in displ_fns]
        except:
            displ_fns = None

        try:
            tile_coordinates = tsne_dict["feats_coords"]
        except:
            tile_coordinates = None

        if highlights:
            # Iterate over different highlited transparencies
            non_highlight_transparency = 0.05
            for transp_ind in range(len(label_legends)):
                transparencies = [non_highlight_transparency] * len(label_legends)
                transparencies[transp_ind] = 1

                scatter_plot(
                    features,
                    labels,
                    unique_labels=unique_labels,
                    title=f"Filename: {fn}\n{perplexity_title_str} - highlighted label: {label_legends[transp_ind]}",
                    label_legends=label_legends,
                    unique_label_colors=unique_label_colors,
                    unique_label_transparencies=transparencies,
                    display_image_fns=displ_fns,
                    display_image_tile_coords=tile_coordinates,
                    marker_size=marker_size,
                )
                # TODO:
                plt.show(block=True)
                # plt.close()
        else:

            scatter_plot(
                features,
                labels,
                unique_labels=unique_labels,
                title=f"Filename: {fn}\n{perplexity_title_str}\n{colorbyfeat_title_str}",
                label_legends=label_legends,
                unique_label_colors=unique_label_colors,
                display_image_fns=displ_fns,
                display_image_tile_coords=tile_coordinates,
                marker_size=marker_size,
            )
            # TODO:
            plt.show(block=True)
            # plt.close()

    else:

        # SAVE PLOTS, NON-INTERACTIVE

        if highlights:
            # Iterate over different highlighted transparencies
            non_highlight_transparency = 0.05
            for transp_ind in range(len(label_legends)):
                transparencies = [non_highlight_transparency] * len(label_legends)
                transparencies[transp_ind] = 1

                # SAVE REGULAR SCATTER PLOT TO DISK
                save_ext = ".png"
                savepath = os.path.join(path_output_dir, f"{fname}_{transp_ind}{save_ext}")
                scatter_plot(
                    features,
                    labels,
                    unique_labels=unique_labels,
                    title="{perplexity_title_str} - highlighted label: {label_legends[transp_ind]}",
                    label_legends=label_legends,
                    unique_label_colors=unique_label_colors,
                    unique_label_transparencies=transparencies,
                    save_path=savepath,
                    marker_size=marker_size,
                )
        else:
            # SAVE REGULAR SCATTER PLOT TO DISK
            save_ext = ".png"
            savepath = os.path.join(path_output_dir, f"{fname}{save_ext}")

            if path_output_dir.startswith("figures_whole_stack"):
                print("GOING TO USE THE CANCER PLOT")
                this_scatter_plot_is_cancer(
                    features,
                    labels,
                    unique_labels=unique_labels,
                    title=f"{perplexity_title_str}\n{colorbyfeat_title_str}",
                    label_legends=label_legends,
                    unique_label_colors=unique_label_colors,
                    save_path=savepath,
                    save_dpi=600,
                    marker_size=marker_size,
                )
            else:
                scatter_plot(
                    features,
                    labels,
                    unique_labels=unique_labels,
                    title=f"{perplexity_title_str}\n{colorbyfeat_title_str}",
                    label_legends=label_legends,
                    unique_label_colors=unique_label_colors,
                    save_path=savepath,
                    marker_size=marker_size,
                )
                # in case of PCA, plot also biplot (score plot (regular) + loading plot)
                if method.lower() == "pca":
                    # for n_loads in [5, 10, 15, 20, 25, 30, 35, 40, 45, 50]:
                    for n_loads in [20, None]:
                        savepath = os.path.join(path_output_dir, f"{fname}_biplot_{n_loads}loadings{save_ext}")

                        # # biplots
                        # pca_biplot(
                        #     features,
                        #     labels,
                        #     unique_labels=unique_labels,
                        #     title=f"{perplexity_title_str}\n{colorbyfeat_title_str}",
                        #     label_legends=label_legends,
                        #     unique_label_colors=unique_label_colors,
                        #     save_path=savepath,
                        #     marker_size=marker_size,
                        #     pca_object=tsne_dict["object"],
                        #     feats_names=tsne_dict["feats_names"],
                        #     plot_n_loadings=n_loads
                        # )

                        # # biplots, with feature names on arrows (pretty cluttered)
                        # pca_biplot(
                        #     features,
                        #     labels,
                        #     unique_labels=unique_labels,
                        #     title=f"{perplexity_title_str}\n{colorbyfeat_title_str}",
                        #     label_legends=None, #!!
                        #     unique_label_colors=unique_label_colors,
                        #     save_path=savepath,
                        #     marker_size=marker_size,
                        #     pca_object=tsne_dict["object"],
                        #     feats_names=None,
                        #     # feats_names=tsne_dict["feats_names"],
                        #     plot_n_loadings=n_loads,
                        #     show_only_loadings=True
                        # )

                        # loadings plot, with numbers on arrows
                        pca_loadings_plot(
                            features,
                            labels,
                            unique_labels=unique_labels,
                            title=f"{perplexity_title_str}\n{colorbyfeat_title_str}",
                            label_legends=tsne_dict["feats_names"],
                            unique_label_colors=unique_label_colors,
                            save_path=savepath,
                            marker_size=marker_size,
                            pca_object=tsne_dict["object"],
                            arrow_labels=None,
                            plot_n_loadings=n_loads,
                            show_only_loadings=True
                        )



def listdir_fullpath_filter(path_dir: str, regex: str = None):
    fns = os.listdir(path_dir)
    fns = [os.path.join(path_dir, f) for f in fns]
    if regex is not None and regex != "":
        matches = [re.findall(regex, f) for f in fns]
        fns = [f[0] for f in filter(None, matches)]
    return fns


def listdir_fullpath_recursive(path_dir: str, files_only: bool = True, N_first: int = None):
    path_dir_full = os.path.abspath(path_dir)
    if N_first:
        fns = [next(glob.iglob(os.path.join(path_dir_full, "**"), recursive=True)) for i in range(N_first)]
    else:
        fns = list(glob.iglob(os.path.join(path_dir_full, "**"), recursive=True))
    if files_only:
        # filter out folders
        fns = list(filter(os.path.isfile, fns))
    return fns


def per_lesion_plots_interactive(path_objects_dir, highlights: bool = True):
    # No tile aggregation plots
    fns = listdir_fullpath_filter(path_objects_dir, regex=".*noaggr.*")
    plot_tsne_objects(fns, highlights=highlights)

    # Per feature median plots
    fns = listdir_fullpath_filter(path_objects_dir, regex=".*median-per-feature.*")
    plot_tsne_objects(fns, highlights=highlights)

    # shortest distance to all tiles within class, geometric median
    fns = listdir_fullpath_filter(path_objects_dir, regex=".*geometric-median.*")
    plot_tsne_objects(fns, highlights=highlights)


def middle_ones_interactive(path_objects_dir, highlights: bool = True):
    # No tile aggregation plots
    fns = listdir_fullpath_filter(path_objects_dir, regex=".*noaggr.*")
    plot_tsne_objects(fns, highlights=highlights)


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("--save", default=None, type=int)
    parser.add_argument("--highlights", type=str2bool, nargs="?", const=True, default=False)
    parser.add_argument("--interactive", type=str2bool, nargs="?", const=True, default=False)
    parser.add_argument("--color_by_feature", type=str, default=None)
    parser.add_argument("--color_by_mouse", type=str2bool, nargs="?", const=True, default=False)
    parser.add_argument("--color_by_genotype", type=str2bool, nargs="?", const=True, default=False)
    args = vars(parser.parse_args())

    if not args["interactive"]:

        # save figures
        import matplotlib

        matplotlib.use("Agg")

        # TSNE PER LESION
        path_objects_dir = "tsne_data_per_lesion"
        # No tile aggregation plots

        if args["save"] is None or args["save"] == 1:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*tsne-data-noaggr.*")
            plot_tsne_objects(
                fns,
                f"figures_tsne_per_lesion_no_tile_aggregation__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
            )

        # Per feature median plots
        if args["save"] is None or args["save"] == 2:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*tsne-data-median-per-feature.*")
            plot_tsne_objects(
                fns,
                f"figures_tsne_per_lesion_median_per_feature__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
            )

        # shortest distance to all tiles within class, geometric median
        if args["save"] is None or args["save"] == 3:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*tsne-data-geometric-median.*")
            plot_tsne_objects(
                fns,
                f"figures_tsne_per_lesion_geometric_median__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
            )

        if args["save"] is None or args["save"] == 4:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*tsne-data-multiple-stats.*")
            plot_tsne_objects(
                fns,
                f"figures_tsne_per_lesion_multiple_stats__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
            )

        # TSNE MIDDLE
        path_objects_dir = "tsne_data_middleones"
        # No tile aggregation plots
        if args["save"] is None or args["save"] == 5:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*noaggr.*")
            plot_tsne_objects(fns, "figures_tsne_middleones_tiles_no_aggregation", highlights=args["highlights"])

        # # TSNE FSEL VAR
        # if args["save"] is None or args["save"] == 6:
        #     for v in [10, 25, 50]:
        #         path_objects_dir = f"tsne_data_middleones_top{v}var"
        #         # No tile aggregation plots
        #         fns = listdir_fullpath_filter(path_objects_dir)
        #         plot_tsne_objects(fns, f"figures_tsne_middleones_no_aggregation_top{v}var", highlights=args["highlights"])

        # # TSNE FSEL VAR
        # if args["save"] is None or args["save"] == 7:
        #     for v in [10, 25, 50]:
        #         path_objects_dir = f"tsne_data_per_lesion_top{v}var"
        #         # No tile aggregation plots
        #         fns = listdir_fullpath_filter(path_objects_dir)
        #         plot_tsne_objects(fns, f"figures_tsne_per_lesion_no_aggregation_top{v}var", highlights=args["highlights"])

        # UMAP PER LESION
        path_objects_dir = "umap_data_per_lesion"

        # No tile aggregation plots
        if args["save"] is None or args["save"] == 8:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*umap-data-noaggr.*")
            plot_tsne_objects(
                fns,
                f"figures_umap_per_lesion_no_tile_aggregation__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
            )

        # Per feature median plots
        if args["save"] is None or args["save"] == 9:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*umap-data-median-per-feature.*")
            plot_tsne_objects(
                fns,
                f"figures_umap_per_lesion_median_per_feature__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
            )

        # shortest distance to all tiles within class, geometric median
        if args["save"] is None or args["save"] == 10:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*umap-data-geometric-median.*")
            plot_tsne_objects(
                fns,
                f"figures_umap_per_lesion_geometric_median__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
            )

        if args["save"] is None or args["save"] == 11:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*umap-data-multiple-stats.*")
            plot_tsne_objects(
                fns,
                f"figures_umap_per_lesion_multiple_stats__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
            )

        # UMAP MIDDLE
        path_objects_dir = "umap_data_middleones"
        # No tile aggregation plots
        if args["save"] is None or args["save"] == 12:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*noaggr.*")
            plot_tsne_objects(fns, "figures_umap_middleones_tiles_no_aggregation", highlights=args["highlights"])

        # # UMAP FSEL VAR MIDDLE
        # if args["save"] is None or args["save"] == 13:
        #     for v in [10, 25, 50]:
        #         path_objects_dir = f"umap_data_middleones_top{v}var"
        #         # No tile aggregation plots
        #         fns = listdir_fullpath_filter(path_objects_dir)
        #         plot_tsne_objects(fns, f"figures_umap_middleones_no_aggregation_top{v}var", highlights=args["highlights"])

        # # UMAP FSEL VAR  PER LESION
        # if args["save"] is None or args["save"] == 14:
        #     for v in [10, 25, 50]:
        #         path_objects_dir = f"umap_data_per_lesion_top{v}var"
        #         # No tile aggregation plots
        #         fns = listdir_fullpath_filter(path_objects_dir)
        #         plot_tsne_objects(fns, f"figures_umap_per_lesion_no_aggregation_top{v}var", highlights=args["highlights"])

        # PCA PER LESION
        path_objects_dir = "pca_data_per_lesion"

        # No tile aggregation plots
        if args["save"] is None or args["save"] == 15:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*pca-data-noaggr.*")
            plot_tsne_objects(
                fns,
                f"figures_pca_per_lesion_no_tile_aggregation__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
                color_by_genotype=args["color_by_genotype"],
            )

        # Per feature median plots
        if args["save"] is None or args["save"] == 16:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*pca-data-median-per-feature.*")
            plot_tsne_objects(
                fns,
                f"figures_pca_per_lesion_median_per_feature__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
                color_by_genotype=args["color_by_genotype"],
            )

        # shortest distance to all tiles within class, geometric median
        if args["save"] is None or args["save"] == 17:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*pca-data-geometric-median.*")
            plot_tsne_objects(
                fns,
                f"figures_pca_per_lesion_geometric_median__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
                color_by_genotype=args["color_by_genotype"],
            )

        if args["save"] is None or args["save"] == 18:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*pca-data-multiple-stats.*")
            plot_tsne_objects(
                fns,
                f"figures_pca_per_lesion_multiple_stats__color_{args['color_by_feature']}",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
                color_by_genotype=args["color_by_genotype"],
                marker_size=70
            )

        # PCA MIDDLE
        path_objects_dir = "pca_data_middleones"
        # No tile aggregation plots
        if args["save"] is None or args["save"] == 19:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*noaggr.*")
            plot_tsne_objects(fns, "figures_pca_middleones_tiles_no_aggregation", highlights=args["highlights"])

        # # PCA FSEL VAR MIDDLE
        # if args["save"] is None or args["save"] == 20:
        #     for v in [10, 25, 50]:
        #         path_objects_dir = f"pca_data_middleones_top{v}var"
        #         # No tile aggregation plots
        #         fns = listdir_fullpath_filter(path_objects_dir)
        #         plot_tsne_objects(fns, f"figures_pca_middleones_no_aggregation_top{v}var", highlights=args["highlights"])

        # # PCA FSEL VAR  PER LESION
        # if args["save"] is None or args["save"] == 21:
        #     for v in [10, 25, 50]:
        #         path_objects_dir = f"pca_data_per_lesion_top{v}var"
        #         # No tile aggregation plots
        #         fns = listdir_fullpath_filter(path_objects_dir)
        #         plot_tsne_objects(fns, f"figures_pca_per_lesion_no_aggregation_top{v}var", highlights=args["highlights"])

        # WHOLE STACK
        path_objects_dir = "tsne_data_whole_stack_tumor_vs_normal_flat"

        if args["save"] is None or args["save"] == 22:

            fns = listdir_fullpath_filter(path_objects_dir, regex=".*tsne-data-noaggr.*")
            # NOTE: THE DIRECTORY NEEDS TO BE FLAT WHICH IT ISNT BY DEFAULT, DO IT MANUALLY, REMOVE DIRS

            # # Recursive get from all subdirs
            # fns = listdir_fullpath_recursive(path_objects_dir)

            # # filter matching
            # regex = ".*tsne-data-noaggr.*"
            # if regex is not None and regex != "":
            #     matches = [re.findall(regex, f) for f in fns]
            #     fns = [f[0] for f in filter(None, matches)]

            plot_tsne_objects(
                fns,
                f"figures_whole_stack_tsne",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
                marker_size=1,
            )

        path_objects_dir = "umap_data_whole_stack_tumor_vs_normal_flat"
        if args["save"] is None or args["save"] == 23:
            fns = listdir_fullpath_filter(path_objects_dir, regex=".*umap-data-noaggr.*")

            # # Recursive get from all subdirs
            # fns = listdir_fullpath_recursive(path_objects_dir)

            # # filter matching
            # regex = ".*umap-data-noaggr.*"
            # if regex is not None and regex != "":
            #     matches = [re.findall(regex, f) for f in fns]
            #     fns = [f[0] for f in filter(None, matches)]

            plot_tsne_objects(
                fns,
                f"figures_whole_stack_umap",
                highlights=args["highlights"],
                color_by_feature=args["color_by_feature"],
                color_by_mouse=args["color_by_mouse"],
                marker_size=1,
            )

    else:
        # interactive mouseover plot
        # middle_ones_interactive("tsne_data_middleones", highlights=False)
        per_lesion_plots_interactive("tsne_data_per_lesion", highlights=False)

        middle_ones_interactive("umap_data_middleones", highlights=False)
        # per_lesion_plots_interactive("umap_data_per_lesion", highlights=False)

        # middle_ones_interactive("pca_data_middleones", highlights=False)
        # per_lesion_plots_interactive("pca_data_per_lesion", highlights=False)
