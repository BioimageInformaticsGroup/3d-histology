import os
import re
import sys
import copy
import glob
import time
import logging
from typing import List
from argparse import ArgumentParser, ArgumentTypeError
from pprint import pprint

logging.basicConfig(level=logging.INFO)

import matplotlib

matplotlib.use("Agg")
import natsort
import skimage
import numpy as np
import pandas as pd
import seaborn as sns
from PIL import Image
import scipy.io as sio
from scipy import stats

# from scipy.misc import imread
from skimage.io import imread

import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from umap import UMAP
from sklearn.decomposition import PCA

# from MulticoreTSNE import MulticoreTSNE as TSNE
from joblib import Parallel, delayed
from scipy.spatial.distance import cdist, euclidean

Image.MAX_IMAGE_PIXELS = 5000000000
sys.path.insert(0, "../../")
sys.path.insert(0, "../")

from compress import save_object, load_object
from utils.utils import extract_sequence_numbers_from_filenames, get_mem_usage
from featstats import feature_statistics, feature_statistics_small

PRINT_TILE_LABELS = False

NUM_INTERPOLATED_LAYERS = 54
NUM_FEATS_BLOCK = 250
NUM_FEATS_NUCLEI = 602

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ("yes", "true", "t", "y", "1"):
        return True
    elif v.lower() in ("no", "false", "f", "n", "0"):
        return False
    else:
        raise ArgumentTypeError("Boolean value expected.")


parser = ArgumentParser()
parser.add_argument("output_dir", help="Directory where volume files are saved", type=str)
parser.add_argument("--computation", default=None, type=str)
parser.add_argument("--perplexity", default=300, type=int)
parser.add_argument("--num_cores", default=1, type=int)
parser.add_argument("--use_matlab", default=True, type=str2bool, nargs=1)
parser.add_argument("--use_ae", default=True, type=str2bool, nargs=1)
parser.add_argument("--use_3d", default=True, type=str2bool, nargs=1)
parser.add_argument("--method", default="tsne", type=str)
parser.add_argument("--fselect", default=None, type=str)
parser.add_argument("--fselect_N", default=0, type=int)
args = vars(parser.parse_args())


def listdir_fullpath_recursive(path_dir: str, files_only: bool = True, N_first: int = None):
    path_dir_full = os.path.abspath(path_dir)
    if N_first:
        fns = [next(glob.iglob(os.path.join(path_dir_full, "**"), recursive=True)) for i in range(N_first)]
    else:
        fns = list(glob.iglob(os.path.join(path_dir_full, "**"), recursive=True))
    if files_only:
        # filter out folders
        fns = list(filter(os.path.isfile, fns))
    return fns


def get_paths_with_tumormasks(base_path):

    # if running locally, fix paths
    if os.getenv("HOSTNAME") == "x1g6":
        base_path = "/home/masi/Projects/RSYNC_THIS/narvi.cc.tut.fi" + base_path

    mice_with_annot = ["PTENX036-011", "PTENX036-14", "PTENX036-15", "PTENX036-20", "PTENX036-32", "PTENX036-33"]

    logging.info(f"Using base path {base_path}")

    tumor_fns = []
    for m in mice_with_annot:
        dpath = os.path.join(base_path, m, "tumormasks")
        fns = listdir_fullpath_recursive(dpath, files_only=True)
        tumor_fns.extend(fns)
    logging.info("Found {} tumormask files".format(len(tumor_fns)))

    matlab_features_fns = []
    for m in mice_with_annot:
        dpath = os.path.join(base_path, m, "matlab_features")
        fns = listdir_fullpath_recursive(dpath, files_only=True)
        matlab_features_fns.extend(fns)
    logging.info("Found {} matlab feature files".format(len(matlab_features_fns)))

    ae_fns = []
    for m in mice_with_annot:
        dpath = os.path.join(base_path, m, "autoencoder_features")
        fns = listdir_fullpath_recursive(dpath, files_only=True)
        ae_fns.extend(fns)
    logging.info("Found {} autoencoder feature files".format(len(ae_fns)))

    mask_features_fns = []
    for m in mice_with_annot:
        dpath = os.path.join(base_path, m, "mask_features")
        fns = listdir_fullpath_recursive(dpath, files_only=True)
        mask_features_fns.extend(fns)
    logging.info("Found {} 3D mask feature files".format(len(mask_features_fns)))

    tumor_fns = natsort.natsorted(tumor_fns)
    matlab_features_fns = natsort.natsorted(matlab_features_fns)
    ae_fns = natsort.natsorted(ae_fns)
    mask_features_fns = natsort.natsorted(mask_features_fns)

    tseq = extract_sequence_numbers_from_filenames(tumor_fns)
    mseq = extract_sequence_numbers_from_filenames(matlab_features_fns)
    aseq = extract_sequence_numbers_from_filenames(
        list(map(lambda x: x.replace(".pkl", "_.pkl"), ae_fns))
    )  # insert undescore to be able to extract seq num from ae_fns

    for i in range(len(tumor_fns)):
        logging.info("    " + os.path.basename(tumor_fns[i]))
        logging.info("    " + os.path.basename(matlab_features_fns[i]))
        logging.info("    " + os.path.basename(ae_fns[i]))

    assert np.all(tseq == mseq == aseq), "Loading filenames in correct order failed, check that all needed files exist"

    return tumor_fns, matlab_features_fns, ae_fns, mask_features_fns


def load_matlab_features(fns: List[str]):
    """Load matlab features into memory

    Return:
        features is a list of dictionaries of numpy arrays:
          - list of features per images
          - dictionary of keys: 'fullsize': Input image resolution (y, x)
                                'features_nuclei': nuclei location and shape? features, based on nuclei segmentation
                                'features_block': features from histology tiles
                                'blockcorners': coordinates of the upper left coordinate of each block
                                                (matlab coordinates! indexing starts from 1)
                                'labels_block': names of features
                                'labels_nuclei': names of features
          - under each key is a NxM numpy array of features
        fns are the filenames of th images in dir

    """
    logging.info("Pre-loading matlab features")
    features = []
    for i, fn in enumerate(fns, start=1):
        logging.info("Loading file {}/{} {}".format(i, len(fns), fn))
        data = sio.loadmat(fn)
        features.append(data)
        logging.info("Memory usage: {}".format(get_mem_usage()))
    return features


def load_ae_features(fns: List[str]):
    """Load autoencoder features into memory"""
    logging.info("Preloading autoencoer features")
    features = []
    for i, fn in enumerate(fns, start=1):
        logging.info("Loading file {}/{} {}".format(i, len(fns), fn))
        di = load_object(fn)
        features.append(di)
        logging.info("Memory usage: {}".format(get_mem_usage()))
    return features


def TicTocGenerator():
    """ Generator that returns time differences """
    ti = 0  # initial time
    tf = time.time()  # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf - ti  # returns the time difference


TicToc = TicTocGenerator()  # create an instance of the TicTocGen generator


# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    """ Prints the time difference yielded by generator instance TicToc """
    tempTimeInterval = next(TicToc)
    if tempBool:
        logging.info("Elapsed time: %f seconds.\n" % tempTimeInterval)


def tic():
    """ Records a time in TicToc, marks the beginning of a time interval """
    toc(False)


def get_nonempty_features_generator(
    features: dict,
    key_name: str,
    fix_nans: bool = False,
    fix_infs: bool = False,
    fix_nans_value: float = None,
    fix_infs_value: float = None,
):
    if key_name != "features_block" and key_name != "features_nuclei":
        raise ValueError("Invalid key name, use either features_block or features_nuclei")

    if not isinstance(features, type({})):
        logging.error("Features should be a dict ({}), not {}".format(type({}), type(features)))
        return

    Y, X = features[key_name].shape
    for y in range(Y):
        for x in range(X):
            fea = features[key_name][y, x][0]

            # for printing blockcorners
            # block_coords = features["blockcorners"][y, x] - np.array([[1, 0], [1, 0]])
            # ymin, ymax, xmin, xmax = block_coords.ravel()
            # logging.info("(ymin, ymax, xmin, xmax): {}".format((ymin, ymax, xmin, xmax)))

            if fea.size > 0:

                # replace nans with fix_nans_value if parameter given
                if fix_nans:
                    fea[np.isnan(fea)] = fix_nans_value

                # replace infs with fix_infs_value if parameter given
                if fix_infs:
                    fea[np.isinf(fea)] = fix_infs_value

                # get coordinates for corresponding block in python notation
                block_coords = features["blockcorners"][y, x] - np.array([[1, 0], [1, 0]])
                ymin, ymax, xmin, xmax = block_coords.ravel()
                yield ymin, ymax, xmin, xmax, fea


def get_win_label(win, ratio=0.2, background_label=0):
    """ Determine label for window

        Return:
            label: label for the input window
            r: ratio for amount of nonbackground in the window
    """
    uvals, ucounts = np.unique(win[:], return_counts=True)
    # get labels
    if background_label in uvals:
        # if background label is among window labels
        # pick the one among non banground labels
        # that is most frequent
        bg_bool = uvals == background_label
        nobg_uvals = uvals[~bg_bool]
        nobg_counts = ucounts[~bg_bool]
        bg_count = ucounts[bg_bool]
        nobg_sumcounts = np.sum(nobg_counts)
        r = nobg_sumcounts / (nobg_sumcounts + bg_count)
        if r > ratio:
            label = nobg_uvals[np.argmax(nobg_counts)]
        else:
            label = background_label
    else:
        # if background label is not included, pick the most frequent
        # print("UVALS: {}".format(uvals))
        # print("UCOUNTS: {}".format(ucounts))
        # print("WIN: {}".format(win))
        label = uvals[np.argmax(ucounts)]
        r = 1.0
    return label, r


def listdir_fullpath(path_dir: str):
    fns = os.listdir(path_dir)
    fns = [os.path.join(path_dir, f) for f in fns]
    return fns


def geometric_median(points):
    distance = euclidean
    gmedian = min(map(lambda p1: (p1, sum(map(lambda p2: distance(p1, p2), points))), points), key=lambda x: x[1])[0]
    # find index of gmedian in the points array
    index = np.where(np.all(points == gmedian, axis=1))[0][0]
    return gmedian, index


# This is parallelized with joblib, not tested with MulticoreTSNE
# TODO: Not handling kwargs
def compute_tsne_parallel_joblib(
    features: List,
    save_path: str,
    perplexities: List[int] = [5, 10, 20, 30, 60, 120, 300, 500],
    n_iter: int = 5000,
    n_iter_without_progress: int = 300,
    verbose: bool = True,
    num_cores: int = 1,
    **kwargs,
):
    """Compute tsne from given features into 2 dimensions with multiple perplexities

    TSNE data is saved onto disk compressed along with following data:
            features
            perplexity
            n_iter
            n_iter_without_progress
            save_path_user_input
            save_path_final
            data_original_dims

    User can also specify additional data as keyword arguments to be saved into the same object

    Args:
        Features (np.ndarray): 2D feature matrix
        save_path (str): Path to save the object to, non-existing directories are created
        preplexities (List[int]): Perplexity values that are used in the tsne computation,
                                  each value is saved into its own object
        n_iter (int): Number of tsne iterations
        n_iter_without_progress (int): Number of tsne iterations to perform without decrease in error until stopping
        verbose (bool): Whether to print verbose output
        kwargs: Additional entries to be saved to disk,
                each kwarg becomes a key-value pair in the output dictionary


    Returns:
        dictionary: contains all the data described above and additional data given by the user

    """

    # For some reason, the original dtype float32 results in
    # "All probabilities should be finite" assertion error
    # this conversion gets rid of that, other datatypes could work as well
    features = features.astype(np.float64)

    # Process in parallel over perplexities
    Parallel(n_jobs=args["num_cores"])(
        delayed(_tsne)(
            features=features,
            save_path=save_path,
            perplexity=p,
            n_iter=n_iter,
            n_iter_without_progress=n_iter_without_progress,
            verbose=verbose,
            num_cores=1,
            **kwargs,
        )
        for p in perplexities
    )
    gc.collect()


def compute_tsne(
    feats: List,
    save_path: str,
    n_iter: int = 10000,
    n_iter_without_progress: int = 300,
    verbose: bool = True,
    num_cores: int = 1,
    **kwargs,
):
    """Compute tsne from given features into 2 dimensions

    TSNE data is saved onto disk compressed along with following data:
            features
            perplexity
            n_iter
            n_iter_without_progress
            save_path_user_input
            save_path_final
            data_original_dims

    User can also specify additional data as keyword arguments to be saved into the same object

    Args:
        Features (np.ndarray): 2D feature matrix
        save_path (str): Path to save the object to, non-existing directories are created
        preplexities (List[int]): Perplexity values that are used in the tsne computation,
                                  each value is saved into its own object
        n_iter (int): Number of tsne iterations
        n_iter_without_progress (int): Number of tsne iterations to perform without decrease in error until stopping
        verbose (bool): Whether to print verbose output
        kwargs: Additional entries to be saved to disk,
                each kwarg becomes a key-value pair in the output dictionary


    Returns:
        dictionary: contains all the data described above and additional data given by the user

    """

    # For some reason, the original dtype float32 results in
    # "All probabilities should be finite" assertion error
    # this conversion gets rid of that, other datatypes could work as well
    feats = feats.astype(np.float64)

    _tsne(
        features=feats,
        save_path=save_path,
        perplexity=args["perplexity"],
        n_iter=n_iter,
        n_iter_without_progress=n_iter_without_progress,
        verbose=verbose,
        num_cores=args["num_cores"],
        **kwargs,
    )


def _tsne(
    features: np.ndarray,
    save_path: str,
    perplexity: int = 300,
    n_iter: int = 5000,
    n_iter_without_progress: int = 300,
    verbose: bool = True,
    num_cores: int = 1,
    **kwargs,
):
    logging.info(
        "Computing {} for {} features, with perplexity: {}".format(args["method"].upper(), features.shape, perplexity)
    )

    # make sure dir exist for output files
    os.makedirs(os.path.dirname(save_path), exist_ok=True)

    if args["method"] == "tsne":
        verbosity_int = 2 if verbose else 0
        try:
            O = TSNE(
                n_components=2,
                perplexity=perplexity,
                n_iter=n_iter,
                n_iter_without_progress=n_iter_without_progress,
                verbose=verbosity_int,
                # n_jobs=num_cores,
            )
            feats_tsne = O.fit_transform(features)
        except Exception as e:
            logging.error("Exception in tsne fit_transform: {}".format(e))
            import pdb

            pdb.set_trace()
            sys.exit(1)

    elif args["method"] == "umap":
        try:
            O = UMAP(
                n_components=2,
                n_neighbors=perplexity,  # similar to perplexity, extent of local neighborhood
                min_dist=0.2,  # min distance points can be apart from each other
                metric="euclidean",  # controls how distance is computed in the ambient space (ambient space i.e. low dimensional space) of the input data
                n_epochs=int(n_iter / 10),
            )
            feats_tsne = O.fit_transform(features)
        except Exception as e:
            logging.error("Exception in umap fit_transform: {}".format(e))
            import pdb

            pdb.set_trace()
            sys.exit(1)

    elif args["method"] == "pca":
        perplexity = None
        try:
            O = PCA(n_components=2)
            feats_tsne = O.fit_transform(features)
        except Exception as e:
            logging.error("Exception in pca fit_transform: {}".format(e))
            import pdb

            pdb.set_trace()
            sys.exit(1)

    # save tsne features to disk for plotting it later
    # use separate files for each perplexity to avoid loading all data at once to RAM
    fname, ext = os.path.splitext(save_path)
    save_path_with_info = "{}_perplexity{}_iters{}_iterswoprog{}{}".format(
        fname, perplexity, n_iter, n_iter_without_progress, ext
    )

    # create a dictionary to save data with related information
    save_dict = dict()
    save_dict["features"] = feats_tsne
    save_dict["original_features"] = features
    save_dict["perplexity"] = perplexity
    save_dict["n_iter"] = n_iter
    save_dict["n_iter_without_progress"] = n_iter_without_progress
    save_dict["save_path_user_input"] = save_path
    save_dict["save_path_final"] = save_path_with_info
    save_dict["data_original_dims"] = features.shape
    save_dict["method"] = args["method"]
    save_dict["object"] = O

    # if additional data given, add it to dictionary
    for k, v in kwargs.items():
        save_dict[k] = v

    save_object(save_path_with_info, save_dict)

    logging.info("Saved {} object for perplexity {}".format(args["method"].upper(), perplexity))


def print_matlab_feature_info(F):
    logging.info("Matlab feature info:")

    for ftype in ["features_block", "features_nuclei"]:
        tot_num_nonzero_blocks = 0
        Z = []
        for i in range(len(F)):
            O = F[i][ftype]
            Y, X = O.shape
            for x in range(X):
                for y in range(Y):
                    if len(O[y, x][0]) > 0:
                        ratio = np.sum(O[y, x] == 0) / O[y, x].size
                        Z.append(ratio)
                        tot_num_nonzero_blocks += 1

        Z = np.array(Z)
        logging.info(
            f"    Matlab {ftype} zero features ratio per block: min {Z.min()} - max: {Z.max()} - avg: {Z.mean()} - std: {Z.std()}"
        )
        logging.info(f"    Total number of nonzero blocks: {tot_num_nonzero_blocks} in {ftype}")
    logging.info("    Matlab features info: blocksize {}".format(F[0]["blockcorners"][0, 0]))


def print_nan_ratios(feats, feats_names):
    nans = np.isnan(feats)
    nanratios = np.sum(nans, axis=0) / nans.shape[0]


import re
def listdir_fullpath_filter(path_dir: str, regex: str = None):
    fns = os.listdir(path_dir)
    fns = [os.path.join(path_dir, f) for f in fns]
    if regex is not None and regex != "":
        matches = [re.findall(regex, f) for f in fns]
        fns = [f[0] for f in filter(None, matches)]
    return fns


def load_tile_features(matlab_fns, feats_ae_fns, tumormask_fns, only_from_tumor_regions=True, print_tile_labels=False, regex=None):
    """
    Load features from each provided file and combine them together
    so that one row corresponds to one block in input images

    output is a dictionary where "feats"-key contains all non-empty features
    """

    if regex is not None and regex != "":
        # Filter all with same regex, if this does not work, give three separate regex arguments
        # after filtering, the length of each fns array should be equal
        matlab_matches = [re.findall(regex, f) for f in matlab_fns]
        feats_ae_matches = [re.findall(regex, f) for f in feats_ae_fns]
        tumormask_matches = [re.findall(regex, f) for f in tumormask_fns]

        matlab_fns = [f[0] for f in filter(None, matlab_matches)]
        feats_ae_fns = [f[0] for f in filter(None, feats_ae_matches)]
        tumormask_fns = [f[0] for f in filter(None, tumormask_matches)]

        print(f"Regex '{regex}' given")
        print("Matlab feature filenames after filtering:")
        for f in matlab_fns:
            print(f)
        print("AE feature filenames after filtering:")
        for f in feats_ae_fns:
            print(f)
        print("Tumormask filenames after filtering:")
        for f in tumormask_fns:
            print(f)

        assert(len(matlab_fns) == len(feats_ae_fns) == len(tumormask_fns))

    # clean features later
    fix_matlab_nans = False
    fix_matlab_infs = False

    # PRELOAD FEATURES FIRST INTO MEMORY
    # get matlab features
    # 'fullsize', 'features_nuclei', 'features_block', 'blockcorners', 'labels_block', 'labels_nuclei'
    F = load_matlab_features(matlab_fns)

    print_matlab_feature_info(F)

    # get AE features
    F2 = load_ae_features(feats_ae_fns)

    # FEATURE NAMES
    block_names = [i[0] for i in F[0]["labels_block"].ravel()]
    nuclei_names = [i[0] for i in F[0]["labels_nuclei"].ravel()]
    ae_names = F2[0]["feature_names"]
    feats_names = block_names + nuclei_names + ae_names

    feats = []
    feats_coords = []
    feats_fns = []
    feats_tmfns = []
    feats_mask_ratios = []
    feats_labels = []
    rng = range(len(matlab_fns))
    for i in rng:

        # read tumor mask to variable
        tumor_mask = imread(tumormask_fns[i], plugin="imageio")

        if only_from_tumor_regions and not print_tile_labels:
            # if there are no masks on this slice, we can skip
            # we don't skip if we wish to print all tile lables
            if np.sum(np.sum(tumor_mask)) == 0:
                logging.info(f"Skipping file {i + 1}/{len(matlab_fns)} {matlab_fns[i]} - no annotations")
                continue

        # iterate features_nuclei-generator along with features_block-generator
        f_nuc_gen = get_nonempty_features_generator(
            F[i],
            "features_nuclei",
            fix_nans=fix_matlab_nans,
            fix_nans_value=0,
            fix_infs=fix_matlab_infs,
            fix_infs_value=sys.float_info.max,
        )
        logging.info("Loading non-empty features for image {}/{} - {}".format(i + 1, len(matlab_fns), matlab_fns[i]))
        counter = 0
        for ymin, ymax, xmin, xmax, f_block in get_nonempty_features_generator(
            F[i],
            "features_block",
            fix_nans=fix_matlab_nans,
            fix_nans_value=0,
            fix_infs=fix_matlab_infs,
            fix_infs_value=sys.float_info.max,
        ):

            logging.debug(
                "Processing non empty window number {} (ymin, ymax, xmin, xmax): {}".format(
                    counter, (ymin, ymax, xmin, xmax)
                )
            )

            f_all_win = []

            # MATLAB FEATURES
            _, _, _, _, f_nuclei = next(f_nuc_gen)

            # AUTOENCODER FEATURES
            # store autoencoder features to the data structure
            # index window from dict with upper left coordinate
            try:
                # get features
                tile_name, f_auto = F2[i]["{},{}".format(ymin, xmin)]
            except KeyError as e:
                logging.error("Coulnd't find matching window from autoencoder features")
                logging.error(e)
                import pdb

                pdb.set_trace()

            # Concatenate all
            f_all_win = np.concatenate((f_all_win, f_block))
            f_all_win = np.concatenate((f_all_win, f_nuclei))
            f_all_win = np.concatenate((f_all_win, f_auto))

            # win contains mask labels also (0, 1, 2, 3...)
            win = tumor_mask[ymin:ymax, xmin:xmax]
            # win_label = stats.mode(win, axis=None).mode[0]  # mode
            win_label, ratio = get_win_label(win, ratio=0.5)

            if print_tile_labels:
                mfn = os.path.basename(matlab_fns[i])
                logging.info(
                    f"*** {mfn.replace('Features_', '').replace('.mat', '')}_{ymin}_{ymax - 1}_{xmin}_{xmax - 1}.png: {win_label}"
                )

            # load features depending on only_from_tumor_regions setting
            # if true:
            # store features only if they are from tumor region
            # tumorwins can have labels from 1 ->
            # else
            # load all features
            if (not only_from_tumor_regions) or (win_label > 0):
                pattern = "(xhimyc-[0-9]*?-|ptenx036-[0-9]*?-)"
                matches = re.findall(pattern, matlab_fns[i].lower())
                mouseid = matches[0]
                logging.debug("Label {} in at {}".format(win_label, counter))
                feats.append(f_all_win)
                # feats = np.concatenate((feats, f_all_win.reshape(1, len(feats_names))), axis=0)
                feats_mask_ratios.append(ratio)
                feats_labels.append("Tilelabel{}_{}".format(win_label, mouseid))
                feats_coords.append([ymin, ymax, xmin, xmax])
                feats_fns.append(matlab_fns[i])
                feats_tmfns.append(tumormask_fns[i])

            counter += 1

        logging.info("    memory usage: {}".format(get_mem_usage()))
        logging.info(f"    {counter} non-empty feature blocks in 'features_block'-features")

    logging.info(f"Found {len(feats)} non-zero feature blocks in all given matlab feature files")
    feats = np.array(feats)
    feats_coords = np.array(feats_coords)

    # # DEBUGGG
    # # check nonzero feat ratios
    # # get only matlab
    # mfeats_block = feats[:, 0:250]
    # mfeats_nuclei = feats[:, 251:251+602]
    # ftype = "features_block"
    # Z = []
    # for row in mfeats_block:
    #     ratio = np.sum(row == 0) / row.size
    #     Z.append(ratio)
    # logging.info(f"    Matlab {ftype} zero features ratio per block: min {min(Z)} - max: {max(Z)}")
    # Z = np.array(Z)
    # logging.info(f"    Matlab {ftype} zero features ratio per block: min {Z.min()} - max: {Z.max()} - avg: {Z.mean()} - std: {Z.std()}")
    # ftype = "features_nuclei"
    # Z = []
    # for i, row in enumerate(mfeats_nuclei):
    #     ratio = np.sum(row == 0) / row.size
    #     Z.append(ratio)
    # logging.info(f"    Matlab {ftype} zero features ratio per block: min {min(Z)} - max: {max(Z)}")
    # Z = np.array(Z)
    # logging.info(f"    Matlab {ftype} zero features ratio per block: min {Z.min()} - max: {Z.max()} - avg: {Z.mean()} - std: {Z.std()}")
    # import pdb; pdb.set_trace()

    # wrap everything into a dict
    feats_dict = {}
    feats_dict["feats"] = feats
    feats_dict["feats_names"] = feats_names
    feats_dict["feats_coords"] = feats_coords
    feats_dict["feats_fns"] = feats_fns
    feats_dict["feats_tmfns"] = feats_tmfns
    feats_dict["feats_mask_ratios"] = feats_mask_ratios
    feats_dict["feats_labels"] = feats_labels

    return feats_dict


def check_for_value(features, feature_names, value=np.nan):
    if value == "extrema":
        max_ind_linear = np.argmax(features)
        min_ind_linear = np.argmin(features)
        ymax, xmax = np.unravel_index(max_ind_linear, features.shape)
        ymin, xmin = np.unravel_index(min_ind_linear, features.shape)
        logging.info(
            f"Maximum value {features[ymax, xmax]} in {feature_names[xmax]} at index (ymax, xmax): ({ymax}, {xmax})"
        )
        logging.info(f"    {feature_names[xmax]} average {np.mean(features[:, xmax])}")
        logging.info(f"    {feature_names[xmax]} std {np.std(features[:, xmax])}")
        logging.info(
            f"Minimum value {features[ymin, xmin]} in {feature_names[xmin]} at index (ymin, xmin): ({ymin}, {xmin})"
        )
        logging.info(f"    {feature_names[xmin]} average {np.mean(features[:, xmin])}")
        logging.info(f"    {feature_names[xmin]} std {np.std(features[:, xmin])}")
    else:
        val_y, val_x = np.where(features == value)
        for y, x in zip(val_y, val_x):
            logging.info("Found {} at ({}, {}) - feature name: {}".format(value, y, x, feature_names[x]))


def aggregate_multiple_stats(path_output_objects, feats_dict, fewer_features=False):
    logging.info("Aggregate features with multiple stats")

    unique_labels = np.unique(feats_dict["feats_labels"])

    aggr_feats = []
    aggr_labels = []
    # there is no tile to represent this aggregation
    for label in unique_labels:
        F = {}
        bool_arr = np.array(feats_dict["feats_labels"]) == label
        class_features = feats_dict["feats"][bool_arr]
        # compute multiple statistics from each feature
        for i, c in enumerate(feats_dict["feats_names"]):
            if fewer_features:
                F = feature_statistics_small(F, class_features[:, i], c, augment=False, print_stats=False)
            else:
                F = feature_statistics(F, class_features[:, i], c, augment=False, print_stats=False)

        aggr_feats_names = list(F.keys())
        f = [F[k] for k in aggr_feats_names]

        aggr_feats.append(f)
        aggr_labels.append(label)
    aggr_feats = np.array(aggr_feats)
    aggr_labels = np.array(aggr_labels)

    # Keys in feats_dict
    # ["feats"]
    # ["feats_labels"]
    # ["feats_names"]
    # ["feats_coords"]
    # ["feats_fns"]
    # ["feats_tmnfs"]
    # ["feats_mask_ratios"]

    aggr_feats_dict = {}
    aggr_feats_dict["feats"] = aggr_feats
    aggr_feats_dict["feats_labels"] = aggr_labels
    aggr_feats_dict["feats_names"] = copy.copy(aggr_feats_names)
    # feats_coords makes no sense with this aggregation
    # feats_fns makes no sense (name of corresponding .mat feature file)
    # feats_tmfns makes no sense
    # feats_mask_ratios makes no sense
    # feats_labels makes no sense

    return aggr_feats_dict


def aggregate_median_per_feature(path_output_objects, feats_dict):
    logging.info("Aggregate features with median per feature")

    unique_labels = np.unique(feats_dict["feats_labels"])

    aggr_feats = []
    aggr_labels = []
    # there is no tile to represent this aggregation
    for label in unique_labels:
        bool_arr = np.array(feats_dict["feats_labels"]) == label
        class_features = feats_dict["feats"][bool_arr]
        # compute median individually along each feature
        f = np.median(class_features, axis=0)
        aggr_feats.append(f)
        aggr_labels.append(label)
    aggr_feats = np.array(aggr_feats)
    aggr_labels = np.array(aggr_labels)

    # Keys in feats_dict
    # ["feats"]
    # ["feats_labels"]
    # ["feats_names"]
    # ["feats_coords"]
    # ["feats_fns"]
    # ["feats_tmnfs"]
    # ["feats_mask_ratios"]

    aggr_feats_dict = {}
    aggr_feats_dict["feats"] = aggr_feats
    aggr_feats_dict["feats_labels"] = aggr_labels
    aggr_feats_dict["feats_names"] = copy.copy(feats_dict["feats_names"])
    # feats_coords makes no sense with this aggregation
    # feats_fns makes no sense (name of corresponding .mat feature file)
    # feats_tmfns makes no sense
    # feats_mask_ratios makes no sense
    # feats_labels makes no sense

    return aggr_feats_dict


def aggregate_geometric_median(path_output_objects, feats_dict):
    logging.info("Aggregate features with geometric median")

    unique_labels = np.unique(feats_dict["feats_labels"])
    label_legends = unique_labels

    # shortest distance to all tiles (feature vector) in the same class
    aggr_feats = []
    aggr_labels = []
    aggr_coords = []
    aggr_feats_fns = []
    aggr_feats_tmfns = []
    aggr_feats_mask_ratios = []
    for label in unique_labels:
        bool_arr = np.array(feats_dict["feats_labels"]) == label
        class_features = feats_dict["feats"][bool_arr]
        try:
            gmed, _ = geometric_median(class_features)
        except:
            check_for_value(class_features, feats_dict["feats_names"], value=np.nan)
            check_for_value(class_features, feats_dict["feats_names"], value=np.inf)
            import pdb

            pdb.set_trace()
        # find index of the gmedian among all features, not just class_features
        index = np.where(np.all(feats_dict["feats"] == gmed, axis=1))[0][0]
        gmed_coords = feats_dict["feats_coords"][index]
        aggr_feats.append(gmed)
        aggr_labels.append(label)
        aggr_coords.append(gmed_coords)
        aggr_feats_fns.append(feats_dict["feats_fns"][index])
        aggr_feats_tmfns.append(feats_dict["feats_tmfns"][index])
        aggr_feats_mask_ratios.append(feats_dict["feats_mask_ratios"][index])
    aggr_feats = np.array(aggr_feats)
    aggr_labels = np.array(aggr_labels)
    aggr_coords = np.array(aggr_coords)

    # Keys in feats_dict
    # ["feats"]
    # ["feats_labels"]
    # ["feats_names"]
    # ["feats_coords"]
    # ["feats_fns"]
    # ["feats_tmnfs"]
    # ["feats_mask_ratios"]

    aggr_feats_dict = {}
    aggr_feats_dict["feats"] = aggr_feats
    aggr_feats_dict["feats_labels"] = aggr_labels
    aggr_feats_dict["feats_names"] = feats_dict["feats_names"]
    aggr_feats_dict["feats_coords"] = aggr_coords
    aggr_feats_dict["feats_fns"] = aggr_feats_fns
    aggr_feats_dict["feats_tmfns"] = aggr_feats_tmfns
    aggr_feats_dict["feats_mask_ratios"] = aggr_feats_mask_ratios

    return aggr_feats_dict


def pick_feature_types(feats_dict, matlab: bool = True, ae: bool = True, shape3d: bool = True):
    matlab = matlab[0] if isinstance(matlab, list) else matlab
    ae = ae[0] if isinstance(ae, list) else ae
    shape3d = shape3d[0] if isinstance(shape3d, list) else shape3d

    logging.info(f"Picking features - matlab {matlab} - ae {ae} - shape3d {shape3d} ..")
    logging.info(f"Feats shape before picking features {feats_dict['feats'].shape}")

    feats = feats_dict["feats"]
    feats_names = feats_dict["feats_names"]

    # first_matlab_feat_name = "??"
    first_ae_feat_name = feats_names[[re.search("Autoencoded[0-9]*", strr) != None for strr in feats_names].index(True)]
    first_3d_feat_name = "volume"

    # find first index of each feature type
    ind_ae = feats_names.index(first_ae_feat_name)
    try:
        ind_3d = feats_names.index(first_3d_feat_name)
    except ValueError as e:
        # set ind_3d as last index + 1 if 3d features are not
        # present to be able to use indexing below
        ind_3d = len(feats_names)
        if shape3d:
            logging.error("Could not find 3d features")
            import pdb

            pdb.set_trace()

    picked_feats = np.array([]).reshape(feats.shape[0], 0)
    picked_feats_names = []

    # if matlab == ae == shape3d == True:
    #     return feats_dict

    if matlab:
        picked_feats = np.concatenate((picked_feats, feats[:, 0:ind_ae]), axis=1)
        picked_feats_names.extend(feats_names[0:ind_ae])
        logging.info(f"    First matlab feature name: {feats_names[0]}")
        logging.info(f"    Last matlab feature name: {feats_names[ind_ae - 1]}")
        logging.info(f"    Next feature name: {feats_names[ind_ae]}")
    if ae:
        picked_feats = np.concatenate((picked_feats, feats[:, ind_ae:ind_3d]), axis=1)
        picked_feats_names.extend(feats_names[ind_ae:ind_3d])
        logging.info(f"    First ae feature name: {feats_names[ind_ae]}")
        logging.info(f"    Last ae feature name: {feats_names[ind_3d - 1]}")
        try:
            next_feat = feats_names[ind_3d]
        except IndexError as e:
            next_feat = f"Index error {e}"
        logging.info(f"    Next feature name: {next_feat}")
    if shape3d:
        picked_feats = np.concatenate((picked_feats, feats[:, ind_3d:]), axis=1)
        picked_feats_names.extend(feats_names[ind_3d:])
        logging.info(f"    First 3d feature name: {feats_names[ind_3d]}")
        logging.info(f"    Last 3d feature name: {feats_names[-1]}")
        logging.info(f"    Next feature out of bounds")

    feats_dict["feats"] = picked_feats
    feats_dict["feats_names"] = picked_feats_names

    logging.info(f"Feats shape after picking features {feats_dict['feats'].shape}")

    return feats_dict


# def remove_bad_features(feats_dict):
#     feats_dict["feats"], feats_dict["feats_names"] = remove_feature(feats_dict["feats"], feats_dict["feats_names"], index=1756)
#     feats_dict["feats"], feats_dict["feats_names"] = remove_feature_by_name(feats_dict["feats"], feats_dict["feats_names"], fname_to_remove="recerr_mean_energy")
#     # TEST THIS: Delete matlab veature 831 NhoodNucAngleSkew0_Nuc_std because it gives inf fairly often. Still true?
#     feats_dict["feats"], feats_dict["feats_names"] = remove_feature_by_name(feats_dict["feats"], feats_dict["feats_names"], fname_to_remove="NhoodNucAngleSkew0_Nuc_std")
#     return feats_dict


def remove_feature(features, feature_names, index):
    fname = feature_names[index]
    # axis = 1 means column-wise delete operation
    feature_names = np.delete(feature_names, index).tolist()
    features = np.delete(features, index, axis=1)
    logging.info("** Removing feature index {} - {}".format(index, fname))
    return features, feature_names


def remove_feature_by_name(features, feature_names, fname_to_remove):
    index = feature_names.index(fname_to_remove)
    # axis = 1 means column-wise delete operation
    feature_names = np.delete(feature_names, index).tolist()
    features = np.delete(features, index, axis=1)
    logging.info("** Removing feature by name from index {} - {}".format(index, fname_to_remove))
    return features, feature_names

def remove_feature_by_name_FD(FD, fname_to_remove):
    feats = FD["feats"]
    feats_names = FD["feats_names"]
    feats, feats_names = remove_feature_by_name(feats, feats_names, fname_to_remove)
    FD["feats"] = feats
    FD["feats_names"] = feats_names
    return FD


def remove_volume_correlating_features(FD):

    logging.info("Removing 3d features that correlate with volume...")

    volume_feats = [
            'section_perimeter_min',
            'section_perimeter_kurtosis',
            'dist_pca1_axis_var',
            'dist_pca1_axis_mean',
            'dist_pca1_axis_median',
            'pca1_moment_of_inertia',
            'length_pca2',
            'dist_pca1_axis_max',
            'bounding_cube_dim_min',
            'length_pca3',
            'dist_section_center_adj_sum',
            'dist_section_center_endpoints',
            'dist_section_center_skew',
            'dist_section_center_kurtosis',
            'sphericity',
            'section_perimeter_median',
            'section_perimeter_mean',
            'section_perimeter_max',
            'bounding_cube_dim_median',
            'dist_pca3_axis_mean',
            'dist_pca3_axis_median',
            'dist_to_tumor_center_median',
            'moment_of_inertia',
            'dist_to_tumor_center_mean',
            'dist_pca2_axis_mean',
            'dist_pca2_axis_median',
            'bounding_cube_dim_max',
            'length_pca1',
            'dist_pca2_axis_max',
            'dist_pca3_axis_max',
            'dist_to_tumor_center_max',
            'bounding_cube_diagonal_length',
            'bounding_cube_dim_mean',
            'surface_area',
            'section_perimeter_sum',
            'bounding_cube_volume',
            'convex_hull_surface_area',
            'volume',
            'convex_hull_volume',
            'bounding_cube_dim_var',
            'pca2_moment_of_inertia',
            'pca3_moment_of_inertia',
            'dist_pca2_axis_var',
            'dist_pca3_axis_var',
            'dist_to_tumor_center_var',
            'section_perimeter_var',
            'dist_section_center_mean_med_dev',
            'dist_section_center_var',
            'dist_section_center_max']

    for f in volume_feats:
        FD = remove_feature_by_name_FD(FD, f)

    return FD

def keep_select_features(feats_dict, select_features):
    logging.info(f"keeping features {select_features}..")
    # Put these here?
    #### feats_dict["feats"], feats_dict["feats_names"] = remove_feature(feats_dict["feats"], feats_dict["feats_names"], index=1756)
    #### feats_dict["feats"], feats_dict["feats_names"] = remove_feature_by_name(feats_dict["feats"], feats_dict["feats_names"], fname_to_remove="recerr_mean_energy")
    #### # TEST THIS: Delete matlab veature 831 NhoodNucAngleSkew0_Nuc_std because it gives inf fairly often. Still true?
    #### feats_dict["feats"], feats_dict["feats_names"] = remove_feature_by_name(feats_dict["feats"], feats_dict["feats_names"], fname_to_remove="NhoodNucAngleSkew0_Nuc_std")
    #### return feats_dict

    # dict_keys(['feats', 'feats_names', 'feats_coords', 'feats_fns', 'feats_tmfns', 'feats_mask_ratios', 'feats_labels'])
    # dataframe for feature data
    df = pd.DataFrame(feats_dict["feats"], columns=feats_dict["feats_names"])

    # dataframe for metadata
    df_meta = pd.DataFrame(feats_dict["feats_labels"], columns=["labels"])
    if "feats_coords" in feats_dict.keys():
        dfc = pd.DataFrame(feats_dict["feats_coords"], columns=["ymin", "ymax", "xmin", "xmax"])
        df_meta = pd.concat((df_meta, dfc), axis=1)  # concatenate columns
    if "feats_fns" in feats_dict.keys():
        df_meta["fns"] = feats_dict["feats_fns"]
    if "feats_tmfns" in feats_dict.keys():
        df_meta["tmfns"] = feats_dict["feats_tmfns"]
    if "feats_mask_ratios" in feats_dict.keys():
        df_meta["mask_ratios"] = feats_dict["feats_mask_ratios"]

    df = df[select_features]

    feats_dict["feats"] = df.values
    feats_dict["feats_names"] = df.columns.tolist()
    feats_dict["feats_labels"] = df_meta["labels"].tolist()
    if "feats_coords" in feats_dict.keys():
        feats_dict["feats_coords"] = df_meta[["ymin", "ymax", "xmin", "xmax"]].values
    if "feats_fns" in feats_dict.keys():
        feats_dict["feats_fns"] = df_meta["fns"].tolist()
    if "feats_tmfns" in feats_dict.keys():
        feats_dict["feats_tmfns"] = df_meta["tmfns"].tolist()
    if "feats_mask_ratios" in feats_dict.keys():
        feats_dict["feats_mask_ratios"] = df_meta["mask_ratios"].tolist()

    return feats_dict



def clean_features(feats_dict, remove_rows_with_nans=True, remove_columns_with_nans=False):
    logging.info("Cleaning features..")
    # Put these here?
    #### feats_dict["feats"], feats_dict["feats_names"] = remove_feature(feats_dict["feats"], feats_dict["feats_names"], index=1756)
    #### feats_dict["feats"], feats_dict["feats_names"] = remove_feature_by_name(feats_dict["feats"], feats_dict["feats_names"], fname_to_remove="recerr_mean_energy")
    #### # TEST THIS: Delete matlab veature 831 NhoodNucAngleSkew0_Nuc_std because it gives inf fairly often. Still true?
    #### feats_dict["feats"], feats_dict["feats_names"] = remove_feature_by_name(feats_dict["feats"], feats_dict["feats_names"], fname_to_remove="NhoodNucAngleSkew0_Nuc_std")
    #### return feats_dict

    # dict_keys(['feats', 'feats_names', 'feats_coords', 'feats_fns', 'feats_tmfns', 'feats_mask_ratios', 'feats_labels'])
    # dataframe for feature data
    df = pd.DataFrame(feats_dict["feats"], columns=feats_dict["feats_names"])

    # dataframe for metadata
    df_meta = pd.DataFrame(feats_dict["feats_labels"], columns=["labels"])
    if "feats_coords" in feats_dict.keys():
        dfc = pd.DataFrame(feats_dict["feats_coords"], columns=["ymin", "ymax", "xmin", "xmax"])
        df_meta = pd.concat((df_meta, dfc), axis=1)  # concatenate columns
    if "feats_fns" in feats_dict.keys():
        df_meta["fns"] = feats_dict["feats_fns"]
    if "feats_tmfns" in feats_dict.keys():
        df_meta["tmfns"] = feats_dict["feats_tmfns"]
    if "feats_mask_ratios" in feats_dict.keys():
        df_meta["mask_ratios"] = feats_dict["feats_mask_ratios"]

    # replace infs with nans
    df2 = df.replace([np.inf, -np.inf], np.nan)
    df = df.replace([np.inf, -np.inf], np.nan)

    # print featue nan percentages
    cols_with_nan = df.isna().any(axis=0)
    cols_with_nan = cols_with_nan.index[cols_with_nan]
    nan_percentage = df[cols_with_nan].isna().apply(lambda x: np.sum(x), axis=0) / df.shape[0]
    logging.info("Feature nan percentages: ")
    for c, v in nan_percentage.iteritems():
        logging.info(f"    Nan percentage {c}: {v:.3}")

    if remove_rows_with_nans:
        # remove ROWS with nan values
        # axis=0 drops rows...
        logging.info(f"Number of rows before removing nans: {df.shape[0]}")
        df = df.dropna(axis=0, how="any")
        df_meta = df_meta[df_meta.index.isin(df.index)]
        logging.info(f"Number of rows after removing nans: {df.shape[0]}")

    if remove_columns_with_nans:
        # remove COLUMNS with nan values
        logging.info(f"Number of columns before removing nans: {df.shape[1]}")
        df = df.dropna(axis=1, how="any")
        # metadata stays intact if no rows are removed, so no need to touch df_meta
        logging.info(f"Number of rows after removing nans: {df.shape[1]}")

    # remove constant valued features
    nonconstant_feats = (df != df.iloc[0]).any()
    for fname in nonconstant_feats[nonconstant_feats == False].index:
        logging.info(f"Removing constant valued feature {fname}: value {df[fname].mean()}")
    df = df.loc[:, nonconstant_feats]

    feats_dict["feats"] = df.values
    feats_dict["feats_names"] = df.columns.tolist()
    feats_dict["feats_labels"] = df_meta["labels"].tolist()
    if "feats_coords" in feats_dict.keys():
        feats_dict["feats_coords"] = df_meta[["ymin", "ymax", "xmin", "xmax"]].values
    if "feats_fns" in feats_dict.keys():
        feats_dict["feats_fns"] = df_meta["fns"].tolist()
    if "feats_tmfns" in feats_dict.keys():
        feats_dict["feats_tmfns"] = df_meta["tmfns"].tolist()
    if "feats_mask_ratios" in feats_dict.keys():
        feats_dict["feats_mask_ratios"] = df_meta["mask_ratios"].tolist()

    return feats_dict


def standardize_features(feats_dict: dict):
    logging.info("Standardizing features: zero mean, unit variance..")
    df = pd.DataFrame(feats_dict["feats"], columns=feats_dict["feats_names"])
    # zero mean
    normalized_df = df - df.mean()
    # unit variance
    normalized_df = normalized_df / normalized_df.std()
    feats_dict["feats"] = normalized_df.values
    feats_dict["feats_names"] = normalized_df.columns.tolist()
    return feats_dict


def normalize_features_01(feats_dict: dict):
    logging.info("Scale features between 0-1")
    df = pd.DataFrame(feats_dict["feats"], columns=feats_dict["feats_names"])
    scaled_df = df - df.min()
    scaled_df = scaled_df / scaled_df.max()
    feats_dict["feats"] = scaled_df.values
    feats_dict["feats_names"] = scaled_df.columns.tolist()
    return feats_dict


def append_3d_features(features_dict, mask_features_fns):
    # load all 3d features into memory (they are small)
    all3d = [pd.read_csv(fn) for fn in mask_features_fns]

    # # TODO: remove bad features in feature extraction code rather than here
    # all3d = [df.drop(columns="Unnamed: 121") for df in all3d]

    # number of features to omit from beginning of 3d features (e.g. mouse_id and lesion_id)
    num_feats_omit = 2

    feature_names = all3d[0].columns.tolist()[num_feats_omit:]

    # iterate over all matlab + ae features and try to find matching feature for each row (sample)
    feats_3d = []
    for i, label in enumerate(features_dict["feats_labels"]):
        # parse Tilelabel
        tile_label = int(re.findall("Tilelabel([0-9]+)", label)[0])
        # parse mouse
        mouse_str = re.findall("Tilelabel[0-9]+_(ptenx036-.*)-", label)[0]
        # one sample in features dict corresponds to one lesion
        # here we select the dataframe that contains all shape features for current mouse
        mouse_str_u = mouse_str.upper().replace("-0", "-")
        try:
            df = [f for f in all3d if f["mouse_id"][0].replace("-0", "-") == mouse_str_u][0]
        except IndexError as e:
            print(f"IndexError for mouse {mouse_str_u} when finding matching features for 3d features")

        f = np.array(df[df["lesion_id"] == tile_label])[0]
        # remove mouse_id and lesion_ide from the beginning before joining features
        f = f[num_feats_omit:]
        feats_3d.append(f)

    # concatenate features
    feats_3d = np.array(feats_3d)
    temp_feats = features_dict["feats"]
    temp_feats = np.concatenate((temp_feats, feats_3d), axis=1)
    features_dict["feats"] = temp_feats

    # concatenate feature names
    features_dict["feats_names"].extend(feature_names)

    return features_dict


def middle_sections(path_output_objects):
    os.makedirs(path_output_objects, exist_ok=True)

    # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE_middleones/"
    )

    # ftemp = "preloaded_middle.pkl"
    # if not os.path.exists(ftemp):
    #     # load all features into memory
    #     FD = load_tile_features(
    #         matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, print_tile_labels=PRINT_TILE_LABELS
    #     )
    #     save_object(ftemp, FD)
    # else:
    #     FD = load_object(ftemp)

    # load all features into memory
    FD = load_tile_features(
        matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, print_tile_labels=PRINT_TILE_LABELS
    )

    # pick feature types
    FD = pick_feature_types(FD, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=False)

    # clean features
    FD = clean_features(FD)

    # select features
    if args["fselect"] is not None:
        FD = normalize_features_01(FD)
        FD = select_features(FD, method=args["fselect"], N=args["fselect_N"])

    # standardize features
    FD = standardize_features(FD)

    # check some feature statistics
    check_for_value(FD["feats"], FD["feats_names"], value="extrema")

    # aggregate features, compute tsne and save to disk
    label_legends = np.unique(FD["feats_labels"])

    if args["computation"] == None or args["computation"] == ("middle_1"):
        outpath = os.path.join(
            path_output_objects,
            f"{args['method']}-data-noaggr__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
        )
        compute_tsne(save_path=outpath, label_legends=label_legends, **FD)


def preloader(ftemp, matlab_fns, ae_fns, tmfns, only_from_tumor_regions=True, regex=None):

    # SKIP ALL PRELOADDING FOR NOW
    FD = load_tile_features(
        matlab_fns, ae_fns, tmfns, only_from_tumor_regions=only_from_tumor_regions, print_tile_labels=PRINT_TILE_LABELS, regex=regex
    )

    # if not os.path.exists(ftemp):
    #     # load all features into memory
    #     FD = load_tile_features(
    #         matlab_fns, ae_fns, tmfns, only_from_tumor_regions=only_from_tumor_regions, print_tile_labels=PRINT_TILE_LABELS, regex=regex
    #     )
    #     save_object(ftemp, FD)
    # else:
    #     FD = load_object(ftemp)

    return FD


def middle_sections_tumor_vs_normal(path_output_objects):
    os.makedirs(path_output_objects, exist_ok=True)

    # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE_middleones/"
    )

    # ftemp = "preloaded_middle.pkl"
    # if not os.path.exists(ftemp):
    #     # load all features into memory
    #     FD = load_tile_features(
    #         matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, print_tile_labels=PRINT_TILE_LABELS
    #     )
    #     save_object(ftemp, FD)
    # else:
    #     FD = load_object(ftemp)

    # load all features into memory
    FD = load_tile_features(
        matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, print_tile_labels=PRINT_TILE_LABELS
    )

    # pick feature types
    FD = pick_feature_types(FD, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=False)

    # clean features
    FD = clean_features(FD)

    # select features
    if args["fselect"] is not None:
        FD = normalize_features_01(FD)
        FD = select_features(FD, method=args["fselect"], N=args["fselect_N"])

    # standardize features
    FD = standardize_features(FD)

    # check some feature statistics
    check_for_value(FD["feats"], FD["feats_names"], value="extrema")

    # modify lables to get tumor_vs_normal
    temp = [re.sub("Tilelabel0.*", "Normal", x) for x in FD["feats_labels"]]
    FD["feats_labels"] = [re.sub("Tilelabel[1-9].*", "Lesion", x) for x in temp]

    # aggregate features, compute tsne and save to disk
    label_legends = np.unique(FD["feats_labels"])

    if args["computation"] == None or args["computation"] == ("tumor_vs_normal_middle_1"):
        outpath = os.path.join(
            path_output_objects,
            f"{args['method']}-data-noaggr__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
        )
        compute_tsne(save_path=outpath, label_legends=label_legends, **FD)


def select_features(feats_dict, method, N, feature_name=None):
    df = pd.DataFrame(feats_dict["feats"], columns=feats_dict["feats_names"])

    if method == "variance":
        logging.info("Selecting {N} highest variance features:")
        # select N highest variance features
        index = df.var().sort_values(ascending=False).head(N).index
        for i, feature in enumerate(index):
            logging.info(f"    Feature {feature} - variance {df[feature].var()}")
        df = df[index]
    elif method == "correlation":
        import pdb

        pdb.set_trace()

    feats_dict["feats"] = df.values
    feats_dict["feats_names"] = df.columns.tolist()
    return feats_dict


def per_lesion_plots(path_output_objects):
    os.makedirs(path_output_objects, exist_ok=True)

    # # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/"
    )

    # ftemp = "preloaded_per_lesion.pkl"
    # if not os.path.exists(ftemp):
    #     # load all features into memory
    #     FD = load_tile_features(
    #         matlab_fns, ae_fns, tmfns, only_from_tumor_regions=True, print_tile_labels=PRINT_TILE_LABELS
    #     )
    #     save_object(ftemp, FD)
    # else:
    #     FD = load_object(ftemp)

    # load all features into memory
    FD = load_tile_features(
        matlab_fns, ae_fns, tmfns, only_from_tumor_regions=True, print_tile_labels=PRINT_TILE_LABELS
    )

    # remove troublesome features
    FD = clean_features(FD)

    # aggregate features, compute tsne and save to disk
    if args["computation"] == None or args["computation"] == "per_lesion_0":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_multiple_stats(path_output_objects, FD)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        logging.info("Feature names before cleaning & selection: {}".format(aggr_feats_dict["feats_names"]))

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        logging.info("Feature names used in this computation: {}".format(aggr_feats_dict["feats_names"]))

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        # compute tsne
        outpath = os.path.join(
            path_output_objects,
            f"{args['method']}-data-multiple-stats__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
        )
        compute_tsne(save_path=outpath, label_legends=np.unique(aggr_feats_dict["feats_labels"]), **aggr_feats_dict)


    if args["computation"] == None or args["computation"] == "per_lesion_1":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_median_per_feature(path_output_objects, FD)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        logging.info("Feature names before cleaning & selection: {}".format(aggr_feats_dict["feats_names"]))

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        logging.info("Feature names used in this computation: {}".format(aggr_feats_dict["feats_names"]))

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        # compute tsne
        outpath = os.path.join(
            path_output_objects,
            f"{args['method']}-data-median-per-feature__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
        )
        compute_tsne(save_path=outpath, label_legends=np.unique(aggr_feats_dict["feats_labels"]), **aggr_feats_dict)

    if args["computation"] == None or args["computation"] == "per_lesion_2":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_geometric_median(path_output_objects, FD)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        logging.info("Feature names before cleaning & selection: {}".format(aggr_feats_dict["feats_names"]))

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        logging.info("Feature names used in this computation: {}".format(aggr_feats_dict["feats_names"]))

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        # compute tsne
        outpath = os.path.join(
            path_output_objects,
            f"{args['method']}-data-geometric-median__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
        )
        compute_tsne(save_path=outpath, label_legends=np.unique(aggr_feats_dict["feats_labels"]), **aggr_feats_dict)

    if args["computation"] == None or args["computation"] == "per_lesion_3":

        noaggr_feats_dict = pick_feature_types(FD, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=False)

        logging.info("Feature names before cleaning & selection: {}".format(noaggr_feats_dict["feats_names"]))

        # select features
        if args["fselect"] is not None:
            noaggr_feats_dict = normalize_features_01(noaggr_feats_dict)
            noaggr_feats_dict = select_features(noaggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        logging.info("Feature names used in this computation: {}".format(noaggr_feats_dict["feats_names"]))

        # check some feature statistics
        check_for_value(noaggr_feats_dict["feats"], noaggr_feats_dict["feats_names"], value="extrema")

        outpath = os.path.join(
            path_output_objects,
            f"{args['method']}-data-noaggr__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
        )
        compute_tsne(save_path=outpath, label_legends=np.unique(noaggr_feats_dict["feats_labels"]), **noaggr_feats_dict)

    # Like per_lesion_0 but without volume correlating features
    if args["computation"] == None or args["computation"] == "per_lesion_4":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_multiple_stats(path_output_objects, FD)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        # remove volume correlating features
        aggr_feats_dict = remove_volume_correlating_features(aggr_feats_dict)

        logging.info("Feature names before cleaning & selection: {}".format(aggr_feats_dict["feats_names"]))

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        logging.info("Feature names used in this computation: {}".format(aggr_feats_dict["feats_names"]))

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        # compute tsne
        outpath = os.path.join(
            path_output_objects,
            f"{args['method']}-data-multiple-stats-wovolfeats__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
        )
        compute_tsne(save_path=outpath, label_legends=np.unique(aggr_feats_dict["feats_labels"]), **aggr_feats_dict)


def plot_clustergram(FD, save_path, mode="as_is", row_cluster=True, col_cluster=False):
    # convert to dataframe to pass feature names
    df = pd.DataFrame(FD["feats"], columns=FD["feats_names"])
    if mode == "tumor_normal":
        mice_labels = [re.sub("Tilelabel[1-9]+_", "tumor_", l) for l in FD["feats_labels"]]
        mice_labels = [re.sub("Tilelabel0_", "normal_", l) for l in mice_labels]
    elif mode == "as_is":

        # old
        # mice_labels = [re.sub("Tilelabel([1-9]+)_", r"tumor\1_", l) for l in FD["feats_labels"]]
        # mice_labels = [re.sub("Tilelabel0_", "normal_", l) for l in mice_labels]

        # # replace mice
        # mice_labels = [re.sub("ptenx036-011-", "A", l, flags=re.IGNORECASE) for l in FD["feats_labels"]]
        # mice_labels = [re.sub("ptenx036-14-", "B", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-15-", "C", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-20-", "D", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-21-", "E", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-032-", "F", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-033-", "G", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-0?34-", "H", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-0?39-", "I", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-0?40-", "J", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-0?42-", "K", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("ptenx036-0?68-", "L", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?003-", "M", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?006-", "N", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?007-", "O", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?008-", "P", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?022-", "Q", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?045-", "R", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?1387-", "S", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?199-", "T", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?211-", "U", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?223-", "V", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?355-", "W", l, flags=re.IGNORECASE) for l in mice_labels]
        # mice_labels = [re.sub("utu186-?xhimyc-?356-", "X", l, flags=re.IGNORECASE) for l in mice_labels]

        # # replace tumors
        # mice_labels = [re.sub("Tilelabel([1-9]+)_", r"\1", l) for l in mice_labels]
        # # swap labels
        # mice_labels = [re.sub("([0-9])([A-Z])", r"\2\1", l) for l in mice_labels]

        # replace mice
        mice_labels = [re.sub("ptenx036-011-", "1", l, flags=re.IGNORECASE) for l in FD["feats_labels"]]
        mice_labels = [re.sub("ptenx036-14-", "2", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-15-", "4", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-20-", "5", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-21-", "7", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-032-", "6", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-033-", "3", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-0?34-", "8", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-0?39-", "9", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-0?40-", "10", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-0?42-", "11", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("ptenx036-0?68-", "12", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?003-", "13", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?006-", "14", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?007-", "15", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?008-", "16", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?022-", "17", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?045-", "18", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?1387-", "19", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?199-", "20", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?211-", "21", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?223-", "22", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?355-", "23", l, flags=re.IGNORECASE) for l in mice_labels]
        mice_labels = [re.sub("utu186-?xhimyc-?356-", "24", l, flags=re.IGNORECASE) for l in mice_labels]

        # replace tumors
        new_mice_labels = []
        for l in mice_labels:
            match = re.search("Tilelabel([1-9]+)", l)
            new_l = chr(int(l[match.start():match.end()].replace("Tilelabel", "")) + 64)
            new_mice_labels.append(new_l + l[match.end():])
            
        # swap labels
        mice_labels = [re.sub("([A-Z])_([0-9])", r"\2 \1", l) for l in new_mice_labels]


        # use continuous labels 1a 1b 1c instead of 1a 1c 1d
        rename_dict = dict()
        for tl in range(1, 25):
            unique_labels_per_mouse = []
            for i, lab in enumerate(mice_labels):
                match = re.findall(f"{tl} [A-Z]", lab)
                if match:
                    unique_labels_per_mouse.append(match)
            old_labels = np.unique(unique_labels_per_mouse)
            new_labels = [f"{tl}{chr(num + 96 + 1)}" for num in range(len(unique_labels_per_mouse))]
            # append renaming rules to rename_dict
            # update extends existing dict
            rename_dict.update(dict(zip(old_labels, new_labels)))
        # use rename_dict to rename labels
        mice_labels = [rename_dict[lab] for lab in mice_labels]

    else:
        raise ValueError(f"Unrecognized mode in plot_clustergram: {mode}")

    ysize = df.shape[1]
    df.index = mice_labels
    # swap places between tumorx and mouse_id to sort better
    # df = df.rename(index=lambda x: f"{x.split('_')[1]}_{x.split('_')[0]}")
    # df = df.rename(index=lambda x: x.replace("032", "32").replace("033", "33"))
    df = df.sort_index()
    df = df.transpose()  # transpose so that samples are columns
    fig = plt.figure()
    # fig.set_size_inches((8, 8))
    # methods: https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
    logging.info(f"Creating clustergram..")
    # sns.set(font_scale=0.25) # good for other clustergrams
    sns.set(font_scale=0.35) # good for 3D clustergram, bad for others, USED IN MANU
    # feature_label_spacing = 0.12
    feature_label_spacing = 0.07
    num_features_split_limit = 1000
    dpi = 300
    quality = 99

    # THIS IS NOT HELPING
    # # IF image is going to be tooo large, lower dpi
    # if ysize > num_features_split_limit:
    #     dpi = 10

    cobj = sns.clustermap(
        df,
        method="average",
        metric="euclidean",
        cmap="seismic",
        z_score=None,
        standard_scale=None,
        figsize=(9*0.5, (9 + ysize * feature_label_spacing)*0.5),
        cbar_kws=None,
        row_cluster=row_cluster,
        col_cluster=col_cluster,
        row_linkage=None,
        col_linkage=None,
        row_colors=None,
        col_colors=None,
        mask=None,
        yticklabels=True,
    )
    # save figure
    logging.info(f"Saving plot {save_path}")
    # use cobjs's savefig to prevent cropping some labels out
    cobj.savefig(save_path, dpi=dpi, quality=quality)
    plt.close()


    # # if plot would be too large, split it
    # if ysize < num_features_split_limit:
    #     cobj = sns.clustermap(
    #         df,
    #         method="average",
    #         metric="euclidean",
    #         cmap="seismic",
    #         z_score=None,
    #         standard_scale=None,
    #         figsize=(9, 9 + ysize * feature_label_spacing),
    #         cbar_kws=None,
    #         row_cluster=row_cluster,
    #         col_cluster=col_cluster,
    #         row_linkage=None,
    #         col_linkage=None,
    #         row_colors=None,
    #         col_colors=None,
    #         mask=None,
    #         yticklabels=True,
    #     )
    #     # save figure
    #     logging.info(f"Saving plot {save_path}")
    #     # use cobjs's savefig to prevent cropping some labels out
    #     cobj.savefig(save_path, dpi=dpi, quality=quality)
    #     plt.close()
    # else:
    #     N = int(np.ceil(ysize / num_features_split_limit))
    #     piece_len = int(ysize / N)
    #     split_points = piece_len * np.arange(N + 1)
    #     # set last point as size to get all samples in case of rounding errors etc
    #     split_points[-1] = ysize
    #     path_pieces = os.path.splitext(save_path)
    #     for i in range(len(split_points) - 1):
    #         save_path_part = f"{path_pieces[0]}_part{i + 1}_{path_pieces[1]}"
    #         # samples are columns, features are rows
    #         # take features until splitpoint
    #         df_part = df.iloc[split_points[i] : split_points[i + 1], :]
    #         cobj_part = sns.clustermap(
    #             df_part,
    #             method="average",
    #             metric="euclidean",
    #             cmap="seismic",
    #             z_score=None,
    #             standard_scale=None,
    #             figsize=(9, 9 + piece_len * feature_label_spacing),
    #             cbar_kws=None,
    #             row_cluster=row_cluster,
    #             col_cluster=col_cluster,
    #             row_linkage=None,
    #             col_linkage=None,
    #             row_colors=None,
    #             col_colors=None,
    #             mask=None,
    #             yticklabels=True,
    #         )
    #         logging.info(
    #             f"Saving part {i + 1} plot ({split_points[i + 1] - split_points[i]} features) {save_path_part}"
    #         )
    #         cobj_part.savefig(save_path_part, dpi=dpi, quality=quality)
    #         plt.close()


def clustergram_middle(path_output_objects):
    os.makedirs(path_output_objects, exist_ok=True)

    # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE_middleones/"
    )

    # ftemp = "preloaded_middle.pkl"
    # if not os.path.exists(ftemp):
    #     # load all features into memory
    #     FD = load_tile_features(
    #         matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, print_tile_labels=PRINT_TILE_LABELS
    #     )
    #     save_object(ftemp, FD)
    # else:
    #     FD = load_object(ftemp)


    # load all features into memory
    FD = load_tile_features(
        matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, print_tile_labels=PRINT_TILE_LABELS
    )

    # pick feature types
    FD = pick_feature_types(FD, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=False)

    # clean features
    FD = clean_features(FD)

    # select features
    if args["fselect"] is not None:
        FD = normalize_features_01(FD)
        FD = select_features(FD, method=args["fselect"], N=args["fselect_N"])

    # # standardize features
    # FD = standardize_features(FD)

    # check some feature statistics
    check_for_value(FD["feats"], FD["feats_names"], value="extrema")

    # save figure
    savep = os.path.join(
        path_output_objects,
        f"clustergram-middle__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}.png",
    )
    plot_clustergram(FD, savep, mode="as_is")


def clustergram_per_lesion(path_output_objects):
    os.makedirs(path_output_objects, exist_ok=True)

    # # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/"
    )

    # ftemp = "preloaded_per_lesion.pkl"
    # if not os.path.exists(ftemp):
    #     # load all features into memory
    #     FD = load_tile_features(
    #         matlab_fns, ae_fns, tmfns, only_from_tumor_regions=True, print_tile_labels=PRINT_TILE_LABELS
    #     )
    #     save_object(ftemp, FD)
    # else:
    #     FD = load_object(ftemp)

    # load all features into memory
    FD = load_tile_features(
        matlab_fns, ae_fns, tmfns, only_from_tumor_regions=True, print_tile_labels=PRINT_TILE_LABELS
    )

    logging.info("Feature names: {}".format(FD["feats_names"]))

    # remove troublesome features
    FD = clean_features(FD)

    # aggregate features, compute tsne and save to disk
    if args["computation"] == None or args["computation"] == "clustergram_per_lesion_0":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_multiple_stats(path_output_objects, FD)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        # compute tsne
        outpath = os.path.join(
            path_output_objects,
            f"clustergram-aggr-multiple-stats__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}.png",
        )
        plot_clustergram(aggr_feats_dict, outpath, mode="as_is")

    if args["computation"] == None or args["computation"] == "clustergram_per_lesion_1":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_median_per_feature(path_output_objects, FD)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        # compute tsne
        outpath = os.path.join(
            path_output_objects,
            f"clustergram-aggr-median-per-feature__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}.png",
        )
        plot_clustergram(aggr_feats_dict, outpath, mode="as_is", row_cluster=True, col_cluster=True)

    if args["computation"] == None or args["computation"] == "clustergram_per_lesion_2":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_geometric_median(path_output_objects, FD)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        # compute tsne
        outpath = os.path.join(
            path_output_objects,
            f"clustergram-aggr-geometric-median__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}.png",
        )
        plot_clustergram(aggr_feats_dict, outpath, mode="as_is")

    # same as clustergram_per_lesion_0 but fewer statistics computed
    if args["computation"] == None or args["computation"] == "clustergram_per_lesion_3":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_multiple_stats(path_output_objects, FD, fewer_features=True)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        for cluster_cols in [True, False]:
            # compute tsne
            outpath = os.path.join(
                path_output_objects,
                f"clustergram-aggr-multiple-stats-small__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}_cluster_cols{[cluster_cols]}.png",
            )
            plot_clustergram(aggr_feats_dict, outpath, mode="as_is", row_cluster=True, col_cluster=cluster_cols)  

    # same as clustergram_per_lesion_3 but remove features that correlate alot with volume
    if args["computation"] == None or args["computation"] == "clustergram_per_lesion_4":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_multiple_stats(path_output_objects, FD, fewer_features=True)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)
        
        aggr_feats_dict = remove_volume_correlating_features(aggr_feats_dict)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        # compute tsne
        outpath = os.path.join(
            path_output_objects,
            f"clustergram-aggr-multiple-stats-small-wovolfeats__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}.png",
        )
        plot_clustergram(aggr_feats_dict, outpath, mode="as_is", row_cluster=True, col_cluster=True)  



def load_features(path_tempfile, path_base):
    if not os.path.exists(path_tempfile):
        # read paths to features from disk
        tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(path_base)

        # load all features into memory
        FD = load_tile_features(
            matlab_fns, ae_fns, tmfns, only_from_tumor_regions=True, print_tile_labels=PRINT_TILE_LABELS
        )
        save_object(path_tempfile, [FD, tmfns, matlab_fns, ae_fns, mask_features_fns])
    else:
        [FD, tmfns, matlab_fns, ae_fns, mask_features_fns] = load_object(path_tempfile)

    return FD, tmfns, matlab_fns, ae_fns, mask_features_fns


def analysis_per_lesion(path_output_objects):
    os.makedirs(path_output_objects, exist_ok=True)

    FD, tmfns, matlab_fns, ae_fns, mask_features_fns = load_features(
        "preloaded_per_lesion.pkl", "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/"
    )

    logging.info("Feature names: {}".format(FD["feats_names"]))

    # remove troublesome features
    FD = clean_features(FD)

    # aggregate features, compute tsne and save to disk
    if args["computation"] == None or args["computation"] == "feature_analysis":
        # aggregate features, one sample per lesion
        aggr_feats_dict = aggregate_multiple_stats(path_output_objects, FD)

        # append 3d shape features
        aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

        # pick feature types to plot
        aggr_feats_dict = pick_feature_types(
            aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
        )

        # clean features, do not remove rows since there are so few of them
        aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

        # select features
        if args["fselect"] is not None:
            aggr_feats_dict = normalize_features_01(aggr_feats_dict)
            aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

        # standardize features
        aggr_feats_dict = standardize_features(aggr_feats_dict)

        # print feature statistics
        check_for_value(aggr_feats_dict["feats"], aggr_feats_dict["feats_names"], value="extrema")

        aggr_feats_dict = select_features(aggr_feats_dict, method="correlation", N=10, feature_name="volume")


def whole_stack_tumor_vs_normal(path_output_objects):
    os.makedirs(path_output_objects, exist_ok=True)

    # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/"
    )

    miced = [
        "PTENX036-011",
        "PTENX036-14",
        "PTENX036-15",
        "PTENX036-20",
        "PTENX036-32",
        "PTENX036-33",
    ]
    
    for md in miced:

        ftemp = f"preloaded_whole_stack_tumor_vs_normal_{md}.pkl"
        FD = preloader(ftemp, matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, regex=f".*{md}.*")

        # pick feature types
        FD = pick_feature_types(FD, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=False)

        # clean features
        FD = clean_features(FD)

        # select features
        if args["fselect"] is not None:
            FD = normalize_features_01(FD)
            FD = select_features(FD, method=args["fselect"], N=args["fselect_N"])

        # standardize features
        FD = standardize_features(FD)

        # modify lables to get tumor_vs_normal
        temp = [re.sub("Tilelabel0.*", "Normal", x) for x in FD["feats_labels"]]
        FD["feats_labels"] = [re.sub("Tilelabel([1-9]).*", r"Lesion \1", x) for x in temp]

        # aggregate features, compute tsne and save to disk
        label_legends = np.unique(FD["feats_labels"])

        if args["computation"] == None or args["computation"] == ("whole_stack_tumor_vs_normal"):
            outpath = os.path.join(
                path_output_objects,
                md,
                f"{args['method']}-data-noaggr__matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
            )
            compute_tsne(save_path=outpath, label_legends=label_legends, **FD)


def scatter_whole_stack_tumor_vs_normal(path_output_objects):                                                             
    os.makedirs(path_output_objects, exist_ok=True)
    
    # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/"
    )                       
                                     
    miced = [        
        "PTENX036-011",       
        "PTENX036-14",               
        "PTENX036-15",                 
        "PTENX036-20",
        "PTENX036-32",
        "PTENX036-33",
    ]


    for md in miced:
    
        ftemp = f"preloaded_scatter_whole_stack_tumor_vs_normal_{md}.pkl"
        FD = preloader(ftemp, matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, regex=f".*{md}.*")                                                                           

        # pick feature types                                                                                                                                                         
        FD = pick_feature_types(FD, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=False)                                                                                     
    
        # pick features
        FD = keep_select_features(FD, ["nucNumber", "IntensityMean_E"])

        # # clean features
        # FD = clean_features(FD)
    
        # # select features
        # if args["fselect"] is not None:
        #     FD = normalize_features_01(FD)
        #     FD = select_features(FD, method=args["fselect"], N=args["fselect_N"])
    
        # # standardize features
        # FD = standardize_features(FD)
    
        # # check some feature statistics
        # check_for_value(FD["feats"], FD["feats_names"], value="extrema")
    
        # modify lables to get tmor_vs_normal
        temp = [re.sub("Tilelabel0.*", "Normal", x) for x in FD["feats_labels"]]
        FD["feats_labels"] = [re.sub("Tilelabel([1-9]).*", r"Lesion \1", x) for x in temp]

        if args["computation"] == None or args["computation"] == ("scatter_whole_stack_tumor_vs_normal"):
            outpath = os.path.join(
                path_output_objects,
                md,
                f"scatter-{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
            )
            os.makedirs(os.path.join(path_output_objects, md), exist_ok=True)

            # Colors from matlab's distinguishable_colors function
            color_cycle = [( 0    ,   0     ,   1.0000 ),
                           (1.0000,        0,        0 ),
                           (     0,   1.0000,        0 ),
                           (     0,        0,   0.1724 ),
                           (1.0000,   0.1034,   0.7241 ),
                           (1.0000,   0.8276,        0 ),
                           (     0,   0.3448,        0 ),
                           (0.5172,   0.5172,   1.0000 ),
                           (0.6207,   0.3103,   0.2759 ),
                           (     0,   1.0000,   0.7586 ),
                           (     0,   0.5172,   0.5862 ),
                           (     0,        0,   0.4828 ),
                           (0.5862,   0.8276,   0.3103 ),
                           (0.9655,   0.6207,   0.8621 ),
                           (0.8276,   0.0690,   1.0000 ),
                           (0.4828,   0.1034,   0.4138 ),
                           (0.9655,   0.0690,   0.3793 ),
                           (1.0000,   0.7586,   0.5172 ),
                           (0.1379,   0.1379,   0.0345 ),
                           (0.5517,   0.6552,   0.4828 ),
                           (0.9655,   0.5172,   0.0345 ),
                           (0.5172,   0.4483,        0 ),
                           (0.4483,   0.9655,   1.0000 ),
                           (0.6207,   0.7586,   1.0000 ),
                           (0.4483,   0.3793,   0.4828 ),
                           (0.6207,        0,        0 ),
                           (     0,   0.3103,   1.0000 ),
                           (     0,   0.2759,   0.5862 ),
                           (0.8276,   1.0000,        0 ),
                           (0.7241,   0.3103,   0.8276 ),
                           (0.2414,        0,   0.1034 ),
                           (0.9310,   1.0000,   0.6897 ),
                           (1.0000,   0.4828,   0.3793 ),
                           (0.2759,   1.0000,   0.4828 ),
                           (0.0690,   0.6552,   0.3793 ),
                           (0.8276,   0.6552,   0.6552 ),
                           (0.8276,   0.3103,   0.5172 ),
                           (0.4138,        0,   0.7586 ),
                           (0.1724,   0.3793,   0.2759 ),
                           (     0,   0.5862,   0.9655 )]

            # color_default = np.array([31, 119, 180])/255
            # color_default = np.array([50, 50, 50]) / 255
            # color_default = np.array((0, 1.0000, 0.7586))
            color_default = np.array((80, 80, 80)) / 255

            uni, uni_counts = np.unique(FD["feats_labels"], return_counts=True)
            idx = np.where(uni == "Normal")[0][0]
            uni_nonormal = np.delete(uni, idx)
            uni_counts_nonormal = np.delete(uni_counts, idx)

            lesion_color_idx = list(range(len(uni_nonormal)))
            idx_dict = dict(zip(uni_nonormal, lesion_color_idx))
            
            label_colors_for_each_sample = []
            for lab in FD["feats_labels"]:
                if lab == "Normal":
                    label_colors_for_each_sample.append(color_default)
                else:
                    # idx = int(re.sub("Lesion ([0-9])", r"\1", lab)) - 1
                    
                    label_colors_for_each_sample.append(color_cycle[idx_dict[lab]])

            ff = np.array(FD["feats_labels"])
            inds = []
            for u in uni:
                ii = np.where(ff == u)[0]
                inds.append(ii)

            fig = plt.figure()
            sns.set_style("darkgrid")
            sns.color_palette()
            ax = fig.add_subplot(111)
            marker_size = 7

            label_colors_for_each_sample = np.array(label_colors_for_each_sample)
            
            # plot separately and in this order to have tumor colors on top
            sort_idx = np.argsort(uni_counts)
            sort_idx = sort_idx[::-1]  # make descending order
            inds = np.array(inds)[sort_idx].tolist()
            uni = np.array(uni)[sort_idx].tolist()
            uni_counts = np.array(uni_counts)[sort_idx].tolist()

            for ii, u in zip(inds, uni):
                sc = ax.scatter(FD["feats"][ii, 0], FD["feats"][ii, 1], color=label_colors_for_each_sample[ii], s=marker_size, label=u)
            plt.xlabel(FD["feats_names"][0])
            plt.ylabel(FD["feats_names"][1])
            plt.legend(uni)
            plt.savefig(outpath, dpi=300)
            plt.close()



def violin_whole_stack_tumor_vs_normal(path_output_objects):                                                             
    os.makedirs(path_output_objects, exist_ok=True)
    
    # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/"
    )                       
                                     
    miced = [        
        "PTENX036-011",       
        "PTENX036-14",               
        "PTENX036-15",                 
        "PTENX036-20",
        "PTENX036-32",
        "PTENX036-33",
    ]

    
    alldf_list = []

    for md in miced:
    
        ftemp = f"preloaded_scatter_whole_stack_tumor_vs_normal_{md}.pkl"
        FD = preloader(ftemp, matlab_fns, ae_fns, tmfns, only_from_tumor_regions=False, regex=f".*{md}.*")                                                                           

        # pick feature types                                                                                                                                                         
        FD = pick_feature_types(FD, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=False)                                                                                     
    
        # pick features
        FD = keep_select_features(FD, ["nucNumber", "IntensityMean_E"])

        # # clean features
        # FD = clean_features(FD)
    
        # # select features
        # if args["fselect"] is not None:
        #     FD = normalize_features_01(FD)
        #     FD = select_features(FD, method=args["fselect"], N=args["fselect_N"])
    
        # # standardize features
        # FD = standardize_features(FD)
    
        # # check some feature statistics
        # check_for_value(FD["feats"], FD["feats_names"], value="extrema")
    
        # HERE MODIFY LABELS

        # modify lables to get tmor_vs_normal
        temp = [re.sub("Tilelabel0.*", "Normal", x) for x in FD["feats_labels"]]
        # FD["feats_labels"] = [re.sub("Tilelabel([1-9]).*", r"Lesion \1", x) for x in temp]
        FD["feats_labels"] = [re.sub("Tilelabel([1-9]).*", "Lesion", x) for x in temp]

        df = pd.DataFrame(FD["feats"], columns=FD["feats_names"])
        df["label"] = FD["feats_labels"]
        df["Mouse"] = md
        # df = df[:20000]
        # df = df[df["nucNumber"] > 20]
        alldf_list.append(df)
        

    alldf = pd.concat(alldf_list)

    outpath = os.path.join(
        path_output_objects,
        f"violin-IntensityMean_E-matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
    )
    print(f"Creating: {outpath}")
    plt.figure(figsize=(15, 4))
    sns.set_style("whitegrid")
    ax = sns.violinplot(x="Mouse", y="IntensityMean_E", hue="label", data=alldf, palette="muted")
    plt.savefig(outpath, dpi=300)
    plt.close()

    outpath = os.path.join(
        path_output_objects,
        f"violin-nucNumber-matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
    )
    print(f"Creating: {outpath}")
    plt.figure(figsize=(15, 4))
    sns.set_style("whitegrid")
    ax = sns.violinplot(x="Mouse", y="nucNumber", hue="label", data=alldf, palette="muted")
    outpath = os.path.join(
        path_output_objects,
        f"violin-nucNumber-matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
    )
    plt.savefig(outpath, dpi=300)
    plt.close()


    # outpath = os.path.join(
    #     path_output_objects,
    #     f"stripplot-IntensityMean_E-matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
    # )
    # print(f"Creating: {outpath}")
    # plt.figure(figsize=(15, 4))
    # sns.set_style("whitegrid")
    # # Show each observation with a scatterplot
    # sns.stripplot(x="Mouse", y="IntensityMean_E", hue="label", data=alldf, dodge=True, alpha=.25, zorder=1)
    # # Show the conditional means
    # sns.pointplot(x="Mouse", y="IntensityMean_E", hue="label", data=alldf, dodge=.532, join=False, palette="dark", markers="d", scale=.75, ci=None)
    # plt.savefig(outpath, dpi=300)
    # plt.close()

    # outpath = os.path.join(
    #     path_output_objects,
    #     f"stripplot-nucNumber-matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
    # )
    # print(f"Creating: {outpath}")
    # plt.figure(figsize=(15, 4))
    # sns.set_style("whitegrid")
    # # Show each observation with a scatterplot
    # sns.stripplot(x="Mouse", y="nucNumber", hue="label", data=alldf, dodge=True, alpha=.25, zorder=1, jitter=True)
    # # Show the conditional means
    # sns.pointplot(x="Mouse", y="nucNumber", hue="label", data=alldf, dodge=.532, join=False, palette="dark", markers="d", scale=.75, ci=None)
    # plt.savefig(outpath, dpi=300)
    # plt.close()


    for df, md in zip(alldf_list, miced):
        # df = df[:1000]
        outpath = os.path.join(
            path_output_objects,
            f"stripplot-both-{md}-matlab{args['use_matlab']}_ae{args['use_ae']}_3d{False}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}",
        )
        print(f"Creating: {outpath}")
        # plt.figure()
        plt.figure(figsize=(20, 10))
        sns.set_style("whitegrid")
        # Show each observation with a scatterplot
        # sns.stripplot(x="nucNumber", y="IntensityMean_E", hue="label", data=df, size=5, alpha=0.7, zorder=1, jitter=True, orient="v", palette=sns.cubehelix_palette(45))
        sns.stripplot(x="nucNumber", y="IntensityMean_E", hue="label", dodge=True, data=df, size=5, alpha=0.5, zorder=1, jitter=True, orient="v")
        # sns.stripplot(x="nucNumber", y="IntensityMean_E", data=df, size=3, alpha=0.7, zorder=1, jitter=True, orient="v", palette="Blues")
        # sns.stripplot(x="nucNumber", y="IntensityMean_E", data=df, size=3, alpha=0.7, zorder=1, jitter=True, orient="v", color=sns.dark_palette("purple"))
        # sns.stripplot(x="nucNumber", y="IntensityMean_E", data=df, size=5, alpha=0.7, zorder=1, jitter=True, orient="v", palette="GnBu")
        # Show the conditional means
        sns.pointplot(x="nucNumber", y="IntensityMean_E", data=df, join=False, color="black", markers="d", scale=.75, ci=None)
        plt.title(md)
        plt.savefig(outpath, dpi=300)
        plt.close()




def correlations_per_lesion(path_output_objects):
    os.makedirs(path_output_objects, exist_ok=True)

    # # read paths to features from disk
    tmfns, matlab_fns, ae_fns, mask_features_fns = get_paths_with_tumormasks(
        "/bmt-data/bii/projects/3D-histology/output_GRID_SEARCH_best_laplacianMSE/"
    )

    # ftemp = "preloaded_per_lesion.pkl"
    # if not os.path.exists(ftemp):
    #     # load all features into memory
    #     FD = load_tile_features(
    #         matlab_fns, ae_fns, tmfns, only_from_tumor_regions=True, print_tile_labels=PRINT_TILE_LABELS
    #     )
    #     save_object(ftemp, FD)
    # else:
    #     FD = load_object(ftemp)

    # load all features into memory
    FD = load_tile_features(
        matlab_fns, ae_fns, tmfns, only_from_tumor_regions=True, print_tile_labels=PRINT_TILE_LABELS
    )

    logging.info("Feature names: {}".format(FD["feats_names"]))

    # remove troublesome features
    FD = clean_features(FD)

    # aggregate features, one sample per lesion
    aggr_feats_dict = aggregate_multiple_stats(path_output_objects, FD, fewer_features=True)

    # append 3d shape features
    aggr_feats_dict = append_3d_features(aggr_feats_dict, mask_features_fns)

    # pick feature types to plot
    aggr_feats_dict = pick_feature_types(
        aggr_feats_dict, matlab=args["use_matlab"], ae=args["use_ae"], shape3d=args["use_3d"]
    )

    # clean features, do not remove rows since there are so few of them
    aggr_feats_dict = clean_features(aggr_feats_dict, remove_rows_with_nans=False, remove_columns_with_nans=True)

    # select features
    if args["fselect"] is not None:
        aggr_feats_dict = normalize_features_01(aggr_feats_dict)
        aggr_feats_dict = select_features(aggr_feats_dict, method=args["fselect"], N=args["fselect_N"])

    # standardize features
    aggr_feats_dict = standardize_features(aggr_feats_dict)


    feature_pairs = [
            ["pca1_moment_of_inertia_scaled_sphere", "pca2_moment_of_inertia_scaled_sphere"],
            ["pca1_moment_of_inertia_scaled_sphere", "pca3_moment_of_inertia_scaled_sphere"],
            ["pca2_moment_of_inertia_scaled_sphere", "pca3_moment_of_inertia_scaled_sphere"],
            ["pca1_moment_of_inertia_vanilla", "pca2_moment_of_inertia_vanilla"],
            ["pca1_moment_of_inertia_vanilla", "pca3_moment_of_inertia_vanilla"],
            ["pca2_moment_of_inertia_vanilla", "pca3_moment_of_inertia_vanilla"],
            ["pca1_sum_dists_per_volume", "pca2_sum_dists_per_volume"],
            ["pca1_sum_dists_per_volume", "pca3_sum_dists_per_volume"],
            ["pca2_sum_dists_per_volume", "pca3_sum_dists_per_volume"],
            ]

    for f1, f2 in feature_pairs:
        fig = plt.figure()
        sns.set_style("darkgrid")
        sns.color_palette()
        ax = fig.add_subplot(111)
        marker_size = 7

        outpath = os.path.join(
            path_output_objects,
            f"correlation-aggr-multiple-stats-small_--f1{f1}--f2{f2}--_matlab{args['use_matlab']}_ae{args['use_ae']}_3d{args['use_3d']}_{'_fsel_' + str(args['fselect']) + '_' + str(args['fselect_N']) + '_'}.png",
        )

        f1_ind = aggr_feats_dict["feats_names"].index(f1)
        f2_ind = aggr_feats_dict["feats_names"].index(f2)
        plt.scatter(aggr_feats_dict["feats"][:, f1_ind], aggr_feats_dict["feats"][:, f2_ind], s=marker_size)
        plt.xlabel(f1)
        plt.ylabel(f2)
        plt.savefig(outpath, dpi=300)
        plt.close()




if __name__ == "__main__":

    # PER_LESION_PLOTS
    if args["computation"] == None or args["computation"].startswith("per_lesion"):
        logging.info("COMPUTING PER LESION PLOTS")
        per_lesion_plots(args["output_dir"])

    # CLUSTERGRAM_PER_LESION
    if args["computation"] == None or args["computation"].startswith("clustergram_per_lesion"):
        logging.info("COMPUTING CLUSTERGRAM PLOTS")
        clustergram_per_lesion(args["output_dir"])

    # MIDDLE_SECTIONS_TUMOR_VS_NORMAL
    if args["computation"] == None or args["computation"].startswith("tumor_vs_normal"):
        logging.info("COMPUTING TUMOR VS NORMAL PLOTS")
        middle_sections_tumor_vs_normal(args["output_dir"])

    # WHOLE_STACK_TUMOR_VS_NORMAL
    if args["computation"] == None or args["computation"].startswith("whole_stack_tumor_vs_normal"):
        logging.info("COMPUTING WHOLE STACK TUMOR VS NORMAL PLOTS")
        whole_stack_tumor_vs_normal(args["output_dir"])

    # SCATTER_WHOLE_STACK_TUMOR_VS_NORMAL
    if args["computation"] == None or args["computation"].startswith("scatter_whole_stack_tumor_vs_normal"):
        logging.info("COMPUTING WHOLE STACK TUMOR VS NORMAL SCATTER PLOTS")
        scatter_whole_stack_tumor_vs_normal(args["output_dir"])

    # VIOLIN_WHOLE_STACK_TUMOR_VS_NORMAL
    if args["computation"] == None or args["computation"].startswith("violin_whole_stack_tumor_vs_normal"):
        logging.info("COMPUTING WHOLE STACK TUMOR VS NORMAL VIOLIN PLOTS")
        violin_whole_stack_tumor_vs_normal(args["output_dir"])

    # CLUSTERGRAM_MIDDLE
    if args["computation"] == None or args["computation"].startswith("clustergram_middle"):
        logging.info("COMPUTING CLUSTERGRAM PLOTS")
        clustergram_middle(args["output_dir"])

    # ANALYSIS_PER_LESION
    if args["computation"] == None or args["computation"].startswith("feature_analysis"):
        logging.info("COMPUTING FEATURE ANALYSIS")
        analysis_per_lesion(args["output_dir"])

    # CORRELATIONS_PER_LESION
    if args["computation"] == None or args["computation"].startswith("feature_pair_correlations"):
        logging.info("COMPUTING FEATURE_PAIR_CORRELATIONS")
        correlations_per_lesion(args["output_dir"])

    # MIDDLE_SECTIONS
    if args["computation"] == None or args["computation"].startswith("middle"):
        logging.info("COMPUTING MIDDLE SECTION PLOTS")
        middle_sections(args["output_dir"])

