import bz2
import shutil
import pickle
import logging
import tempfile

logging.basicConfig(level=logging.INFO)


def load_compressed_object(path_in, spool_size_MB=5000):
    """Load compressed pickled object from disk

    Args:
        path_in (str): path to loaded object
        spool size (int): Threshold for writing temporary file from RAM to disk.
                          This file is used in between decompression and deserialization.

    Returns: deserialized object
    """

    ext = ".pkl.bz2"
    if not path_in.endswith(ext):
        logging.warn("Input file does not have extension {}".format(ext))

    with tempfile.SpooledTemporaryFile(max_size=(spool_size_MB * 1024 * 1024)) as outp:
        with open(path_in, "rb") as inp:
            z = bz2.BZ2Decompressor()
            for data in iter(lambda: inp.read(100 * 1024), b""):
                outp.write(z.decompress(data))
        outp.seek(0)  # move file pointer back to beginning
        obj = pickle.load(outp)
    return obj


def save_compressed_object(save_path, obj, compress_level=1, spool_size_MB=5000):
    """Save compressed pickled object to disk

    Args:
        save_path (str):         Path to saved object
        spool_size_MB (int):     Threshold for writing temporary file from RAM to disk.
                                 This file is used in between decompression and deserialization.
        compress_level (int):    Compression level for bz2

    Returns: deserialized object
    """

    if not save_path.endswith(".pkl.bz2"):
        final_save_path = save_path + ".pkl.bz2"
    else:
        final_save_path = save_path

    with tempfile.SpooledTemporaryFile(max_size=(spool_size_MB * 1024 * 1024)) as inp:
        pickle.dump(obj, inp)
        inp.seek(0)
        with bz2.BZ2File(final_save_path, "wb", compresslevel=compress_level) as outp:
            shutil.copyfileobj(inp, outp)
    return final_save_path


def load_object(path_in):
    """Load compressed pickled object from disk"""
    ext = ".pkl"
    if not path_in.endswith(ext):
        logging.warn("Input file {} does not have extension {}".format(path_in, ext))

    with open(path_in, "rb") as f:
        obj = pickle.load(f)
    return obj


def save_object(save_path, obj):
    """Save pickled object to disk using pickle"""
    if not save_path.endswith(".pkl"):
        final_save_path = save_path + ".pkl"
    else:
        final_save_path = save_path

    with open(final_save_path, "wb") as f:
        pickle.dump(obj, f, protocol=pickle.HIGHEST_PROTOCOL)
    return final_save_path

