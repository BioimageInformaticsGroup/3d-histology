import logging
import os
import sys
from argparse import ArgumentParser
from typing import List, Dict

# import matplotlib as mpl
# mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from scipy.misc import imresize, imread

from numpy.linalg import norm
from scipy.ndimage import center_of_mass
from sklearn.decomposition import PCA

Image.MAX_IMAGE_PIXELS = 5000000000

sys.path.append("../")  # find a better way to manage imports
from utils.utils import extract_sequence_numbers_from_filenames, filter_filelist_with_regex, \
    print_mem_usage, get_mem_usage, start_timing, end_timing


class Volume:
    # slice images one after another in an ndarray
    stack = None
    # slice images in an ndarray that represents correct distances between slices.
    # There is thickness_px amount of zeros in between each slice
    stack_dense = None
    label = None  # lesion's label (1, 2, 3 ...)
    scaling_coeff = None  # 1 == no scaling, 0.5 == 50% subsampling

    voxel_size_um_symmetric = 0.46  # in micro meters
    distance_between_slices_um = 50.0

    def __init__(self, lesion_stack: np.ndarray, scale_percentge: float):
        # this should be defined before calls to _make_dense
        self.scaling_coeff = scale_percentge / 100.0
        self.label = lesion_stack.max()
        self.stack = (lesion_stack > 0).astype(np.uint8)  # get rid of labels

        pixels_between_slices_dense = self.scaling_coeff * \
                                      self.distance_between_slices_um / \
                                      self.voxel_size_um_symmetric
        logging.debug("Memory usage before make dense: {}".format(get_mem_usage()))
        self.stack_dense = self._make_dense(self.stack, pixels_between_slices_dense)
        logging.debug("Memory usage after make dense: {}".format(get_mem_usage()))

    def _make_dense(self, lesion_stack: np.ndarray, slice_thickness_px: float):
        """create ndarray with slice_thickness_px amount of empty layers in between each layer"""
        num_slices = lesion_stack.shape[0]
        z_out = int(np.ceil(slice_thickness_px)) * num_slices
        new_stack = np.zeros((z_out, lesion_stack.shape[1], lesion_stack.shape[2]))
        for i in range(num_slices):
            # round the position of the images in the array
            pos = int(np.round(slice_thickness_px * i))
            new_stack[pos, :, :] = lesion_stack[i, :, :]
        return new_stack

    # def get_nonzero_layers(self):
    #     uniq_inds = np.unique(self.stack.nonzero()[0])
    #     return self.stack[uniq_inds, :, :]


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("source_dir", metavar="source-dir", help="Path to masks directory", type=str)
    parser.add_argument("output_csv_path", metavar="output-csv-path", help="Path to output csv", type=str)
    parser.add_argument("--images_dir", metavar="images-dir", help="Path to histological image derectory", type=str)
    parser.add_argument("--resize", default=None, help="Resize masks and imag after loading", type=float)
    parser.add_argument("--image-regex", default=None,
                        help="Regular expression to use when reading images/masks from 'source-dir'",
                        type=str)
    args = parser.parse_args()
    return args


def check_arguments(args):
    if not os.path.exists(args.source_dir):
        logging.info("Path to source directory does not exist", file=sys.stderr)
        sys.exit(1)


def extract_histo_features(vol: Volume):
    pass


def extract_mask_features(lesion: Volume, lesion_dict: Dict):
    """
    Extract features from binary lesion masks and save them to lesion_dict
    Notice that lesion_dict is also the "output" variable

    Computed features:
        volume
        length_adjacent
        length_endpoints
        straightness
        length_pca1
        length_pca2
        length_pca3
        moment_of_inertia_average

    Args:
        lesion: Lesion from which features are extracted
        lesion_dict: Dictionary of features that is populated in this function

    Returns:
        Feature dictionary
    """

    F = lesion_dict

    F["volume"] = \
        lesion.distance_between_slices_um * \
        1 / lesion.scaling_coeff * lesion.voxel_size_um_symmetric * \
        1 / lesion.scaling_coeff * lesion.voxel_size_um_symmetric * \
        lesion.stack.sum()

    coms = [center_of_mass(im) for im in lesion.stack]
    coms = [(i * lesion.distance_between_slices_um,
             lesion.voxel_size_um_symmetric * com[0],
             lesion.voxel_size_um_symmetric * com[1]) for i, com in enumerate(coms)]
    coms = np.array(coms)
    diff = np.diff(coms, axis=0)
    lengths = [norm(x) for x in diff]
    length_adjacent = sum(lengths)
    F["length_adjacent"] = length_adjacent
    length_endpoints = norm(coms[-1] - coms[0])
    F["length_endpoints"] = length_endpoints
    F["straightness"] = length_endpoints / length_adjacent

    pca = PCA(n_components=3)
    coords = np.array(lesion.stack_dense.nonzero()).T
    out = pca.fit_transform(coords)
    F["length_pca1"] = (out[:, 0].max() - out[:, 0].min()) * lesion.voxel_size_um_symmetric * (1 / lesion.scaling_coeff)
    F["length_pca2"] = (out[:, 1].max() - out[:, 1].min()) * lesion.voxel_size_um_symmetric * (1 / lesion.scaling_coeff)
    F["length_pca3"] = (out[:, 2].max() - out[:, 2].min()) * lesion.voxel_size_um_symmetric * (1 / lesion.scaling_coeff)

    # "moment of inertia average"
    # actual moment of inertia is the sum of all mass elements, here average is computed
    com = center_of_mass(lesion.stack_dense)
    dists = np.array([norm(a) for a in com - coords]).sum()
    F["moment_of_inertia_average"] = 1 / lesion.scaling_coeff * lesion.voxel_size_um_symmetric * dists / coords.shape[0]

    return F


def extract_features(volumes: List[Volume], mouse_id: str):
    # crate empty features data structure
    features = [dict() for i in range(len(volumes))]

    # insert mouse identifier into each dict
    for F in features:
        F["mouse_id"] = mouse_id

    for l, f in zip(volumes, features):
        f["lesion_id"] = l.label
        extract_mask_features(l, f)

    return features


# def get_stack_unique_values(slice_stack: np.ndarray):
#     unique_candidates = []
#     for i, s in enumerate(slice_stack):
#         logging.info("Getting uniques for slice {}/{}".format(i, slice_stack.shape[0]))
#         unique_candidates.extend(list(np.unique(s)))
#         print_mem_usage()
#     uniques = list(set(unique_candidates))
#     return uniques


def extract_volumes(mask_stack: np.ndarray, volume_labels: List[int], resize_percentage: float):
    """
    Extract volumes by finding a 3D bounding box around each unique label
    Args:
        mask_stack: Image stack containing each lesion denoted with unique integer
        volume_labels: Unique integer denoting each region of interest in the images (e.g. lesions or tissue)
        resize_percentage: A amount of resizing applied to the original full resolution WSI images.
                           This is used to scale computed features to match full resolution images.

    Returns:
        List of Volume objects
    """

    # remove background from uniques
    if np.where(volume_labels == 0):
        volume_labels = np.delete(volume_labels, 0)
    else:
        logging.info("ERROR: Stack has non-zero background", file=sys.stderr)
        sys.exit(1)

    lesions = []
    for u in volume_labels:
        single_lesion_stack = mask_stack == u
        ymin, ymax, xmin, xmax, zmin, zmax = bounding_box_nonzero(single_lesion_stack)
        data = mask_stack[ymin:ymax + 1, xmin:xmax + 1, zmin:zmax + 1]
        lesion = Volume(data, resize_percentage)
        lesions.append(lesion)
    return lesions


def bounding_box_nonzero(image_stack: np.ndarray):
    r = np.any(image_stack, axis=(1, 2))
    c = np.any(image_stack, axis=(0, 2))
    z = np.any(image_stack, axis=(0, 1))
    ymin, ymax = np.where(r)[0][[0, -1]]
    xmin, xmax = np.where(c)[0][[0, -1]]
    zmin, zmax = np.where(z)[0][[0, -1]]
    return ymin, ymax, xmin, xmax, zmin, zmax


def load_images(path: str, image_regex: str = None, resize_percentage: float = None):
    # select images to load and sort them by sequence number
    fns = os.listdir(path)
    if image_regex:
        fns = filter_filelist_with_regex(image_regex, fns)
    sequence = extract_sequence_numbers_from_filenames(fns)
    sort_idx = np.argsort(sequence)
    fns = [fns[i] for i in sort_idx]

    # load images
    images = []
    for i, fn in enumerate(fns, start=1):
        impath = os.path.join(path, fn)
        im = imread(impath, flatten=True)

        logging.info("Loaded image {}/{} - shape {} - {}"
                     "".format(i, len(fns), 100 / resize_percentage, im.shape, fn))

        print_mem_usage()
        images.append(im)

    return images, fns


def plot_stack_3d(stack: np.ndarray):
    # compute edges
    # edges = binary_dilation(stack) - stack
    # y, x, z = np.nonzero(edges)

    z, y, x = np.nonzero(stack)
    logging.info(stack.shape)

    # subsample
    y = y[::6]
    x = x[::6]
    z = z[::6] * -109

    # plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(x, y, z)
    fig.tight_layout()
    plt.show()


def save_features(features: dict, output_filename, delim: str = ","):
    keys = features[0].keys()

    with open(output_filename, 'w') as f:
        # write column names
        for key in keys:
            f.write(key + delim)
        f.write("\n")

        for F in features:
            for key in keys:
                f.write("{}{}".format(F[key], delim))
            f.write("\n")


def visualize_lesions(lesions: List[Volume]):
    for i, lesion in enumerate(lesions, start=1):
        logging.info("Displaying lesion {}/{}".format(i, len(lesions)))
        logging.info("Lesion labels: {}".format(np.unique(lesion)))

        for im in lesion.stack:
            plt.figure()
            plt.imshow(im)
            plt.title("Lesion labels: {}".format(np.unique(lesion)))
            plt.show()
        plot_stack_3d(lesion.stack)


def main():
    start_timing()
    args = parse_arguments()
    check_arguments(args)
    logging.info("Command line arguments: {}".format(args))

    images, fns = load_images(args.source_dir, args.image_regex)

    # # resize image
    # if resize_percentage:
    #     sh = np.array(im.shape)
    #     new_shape = (sh * (resize_percentage / 100.0)).astype(int)
    #     im = imresize(im, new_shape, interp='nearest')

    stack = np.array(images, ndmin=3)

    del images
    logging.info("Stack shape {}".format(stack.shape))

    logging.info("Extract unique labels from the mask volume")
    volume_labels = np.unique(stack)
    logging.info("Volume labels (e.g. one per each lesion (1, 2, 3, ...)) in the mask images: {}".format(volume_labels))

    logging.info("Extracting volumes from images..")
    lesions = extract_volumes(stack, volume_labels, args.resize)
    del stack
    
    for l in lesions()
    

    logging.info("Extracting features..")
    # features = extract_features(lesions, extract_mouse_identifier(fns[0]))
    features = extract_features(lesions, "TEST")

    logging.info("Features: {}".format(features))

    logging.info("Saving features..")
    save_features(features, args.output_csv_path)

    end_timing()


if __name__ == '__main__':
    main()
