#!/bin/bash
#
#SBATCH -J H_feat
#
#SBATCH --output=logs/features_%j.log
#SBATCH --error=logs/features_%j.log
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=4
#
# time format hours:minutes:seconds or days-hours
#SBATCH --time=3:59:00
#SBATCH --mem=60000
#SBATCH --partition=test 

# Load modules
module load matlab/r2017b

# Get the paths, filename and extension.
FULLFILE="${1}" # full path to input image
FILE="${FULLFILE##*/}" # file name only of the input image
EXT="${FILE##*.}" # extension
INPUTPATH="${FULLFILE%/*}" # directory of the input image
TISSUEMASK="${2}" # path to tissuemask
OUTPUTPATH="${3}" # path to output directory
BLOCKSIZE="${4:-"100"}"
STRIDE="${5:-"100"}"
REDUCTIONLEVELCOMP="${6:-"0"}"
REDUCTIONLEVEL="${7:-"3"}"
FULLRESPIXELSIZE="${8:-"0.46"}"
NUCNHOODSIZE="${9:-"50"}"
MINTISSUEINBLOCK="${10:-"0.5"}"
NUCLEUSLIM1="${11:-"2"}"
NUCLEUSLIM2="${12:-"15"}"


echo "Extract features starting with parameters:"
echo "FULLFILE(\$1)="${1}""
echo "INPUTPATH="${FULLFILE%/*}""
echo "FILE="${FULLFILE##*/}""
echo "EXT="${FILE##*.}""
echo "INPUTFILE="${FILE}""
echo "TISSUEMASK(\$2)="${TISSUEMASK}""
echo "OUTPUTPATH(\$3)="${OUTPUTPATH}""
echo "BLOCKSIZE(\$4)="${BLOCKSIZE}""
echo "STRIDE(\$5)="${STRIDE}""
echo "REDUCTIONLEVELCOMP(\$6)="${REDUCTIONLEVELCOMP}""
echo "REDUCTIONLEVEL(\$7)="${REDUCTIONLEVEL}""
echo "FULLRESPIXELSIZE(\$8)="${FULLRESPIXELSIZE}""
echo "NUCNHOODSIZE(\$9)="${NUCNHOODSIZE}""
echo "MINTISSUEINBLOCK(\$10)="${MINTISSUEINBLOCK}""
echo "NUCLEUSLIM1(\$11)="${NUCLEUSLIM1}""
echo "NUCLEUSLIM2(\$12)="${NUCLEUSLIM2}""



# % Required parameters:
# % 'inputimagefile' - Full path to the input image.
# % 'tissuemaskfile' - Full path to mask image file indicating tissue region.
# % 'outputfile' - Full path to the output file where features are stored.
# %
# % Optional parameter-value combinations:
# % 'fullrespixelsize' - Full resolution pixel size in um (default 0.46).
# % 'reductionlevel' - Level of resolution pyramid for masks (default 3).
# % 'reductionlevelcomp' - Level of resolution pyramid for feature computation (default 0).
# % 'blocksize' - Size (NxN) of square block in um (default 100).
# % 'stride' - Spacing between adjacent blocks in um (default 100).
# % 'nucnhoodsize' - Size (NxN) of nucleus neighborhood in um (default 50).
# % 'mintissueinblock' - Minimum proportion of tissue in block (default 0.5).
# % 'nucleuslim1' - Nuclear axis min length in um (default 2).
# % 'nucleuslim2' - Nuclear axis max length in um (default 15).
# % 'maskbackground' - Set background pixels to white (default true).


# matlab -nodesktop -nodisplay -nosplash -singleCompThread -r "inputname='${INPUTFILE}'; numcores=1; blocksize=str2num('${3}'); stride=str2num('${4}'); reductionlevelcomp=str2num('${5}'); rackham_extractfeatures; exit;"

# matlab -nodesktop -nodisplay -nosplash -singleCompThread -r " \
#     addpath(genpath('/home/memasva/Projects/Registration/3D-histology/biit-wsi-featureextraction')); \
#     biitwsi_extractFeatures('${FULLFILE}',tissuemaskfile,'${OUTPUTPATH}/Features_${FILE/${EXT}/mat}',blocksize=str2num('${3}'),stride=str2num('${4}'),reductionlevelcomp=str2num('${5}')); \
#     exit"

# set matlab license server manually
export MLM_LICENSE_FILE=38002@130.230.106.131

matlab -nodesktop -nodisplay -nosplash -singleCompThread -r " \
    addpath(genpath('/home/memasva/Projects/Registration/3D-histology/biit-wsi-featureextraction')); \
    biitwsi_extractFeatures(\
        '${FULLFILE}', \
        '${TISSUEMASK}', \
        '${OUTPUTPATH}/Features_${FILE/${EXT}/mat}', \
        'blocksize',str2num('${BLOCKSIZE}'), \
        'stride',str2num('${STRIDE}'), \
        'reductionlevelcomp',str2num('${REDUCTIONLEVELCOMP}'), \
        'reductionlevel',str2num('${REDUCTIONLEVEL}'), \
        'fullrespixelsize',str2num('${FULLRESPIXELSIZE}'), \
        'nucnhoodsize',str2num('${NUCNHOODSIZE}'), \
        'mintissueinblock',str2num('${MINTISSUEINBLOCK}'), \
        'nucleuslim1',str2num('${NUCLEUSLIM1}'), \
        'nucleuslim2',str2num('${NUCLEUSLIM2}') \
        ); \
    exit"

echo "Feature computation done"

