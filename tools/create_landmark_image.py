import os
import logging
from argparse import ArgumentParser

from PIL import Image

Image.MAX_IMAGE_PIXELS = 5000000000

import skimage
import numpy as np
import pandas as pd
from skimage.morphology import disk

logging.basicConfig(level=logging.DEBUG)


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument(
        "path_coordinates", help="path to coordinate .csv file. One coordinate (y,x) pair per row", type=str
    )
    parser.add_argument("target_image_size_y", help="target image size y", type=int)
    parser.add_argument("target_image_size_x", help="target image size x", type=int)
    parser.add_argument("path_output", help="path to outout landmark image", type=str)
    parser.add_argument("--radius", help="size of the disk", default=10, type=int)
    args = vars(parser.parse_args())
    return args


def over_bounds(x, image_size):
    ret = x[0] >= image_size[0] or x[0] < 0 or x[1] >= image_size[1] or x[1] < 0
    if ret:
        logging.warn(f"point {x} out of bounds for image of size {image_size}")
    return ret


def create_landmark_image(points, image_size, radius=10):
    """ Create binary landmark image from list of points

    Args:
        points(list(list(float, float))): point coordinates [(y, x), (y, x), ...]
        image_size: size of the binary image (y, x)

    Returns:
        landmark image
    """

    lnds_im = np.zeros((image_size[0], image_size[1], 3))

    # save each point as a small disk to the landmark image
    # to avoid interpolation of the elastic registration method from
    # discarding pixels

    # create disk structuring element
    d = disk(radius, dtype=np.uint8)
    strel = np.array(np.where(d)).T
    # offset to have (0, 0) at the center
    strel = strel - [radius, radius]

    # create distinguishable colors for each point
    # divide 3d color space into 4x4x? grid to be used in the landmark_image to categorize each point
    num_z = np.ceil(len(points) / (4 * 4)) + 1
    x = np.linspace(0, 1, 4)
    y = np.linspace(0, 1, 4)
    z = np.linspace(0, 1, num_z)
    marker_colors = []
    counter = 0
    for r in y:
        for g in x:
            for b in z:
                # skip first iteration to avoid black marker color
                if counter == 0:
                    counter += 1
                    continue
                marker_colors.append([r, g, b])
                counter += 1

    # make marker_colors size match points
    marker_colors = (np.array(marker_colors[0 : len(points)]) * 255).astype(int)
    logging.debug("create_landmark_image: points: {}".format(points))
    logging.debug("create_landmark_image: marker_colors: {}".format(marker_colors))
    assert len(points) == len(marker_colors), "{} != {}".format(len(points), len(marker_colors))
    logging.debug("create_landmark_image: number of points and marker_colors match: {}".format(len(points)))

    for i, p in enumerate(points):
        # p order: (y, x)
        translated_strel = strel + [int(np.round(p[0])), int(np.round(p[1]))]
        filtered = np.array([p.tolist() for p in translated_strel if not over_bounds(p, image_size)])
        lnds_im[filtered[:, 0], filtered[:, 1], :] = marker_colors[i, :]

    return lnds_im.astype(np.uint8), marker_colors


def save_landmark_image(points_list, image_shape, save_path, radius):
    """

    Args:
        image_shape:
        points_list: [(y1, x1), (y2, x2), ...]
        save_path:
        work_dir:

    Returns:

    """
    logging.info(f"Saving landmark image of size {image_shape} for points\n{points}\n to path {save_path}")

    try:
        lnds_im_move, marker_colors = create_landmark_image([[p[0], p[1]] for p in points_list], image_shape, radius)
    except AssertionError as e:
        logging.error("Errors in creating landmark image")
        logging.error("number of generated marker_colors may not match the number of points")
        logging.error(e)

    skimage.io.imsave(save_path, lnds_im_move, plugin="imageio")

    # use save path filename part for saving marker colors as well
    fname = os.path.splitext(save_path)[0]

    # save also marker_colors to work_dir for debugging purposes
    np.save(f"{fname}_marker_colors.npy", marker_colors)
    with open(f"{fname}_marker_colors.txt", "w") as f:
        for m in marker_colors:
            f.write("{}\n".format(m))

    return marker_colors


def read_coordnates(path):
    df = pd.read_csv(path, header=None)
    return df.values


if __name__ == "__main__":
    logging.info("Creating landmark file..")
    args = parse_arguments()
    points = read_coordnates(args["path_coordinates"])
    logging.debug(f"read points: {points}")
    save_landmark_image(
        points, [args["target_image_size_y"], args["target_image_size_x"]], args["path_output"], args["radius"]
    )
    logging.info("Landmark file created succesfully")
