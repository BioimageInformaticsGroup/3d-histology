#!/usr/bin/env python
import os
import sys
import argparse
import itertools
from collections import OrderedDict

sys.path.insert(1, os.path.join(sys.path[0], '..'))
from utils.esa_params import EsaParams

# # FOR FULL RES
# # if some parameter is not present here, the default value will be used
# param_ranges = OrderedDict()
# param_ranges["layerscale"] = [1 / 32, 1 / 64, 1 / 128]  # 1/16 (~= 7.36 um), 1/109 (~= 50 um)
# param_ranges["siftfdsize"] = [4, 8]
# param_ranges["sectionthickness"] = [50.0]
# param_ranges["siftmaxoctavesize"] = [1024, 2048, 4096]
# param_ranges["siftminoctavesize"] = [32]
# param_ranges["maxnumneighbors"] = [1, 2]
# param_ranges["rod"] = [0.874, 0.95, 0.99]
# param_ranges["maxepsilon"] = [561.4478]
# param_ranges["minr"] = [0.5, 0.6, 0.723]
# param_ranges["maxiterationsspringmesh"] = [1508]  # use this for ESA, set it to 0 for basic SIFT + RANSAC

# # FOR 0.5X
#---PARAMS_RANGE_1---
# # if some parameter is not present here, the default value will be used
# param_ranges = OrderedDict()
# param_ranges["imagepixelsize"] = [0.92]  
# param_ranges["layerscale"] = [1 / 8, 1 / 16]  # 1/16 (~= 7.36 um), 1/109 (~= 50 um)
# param_ranges["siftfdsize"] = [4, 8]
# param_ranges["sectionthickness"] = [25]
# param_ranges["siftmaxoctavesize"] = [512, 1024]
# param_ranges["siftminoctavesize"] = [16]
# param_ranges["maxnumneighbors"] = [1, 2]
# param_ranges["rod"] = [0.874, 0.95, 0.99]
# param_ranges["maxepsilon"] = [280.7239]
# param_ranges["minr"] = [0.5, 0.6, 0.723]
# param_ranges["maxiterationsspringmesh"] = [1508]  # use this for ESA, set it to 0 for basic SIFT + RANSAC


## FOR 0.5X
##---PARAMS_RANGE_2---
## if some parameter is not present here, the default value will be used
#param_ranges = OrderedDict()
#param_ranges["imagepixelsize"] = [0.92]  
#param_ranges["layerscale"] = [1 / 16, 1 / 32]  # 1/16 (~= 7.36 um), 1/109 (~= 50 um)
#param_ranges["siftfdsize"] = [4, 8]
#param_ranges["sectionthickness"] = [25]
#param_ranges["siftmaxoctavesize"] = [1024, 2048]
#param_ranges["siftminoctavesize"] = [16]
#param_ranges["maxnumneighbors"] = [2, 3, 6, 10]
#param_ranges["rod"] = [0.874, 0.95, 0.99]
#param_ranges["maxepsilon"] = [280.7239]
#param_ranges["minr"] = [0.5, 0.6, 0.723]
#param_ranges["maxiterationsspringmesh"] = [1508]  # use this for ESA, set it to 0 for basic SIFT + RANSAC


# # REMOVE ME
# param_ranges = OrderedDict()
# param_ranges["imagepixelsize"] = [0.92]  
# param_ranges["layerscale"] = [1]  # 1/16 (~= 7.36 um), 1/109 (~= 50 um)
# param_ranges["siftfdsize"] = [4, 8]
# param_ranges["sectionthickness"] = [25]
# param_ranges["siftmaxoctavesize"] = [2048]
# param_ranges["siftminoctavesize"] = [16]
# param_ranges["maxnumneighbors"] = [2, 3, 4]
# param_ranges["rod"] = [0.874, 0.95, 0.99]
# param_ranges["maxepsilon"] = [280.7239]
# param_ranges["minr"] = [0.5, 0.6, 0.723]
# param_ranges["maxiterationsspringmesh"] = [1508]  # use this for ESA, set it to 0 for basic SIFT + RANSAC

# LOL
param_ranges = OrderedDict()
param_ranges["imagepixelsize"] = [23]  
param_ranges["layerscale"] = [1]  # 1/16 (~= 7.36 um), 1/109 (~= 50 um)
param_ranges["siftfdsize"] = [4, 8]
param_ranges["sectionthickness"] = [20]
param_ranges["siftmaxoctavesize"] = [2048]
param_ranges["siftminoctavesize"] = [16]
param_ranges["maxnumneighbors"] = [2, 3, 4]
param_ranges["rod"] = [0.874, 0.95, 0.99]
param_ranges["maxepsilon"] = [12]
param_ranges["minr"] = [0.2, 0.35, 0.5, 0.6, 0.723]
param_ranges["maxiterationsspringmesh"] = [1508]  # use this for ESA, set it to 0 for basic SIFT + RANSAC
param_ranges["maxcurvaturer"] = [ 7.00582628641793 ]
param_ranges["rodr"] = [ 0.863975346789895 ]
param_ranges["maxlocaltrust"] = [ 2.5798143212755 ]
param_ranges["searchradius"] = [ 6 ]
param_ranges["blockradius"] = [ 10 ]
param_ranges["localregionsigma"] = [ 19 ]
param_ranges["maxlocalepsilon"] = [ 6 ]


def parse_arguments():
    parser = argparse.ArgumentParser(description="Generate grid search file for histo gsearch command")
    parser.add_argument("source_dir", help="", type=str)
    parser.add_argument("target_dir", help="", type=str)
    args = vars(parser.parse_args())
    return args


def get_argument_permutations(ranges):
    # construct list of lists where each sublist contains all the possible combinations for one ESA parameter
    # this structure is created to be able to use itertools.product, which computes all possible combinations
    argument_lists = []
    for k in param_ranges.keys():
        argument_lists.append([(k, v) for v in param_ranges[k]])

    # create all possible combinations of each parameter
    combinations = list(itertools.product(*argument_lists))

    # insert all values from one combination to EsaParams object. The class outputs command line argument string
    params = EsaParams()
    lines = []
    for combination in combinations:
        for pair in combination:
            params.params[pair[0]] = pair[1]

        line = params.get_cli_arguments()
        lines.append(line)
    return lines


if __name__ == '__main__':

    args = parse_arguments()

    # get list of all combinations of ESA parameters without soure, target and transforms directories
    lines = get_argument_permutations(param_ranges)

    # this generates the following directory hierarchy
    #
    # PTENX036-011
    #   0
    #   1
    #   2
    #       full_res
    #       intermediate_results
    #       transforms
    #       params_file.txt

    # prepend each line with source, target and transforms dirs
    for i, line in enumerate(lines):

        # positonal
        source_dir = args["source_dir"]
        iteration_dir = os.path.join(args["target_dir"], str(i))
        output_dir = os.path.join(iteration_dir, "full_res")
        transforms_dir = os.path.join(iteration_dir, "transforms")
        

        # params file
        # [--params-file PARAMS_FILE]
        params_file_path = os.path.join(iteration_dir, "{}_params.txt".format(i))
        params_arg = "--params-file {}".format(params_file_path)

        # subsampling
        # [--subsample SUBSAMPLE] 
        # [--subsample-dir SUBSAMPLE_DIR]
        subsample_amount = 6
        subsample_arg = "--subsample {} --subsample-dir {}_subsampled{}".format(subsample_amount, output_dir.rstrip("/"), subsample_amount)

        # remove full res images
        # [--remove-full-res [bool]]
        remove_full_res = True
        remove_full_res_arg = "--remove-full-res {}".format(remove_full_res)

        print("align {} {} {} {} {} {} {}".format(source_dir, output_dir, transforms_dir, params_arg, subsample_arg, remove_full_res_arg, line))



