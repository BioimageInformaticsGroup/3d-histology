"""
Argv1: path to image
Argv2: path to template
"""

import logging
import sys
import time

import cv2
import numpy as np

sys.path.insert(1, "../biit_wsi_io")

logging.basicConfig(level=logging.DEBUG)


def match_template(image, template, method=cv2.TM_CCORR_NORMED, visualize=False):
    # note: this part is not tested well
    if method == cv2.TM_SQDIFF or method == cv2.TM_SQDIFF_NORMED:
        argfun = np.argmin
    else:
        argfun = np.argmax

    # Perform matching operations
    startt = time.time()
    res = cv2.matchTemplate(image, template, method)
    logging.debug("Matching took: {} seconds".format(time.time() - startt))
    res -= res.min()
    res /= res.max()
    res *= 255
    logging.debug("Result matrix: {}".format(res))

    # Store the coordinates of matched area in a numpy array
    # loc = np.where(res >= threshold)
    loc = np.unravel_index(argfun(res), res.shape)
    locs = np.where(res == res[loc[0], loc[1]])
    coords = np.array(loc).T
    coords_all = np.array(locs).T
    num_matches = len(locs[0])
    logging.debug("Found {} possible matches: {}".format(num_matches, coords_all))

    if visualize:
        h, w = template.shape
        # Draw a rectangle around the matched region.
        for c in coords_all:
            cv2.rectangle(image, (c[1], c[0]), (c[1] + w, c[0] + h), (0, 255, 255), 2)
        cv2.imwrite("match.jpg", image)
        logging.debug("Wrote match.jpg")
        cv2.imwrite("template.jpg", template)
        logging.debug("Wrote template.jpg")
        margin = 10
        for c in coords_all:
            cv2.rectangle(res, (c[1] - margin, c[0] - margin), (c[1] + margin, c[0] + margin), (0, 255, 255), 2)
        cv2.imwrite("resultmat.jpg", res)
        logging.debug("Wrote resultmat.jpg")
    return coords


# def search(image, template, method=cv2.TM_CCORR_NORMED):
#     # reshape
#     SUB_SCALE = 0.05
#     img_sub = cv2.resize(img, None, fx=SUB_SCALE, fy=SUB_SCALE)
#     template_sub = cv2.resize(template, None, fx=SUB_SCALE, fy=SUB_SCALE)
#
#     temp_sub = template_sub[0:10000, 0:10000]
#     w_sub, h_sub = temp_sub.shape[::-1]
#     logging.debug("Subsampled template width: {} height: {}".format(w_sub, h_sub))
#
#     logging.debug("Starting template matching for subsampled images..")
#     coords = match_template(img_sub, temp_sub, method)
#
#     # convert match location to full resolution
#     full_res_coords = coords / SUB_SCALE
#     w_sub_upscaled = w_sub / SUB_SCALE
#     h_sub_upscaled = h_sub / SUB_SCALE
#
#     # # crop search area from full res image, margin is half of pixels "lost in subsampling" (-0.5/SUB_SCALE)
#     # # take first match if multiple
#     # x_start = full_res_coords[1] - (0.5 / SUB_SCALE)
#     # x_end = full_res_coords[1] + w_sub_upscaled + (0.5 / SUB_SCALE)
#     # y_start = full_res_coords[0] - (0.5 / SUB_SCALE)
#     # y_end = full_res_coords[0] + h_sub_upscaled + (0.5 / SUB_SCALE)
#     # # ensure image borders are respected
#     # x_start = np.max((x_start, 0))
#     # y_start = np.max((y_start, 0))
#     # x_end = np.min((x_end, img.shape[1]))
#     # y_end = np.min((y_end, img.shape[0]))
#
#     # # get a smaller template from the full res image to speed up computations
#     # temp = template[0:2000, 0:2000]
#
#     # TODO: decimate to speed up computations
#
#     logging.debug("Starting template matching for full res images..")
#     coords2 = match_template(img, template, method)
#     logging.info("Found template at location (y, x): {}".format(coords2))


# def main2():
#     logging.debug("Image: {}".format(sys.argv[1]))
#     logging.debug("Template: {}".format(sys.argv[2]))
#
#     # Read the main image
#     img, _ = biitwsi_imread(sys.argv[1], reduction_level=0)
#     logging.debug("Image shape: {}".format(img.shape))
#     img = rgb2gray(img).astype(np.uint8)
#     logging.debug("Image shape gray: {}".format(img.shape))
#
#     # new_shape = tuple(np.array(img.shape) // 10)
#     # print(new_shape)
#     # img = cv2.resize(img, new_shape)
#     # logging.debug("Image shape subsampled: {}".format(img.shape))
#
#     # Read the template
#     template, _ = biitwsi_imread(sys.argv[2], reduction_level=0)
#     logging.debug("Template shape: {}".format(template.shape))
#     template = rgb2gray(template).astype(np.uint8)
#     logging.debug("Template shape gray: {}".format(template.shape))
#
#     # new_shape = tuple(np.array(template.shape) // 10)
#     # print(new_shape)
#     # template = cv2.resize(template, new_shape)
#     # logging.debug("Template shape subsampled: {}".format(img.shape))
#
#     coords = match_template(img, template, cv2.TM_SQDIFF, visualize=True)
#     logging.info("Found template at location (y, x): {}".format(coords))

def main():
    logging.debug("Image: {}".format(sys.argv[1]))
    logging.debug("Template: {}".format(sys.argv[2]))

    # Read the main image
    img = cv2.imread(sys.argv[1], 0)
    # Read the template
    template = cv2.imread(sys.argv[2], 0)

    logging.debug("Image size: {}".format(img.shape))
    logging.debug("Template size: {}".format(template.shape))

    # search(img, template, cv2.TM_SQDIFF)
    coords = match_template(img, template, cv2.TM_SQDIFF, visualize=True)
    logging.info("Found template at location (y, x): {}".format(coords))


if __name__ == '__main__':
    main()
