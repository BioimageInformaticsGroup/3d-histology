"""
Here mask is a mask image created with the old preprocessing system
Target image refers to
"""
import os
import logging
from argparse import ArgumentParser

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.misc import imread, imresize


# path to mask created for old piece image

logging.basicConfig(level=logging.INFO)


def parse_arguments():
    parser = ArgumentParser(description="")
    parser.add_argument("path_source_mask",
                        help="path to source mask",
                        type=str)
    parser.add_argument("source_mask_scale", help="source mask scale (0.5 for 50% subsampling of each dimension)",
                        type=float)
    parser.add_argument("source_y", help="source mask upper left y coordinate in WSI", type=int)
    parser.add_argument("source_x", help="source upper left x coordinate in WSI", type=int)
    parser.add_argument("source_yx_scale", help="source upper left coordinate scale (1 = 100%, 0.5 = 50%)", type=float)
    parser.add_argument("target_y", help="target image upper left y coordinate in WSI", type=int)
    parser.add_argument("target_x", help="target image upper left x coordinate in WSI", type=int)
    parser.add_argument("target_yx_scale", help="source upper left coordinate scale (1 = 100%, 0.5 = 50%)", type=float)
    parser.add_argument("target_y_size", help="target mask y size (height)", type=int)
    parser.add_argument("target_x_size", help="target mask x size (width)", type=int)
    parser.add_argument("output_mask_scale", help="output mask scale (0.5 for 50% downscaling for every dimension)",
                        type=float)
    parser.add_argument("output_path", help="path to target/output mask", type=str)
    args = vars(parser.parse_args())

    args["output_path"] = os.path.abspath(args["output_path"])

    logging.info("Args:")
    for k in args.keys():
        logging.info("    {}: {}".format(k, args[k]))

    return args


args = parse_arguments()

# read coordinates for mask pixels
# with flatten, output has only 1 color channel instead of 3 duplicate ones
mask = imread(args["path_source_mask"], flatten=True)

# check if mask needs inverting
invert_mask = True if (np.sum(mask[:]) / mask.size) > 0.5 else False

if invert_mask:
    logging.debug("Inverting mask..")
    mask = mask.astype(bool)
    mask = (~mask).astype(np.uint8)

logging.info("Mask before resize dtype {}, size {}".format(
    mask.dtype, mask.shape))

# scale mask to output scale
# and take this resizing into account when calculating new coordinates at 100% scale
mask_scale_coeff = args["output_mask_scale"] / args["source_mask_scale"]
mask = imresize(mask, (np.array(mask.shape) * mask_scale_coeff).astype(int),
                interp='nearest')
logging.info("Mask after resize dtype {}, size {}".format(
    mask.dtype, mask.shape))

coords = np.array(mask.nonzero()).T

# scale all but coords because scaling them alters spacing between individual mask pixels
coords = coords  # these are in output scale
source_y = args["source_y"] / args["source_yx_scale"]  # these are in WSI scale
source_x = args["source_x"] / args["source_yx_scale"]  # these are in WSI scale
target_y = args["target_y"] / args["target_yx_scale"]  # these are in WSI scale
target_x = args["target_x"] / args["target_yx_scale"]  # these are in WSI scale

logging.info("Source upper left full res coordinate (y,x): {}".format(
    (source_y, source_x)))
logging.info("    Scaled from (({}, {}) / {})".format(args["source_y"],
                                                      args["source_x"],
                                                      args["source_yx_scale"]))
logging.info("Target upper left coordinate (y,x): {}".format(
    (target_y, target_x)))
logging.info("    Scaled from (({}, {}) / {})".format(args["target_y"],
                                                      args["target_x"],
                                                      args["target_yx_scale"]))

# transform coordinates to new coordinate system in output scale
target_coords = coords + np.array([source_y, source_x])*args["output_mask_scale"] - \
    np.array([target_y, target_x])*args["output_mask_scale"]
target_coords = np.round(target_coords).astype(int)

# # For debugging
# import matplotlib
# matplotlib.use("qt5agg")
# import matplotlib.pyplot as plt
# plt.scatter(target_coords[:, 0], target_coords[:, 1])
# plt.plot((0, args["target_x_size"]), (0, 0))
# plt.plot((args["target_x_size"], args["target_x_size"]), (0, args["target_y_size"]))
# plt.plot((args["target_x_size"], 0), (args["target_y_size"], args["target_y_size"]))
# plt.plot((0, 0), (args["target_y_size"], 0))
# plt.gca().invert_yaxis()
# plt.axis("equal")
# plt.show()

# create new target mask image
target_mask = np.zeros((args["target_y_size"], args["target_x_size"]))
target_mask[target_coords[:, 0], target_coords[:, 1]] = 1

if invert_mask:
    target_mask = target_mask.astype(bool)
    target_mask = (~target_mask).astype(np.uint8)

outdir = os.path.dirname(args["output_path"])
if not os.path.exists(outdir):
    logging.info("Creating directory {}".format(outdir))
    os.makedirs(outdir)

plt.set_cmap("gray")
plt.imsave(args["output_path"], target_mask)
